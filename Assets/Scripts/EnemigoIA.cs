﻿using UnityEngine;

public class EnemigoIA : MonoBehaviour
{
    // Variable de velocidad del objeto, saldra en Unity y se podra editar alli .
    public float velocidad = 10f;
    public int vida = 100;
    public int valor = 5;
    public int daño = 1;

    public Transform objetivo;
    //public float rotationSpeed;

    private int indicador = 0;
    

    // Start se encargara de saber donde esta el comienzo de los puntos
    void Start ()
    {
        // objetivo sera lo que el enemigo tiene que buscar para moverse, en este caso los puntos
        objetivo = PuntosRuta.puntos[0];
    }

    public void RecibirDaño(int cantidad)
    {
        vida -= cantidad;
        if (vida <= 0)
        {
            Estadisticas.dinero += valor;
            Destroy(gameObject);
        }
    }

    // Update se encargara de mover al enemigo por los puntos 
    void Update()
    {
        // Le marcamos la direccion, hacia donde se movera y con que velocidad
        Vector3 direccion = objetivo.position - transform.position;
        transform.Translate(direccion.normalized * velocidad * Time.deltaTime, Space.World);

        // Comprobamos la distancia entre el enemigo y los puntos (sabiendo la posicion de ambos) para saber donde tiene que moverse
        if (Vector3.Distance(transform.position, objetivo.position) <= 0.4f)
        {
            SiguientePuntoRuta();
            //transform.Rotate(Vector3.forward, Time.deltaTime * 180, Space.Self);
            //transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(objetivo.position - transform.position).normalized, Time.deltaTime * rotationSpeed);
            transform.LookAt(objetivo);
        }
    }

    // Comprobara donde esta el siguiente punto al que hay que ir
    void SiguientePuntoRuta()
    {
        // Si el objeto ha llegado al final de los puntos marcados, sera eliminado del juego con la funcion Destroy
        if (indicador >= PuntosRuta.puntos.Length -1)
        {
            Destroy(gameObject);

            // Si el enemigo a llegado al final, despues de destruirse, restara la vida del jugador y si se acaba habra un "game over"
            Estadisticas.vida -= daño;
            if (Estadisticas.vida == -1)
            {
                Debug.Log("Game Over");
                Estadisticas.vivo = false;
                Time.timeScale = 0;
                Cursor.visible = true;
            }
            return;
        }

        // Leera donde estan los puntos marcados
        indicador++;
        objetivo = PuntosRuta.puntos[indicador];
    }
}
