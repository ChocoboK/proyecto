﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    //public string myMessage; 

    // Estas variables se pueden ver en Unity y ahi podras poner el numero que quieras para hacer que el metodo use el numero
    // Variable para cambiar el tamaño de un objeto
    public Vector3 scaleChange;
    // Variable para cambiar la posicion de un objeto
    public Vector3 positionChange;
    // Variable para girar un objeto
    public Vector3 rotateChange;

    // Start is called before the first frame update
    void Start()
    {
        //print("Test start function");
    }

    // Update is called once per frame
    void Update()
    {
        //print(myMessage);

        //Transform es la variable de Unity para poder transformar el objeto, localScale coge la escala actual del objeto, position su posicion actual, y Rotate sirve para hacer girar al objeto segun el numero
        // Funcion para cambiar el tamaño
        transform.localScale += scaleChange;
        // Funcion para cambiar la posicion
        transform.position += positionChange;
        // Funcion para girar 
        transform.Rotate (rotateChange);
    }
}
