﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InvocadorOleadas : MonoBehaviour
{
    public Transform EnemigoPrefabricado;
    public Transform puntoAparicion;

    // Variables de tiempo, entre oleadas de enemigos .
    public float tiempoEntreOleadas = 5f; 
    private float contador = 2f;

    // Variables campo de texto 
    public Text oleadaNumeroTexto;
    public Text aviso;

    // Variable para saber el numero de oleada
    private int numeroOleada = 1;

    void Start()
    {
        aviso.gameObject.SetActive(false);
    }

    void Update()
    {
        if (contador <= 0f)
        { 
            /* StarCoroutine permite empezar iteraciones en paralelo
             * En este caso sirve para llamar a InvocarOleada, antes era void, 
             * pero sin esto no puedes llamar a un IEnumerator, que es lo que devolvera StartCoroutine */
            StartCoroutine(InvocarOleada());
            contador = tiempoEntreOleadas;

            aviso.gameObject.SetActive(true);
        }

        // Time bajara en 1 segundo la variable contador
        contador -= Time.deltaTime;
        oleadaNumeroTexto.text = Mathf.Round(contador).ToString();
    }

    // IEnumerator sirve para pausar una iteracion donde hay varios objectos (coleccion)
    // En este caso lo utilizo para contar antes y evitar sacar un enemigo encima del otro
    IEnumerator InvocarOleada()
    {
        for (int i = 0; i < numeroOleada; i++)
        {
            InvocarEnemigo();
            // Esto funcionara como temporizador para spawnear un enemigo y luego otro, sin esto los enemigos estarian uno encima de otro.
            yield return new WaitForSeconds(0.5f);
        }
        numeroOleada++;

        aviso.gameObject.SetActive(false);
        //Debug.Log("Wave Incoming");
    }

    // Funcion para instanciar enemigo a invocar
    void InvocarEnemigo()
    {
        Instantiate(EnemigoPrefabricado, puntoAparicion.position, puntoAparicion.rotation);
    }
}
