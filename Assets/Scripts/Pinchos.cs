﻿using System.Collections;
using UnityEngine;


public class Pinchos : MonoBehaviour
{
    // Variables de la trampa
    public float alcance = 0.5f; 
    public int daño = 20;
    public Transform objetivo;

    // Asi sabremos quien es un enemigo, si tiene el tag indicado
    public string TagEnemigos = "Enemigo";

    private float temporizadorParaDañar;


    void Start()
    {
        // Aqui se empezaria a buscar un nuevo enemigo repetitivamente, cada x tiempo
        InvokeRepeating("EncuentraObjetivo", 0f, 0.5f);
    }


    // Metodo para buscar un enemigo
    void EncuentraObjetivo()
    {
        // Guardamos los enemigos segun el tag que tienen
        GameObject[] enemigos = GameObject.FindGameObjectsWithTag(TagEnemigos);

        // La distancia que buscara la trampa, aunque ira realmente segun el rango
        float distanciaCorta = Mathf.Infinity;
        GameObject enemigoCercano = null;

        // Buscamos los enemigos encontrados segun la distancia 
        foreach(GameObject enemigo in enemigos)
        {
            // Buscamos segun la distancia entre la trampa y el enemigo
            float distancia = Vector3.Distance(transform.position, enemigo.transform.position);
            if (distancia < distanciaCorta)
            {
                distanciaCorta = distancia;
                enemigoCercano = enemigo;
            }
            else
            {
                objetivo = null;
            }
        }

        // Si ha encontrado al enemigo a corta distancia (dentro del alcance), lo marcara como el nuevo objetivo
        if (enemigoCercano != null && distanciaCorta <= alcance)
        {
            objetivo = enemigoCercano.transform;
        }
    }

    void Update()
    {
        // Si no tiene un enemigo que no haga nada, si no, que le golpee
        if (objetivo == null)
        {
            return;
        }

        // Temporizador para que haga daño poco a poco al enemigo, sin esto la trampa dañaria todo el rato, hasta destruir al enemigo
        if (temporizadorParaDañar <= 0f)
        {
            GolpeaObjetivo();
            temporizadorParaDañar = 0.5f;
        }
        temporizadorParaDañar -= Time.deltaTime;

    }

    void GolpeaObjetivo()
    {
        //Debug.Log("HIT");
        Dañar(objetivo);
    }

    // Metodo para dañar al enemigo
    void Dañar(Transform enemigo)
    {
        // Se recoge al enemigo que se ha encontrado antes
        EnemigoIA dañoEnemigo = enemigo.GetComponent<EnemigoIA>();
        // Si se confirma que es un enemigo, se le hara daño
        if (dañoEnemigo != null)
        {
            // Cogemos su metodo publico y le pasamos el daño de la trampa para que reciba ese daño
            dañoEnemigo.RecibirDaño(daño);

        }
    }

    // Esto sera para ver el rango de la trampa (hasta donde llega visualmente)
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, alcance);
    }

}
