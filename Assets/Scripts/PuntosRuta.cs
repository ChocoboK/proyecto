﻿using UnityEngine;

public class PuntosRuta : MonoBehaviour
{
    // Variable para los puntos
    public static Transform[] puntos; 

    // Creara los puntos
    void Awake()
    {
        // Creamos los puntos
        puntos = new Transform[transform.childCount];

        // Metemos los puntos en la variable puntos
        for (int i = 0; i < puntos.Length; i++)
        {
           puntos[i] = transform.GetChild(i);
        }
    }

}
