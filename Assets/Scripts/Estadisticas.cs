﻿using UnityEngine;
using UnityEngine.UI;


public class Estadisticas : MonoBehaviour
{
    // Variables para el dinero 
    public static int dinero;
    public int dineroInicial = 200;
    public int dineroActual;

    // Variables para la vida
    public static int vida;
    public int vidaInicial = 5;
    public int vidaActual;

    public static bool vivo;

    // Variables para el campo de texto en la interfaz
    public Text dineroTexto;
    public Text vidaTexto;
    public Text finTexto;

    public Button Reiniciar;
    public void salir()
    {
    #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
    #else
        Application.Quit();
    #endif

    }
    

    void Start()
    {
        // Al principio marcamos que el jugador esta "vivo"
        vivo = true;
        finTexto.gameObject.SetActive(false);
        //Reiniciar.gameObject.SetActive(false);

        // Cuando empieza el juego se introduce la cantidad inicial
        dinero = dineroInicial;
        vida = vidaInicial;
    }

    void Update()
    {
        // Mientras el juego siga las cantidades se actualizan
        dineroActual = dinero;
        vidaActual = vida;

        // Se pasa a cadena el dinero actual para ponerlo en el campo de texto correspondiente
        dineroTexto.text = Mathf.Round(dineroActual).ToString();
        vidaTexto.text = Mathf.Round(vidaActual).ToString();

        // Si ya no esta vivo, se pondra el texto de "game over"
        if (vivo == false)
        {
            finTexto.gameObject.SetActive(true);
            Reiniciar.gameObject.SetActive(true);
        }

    }
}
