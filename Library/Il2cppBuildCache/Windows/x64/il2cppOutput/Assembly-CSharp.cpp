﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2>
struct VirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};

// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t34AA4AF4E7352129CA58045901530E41445AC16D;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t6778DB95346906446AAD3A1A36904F1846435A09;
// UnityEngine.Renderer[]
struct RendererU5BU5D_tE2D3C4350893C593CA40DE876B9F2F0EBBEC49B7;
// UnityEngine.UI.Selectable[]
struct SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// UnityEngine.Transform[]
struct TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11;
// UnityEngine.Animator
struct Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149;
// UnityEngine.AudioClip
struct AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE;
// UnityEngine.AudioSource
struct AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B;
// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9;
// UnityEngine.UI.Button
struct Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D;
// UnityEngine.Camera
struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C;
// UnityEngine.Canvas
struct Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E;
// UnityEngine.CapsuleCollider
struct CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635;
// UnityEngine.Collider
struct Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7;
// EnemigoIA
struct EnemigoIA_t7F4D590CF8C4897FCB1C723187317E99CD7B4B81;
// Estadisticas
struct Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D;
// UnityEngine.UI.FontData
struct FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// UnityEngine.UI.Graphic
struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// System.Collections.IEnumerator
struct IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105;
// InvocadorOleadas
struct InvocadorOleadas_t7ADC79FAB86E1B7AE663CA487985C11C00B4C530;
// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE;
// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// UnityEngine.PhysicMaterial
struct PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2;
// Pinchos
struct Pinchos_tBABE699D1BAED348FC2D8DAD13582F801A0644DC;
// PuntosRuta
struct PuntosRuta_t1BFAE08531BC3AEEE4B521035B0D948B4BA0F8AE;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15;
// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072;
// Reiniciar
struct Reiniciar_tCA54EF25F87683B96AF8E690C069B2B3BC1B8510;
// UnityEngine.Renderer
struct Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C;
// UnityEngine.Rigidbody
struct Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// Salir
struct Salir_t1BBF788DCACDEDDAA6F34E80CD336C6A7BEC63C0;
// UnityEngine.UI.Selectable
struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD;
// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9;
// System.String
struct String_t;
// Test
struct Test_tB521413BD14DFDD715F0C78DB4B6392F71A6EED5;
// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1;
// UnityEngine.TextGenerator
struct TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70;
// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// UnityEngine.Events.UnityAction
struct UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099;
// UnityEngine.UI.VertexHelper
struct VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013;
// Invector.Utils.vComment
struct vComment_tEBF402879B60173DE75AD1509B72C5EB560FDA80;
// vPickupItem
struct vPickupItem_t9764F53667CE8FA36C0E25286B6039029BD6B4E0;
// Invector.vCharacterController.vThirdPersonAnimator
struct vThirdPersonAnimator_tDFBC4BC178521ACC390B82F0B5418961D2754CB0;
// vThirdPersonCamera
struct vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11;
// Invector.vCharacterController.vThirdPersonController
struct vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A;
// Invector.vCharacterController.vThirdPersonInput
struct vThirdPersonInput_tB15CFA93BCDA688B2E2D8F976E90C4262CB82C50;
// Invector.vCharacterController.vThirdPersonMotor
struct vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3;
// UnityEngine.AudioClip/PCMReaderCallback
struct PCMReaderCallback_t9CA1437D36509A9FAC5EDD8FF2BC3259C24D0E0B;
// UnityEngine.AudioClip/PCMSetPositionCallback
struct PCMSetPositionCallback_tBDD99E7C0697687F1E7B06CDD5DE444A3709CF4C;
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D;
// InvocadorOleadas/<InvocarOleada>d__9
struct U3CInvocarOleadaU3Ed__9_t095DFE910F3D2E69D3A9F620E99A7FD143751007;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4;
// Invector.vCharacterController.vThirdPersonMotor/vMovementSpeed
struct vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25;

IL2CPP_EXTERN_C RuntimeClass* Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* LocomotionType_t273049CC7616CFB97FFD00D80B9900878D3B4C6A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PuntosRuta_t1BFAE08531BC3AEEE4B521035B0D948B4BA0F8AE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CInvocarOleadaU3Ed__9_t095DFE910F3D2E69D3A9F620E99A7FD143751007_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral0C3C4C204DF0A620548A74D496032120462EE463;
IL2CPP_EXTERN_C String_t* _stringLiteral16DD21BE77B115D392226EB71A2D3A9FDC29E3F0;
IL2CPP_EXTERN_C String_t* _stringLiteral1E303E60E7B8FCF9AC7391E2758609BE04B7C842;
IL2CPP_EXTERN_C String_t* _stringLiteral20265357F96C863C747BDDCB8CB50C9DB7DC428B;
IL2CPP_EXTERN_C String_t* _stringLiteral2226C21FDA0CD02B2E2682354C83A6ED9AC5D4EA;
IL2CPP_EXTERN_C String_t* _stringLiteral265E15F1F86F1C766555899D5771CF29055DE75A;
IL2CPP_EXTERN_C String_t* _stringLiteral279C1285646246614D8E6D24E992B80E25ECD01A;
IL2CPP_EXTERN_C String_t* _stringLiteral32BEEE1B185DCAAA32545866BF4C372603A16426;
IL2CPP_EXTERN_C String_t* _stringLiteral4DF60B3121C256419E90E065DA63E5CC56468056;
IL2CPP_EXTERN_C String_t* _stringLiteral578746FC8C216F37F9C51638EE5054B739C4E1F6;
IL2CPP_EXTERN_C String_t* _stringLiteral5E16939F0A0663C8C38E3379DBD3B64A87C5032F;
IL2CPP_EXTERN_C String_t* _stringLiteral70253F929BCE7F81DF1A5A1C0900BED744E86C9C;
IL2CPP_EXTERN_C String_t* _stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E;
IL2CPP_EXTERN_C String_t* _stringLiteral835843700B5BAEFFB185610011BAA3028FB07587;
IL2CPP_EXTERN_C String_t* _stringLiteral88BEE283254D7094E258B3A88730F4CC4F1E4AC7;
IL2CPP_EXTERN_C String_t* _stringLiteral960AF68E8119488D559BE4332DB7AED9EFB6B053;
IL2CPP_EXTERN_C String_t* _stringLiteral9A2C600D86C06D98E5159D56D3B075E446BB8500;
IL2CPP_EXTERN_C String_t* _stringLiteralCAF8804297181FF007CA835529DD4477CFD94A70;
IL2CPP_EXTERN_C String_t* _stringLiteralD07564F70E250E1B2184D654ACE4DECC7C3AE068;
IL2CPP_EXTERN_C String_t* _stringLiteralEA96601D79B410E78ED885E29D7A25794A833FD2;
IL2CPP_EXTERN_C String_t* _stringLiteralF984C9674EDCA2C39EFAD66081B054F073C3815B;
IL2CPP_EXTERN_C String_t* _stringLiteralFFEAEBFEFE135DF7CF4812C255322760E0C94F82;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisAudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_mCC3B7A679ECE504BFAF8C70C4EF527511F46902F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisCamera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_mE17146EF5B0D8E9F9D2D2D94567BF211AD00D320_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisCapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635_m2B9A09217489CB456933DEA112CA70C10AD3B9F9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisEnemigoIA_t7F4D590CF8C4897FCB1C723187317E99CD7B4B81_m66AD0F9A8B1176782FDA81579318B9341B8A7621_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_m9DC24AA806B0B65E917751F7A3AFDB58861157CE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisvThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A_m89E57D0F784DA689BADD06ECF4BFEA8A8822A350_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponentsInChildren_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_m107B791DBC1E809192456359DFF8B8F45A84EAA1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_FindObjectOfType_TisvThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11_m291B5F056AEC7E08B699A8363A313162CAB2B4BD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_Instantiate_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_mC13E8DA0CA1DFD88310E2E066C5479E3994BAA30_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CInvocarOleadaU3Ed__9_System_Collections_IEnumerator_Reset_m36DD77A221E7624753B5F896258F11EC87949022_RuntimeMethod_var;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642;
struct RaycastHitU5BU5D_t6778DB95346906446AAD3A1A36904F1846435A09;
struct RendererU5BU5D_tE2D3C4350893C593CA40DE876B9F2F0EBBEC49B7;
struct TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tFDCAFCBB4B3431CFF2DC4D3E03FBFDF54EFF7E9A 
{
public:

public:
};


// System.Object

struct Il2CppArrayBounds;

// System.Array


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
};

// Invector.vCharacterController.vAnimatorParameters
struct vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF  : public RuntimeObject
{
public:

public:
};

struct vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_StaticFields
{
public:
	// System.Int32 Invector.vCharacterController.vAnimatorParameters::InputHorizontal
	int32_t ___InputHorizontal_0;
	// System.Int32 Invector.vCharacterController.vAnimatorParameters::InputVertical
	int32_t ___InputVertical_1;
	// System.Int32 Invector.vCharacterController.vAnimatorParameters::InputMagnitude
	int32_t ___InputMagnitude_2;
	// System.Int32 Invector.vCharacterController.vAnimatorParameters::IsGrounded
	int32_t ___IsGrounded_3;
	// System.Int32 Invector.vCharacterController.vAnimatorParameters::IsStrafing
	int32_t ___IsStrafing_4;
	// System.Int32 Invector.vCharacterController.vAnimatorParameters::IsSprinting
	int32_t ___IsSprinting_5;
	// System.Int32 Invector.vCharacterController.vAnimatorParameters::GroundDistance
	int32_t ___GroundDistance_6;

public:
	inline static int32_t get_offset_of_InputHorizontal_0() { return static_cast<int32_t>(offsetof(vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_StaticFields, ___InputHorizontal_0)); }
	inline int32_t get_InputHorizontal_0() const { return ___InputHorizontal_0; }
	inline int32_t* get_address_of_InputHorizontal_0() { return &___InputHorizontal_0; }
	inline void set_InputHorizontal_0(int32_t value)
	{
		___InputHorizontal_0 = value;
	}

	inline static int32_t get_offset_of_InputVertical_1() { return static_cast<int32_t>(offsetof(vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_StaticFields, ___InputVertical_1)); }
	inline int32_t get_InputVertical_1() const { return ___InputVertical_1; }
	inline int32_t* get_address_of_InputVertical_1() { return &___InputVertical_1; }
	inline void set_InputVertical_1(int32_t value)
	{
		___InputVertical_1 = value;
	}

	inline static int32_t get_offset_of_InputMagnitude_2() { return static_cast<int32_t>(offsetof(vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_StaticFields, ___InputMagnitude_2)); }
	inline int32_t get_InputMagnitude_2() const { return ___InputMagnitude_2; }
	inline int32_t* get_address_of_InputMagnitude_2() { return &___InputMagnitude_2; }
	inline void set_InputMagnitude_2(int32_t value)
	{
		___InputMagnitude_2 = value;
	}

	inline static int32_t get_offset_of_IsGrounded_3() { return static_cast<int32_t>(offsetof(vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_StaticFields, ___IsGrounded_3)); }
	inline int32_t get_IsGrounded_3() const { return ___IsGrounded_3; }
	inline int32_t* get_address_of_IsGrounded_3() { return &___IsGrounded_3; }
	inline void set_IsGrounded_3(int32_t value)
	{
		___IsGrounded_3 = value;
	}

	inline static int32_t get_offset_of_IsStrafing_4() { return static_cast<int32_t>(offsetof(vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_StaticFields, ___IsStrafing_4)); }
	inline int32_t get_IsStrafing_4() const { return ___IsStrafing_4; }
	inline int32_t* get_address_of_IsStrafing_4() { return &___IsStrafing_4; }
	inline void set_IsStrafing_4(int32_t value)
	{
		___IsStrafing_4 = value;
	}

	inline static int32_t get_offset_of_IsSprinting_5() { return static_cast<int32_t>(offsetof(vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_StaticFields, ___IsSprinting_5)); }
	inline int32_t get_IsSprinting_5() const { return ___IsSprinting_5; }
	inline int32_t* get_address_of_IsSprinting_5() { return &___IsSprinting_5; }
	inline void set_IsSprinting_5(int32_t value)
	{
		___IsSprinting_5 = value;
	}

	inline static int32_t get_offset_of_GroundDistance_6() { return static_cast<int32_t>(offsetof(vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_StaticFields, ___GroundDistance_6)); }
	inline int32_t get_GroundDistance_6() const { return ___GroundDistance_6; }
	inline int32_t* get_address_of_GroundDistance_6() { return &___GroundDistance_6; }
	inline void set_GroundDistance_6(int32_t value)
	{
		___GroundDistance_6 = value;
	}
};


// Invector.vExtensions
struct vExtensions_t9F19C71B510847F136E8E6B9473A4BCB928AE30F  : public RuntimeObject
{
public:

public:
};


// InvocadorOleadas/<InvocarOleada>d__9
struct U3CInvocarOleadaU3Ed__9_t095DFE910F3D2E69D3A9F620E99A7FD143751007  : public RuntimeObject
{
public:
	// System.Int32 InvocadorOleadas/<InvocarOleada>d__9::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object InvocadorOleadas/<InvocarOleada>d__9::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// InvocadorOleadas InvocadorOleadas/<InvocarOleada>d__9::<>4__this
	InvocadorOleadas_t7ADC79FAB86E1B7AE663CA487985C11C00B4C530 * ___U3CU3E4__this_2;
	// System.Int32 InvocadorOleadas/<InvocarOleada>d__9::<i>5__2
	int32_t ___U3CiU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CInvocarOleadaU3Ed__9_t095DFE910F3D2E69D3A9F620E99A7FD143751007, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CInvocarOleadaU3Ed__9_t095DFE910F3D2E69D3A9F620E99A7FD143751007, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CInvocarOleadaU3Ed__9_t095DFE910F3D2E69D3A9F620E99A7FD143751007, ___U3CU3E4__this_2)); }
	inline InvocadorOleadas_t7ADC79FAB86E1B7AE663CA487985C11C00B4C530 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline InvocadorOleadas_t7ADC79FAB86E1B7AE663CA487985C11C00B4C530 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(InvocadorOleadas_t7ADC79FAB86E1B7AE663CA487985C11C00B4C530 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CInvocarOleadaU3Ed__9_t095DFE910F3D2E69D3A9F620E99A7FD143751007, ___U3CiU3E5__2_3)); }
	inline int32_t get_U3CiU3E5__2_3() const { return ___U3CiU3E5__2_3; }
	inline int32_t* get_address_of_U3CiU3E5__2_3() { return &___U3CiU3E5__2_3; }
	inline void set_U3CiU3E5__2_3(int32_t value)
	{
		___U3CiU3E5__2_3 = value;
	}
};


// Invector.vCharacterController.vThirdPersonMotor/vMovementSpeed
struct vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25  : public RuntimeObject
{
public:
	// System.Single Invector.vCharacterController.vThirdPersonMotor/vMovementSpeed::movementSmooth
	float ___movementSmooth_0;
	// System.Single Invector.vCharacterController.vThirdPersonMotor/vMovementSpeed::animationSmooth
	float ___animationSmooth_1;
	// System.Single Invector.vCharacterController.vThirdPersonMotor/vMovementSpeed::rotationSpeed
	float ___rotationSpeed_2;
	// System.Boolean Invector.vCharacterController.vThirdPersonMotor/vMovementSpeed::walkByDefault
	bool ___walkByDefault_3;
	// System.Boolean Invector.vCharacterController.vThirdPersonMotor/vMovementSpeed::rotateWithCamera
	bool ___rotateWithCamera_4;
	// System.Single Invector.vCharacterController.vThirdPersonMotor/vMovementSpeed::walkSpeed
	float ___walkSpeed_5;
	// System.Single Invector.vCharacterController.vThirdPersonMotor/vMovementSpeed::runningSpeed
	float ___runningSpeed_6;
	// System.Single Invector.vCharacterController.vThirdPersonMotor/vMovementSpeed::sprintSpeed
	float ___sprintSpeed_7;

public:
	inline static int32_t get_offset_of_movementSmooth_0() { return static_cast<int32_t>(offsetof(vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25, ___movementSmooth_0)); }
	inline float get_movementSmooth_0() const { return ___movementSmooth_0; }
	inline float* get_address_of_movementSmooth_0() { return &___movementSmooth_0; }
	inline void set_movementSmooth_0(float value)
	{
		___movementSmooth_0 = value;
	}

	inline static int32_t get_offset_of_animationSmooth_1() { return static_cast<int32_t>(offsetof(vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25, ___animationSmooth_1)); }
	inline float get_animationSmooth_1() const { return ___animationSmooth_1; }
	inline float* get_address_of_animationSmooth_1() { return &___animationSmooth_1; }
	inline void set_animationSmooth_1(float value)
	{
		___animationSmooth_1 = value;
	}

	inline static int32_t get_offset_of_rotationSpeed_2() { return static_cast<int32_t>(offsetof(vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25, ___rotationSpeed_2)); }
	inline float get_rotationSpeed_2() const { return ___rotationSpeed_2; }
	inline float* get_address_of_rotationSpeed_2() { return &___rotationSpeed_2; }
	inline void set_rotationSpeed_2(float value)
	{
		___rotationSpeed_2 = value;
	}

	inline static int32_t get_offset_of_walkByDefault_3() { return static_cast<int32_t>(offsetof(vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25, ___walkByDefault_3)); }
	inline bool get_walkByDefault_3() const { return ___walkByDefault_3; }
	inline bool* get_address_of_walkByDefault_3() { return &___walkByDefault_3; }
	inline void set_walkByDefault_3(bool value)
	{
		___walkByDefault_3 = value;
	}

	inline static int32_t get_offset_of_rotateWithCamera_4() { return static_cast<int32_t>(offsetof(vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25, ___rotateWithCamera_4)); }
	inline bool get_rotateWithCamera_4() const { return ___rotateWithCamera_4; }
	inline bool* get_address_of_rotateWithCamera_4() { return &___rotateWithCamera_4; }
	inline void set_rotateWithCamera_4(bool value)
	{
		___rotateWithCamera_4 = value;
	}

	inline static int32_t get_offset_of_walkSpeed_5() { return static_cast<int32_t>(offsetof(vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25, ___walkSpeed_5)); }
	inline float get_walkSpeed_5() const { return ___walkSpeed_5; }
	inline float* get_address_of_walkSpeed_5() { return &___walkSpeed_5; }
	inline void set_walkSpeed_5(float value)
	{
		___walkSpeed_5 = value;
	}

	inline static int32_t get_offset_of_runningSpeed_6() { return static_cast<int32_t>(offsetof(vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25, ___runningSpeed_6)); }
	inline float get_runningSpeed_6() const { return ___runningSpeed_6; }
	inline float* get_address_of_runningSpeed_6() { return &___runningSpeed_6; }
	inline void set_runningSpeed_6(float value)
	{
		___runningSpeed_6 = value;
	}

	inline static int32_t get_offset_of_sprintSpeed_7() { return static_cast<int32_t>(offsetof(vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25, ___sprintSpeed_7)); }
	inline float get_sprintSpeed_7() const { return ___sprintSpeed_7; }
	inline float* get_address_of_sprintSpeed_7() { return &___sprintSpeed_7; }
	inline void set_sprintSpeed_7(float value)
	{
		___sprintSpeed_7 = value;
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// UnityEngine.Color
struct Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// System.Double
struct Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181, ___m_value_0)); }
	inline double get_m_value_0() const { return ___m_value_0; }
	inline double* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(double value)
	{
		___m_value_0 = value;
	}
};

struct Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181_StaticFields
{
public:
	// System.Double System.Double::NegativeZero
	double ___NegativeZero_7;

public:
	inline static int32_t get_offset_of_NegativeZero_7() { return static_cast<int32_t>(offsetof(Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181_StaticFields, ___NegativeZero_7)); }
	inline double get_NegativeZero_7() const { return ___NegativeZero_7; }
	inline double* get_address_of_NegativeZero_7() { return &___NegativeZero_7; }
	inline void set_NegativeZero_7(double value)
	{
		___NegativeZero_7 = value;
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.LayerMask
struct LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};


// UnityEngine.Quaternion
struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___identityQuaternion_4 = value;
	}
};


// UnityEngine.SceneManagement.Scene
struct Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE 
{
public:
	// System.Int32 UnityEngine.SceneManagement.Scene::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};


// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_SelectedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_HighlightedSprite_0)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HighlightedSprite_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_PressedSprite_1)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PressedSprite_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectedSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_SelectedSprite_2)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_SelectedSprite_2() const { return ___m_SelectedSprite_2; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_SelectedSprite_2() { return &___m_SelectedSprite_2; }
	inline void set_m_SelectedSprite_2(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_SelectedSprite_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectedSprite_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_3() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_DisabledSprite_3)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_DisabledSprite_3() const { return ___m_DisabledSprite_3; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_DisabledSprite_3() { return &___m_DisabledSprite_3; }
	inline void set_m_DisabledSprite_3(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_DisabledSprite_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DisabledSprite_3), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E_marshaled_pinvoke
{
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E_marshaled_com
{
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;
};

// UnityEngine.Vector2
struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Vector4
struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___zeroVector_5)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___oneVector_6)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___negativeInfinityVector_8 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;

public:
	inline static int32_t get_offset_of_m_Seconds_0() { return static_cast<int32_t>(offsetof(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013, ___m_Seconds_0)); }
	inline float get_m_Seconds_0() const { return ___m_Seconds_0; }
	inline float* get_address_of_m_Seconds_0() { return &___m_Seconds_0; }
	inline void set_m_Seconds_0(float value)
	{
		___m_Seconds_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_marshaled_pinvoke : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
	float ___m_Seconds_0;
};
// Native definition for COM marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_marshaled_com : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
	float ___m_Seconds_0;
};

// UnityEngine.AnimatorUpdateMode
struct AnimatorUpdateMode_t5E08EEA8E26DAB1765A82EAD782A0E047920B439 
{
public:
	// System.Int32 UnityEngine.AnimatorUpdateMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AnimatorUpdateMode_t5E08EEA8E26DAB1765A82EAD782A0E047920B439, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Invector.ClipPlanePoints
struct ClipPlanePoints_tFA6B5EF4C87C83626BEAEA585214A9C4679433DA 
{
public:
	// UnityEngine.Vector3 Invector.ClipPlanePoints::UpperLeft
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___UpperLeft_0;
	// UnityEngine.Vector3 Invector.ClipPlanePoints::UpperRight
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___UpperRight_1;
	// UnityEngine.Vector3 Invector.ClipPlanePoints::LowerLeft
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___LowerLeft_2;
	// UnityEngine.Vector3 Invector.ClipPlanePoints::LowerRight
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___LowerRight_3;

public:
	inline static int32_t get_offset_of_UpperLeft_0() { return static_cast<int32_t>(offsetof(ClipPlanePoints_tFA6B5EF4C87C83626BEAEA585214A9C4679433DA, ___UpperLeft_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_UpperLeft_0() const { return ___UpperLeft_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_UpperLeft_0() { return &___UpperLeft_0; }
	inline void set_UpperLeft_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___UpperLeft_0 = value;
	}

	inline static int32_t get_offset_of_UpperRight_1() { return static_cast<int32_t>(offsetof(ClipPlanePoints_tFA6B5EF4C87C83626BEAEA585214A9C4679433DA, ___UpperRight_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_UpperRight_1() const { return ___UpperRight_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_UpperRight_1() { return &___UpperRight_1; }
	inline void set_UpperRight_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___UpperRight_1 = value;
	}

	inline static int32_t get_offset_of_LowerLeft_2() { return static_cast<int32_t>(offsetof(ClipPlanePoints_tFA6B5EF4C87C83626BEAEA585214A9C4679433DA, ___LowerLeft_2)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_LowerLeft_2() const { return ___LowerLeft_2; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_LowerLeft_2() { return &___LowerLeft_2; }
	inline void set_LowerLeft_2(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___LowerLeft_2 = value;
	}

	inline static int32_t get_offset_of_LowerRight_3() { return static_cast<int32_t>(offsetof(ClipPlanePoints_tFA6B5EF4C87C83626BEAEA585214A9C4679433DA, ___LowerRight_3)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_LowerRight_3() const { return ___LowerRight_3; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_LowerRight_3() { return &___LowerRight_3; }
	inline void set_LowerRight_3(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___LowerRight_3 = value;
	}
};


// UnityEngine.UI.ColorBlock
struct ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_SelectedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_SelectedColor_3;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_DisabledColor_4;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_5;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_6;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_NormalColor_0)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_HighlightedColor_1)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_PressedColor_2)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_SelectedColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_SelectedColor_3)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_SelectedColor_3() const { return ___m_SelectedColor_3; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_SelectedColor_3() { return &___m_SelectedColor_3; }
	inline void set_m_SelectedColor_3(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_SelectedColor_3 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_4() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_DisabledColor_4)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_DisabledColor_4() const { return ___m_DisabledColor_4; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_DisabledColor_4() { return &___m_DisabledColor_4; }
	inline void set_m_DisabledColor_4(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_DisabledColor_4 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_5() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_ColorMultiplier_5)); }
	inline float get_m_ColorMultiplier_5() const { return ___m_ColorMultiplier_5; }
	inline float* get_address_of_m_ColorMultiplier_5() { return &___m_ColorMultiplier_5; }
	inline void set_m_ColorMultiplier_5(float value)
	{
		___m_ColorMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_6() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_FadeDuration_6)); }
	inline float get_m_FadeDuration_6() const { return ___m_FadeDuration_6; }
	inline float* get_address_of_m_FadeDuration_6() { return &___m_FadeDuration_6; }
	inline void set_m_FadeDuration_6(float value)
	{
		___m_FadeDuration_6 = value;
	}
};

struct ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955_StaticFields
{
public:
	// UnityEngine.UI.ColorBlock UnityEngine.UI.ColorBlock::defaultColorBlock
	ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  ___defaultColorBlock_7;

public:
	inline static int32_t get_offset_of_defaultColorBlock_7() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955_StaticFields, ___defaultColorBlock_7)); }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  get_defaultColorBlock_7() const { return ___defaultColorBlock_7; }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 * get_address_of_defaultColorBlock_7() { return &___defaultColorBlock_7; }
	inline void set_defaultColorBlock_7(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  value)
	{
		___defaultColorBlock_7 = value;
	}
};


// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_pinvoke : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_com : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// UnityEngine.ForceMode
struct ForceMode_t7778317A4C46140D50D98811D65A7B22E38163D5 
{
public:
	// System.Int32 UnityEngine.ForceMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ForceMode_t7778317A4C46140D50D98811D65A7B22E38163D5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.HideFlags
struct HideFlags_tDC64149E37544FF83B2B4222D3E9DC8188766A12 
{
public:
	// System.Int32 UnityEngine.HideFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HideFlags_tDC64149E37544FF83B2B4222D3E9DC8188766A12, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.KeyCode
struct KeyCode_t1D303F7D061BF4429872E9F109ADDBCB431671F4 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_t1D303F7D061BF4429872E9F109ADDBCB431671F4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.SceneManagement.LoadSceneMode
struct LoadSceneMode_tF5060E18B71D524860ECBF7B9B56193B1907E5CC 
{
public:
	// System.Int32 UnityEngine.SceneManagement.LoadSceneMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LoadSceneMode_tF5060E18B71D524860ECBF7B9B56193B1907E5CC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.PhysicMaterialCombine
struct PhysicMaterialCombine_tFE36CA51E5FDF7DB30B07E42BC4D17A3EBE4BBBE 
{
public:
	// System.Int32 UnityEngine.PhysicMaterialCombine::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PhysicMaterialCombine_tFE36CA51E5FDF7DB30B07E42BC4D17A3EBE4BBBE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Ray
struct Ray_t2E9E67CC8B03EE6ED2BBF3D2C9C96DDF70E1D5E6 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t2E9E67CC8B03EE6ED2BBF3D2C9C96DDF70E1D5E6, ___m_Origin_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t2E9E67CC8B03EE6ED2BBF3D2C9C96DDF70E1D5E6, ___m_Direction_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Direction_1 = value;
	}
};


// UnityEngine.RaycastHit
struct RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_Point_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_Normal_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_FaceID_2)); }
	inline uint32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline uint32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(uint32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_UV_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};


// UnityEngine.Space
struct Space_t568D704D2B0AAC3E5894DDFF13DB2E02E2CD539E 
{
public:
	// System.Int32 UnityEngine.Space::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Space_t568D704D2B0AAC3E5894DDFF13DB2E02E2CD539E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Navigation/Mode
struct Mode_t3113FDF05158BBA1DFC78D7F69E4C1D25135CB0F 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t3113FDF05158BBA1DFC78D7F69E4C1D25135CB0F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Selectable/Transition
struct Transition_t1FC449676815A798E758D32E8BE6DC0A2511DF14 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transition_t1FC449676815A798E758D32E8BE6DC0A2511DF14, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Invector.vCharacterController.vThirdPersonMotor/LocomotionType
struct LocomotionType_t273049CC7616CFB97FFD00D80B9900878D3B4C6A 
{
public:
	// System.Int32 Invector.vCharacterController.vThirdPersonMotor/LocomotionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LocomotionType_t273049CC7616CFB97FFD00D80B9900878D3B4C6A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.AudioClip
struct AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:
	// UnityEngine.AudioClip/PCMReaderCallback UnityEngine.AudioClip::m_PCMReaderCallback
	PCMReaderCallback_t9CA1437D36509A9FAC5EDD8FF2BC3259C24D0E0B * ___m_PCMReaderCallback_4;
	// UnityEngine.AudioClip/PCMSetPositionCallback UnityEngine.AudioClip::m_PCMSetPositionCallback
	PCMSetPositionCallback_tBDD99E7C0697687F1E7B06CDD5DE444A3709CF4C * ___m_PCMSetPositionCallback_5;

public:
	inline static int32_t get_offset_of_m_PCMReaderCallback_4() { return static_cast<int32_t>(offsetof(AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE, ___m_PCMReaderCallback_4)); }
	inline PCMReaderCallback_t9CA1437D36509A9FAC5EDD8FF2BC3259C24D0E0B * get_m_PCMReaderCallback_4() const { return ___m_PCMReaderCallback_4; }
	inline PCMReaderCallback_t9CA1437D36509A9FAC5EDD8FF2BC3259C24D0E0B ** get_address_of_m_PCMReaderCallback_4() { return &___m_PCMReaderCallback_4; }
	inline void set_m_PCMReaderCallback_4(PCMReaderCallback_t9CA1437D36509A9FAC5EDD8FF2BC3259C24D0E0B * value)
	{
		___m_PCMReaderCallback_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PCMReaderCallback_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_PCMSetPositionCallback_5() { return static_cast<int32_t>(offsetof(AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE, ___m_PCMSetPositionCallback_5)); }
	inline PCMSetPositionCallback_tBDD99E7C0697687F1E7B06CDD5DE444A3709CF4C * get_m_PCMSetPositionCallback_5() const { return ___m_PCMSetPositionCallback_5; }
	inline PCMSetPositionCallback_tBDD99E7C0697687F1E7B06CDD5DE444A3709CF4C ** get_address_of_m_PCMSetPositionCallback_5() { return &___m_PCMSetPositionCallback_5; }
	inline void set_m_PCMSetPositionCallback_5(PCMSetPositionCallback_tBDD99E7C0697687F1E7B06CDD5DE444A3709CF4C * value)
	{
		___m_PCMSetPositionCallback_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PCMSetPositionCallback_5), (void*)value);
	}
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// System.Boolean UnityEngine.UI.Navigation::m_WrapAround
	bool ___m_WrapAround_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_WrapAround_1() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_WrapAround_1)); }
	inline bool get_m_WrapAround_1() const { return ___m_WrapAround_1; }
	inline bool* get_address_of_m_WrapAround_1() { return &___m_WrapAround_1; }
	inline void set_m_WrapAround_1(bool value)
	{
		___m_WrapAround_1 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_2() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnUp_2)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnUp_2() const { return ___m_SelectOnUp_2; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnUp_2() { return &___m_SelectOnUp_2; }
	inline void set_m_SelectOnUp_2(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnUp_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnUp_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_3() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnDown_3)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnDown_3() const { return ___m_SelectOnDown_3; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnDown_3() { return &___m_SelectOnDown_3; }
	inline void set_m_SelectOnDown_3(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnDown_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnDown_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_4() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnLeft_4)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnLeft_4() const { return ___m_SelectOnLeft_4; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnLeft_4() { return &___m_SelectOnLeft_4; }
	inline void set_m_SelectOnLeft_4(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnLeft_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnLeft_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_5() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnRight_5)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnRight_5() const { return ___m_SelectOnRight_5; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnRight_5() { return &___m_SelectOnRight_5; }
	inline void set_m_SelectOnRight_5(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnRight_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnRight_5), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A_marshaled_com
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;
};

// UnityEngine.PhysicMaterial
struct PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.SystemException
struct SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Collider
struct Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// UnityEngine.Renderer
struct Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Rigidbody
struct Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Animator
struct Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.AudioBehaviour
struct AudioBehaviour_tB44966D47AD43C50C7294AEE9B57574E55AACA4A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.Camera
struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};

struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPreCull_4;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPreRender_5;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPostRender_6;

public:
	inline static int32_t get_offset_of_onPreCull_4() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPreCull_4)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPreCull_4() const { return ___onPreCull_4; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPreCull_4() { return &___onPreCull_4; }
	inline void set_onPreCull_4(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPreCull_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreCull_4), (void*)value);
	}

	inline static int32_t get_offset_of_onPreRender_5() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPreRender_5)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPreRender_5() const { return ___onPreRender_5; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPreRender_5() { return &___onPreRender_5; }
	inline void set_onPreRender_5(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPreRender_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreRender_5), (void*)value);
	}

	inline static int32_t get_offset_of_onPostRender_6() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPostRender_6)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPostRender_6() const { return ___onPostRender_6; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPostRender_6() { return &___onPostRender_6; }
	inline void set_onPostRender_6(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPostRender_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPostRender_6), (void*)value);
	}
};


// UnityEngine.CapsuleCollider
struct CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635  : public Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.AudioSource
struct AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B  : public AudioBehaviour_tB44966D47AD43C50C7294AEE9B57574E55AACA4A
{
public:

public:
};


// EnemigoIA
struct EnemigoIA_t7F4D590CF8C4897FCB1C723187317E99CD7B4B81  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single EnemigoIA::velocidad
	float ___velocidad_4;
	// System.Int32 EnemigoIA::vida
	int32_t ___vida_5;
	// System.Int32 EnemigoIA::valor
	int32_t ___valor_6;
	// System.Int32 EnemigoIA::da?o
	int32_t ___daUF1o_7;
	// UnityEngine.Transform EnemigoIA::objetivo
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___objetivo_8;
	// System.Int32 EnemigoIA::indicador
	int32_t ___indicador_9;

public:
	inline static int32_t get_offset_of_velocidad_4() { return static_cast<int32_t>(offsetof(EnemigoIA_t7F4D590CF8C4897FCB1C723187317E99CD7B4B81, ___velocidad_4)); }
	inline float get_velocidad_4() const { return ___velocidad_4; }
	inline float* get_address_of_velocidad_4() { return &___velocidad_4; }
	inline void set_velocidad_4(float value)
	{
		___velocidad_4 = value;
	}

	inline static int32_t get_offset_of_vida_5() { return static_cast<int32_t>(offsetof(EnemigoIA_t7F4D590CF8C4897FCB1C723187317E99CD7B4B81, ___vida_5)); }
	inline int32_t get_vida_5() const { return ___vida_5; }
	inline int32_t* get_address_of_vida_5() { return &___vida_5; }
	inline void set_vida_5(int32_t value)
	{
		___vida_5 = value;
	}

	inline static int32_t get_offset_of_valor_6() { return static_cast<int32_t>(offsetof(EnemigoIA_t7F4D590CF8C4897FCB1C723187317E99CD7B4B81, ___valor_6)); }
	inline int32_t get_valor_6() const { return ___valor_6; }
	inline int32_t* get_address_of_valor_6() { return &___valor_6; }
	inline void set_valor_6(int32_t value)
	{
		___valor_6 = value;
	}

	inline static int32_t get_offset_of_daUF1o_7() { return static_cast<int32_t>(offsetof(EnemigoIA_t7F4D590CF8C4897FCB1C723187317E99CD7B4B81, ___daUF1o_7)); }
	inline int32_t get_daUF1o_7() const { return ___daUF1o_7; }
	inline int32_t* get_address_of_daUF1o_7() { return &___daUF1o_7; }
	inline void set_daUF1o_7(int32_t value)
	{
		___daUF1o_7 = value;
	}

	inline static int32_t get_offset_of_objetivo_8() { return static_cast<int32_t>(offsetof(EnemigoIA_t7F4D590CF8C4897FCB1C723187317E99CD7B4B81, ___objetivo_8)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_objetivo_8() const { return ___objetivo_8; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_objetivo_8() { return &___objetivo_8; }
	inline void set_objetivo_8(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___objetivo_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___objetivo_8), (void*)value);
	}

	inline static int32_t get_offset_of_indicador_9() { return static_cast<int32_t>(offsetof(EnemigoIA_t7F4D590CF8C4897FCB1C723187317E99CD7B4B81, ___indicador_9)); }
	inline int32_t get_indicador_9() const { return ___indicador_9; }
	inline int32_t* get_address_of_indicador_9() { return &___indicador_9; }
	inline void set_indicador_9(int32_t value)
	{
		___indicador_9 = value;
	}
};


// Estadisticas
struct Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Int32 Estadisticas::dineroInicial
	int32_t ___dineroInicial_5;
	// System.Int32 Estadisticas::dineroActual
	int32_t ___dineroActual_6;
	// System.Int32 Estadisticas::vidaInicial
	int32_t ___vidaInicial_8;
	// System.Int32 Estadisticas::vidaActual
	int32_t ___vidaActual_9;
	// UnityEngine.UI.Text Estadisticas::dineroTexto
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___dineroTexto_11;
	// UnityEngine.UI.Text Estadisticas::vidaTexto
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___vidaTexto_12;
	// UnityEngine.UI.Text Estadisticas::finTexto
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___finTexto_13;
	// UnityEngine.UI.Button Estadisticas::Reiniciar
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___Reiniciar_14;

public:
	inline static int32_t get_offset_of_dineroInicial_5() { return static_cast<int32_t>(offsetof(Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D, ___dineroInicial_5)); }
	inline int32_t get_dineroInicial_5() const { return ___dineroInicial_5; }
	inline int32_t* get_address_of_dineroInicial_5() { return &___dineroInicial_5; }
	inline void set_dineroInicial_5(int32_t value)
	{
		___dineroInicial_5 = value;
	}

	inline static int32_t get_offset_of_dineroActual_6() { return static_cast<int32_t>(offsetof(Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D, ___dineroActual_6)); }
	inline int32_t get_dineroActual_6() const { return ___dineroActual_6; }
	inline int32_t* get_address_of_dineroActual_6() { return &___dineroActual_6; }
	inline void set_dineroActual_6(int32_t value)
	{
		___dineroActual_6 = value;
	}

	inline static int32_t get_offset_of_vidaInicial_8() { return static_cast<int32_t>(offsetof(Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D, ___vidaInicial_8)); }
	inline int32_t get_vidaInicial_8() const { return ___vidaInicial_8; }
	inline int32_t* get_address_of_vidaInicial_8() { return &___vidaInicial_8; }
	inline void set_vidaInicial_8(int32_t value)
	{
		___vidaInicial_8 = value;
	}

	inline static int32_t get_offset_of_vidaActual_9() { return static_cast<int32_t>(offsetof(Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D, ___vidaActual_9)); }
	inline int32_t get_vidaActual_9() const { return ___vidaActual_9; }
	inline int32_t* get_address_of_vidaActual_9() { return &___vidaActual_9; }
	inline void set_vidaActual_9(int32_t value)
	{
		___vidaActual_9 = value;
	}

	inline static int32_t get_offset_of_dineroTexto_11() { return static_cast<int32_t>(offsetof(Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D, ___dineroTexto_11)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_dineroTexto_11() const { return ___dineroTexto_11; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_dineroTexto_11() { return &___dineroTexto_11; }
	inline void set_dineroTexto_11(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___dineroTexto_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dineroTexto_11), (void*)value);
	}

	inline static int32_t get_offset_of_vidaTexto_12() { return static_cast<int32_t>(offsetof(Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D, ___vidaTexto_12)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_vidaTexto_12() const { return ___vidaTexto_12; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_vidaTexto_12() { return &___vidaTexto_12; }
	inline void set_vidaTexto_12(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___vidaTexto_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___vidaTexto_12), (void*)value);
	}

	inline static int32_t get_offset_of_finTexto_13() { return static_cast<int32_t>(offsetof(Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D, ___finTexto_13)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_finTexto_13() const { return ___finTexto_13; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_finTexto_13() { return &___finTexto_13; }
	inline void set_finTexto_13(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___finTexto_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___finTexto_13), (void*)value);
	}

	inline static int32_t get_offset_of_Reiniciar_14() { return static_cast<int32_t>(offsetof(Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D, ___Reiniciar_14)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_Reiniciar_14() const { return ___Reiniciar_14; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_Reiniciar_14() { return &___Reiniciar_14; }
	inline void set_Reiniciar_14(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___Reiniciar_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Reiniciar_14), (void*)value);
	}
};

struct Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D_StaticFields
{
public:
	// System.Int32 Estadisticas::dinero
	int32_t ___dinero_4;
	// System.Int32 Estadisticas::vida
	int32_t ___vida_7;
	// System.Boolean Estadisticas::vivo
	bool ___vivo_10;

public:
	inline static int32_t get_offset_of_dinero_4() { return static_cast<int32_t>(offsetof(Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D_StaticFields, ___dinero_4)); }
	inline int32_t get_dinero_4() const { return ___dinero_4; }
	inline int32_t* get_address_of_dinero_4() { return &___dinero_4; }
	inline void set_dinero_4(int32_t value)
	{
		___dinero_4 = value;
	}

	inline static int32_t get_offset_of_vida_7() { return static_cast<int32_t>(offsetof(Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D_StaticFields, ___vida_7)); }
	inline int32_t get_vida_7() const { return ___vida_7; }
	inline int32_t* get_address_of_vida_7() { return &___vida_7; }
	inline void set_vida_7(int32_t value)
	{
		___vida_7 = value;
	}

	inline static int32_t get_offset_of_vivo_10() { return static_cast<int32_t>(offsetof(Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D_StaticFields, ___vivo_10)); }
	inline bool get_vivo_10() const { return ___vivo_10; }
	inline bool* get_address_of_vivo_10() { return &___vivo_10; }
	inline void set_vivo_10(bool value)
	{
		___vivo_10 = value;
	}
};


// InvocadorOleadas
struct InvocadorOleadas_t7ADC79FAB86E1B7AE663CA487985C11C00B4C530  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Transform InvocadorOleadas::EnemigoPrefabricado
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___EnemigoPrefabricado_4;
	// UnityEngine.Transform InvocadorOleadas::puntoAparicion
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___puntoAparicion_5;
	// System.Single InvocadorOleadas::tiempoEntreOleadas
	float ___tiempoEntreOleadas_6;
	// System.Single InvocadorOleadas::contador
	float ___contador_7;
	// UnityEngine.UI.Text InvocadorOleadas::oleadaNumeroTexto
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___oleadaNumeroTexto_8;
	// UnityEngine.UI.Text InvocadorOleadas::aviso
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___aviso_9;
	// System.Int32 InvocadorOleadas::numeroOleada
	int32_t ___numeroOleada_10;

public:
	inline static int32_t get_offset_of_EnemigoPrefabricado_4() { return static_cast<int32_t>(offsetof(InvocadorOleadas_t7ADC79FAB86E1B7AE663CA487985C11C00B4C530, ___EnemigoPrefabricado_4)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_EnemigoPrefabricado_4() const { return ___EnemigoPrefabricado_4; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_EnemigoPrefabricado_4() { return &___EnemigoPrefabricado_4; }
	inline void set_EnemigoPrefabricado_4(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___EnemigoPrefabricado_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EnemigoPrefabricado_4), (void*)value);
	}

	inline static int32_t get_offset_of_puntoAparicion_5() { return static_cast<int32_t>(offsetof(InvocadorOleadas_t7ADC79FAB86E1B7AE663CA487985C11C00B4C530, ___puntoAparicion_5)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_puntoAparicion_5() const { return ___puntoAparicion_5; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_puntoAparicion_5() { return &___puntoAparicion_5; }
	inline void set_puntoAparicion_5(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___puntoAparicion_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___puntoAparicion_5), (void*)value);
	}

	inline static int32_t get_offset_of_tiempoEntreOleadas_6() { return static_cast<int32_t>(offsetof(InvocadorOleadas_t7ADC79FAB86E1B7AE663CA487985C11C00B4C530, ___tiempoEntreOleadas_6)); }
	inline float get_tiempoEntreOleadas_6() const { return ___tiempoEntreOleadas_6; }
	inline float* get_address_of_tiempoEntreOleadas_6() { return &___tiempoEntreOleadas_6; }
	inline void set_tiempoEntreOleadas_6(float value)
	{
		___tiempoEntreOleadas_6 = value;
	}

	inline static int32_t get_offset_of_contador_7() { return static_cast<int32_t>(offsetof(InvocadorOleadas_t7ADC79FAB86E1B7AE663CA487985C11C00B4C530, ___contador_7)); }
	inline float get_contador_7() const { return ___contador_7; }
	inline float* get_address_of_contador_7() { return &___contador_7; }
	inline void set_contador_7(float value)
	{
		___contador_7 = value;
	}

	inline static int32_t get_offset_of_oleadaNumeroTexto_8() { return static_cast<int32_t>(offsetof(InvocadorOleadas_t7ADC79FAB86E1B7AE663CA487985C11C00B4C530, ___oleadaNumeroTexto_8)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_oleadaNumeroTexto_8() const { return ___oleadaNumeroTexto_8; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_oleadaNumeroTexto_8() { return &___oleadaNumeroTexto_8; }
	inline void set_oleadaNumeroTexto_8(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___oleadaNumeroTexto_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___oleadaNumeroTexto_8), (void*)value);
	}

	inline static int32_t get_offset_of_aviso_9() { return static_cast<int32_t>(offsetof(InvocadorOleadas_t7ADC79FAB86E1B7AE663CA487985C11C00B4C530, ___aviso_9)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_aviso_9() const { return ___aviso_9; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_aviso_9() { return &___aviso_9; }
	inline void set_aviso_9(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___aviso_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___aviso_9), (void*)value);
	}

	inline static int32_t get_offset_of_numeroOleada_10() { return static_cast<int32_t>(offsetof(InvocadorOleadas_t7ADC79FAB86E1B7AE663CA487985C11C00B4C530, ___numeroOleada_10)); }
	inline int32_t get_numeroOleada_10() const { return ___numeroOleada_10; }
	inline int32_t* get_address_of_numeroOleada_10() { return &___numeroOleada_10; }
	inline void set_numeroOleada_10(int32_t value)
	{
		___numeroOleada_10 = value;
	}
};


// Pinchos
struct Pinchos_tBABE699D1BAED348FC2D8DAD13582F801A0644DC  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single Pinchos::alcance
	float ___alcance_4;
	// System.Int32 Pinchos::da?o
	int32_t ___daUF1o_5;
	// UnityEngine.Transform Pinchos::objetivo
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___objetivo_6;
	// System.String Pinchos::TagEnemigos
	String_t* ___TagEnemigos_7;
	// System.Single Pinchos::temporizadorParaDa?ar
	float ___temporizadorParaDaUF1ar_8;

public:
	inline static int32_t get_offset_of_alcance_4() { return static_cast<int32_t>(offsetof(Pinchos_tBABE699D1BAED348FC2D8DAD13582F801A0644DC, ___alcance_4)); }
	inline float get_alcance_4() const { return ___alcance_4; }
	inline float* get_address_of_alcance_4() { return &___alcance_4; }
	inline void set_alcance_4(float value)
	{
		___alcance_4 = value;
	}

	inline static int32_t get_offset_of_daUF1o_5() { return static_cast<int32_t>(offsetof(Pinchos_tBABE699D1BAED348FC2D8DAD13582F801A0644DC, ___daUF1o_5)); }
	inline int32_t get_daUF1o_5() const { return ___daUF1o_5; }
	inline int32_t* get_address_of_daUF1o_5() { return &___daUF1o_5; }
	inline void set_daUF1o_5(int32_t value)
	{
		___daUF1o_5 = value;
	}

	inline static int32_t get_offset_of_objetivo_6() { return static_cast<int32_t>(offsetof(Pinchos_tBABE699D1BAED348FC2D8DAD13582F801A0644DC, ___objetivo_6)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_objetivo_6() const { return ___objetivo_6; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_objetivo_6() { return &___objetivo_6; }
	inline void set_objetivo_6(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___objetivo_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___objetivo_6), (void*)value);
	}

	inline static int32_t get_offset_of_TagEnemigos_7() { return static_cast<int32_t>(offsetof(Pinchos_tBABE699D1BAED348FC2D8DAD13582F801A0644DC, ___TagEnemigos_7)); }
	inline String_t* get_TagEnemigos_7() const { return ___TagEnemigos_7; }
	inline String_t** get_address_of_TagEnemigos_7() { return &___TagEnemigos_7; }
	inline void set_TagEnemigos_7(String_t* value)
	{
		___TagEnemigos_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TagEnemigos_7), (void*)value);
	}

	inline static int32_t get_offset_of_temporizadorParaDaUF1ar_8() { return static_cast<int32_t>(offsetof(Pinchos_tBABE699D1BAED348FC2D8DAD13582F801A0644DC, ___temporizadorParaDaUF1ar_8)); }
	inline float get_temporizadorParaDaUF1ar_8() const { return ___temporizadorParaDaUF1ar_8; }
	inline float* get_address_of_temporizadorParaDaUF1ar_8() { return &___temporizadorParaDaUF1ar_8; }
	inline void set_temporizadorParaDaUF1ar_8(float value)
	{
		___temporizadorParaDaUF1ar_8 = value;
	}
};


// PuntosRuta
struct PuntosRuta_t1BFAE08531BC3AEEE4B521035B0D948B4BA0F8AE  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};

struct PuntosRuta_t1BFAE08531BC3AEEE4B521035B0D948B4BA0F8AE_StaticFields
{
public:
	// UnityEngine.Transform[] PuntosRuta::puntos
	TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* ___puntos_4;

public:
	inline static int32_t get_offset_of_puntos_4() { return static_cast<int32_t>(offsetof(PuntosRuta_t1BFAE08531BC3AEEE4B521035B0D948B4BA0F8AE_StaticFields, ___puntos_4)); }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* get_puntos_4() const { return ___puntos_4; }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D** get_address_of_puntos_4() { return &___puntos_4; }
	inline void set_puntos_4(TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* value)
	{
		___puntos_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___puntos_4), (void*)value);
	}
};


// Reiniciar
struct Reiniciar_tCA54EF25F87683B96AF8E690C069B2B3BC1B8510  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// Salir
struct Salir_t1BBF788DCACDEDDAA6F34E80CD336C6A7BEC63C0  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// Test
struct Test_tB521413BD14DFDD715F0C78DB4B6392F71A6EED5  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Vector3 Test::scaleChange
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___scaleChange_4;
	// UnityEngine.Vector3 Test::positionChange
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positionChange_5;
	// UnityEngine.Vector3 Test::rotateChange
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rotateChange_6;

public:
	inline static int32_t get_offset_of_scaleChange_4() { return static_cast<int32_t>(offsetof(Test_tB521413BD14DFDD715F0C78DB4B6392F71A6EED5, ___scaleChange_4)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_scaleChange_4() const { return ___scaleChange_4; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_scaleChange_4() { return &___scaleChange_4; }
	inline void set_scaleChange_4(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___scaleChange_4 = value;
	}

	inline static int32_t get_offset_of_positionChange_5() { return static_cast<int32_t>(offsetof(Test_tB521413BD14DFDD715F0C78DB4B6392F71A6EED5, ___positionChange_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positionChange_5() const { return ___positionChange_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positionChange_5() { return &___positionChange_5; }
	inline void set_positionChange_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positionChange_5 = value;
	}

	inline static int32_t get_offset_of_rotateChange_6() { return static_cast<int32_t>(offsetof(Test_tB521413BD14DFDD715F0C78DB4B6392F71A6EED5, ___rotateChange_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rotateChange_6() const { return ___rotateChange_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rotateChange_6() { return &___rotateChange_6; }
	inline void set_rotateChange_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rotateChange_6 = value;
	}
};


// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// Invector.Utils.vComment
struct vComment_tEBF402879B60173DE75AD1509B72C5EB560FDA80  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// vPickupItem
struct vPickupItem_t9764F53667CE8FA36C0E25286B6039029BD6B4E0  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.AudioSource vPickupItem::_audioSource
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ____audioSource_4;
	// UnityEngine.AudioClip vPickupItem::_audioClip
	AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ____audioClip_5;
	// UnityEngine.GameObject vPickupItem::_particle
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ____particle_6;

public:
	inline static int32_t get_offset_of__audioSource_4() { return static_cast<int32_t>(offsetof(vPickupItem_t9764F53667CE8FA36C0E25286B6039029BD6B4E0, ____audioSource_4)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get__audioSource_4() const { return ____audioSource_4; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of__audioSource_4() { return &____audioSource_4; }
	inline void set__audioSource_4(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		____audioSource_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____audioSource_4), (void*)value);
	}

	inline static int32_t get_offset_of__audioClip_5() { return static_cast<int32_t>(offsetof(vPickupItem_t9764F53667CE8FA36C0E25286B6039029BD6B4E0, ____audioClip_5)); }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * get__audioClip_5() const { return ____audioClip_5; }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE ** get_address_of__audioClip_5() { return &____audioClip_5; }
	inline void set__audioClip_5(AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * value)
	{
		____audioClip_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____audioClip_5), (void*)value);
	}

	inline static int32_t get_offset_of__particle_6() { return static_cast<int32_t>(offsetof(vPickupItem_t9764F53667CE8FA36C0E25286B6039029BD6B4E0, ____particle_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get__particle_6() const { return ____particle_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of__particle_6() { return &____particle_6; }
	inline void set__particle_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		____particle_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____particle_6), (void*)value);
	}
};


// vThirdPersonCamera
struct vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Transform vThirdPersonCamera::target
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___target_4;
	// System.Single vThirdPersonCamera::smoothCameraRotation
	float ___smoothCameraRotation_5;
	// UnityEngine.LayerMask vThirdPersonCamera::cullingLayer
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___cullingLayer_6;
	// System.Boolean vThirdPersonCamera::lockCamera
	bool ___lockCamera_7;
	// System.Single vThirdPersonCamera::rightOffset
	float ___rightOffset_8;
	// System.Single vThirdPersonCamera::defaultDistance
	float ___defaultDistance_9;
	// System.Single vThirdPersonCamera::height
	float ___height_10;
	// System.Single vThirdPersonCamera::smoothFollow
	float ___smoothFollow_11;
	// System.Single vThirdPersonCamera::xMouseSensitivity
	float ___xMouseSensitivity_12;
	// System.Single vThirdPersonCamera::yMouseSensitivity
	float ___yMouseSensitivity_13;
	// System.Single vThirdPersonCamera::yMinLimit
	float ___yMinLimit_14;
	// System.Single vThirdPersonCamera::yMaxLimit
	float ___yMaxLimit_15;
	// System.Int32 vThirdPersonCamera::indexList
	int32_t ___indexList_16;
	// System.Int32 vThirdPersonCamera::indexLookPoint
	int32_t ___indexLookPoint_17;
	// System.Single vThirdPersonCamera::offSetPlayerPivot
	float ___offSetPlayerPivot_18;
	// System.String vThirdPersonCamera::currentStateName
	String_t* ___currentStateName_19;
	// UnityEngine.Transform vThirdPersonCamera::currentTarget
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___currentTarget_20;
	// UnityEngine.Vector2 vThirdPersonCamera::movementSpeed
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___movementSpeed_21;
	// UnityEngine.Transform vThirdPersonCamera::targetLookAt
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___targetLookAt_22;
	// UnityEngine.Vector3 vThirdPersonCamera::currentTargetPos
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___currentTargetPos_23;
	// UnityEngine.Vector3 vThirdPersonCamera::lookPoint
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___lookPoint_24;
	// UnityEngine.Vector3 vThirdPersonCamera::current_cPos
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___current_cPos_25;
	// UnityEngine.Vector3 vThirdPersonCamera::desired_cPos
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___desired_cPos_26;
	// UnityEngine.Camera vThirdPersonCamera::_camera
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ____camera_27;
	// System.Single vThirdPersonCamera::distance
	float ___distance_28;
	// System.Single vThirdPersonCamera::mouseY
	float ___mouseY_29;
	// System.Single vThirdPersonCamera::mouseX
	float ___mouseX_30;
	// System.Single vThirdPersonCamera::currentHeight
	float ___currentHeight_31;
	// System.Single vThirdPersonCamera::cullingDistance
	float ___cullingDistance_32;
	// System.Single vThirdPersonCamera::checkHeightRadius
	float ___checkHeightRadius_33;
	// System.Single vThirdPersonCamera::clipPlaneMargin
	float ___clipPlaneMargin_34;
	// System.Single vThirdPersonCamera::forward
	float ___forward_35;
	// System.Single vThirdPersonCamera::xMinLimit
	float ___xMinLimit_36;
	// System.Single vThirdPersonCamera::xMaxLimit
	float ___xMaxLimit_37;
	// System.Single vThirdPersonCamera::cullingHeight
	float ___cullingHeight_38;
	// System.Single vThirdPersonCamera::cullingMinDist
	float ___cullingMinDist_39;

public:
	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11, ___target_4)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_target_4() const { return ___target_4; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_4), (void*)value);
	}

	inline static int32_t get_offset_of_smoothCameraRotation_5() { return static_cast<int32_t>(offsetof(vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11, ___smoothCameraRotation_5)); }
	inline float get_smoothCameraRotation_5() const { return ___smoothCameraRotation_5; }
	inline float* get_address_of_smoothCameraRotation_5() { return &___smoothCameraRotation_5; }
	inline void set_smoothCameraRotation_5(float value)
	{
		___smoothCameraRotation_5 = value;
	}

	inline static int32_t get_offset_of_cullingLayer_6() { return static_cast<int32_t>(offsetof(vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11, ___cullingLayer_6)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get_cullingLayer_6() const { return ___cullingLayer_6; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of_cullingLayer_6() { return &___cullingLayer_6; }
	inline void set_cullingLayer_6(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		___cullingLayer_6 = value;
	}

	inline static int32_t get_offset_of_lockCamera_7() { return static_cast<int32_t>(offsetof(vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11, ___lockCamera_7)); }
	inline bool get_lockCamera_7() const { return ___lockCamera_7; }
	inline bool* get_address_of_lockCamera_7() { return &___lockCamera_7; }
	inline void set_lockCamera_7(bool value)
	{
		___lockCamera_7 = value;
	}

	inline static int32_t get_offset_of_rightOffset_8() { return static_cast<int32_t>(offsetof(vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11, ___rightOffset_8)); }
	inline float get_rightOffset_8() const { return ___rightOffset_8; }
	inline float* get_address_of_rightOffset_8() { return &___rightOffset_8; }
	inline void set_rightOffset_8(float value)
	{
		___rightOffset_8 = value;
	}

	inline static int32_t get_offset_of_defaultDistance_9() { return static_cast<int32_t>(offsetof(vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11, ___defaultDistance_9)); }
	inline float get_defaultDistance_9() const { return ___defaultDistance_9; }
	inline float* get_address_of_defaultDistance_9() { return &___defaultDistance_9; }
	inline void set_defaultDistance_9(float value)
	{
		___defaultDistance_9 = value;
	}

	inline static int32_t get_offset_of_height_10() { return static_cast<int32_t>(offsetof(vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11, ___height_10)); }
	inline float get_height_10() const { return ___height_10; }
	inline float* get_address_of_height_10() { return &___height_10; }
	inline void set_height_10(float value)
	{
		___height_10 = value;
	}

	inline static int32_t get_offset_of_smoothFollow_11() { return static_cast<int32_t>(offsetof(vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11, ___smoothFollow_11)); }
	inline float get_smoothFollow_11() const { return ___smoothFollow_11; }
	inline float* get_address_of_smoothFollow_11() { return &___smoothFollow_11; }
	inline void set_smoothFollow_11(float value)
	{
		___smoothFollow_11 = value;
	}

	inline static int32_t get_offset_of_xMouseSensitivity_12() { return static_cast<int32_t>(offsetof(vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11, ___xMouseSensitivity_12)); }
	inline float get_xMouseSensitivity_12() const { return ___xMouseSensitivity_12; }
	inline float* get_address_of_xMouseSensitivity_12() { return &___xMouseSensitivity_12; }
	inline void set_xMouseSensitivity_12(float value)
	{
		___xMouseSensitivity_12 = value;
	}

	inline static int32_t get_offset_of_yMouseSensitivity_13() { return static_cast<int32_t>(offsetof(vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11, ___yMouseSensitivity_13)); }
	inline float get_yMouseSensitivity_13() const { return ___yMouseSensitivity_13; }
	inline float* get_address_of_yMouseSensitivity_13() { return &___yMouseSensitivity_13; }
	inline void set_yMouseSensitivity_13(float value)
	{
		___yMouseSensitivity_13 = value;
	}

	inline static int32_t get_offset_of_yMinLimit_14() { return static_cast<int32_t>(offsetof(vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11, ___yMinLimit_14)); }
	inline float get_yMinLimit_14() const { return ___yMinLimit_14; }
	inline float* get_address_of_yMinLimit_14() { return &___yMinLimit_14; }
	inline void set_yMinLimit_14(float value)
	{
		___yMinLimit_14 = value;
	}

	inline static int32_t get_offset_of_yMaxLimit_15() { return static_cast<int32_t>(offsetof(vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11, ___yMaxLimit_15)); }
	inline float get_yMaxLimit_15() const { return ___yMaxLimit_15; }
	inline float* get_address_of_yMaxLimit_15() { return &___yMaxLimit_15; }
	inline void set_yMaxLimit_15(float value)
	{
		___yMaxLimit_15 = value;
	}

	inline static int32_t get_offset_of_indexList_16() { return static_cast<int32_t>(offsetof(vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11, ___indexList_16)); }
	inline int32_t get_indexList_16() const { return ___indexList_16; }
	inline int32_t* get_address_of_indexList_16() { return &___indexList_16; }
	inline void set_indexList_16(int32_t value)
	{
		___indexList_16 = value;
	}

	inline static int32_t get_offset_of_indexLookPoint_17() { return static_cast<int32_t>(offsetof(vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11, ___indexLookPoint_17)); }
	inline int32_t get_indexLookPoint_17() const { return ___indexLookPoint_17; }
	inline int32_t* get_address_of_indexLookPoint_17() { return &___indexLookPoint_17; }
	inline void set_indexLookPoint_17(int32_t value)
	{
		___indexLookPoint_17 = value;
	}

	inline static int32_t get_offset_of_offSetPlayerPivot_18() { return static_cast<int32_t>(offsetof(vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11, ___offSetPlayerPivot_18)); }
	inline float get_offSetPlayerPivot_18() const { return ___offSetPlayerPivot_18; }
	inline float* get_address_of_offSetPlayerPivot_18() { return &___offSetPlayerPivot_18; }
	inline void set_offSetPlayerPivot_18(float value)
	{
		___offSetPlayerPivot_18 = value;
	}

	inline static int32_t get_offset_of_currentStateName_19() { return static_cast<int32_t>(offsetof(vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11, ___currentStateName_19)); }
	inline String_t* get_currentStateName_19() const { return ___currentStateName_19; }
	inline String_t** get_address_of_currentStateName_19() { return &___currentStateName_19; }
	inline void set_currentStateName_19(String_t* value)
	{
		___currentStateName_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currentStateName_19), (void*)value);
	}

	inline static int32_t get_offset_of_currentTarget_20() { return static_cast<int32_t>(offsetof(vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11, ___currentTarget_20)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_currentTarget_20() const { return ___currentTarget_20; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_currentTarget_20() { return &___currentTarget_20; }
	inline void set_currentTarget_20(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___currentTarget_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currentTarget_20), (void*)value);
	}

	inline static int32_t get_offset_of_movementSpeed_21() { return static_cast<int32_t>(offsetof(vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11, ___movementSpeed_21)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_movementSpeed_21() const { return ___movementSpeed_21; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_movementSpeed_21() { return &___movementSpeed_21; }
	inline void set_movementSpeed_21(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___movementSpeed_21 = value;
	}

	inline static int32_t get_offset_of_targetLookAt_22() { return static_cast<int32_t>(offsetof(vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11, ___targetLookAt_22)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_targetLookAt_22() const { return ___targetLookAt_22; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_targetLookAt_22() { return &___targetLookAt_22; }
	inline void set_targetLookAt_22(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___targetLookAt_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___targetLookAt_22), (void*)value);
	}

	inline static int32_t get_offset_of_currentTargetPos_23() { return static_cast<int32_t>(offsetof(vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11, ___currentTargetPos_23)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_currentTargetPos_23() const { return ___currentTargetPos_23; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_currentTargetPos_23() { return &___currentTargetPos_23; }
	inline void set_currentTargetPos_23(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___currentTargetPos_23 = value;
	}

	inline static int32_t get_offset_of_lookPoint_24() { return static_cast<int32_t>(offsetof(vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11, ___lookPoint_24)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_lookPoint_24() const { return ___lookPoint_24; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_lookPoint_24() { return &___lookPoint_24; }
	inline void set_lookPoint_24(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___lookPoint_24 = value;
	}

	inline static int32_t get_offset_of_current_cPos_25() { return static_cast<int32_t>(offsetof(vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11, ___current_cPos_25)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_current_cPos_25() const { return ___current_cPos_25; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_current_cPos_25() { return &___current_cPos_25; }
	inline void set_current_cPos_25(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___current_cPos_25 = value;
	}

	inline static int32_t get_offset_of_desired_cPos_26() { return static_cast<int32_t>(offsetof(vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11, ___desired_cPos_26)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_desired_cPos_26() const { return ___desired_cPos_26; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_desired_cPos_26() { return &___desired_cPos_26; }
	inline void set_desired_cPos_26(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___desired_cPos_26 = value;
	}

	inline static int32_t get_offset_of__camera_27() { return static_cast<int32_t>(offsetof(vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11, ____camera_27)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get__camera_27() const { return ____camera_27; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of__camera_27() { return &____camera_27; }
	inline void set__camera_27(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		____camera_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____camera_27), (void*)value);
	}

	inline static int32_t get_offset_of_distance_28() { return static_cast<int32_t>(offsetof(vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11, ___distance_28)); }
	inline float get_distance_28() const { return ___distance_28; }
	inline float* get_address_of_distance_28() { return &___distance_28; }
	inline void set_distance_28(float value)
	{
		___distance_28 = value;
	}

	inline static int32_t get_offset_of_mouseY_29() { return static_cast<int32_t>(offsetof(vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11, ___mouseY_29)); }
	inline float get_mouseY_29() const { return ___mouseY_29; }
	inline float* get_address_of_mouseY_29() { return &___mouseY_29; }
	inline void set_mouseY_29(float value)
	{
		___mouseY_29 = value;
	}

	inline static int32_t get_offset_of_mouseX_30() { return static_cast<int32_t>(offsetof(vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11, ___mouseX_30)); }
	inline float get_mouseX_30() const { return ___mouseX_30; }
	inline float* get_address_of_mouseX_30() { return &___mouseX_30; }
	inline void set_mouseX_30(float value)
	{
		___mouseX_30 = value;
	}

	inline static int32_t get_offset_of_currentHeight_31() { return static_cast<int32_t>(offsetof(vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11, ___currentHeight_31)); }
	inline float get_currentHeight_31() const { return ___currentHeight_31; }
	inline float* get_address_of_currentHeight_31() { return &___currentHeight_31; }
	inline void set_currentHeight_31(float value)
	{
		___currentHeight_31 = value;
	}

	inline static int32_t get_offset_of_cullingDistance_32() { return static_cast<int32_t>(offsetof(vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11, ___cullingDistance_32)); }
	inline float get_cullingDistance_32() const { return ___cullingDistance_32; }
	inline float* get_address_of_cullingDistance_32() { return &___cullingDistance_32; }
	inline void set_cullingDistance_32(float value)
	{
		___cullingDistance_32 = value;
	}

	inline static int32_t get_offset_of_checkHeightRadius_33() { return static_cast<int32_t>(offsetof(vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11, ___checkHeightRadius_33)); }
	inline float get_checkHeightRadius_33() const { return ___checkHeightRadius_33; }
	inline float* get_address_of_checkHeightRadius_33() { return &___checkHeightRadius_33; }
	inline void set_checkHeightRadius_33(float value)
	{
		___checkHeightRadius_33 = value;
	}

	inline static int32_t get_offset_of_clipPlaneMargin_34() { return static_cast<int32_t>(offsetof(vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11, ___clipPlaneMargin_34)); }
	inline float get_clipPlaneMargin_34() const { return ___clipPlaneMargin_34; }
	inline float* get_address_of_clipPlaneMargin_34() { return &___clipPlaneMargin_34; }
	inline void set_clipPlaneMargin_34(float value)
	{
		___clipPlaneMargin_34 = value;
	}

	inline static int32_t get_offset_of_forward_35() { return static_cast<int32_t>(offsetof(vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11, ___forward_35)); }
	inline float get_forward_35() const { return ___forward_35; }
	inline float* get_address_of_forward_35() { return &___forward_35; }
	inline void set_forward_35(float value)
	{
		___forward_35 = value;
	}

	inline static int32_t get_offset_of_xMinLimit_36() { return static_cast<int32_t>(offsetof(vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11, ___xMinLimit_36)); }
	inline float get_xMinLimit_36() const { return ___xMinLimit_36; }
	inline float* get_address_of_xMinLimit_36() { return &___xMinLimit_36; }
	inline void set_xMinLimit_36(float value)
	{
		___xMinLimit_36 = value;
	}

	inline static int32_t get_offset_of_xMaxLimit_37() { return static_cast<int32_t>(offsetof(vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11, ___xMaxLimit_37)); }
	inline float get_xMaxLimit_37() const { return ___xMaxLimit_37; }
	inline float* get_address_of_xMaxLimit_37() { return &___xMaxLimit_37; }
	inline void set_xMaxLimit_37(float value)
	{
		___xMaxLimit_37 = value;
	}

	inline static int32_t get_offset_of_cullingHeight_38() { return static_cast<int32_t>(offsetof(vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11, ___cullingHeight_38)); }
	inline float get_cullingHeight_38() const { return ___cullingHeight_38; }
	inline float* get_address_of_cullingHeight_38() { return &___cullingHeight_38; }
	inline void set_cullingHeight_38(float value)
	{
		___cullingHeight_38 = value;
	}

	inline static int32_t get_offset_of_cullingMinDist_39() { return static_cast<int32_t>(offsetof(vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11, ___cullingMinDist_39)); }
	inline float get_cullingMinDist_39() const { return ___cullingMinDist_39; }
	inline float* get_address_of_cullingMinDist_39() { return &___cullingMinDist_39; }
	inline void set_cullingMinDist_39(float value)
	{
		___cullingMinDist_39 = value;
	}
};


// Invector.vCharacterController.vThirdPersonInput
struct vThirdPersonInput_tB15CFA93BCDA688B2E2D8F976E90C4262CB82C50  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String Invector.vCharacterController.vThirdPersonInput::horizontalInput
	String_t* ___horizontalInput_4;
	// System.String Invector.vCharacterController.vThirdPersonInput::verticallInput
	String_t* ___verticallInput_5;
	// UnityEngine.KeyCode Invector.vCharacterController.vThirdPersonInput::jumpInput
	int32_t ___jumpInput_6;
	// UnityEngine.KeyCode Invector.vCharacterController.vThirdPersonInput::strafeInput
	int32_t ___strafeInput_7;
	// UnityEngine.KeyCode Invector.vCharacterController.vThirdPersonInput::sprintInput
	int32_t ___sprintInput_8;
	// System.String Invector.vCharacterController.vThirdPersonInput::rotateCameraXInput
	String_t* ___rotateCameraXInput_9;
	// System.String Invector.vCharacterController.vThirdPersonInput::rotateCameraYInput
	String_t* ___rotateCameraYInput_10;
	// Invector.vCharacterController.vThirdPersonController Invector.vCharacterController.vThirdPersonInput::cc
	vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * ___cc_11;
	// vThirdPersonCamera Invector.vCharacterController.vThirdPersonInput::tpCamera
	vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11 * ___tpCamera_12;
	// UnityEngine.Camera Invector.vCharacterController.vThirdPersonInput::cameraMain
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___cameraMain_13;

public:
	inline static int32_t get_offset_of_horizontalInput_4() { return static_cast<int32_t>(offsetof(vThirdPersonInput_tB15CFA93BCDA688B2E2D8F976E90C4262CB82C50, ___horizontalInput_4)); }
	inline String_t* get_horizontalInput_4() const { return ___horizontalInput_4; }
	inline String_t** get_address_of_horizontalInput_4() { return &___horizontalInput_4; }
	inline void set_horizontalInput_4(String_t* value)
	{
		___horizontalInput_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___horizontalInput_4), (void*)value);
	}

	inline static int32_t get_offset_of_verticallInput_5() { return static_cast<int32_t>(offsetof(vThirdPersonInput_tB15CFA93BCDA688B2E2D8F976E90C4262CB82C50, ___verticallInput_5)); }
	inline String_t* get_verticallInput_5() const { return ___verticallInput_5; }
	inline String_t** get_address_of_verticallInput_5() { return &___verticallInput_5; }
	inline void set_verticallInput_5(String_t* value)
	{
		___verticallInput_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___verticallInput_5), (void*)value);
	}

	inline static int32_t get_offset_of_jumpInput_6() { return static_cast<int32_t>(offsetof(vThirdPersonInput_tB15CFA93BCDA688B2E2D8F976E90C4262CB82C50, ___jumpInput_6)); }
	inline int32_t get_jumpInput_6() const { return ___jumpInput_6; }
	inline int32_t* get_address_of_jumpInput_6() { return &___jumpInput_6; }
	inline void set_jumpInput_6(int32_t value)
	{
		___jumpInput_6 = value;
	}

	inline static int32_t get_offset_of_strafeInput_7() { return static_cast<int32_t>(offsetof(vThirdPersonInput_tB15CFA93BCDA688B2E2D8F976E90C4262CB82C50, ___strafeInput_7)); }
	inline int32_t get_strafeInput_7() const { return ___strafeInput_7; }
	inline int32_t* get_address_of_strafeInput_7() { return &___strafeInput_7; }
	inline void set_strafeInput_7(int32_t value)
	{
		___strafeInput_7 = value;
	}

	inline static int32_t get_offset_of_sprintInput_8() { return static_cast<int32_t>(offsetof(vThirdPersonInput_tB15CFA93BCDA688B2E2D8F976E90C4262CB82C50, ___sprintInput_8)); }
	inline int32_t get_sprintInput_8() const { return ___sprintInput_8; }
	inline int32_t* get_address_of_sprintInput_8() { return &___sprintInput_8; }
	inline void set_sprintInput_8(int32_t value)
	{
		___sprintInput_8 = value;
	}

	inline static int32_t get_offset_of_rotateCameraXInput_9() { return static_cast<int32_t>(offsetof(vThirdPersonInput_tB15CFA93BCDA688B2E2D8F976E90C4262CB82C50, ___rotateCameraXInput_9)); }
	inline String_t* get_rotateCameraXInput_9() const { return ___rotateCameraXInput_9; }
	inline String_t** get_address_of_rotateCameraXInput_9() { return &___rotateCameraXInput_9; }
	inline void set_rotateCameraXInput_9(String_t* value)
	{
		___rotateCameraXInput_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rotateCameraXInput_9), (void*)value);
	}

	inline static int32_t get_offset_of_rotateCameraYInput_10() { return static_cast<int32_t>(offsetof(vThirdPersonInput_tB15CFA93BCDA688B2E2D8F976E90C4262CB82C50, ___rotateCameraYInput_10)); }
	inline String_t* get_rotateCameraYInput_10() const { return ___rotateCameraYInput_10; }
	inline String_t** get_address_of_rotateCameraYInput_10() { return &___rotateCameraYInput_10; }
	inline void set_rotateCameraYInput_10(String_t* value)
	{
		___rotateCameraYInput_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rotateCameraYInput_10), (void*)value);
	}

	inline static int32_t get_offset_of_cc_11() { return static_cast<int32_t>(offsetof(vThirdPersonInput_tB15CFA93BCDA688B2E2D8F976E90C4262CB82C50, ___cc_11)); }
	inline vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * get_cc_11() const { return ___cc_11; }
	inline vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A ** get_address_of_cc_11() { return &___cc_11; }
	inline void set_cc_11(vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * value)
	{
		___cc_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cc_11), (void*)value);
	}

	inline static int32_t get_offset_of_tpCamera_12() { return static_cast<int32_t>(offsetof(vThirdPersonInput_tB15CFA93BCDA688B2E2D8F976E90C4262CB82C50, ___tpCamera_12)); }
	inline vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11 * get_tpCamera_12() const { return ___tpCamera_12; }
	inline vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11 ** get_address_of_tpCamera_12() { return &___tpCamera_12; }
	inline void set_tpCamera_12(vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11 * value)
	{
		___tpCamera_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tpCamera_12), (void*)value);
	}

	inline static int32_t get_offset_of_cameraMain_13() { return static_cast<int32_t>(offsetof(vThirdPersonInput_tB15CFA93BCDA688B2E2D8F976E90C4262CB82C50, ___cameraMain_13)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get_cameraMain_13() const { return ___cameraMain_13; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of_cameraMain_13() { return &___cameraMain_13; }
	inline void set_cameraMain_13(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		___cameraMain_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cameraMain_13), (void*)value);
	}
};


// Invector.vCharacterController.vThirdPersonMotor
struct vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean Invector.vCharacterController.vThirdPersonMotor::useRootMotion
	bool ___useRootMotion_4;
	// System.Boolean Invector.vCharacterController.vThirdPersonMotor::rotateByWorld
	bool ___rotateByWorld_5;
	// System.Boolean Invector.vCharacterController.vThirdPersonMotor::useContinuousSprint
	bool ___useContinuousSprint_6;
	// System.Boolean Invector.vCharacterController.vThirdPersonMotor::sprintOnlyFree
	bool ___sprintOnlyFree_7;
	// Invector.vCharacterController.vThirdPersonMotor/LocomotionType Invector.vCharacterController.vThirdPersonMotor::locomotionType
	int32_t ___locomotionType_8;
	// Invector.vCharacterController.vThirdPersonMotor/vMovementSpeed Invector.vCharacterController.vThirdPersonMotor::freeSpeed
	vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * ___freeSpeed_9;
	// Invector.vCharacterController.vThirdPersonMotor/vMovementSpeed Invector.vCharacterController.vThirdPersonMotor::strafeSpeed
	vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * ___strafeSpeed_10;
	// System.Boolean Invector.vCharacterController.vThirdPersonMotor::jumpWithRigidbodyForce
	bool ___jumpWithRigidbodyForce_11;
	// System.Boolean Invector.vCharacterController.vThirdPersonMotor::jumpAndRotate
	bool ___jumpAndRotate_12;
	// System.Single Invector.vCharacterController.vThirdPersonMotor::jumpTimer
	float ___jumpTimer_13;
	// System.Single Invector.vCharacterController.vThirdPersonMotor::jumpHeight
	float ___jumpHeight_14;
	// System.Single Invector.vCharacterController.vThirdPersonMotor::airSpeed
	float ___airSpeed_15;
	// System.Single Invector.vCharacterController.vThirdPersonMotor::airSmooth
	float ___airSmooth_16;
	// System.Single Invector.vCharacterController.vThirdPersonMotor::extraGravity
	float ___extraGravity_17;
	// System.Single Invector.vCharacterController.vThirdPersonMotor::limitFallVelocity
	float ___limitFallVelocity_18;
	// UnityEngine.LayerMask Invector.vCharacterController.vThirdPersonMotor::groundLayer
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___groundLayer_19;
	// System.Single Invector.vCharacterController.vThirdPersonMotor::groundMinDistance
	float ___groundMinDistance_20;
	// System.Single Invector.vCharacterController.vThirdPersonMotor::groundMaxDistance
	float ___groundMaxDistance_21;
	// System.Single Invector.vCharacterController.vThirdPersonMotor::slopeLimit
	float ___slopeLimit_22;
	// UnityEngine.Animator Invector.vCharacterController.vThirdPersonMotor::animator
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ___animator_23;
	// UnityEngine.Rigidbody Invector.vCharacterController.vThirdPersonMotor::_rigidbody
	Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * ____rigidbody_24;
	// UnityEngine.PhysicMaterial Invector.vCharacterController.vThirdPersonMotor::frictionPhysics
	PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 * ___frictionPhysics_25;
	// UnityEngine.PhysicMaterial Invector.vCharacterController.vThirdPersonMotor::maxFrictionPhysics
	PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 * ___maxFrictionPhysics_26;
	// UnityEngine.PhysicMaterial Invector.vCharacterController.vThirdPersonMotor::slippyPhysics
	PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 * ___slippyPhysics_27;
	// UnityEngine.CapsuleCollider Invector.vCharacterController.vThirdPersonMotor::_capsuleCollider
	CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * ____capsuleCollider_28;
	// System.Boolean Invector.vCharacterController.vThirdPersonMotor::isJumping
	bool ___isJumping_29;
	// System.Boolean Invector.vCharacterController.vThirdPersonMotor::<isGrounded>k__BackingField
	bool ___U3CisGroundedU3Ek__BackingField_30;
	// System.Boolean Invector.vCharacterController.vThirdPersonMotor::<isSprinting>k__BackingField
	bool ___U3CisSprintingU3Ek__BackingField_31;
	// System.Boolean Invector.vCharacterController.vThirdPersonMotor::<stopMove>k__BackingField
	bool ___U3CstopMoveU3Ek__BackingField_32;
	// System.Single Invector.vCharacterController.vThirdPersonMotor::inputMagnitude
	float ___inputMagnitude_33;
	// System.Single Invector.vCharacterController.vThirdPersonMotor::verticalSpeed
	float ___verticalSpeed_34;
	// System.Single Invector.vCharacterController.vThirdPersonMotor::horizontalSpeed
	float ___horizontalSpeed_35;
	// System.Single Invector.vCharacterController.vThirdPersonMotor::moveSpeed
	float ___moveSpeed_36;
	// System.Single Invector.vCharacterController.vThirdPersonMotor::verticalVelocity
	float ___verticalVelocity_37;
	// System.Single Invector.vCharacterController.vThirdPersonMotor::colliderRadius
	float ___colliderRadius_38;
	// System.Single Invector.vCharacterController.vThirdPersonMotor::colliderHeight
	float ___colliderHeight_39;
	// System.Single Invector.vCharacterController.vThirdPersonMotor::heightReached
	float ___heightReached_40;
	// System.Single Invector.vCharacterController.vThirdPersonMotor::jumpCounter
	float ___jumpCounter_41;
	// System.Single Invector.vCharacterController.vThirdPersonMotor::groundDistance
	float ___groundDistance_42;
	// UnityEngine.RaycastHit Invector.vCharacterController.vThirdPersonMotor::groundHit
	RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89  ___groundHit_43;
	// System.Boolean Invector.vCharacterController.vThirdPersonMotor::lockMovement
	bool ___lockMovement_44;
	// System.Boolean Invector.vCharacterController.vThirdPersonMotor::lockRotation
	bool ___lockRotation_45;
	// System.Boolean Invector.vCharacterController.vThirdPersonMotor::_isStrafing
	bool ____isStrafing_46;
	// UnityEngine.Transform Invector.vCharacterController.vThirdPersonMotor::rotateTarget
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___rotateTarget_47;
	// UnityEngine.Vector3 Invector.vCharacterController.vThirdPersonMotor::input
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___input_48;
	// UnityEngine.Vector3 Invector.vCharacterController.vThirdPersonMotor::colliderCenter
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___colliderCenter_49;
	// UnityEngine.Vector3 Invector.vCharacterController.vThirdPersonMotor::inputSmooth
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___inputSmooth_50;
	// UnityEngine.Vector3 Invector.vCharacterController.vThirdPersonMotor::moveDirection
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___moveDirection_51;

public:
	inline static int32_t get_offset_of_useRootMotion_4() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___useRootMotion_4)); }
	inline bool get_useRootMotion_4() const { return ___useRootMotion_4; }
	inline bool* get_address_of_useRootMotion_4() { return &___useRootMotion_4; }
	inline void set_useRootMotion_4(bool value)
	{
		___useRootMotion_4 = value;
	}

	inline static int32_t get_offset_of_rotateByWorld_5() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___rotateByWorld_5)); }
	inline bool get_rotateByWorld_5() const { return ___rotateByWorld_5; }
	inline bool* get_address_of_rotateByWorld_5() { return &___rotateByWorld_5; }
	inline void set_rotateByWorld_5(bool value)
	{
		___rotateByWorld_5 = value;
	}

	inline static int32_t get_offset_of_useContinuousSprint_6() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___useContinuousSprint_6)); }
	inline bool get_useContinuousSprint_6() const { return ___useContinuousSprint_6; }
	inline bool* get_address_of_useContinuousSprint_6() { return &___useContinuousSprint_6; }
	inline void set_useContinuousSprint_6(bool value)
	{
		___useContinuousSprint_6 = value;
	}

	inline static int32_t get_offset_of_sprintOnlyFree_7() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___sprintOnlyFree_7)); }
	inline bool get_sprintOnlyFree_7() const { return ___sprintOnlyFree_7; }
	inline bool* get_address_of_sprintOnlyFree_7() { return &___sprintOnlyFree_7; }
	inline void set_sprintOnlyFree_7(bool value)
	{
		___sprintOnlyFree_7 = value;
	}

	inline static int32_t get_offset_of_locomotionType_8() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___locomotionType_8)); }
	inline int32_t get_locomotionType_8() const { return ___locomotionType_8; }
	inline int32_t* get_address_of_locomotionType_8() { return &___locomotionType_8; }
	inline void set_locomotionType_8(int32_t value)
	{
		___locomotionType_8 = value;
	}

	inline static int32_t get_offset_of_freeSpeed_9() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___freeSpeed_9)); }
	inline vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * get_freeSpeed_9() const { return ___freeSpeed_9; }
	inline vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 ** get_address_of_freeSpeed_9() { return &___freeSpeed_9; }
	inline void set_freeSpeed_9(vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * value)
	{
		___freeSpeed_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___freeSpeed_9), (void*)value);
	}

	inline static int32_t get_offset_of_strafeSpeed_10() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___strafeSpeed_10)); }
	inline vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * get_strafeSpeed_10() const { return ___strafeSpeed_10; }
	inline vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 ** get_address_of_strafeSpeed_10() { return &___strafeSpeed_10; }
	inline void set_strafeSpeed_10(vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * value)
	{
		___strafeSpeed_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___strafeSpeed_10), (void*)value);
	}

	inline static int32_t get_offset_of_jumpWithRigidbodyForce_11() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___jumpWithRigidbodyForce_11)); }
	inline bool get_jumpWithRigidbodyForce_11() const { return ___jumpWithRigidbodyForce_11; }
	inline bool* get_address_of_jumpWithRigidbodyForce_11() { return &___jumpWithRigidbodyForce_11; }
	inline void set_jumpWithRigidbodyForce_11(bool value)
	{
		___jumpWithRigidbodyForce_11 = value;
	}

	inline static int32_t get_offset_of_jumpAndRotate_12() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___jumpAndRotate_12)); }
	inline bool get_jumpAndRotate_12() const { return ___jumpAndRotate_12; }
	inline bool* get_address_of_jumpAndRotate_12() { return &___jumpAndRotate_12; }
	inline void set_jumpAndRotate_12(bool value)
	{
		___jumpAndRotate_12 = value;
	}

	inline static int32_t get_offset_of_jumpTimer_13() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___jumpTimer_13)); }
	inline float get_jumpTimer_13() const { return ___jumpTimer_13; }
	inline float* get_address_of_jumpTimer_13() { return &___jumpTimer_13; }
	inline void set_jumpTimer_13(float value)
	{
		___jumpTimer_13 = value;
	}

	inline static int32_t get_offset_of_jumpHeight_14() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___jumpHeight_14)); }
	inline float get_jumpHeight_14() const { return ___jumpHeight_14; }
	inline float* get_address_of_jumpHeight_14() { return &___jumpHeight_14; }
	inline void set_jumpHeight_14(float value)
	{
		___jumpHeight_14 = value;
	}

	inline static int32_t get_offset_of_airSpeed_15() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___airSpeed_15)); }
	inline float get_airSpeed_15() const { return ___airSpeed_15; }
	inline float* get_address_of_airSpeed_15() { return &___airSpeed_15; }
	inline void set_airSpeed_15(float value)
	{
		___airSpeed_15 = value;
	}

	inline static int32_t get_offset_of_airSmooth_16() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___airSmooth_16)); }
	inline float get_airSmooth_16() const { return ___airSmooth_16; }
	inline float* get_address_of_airSmooth_16() { return &___airSmooth_16; }
	inline void set_airSmooth_16(float value)
	{
		___airSmooth_16 = value;
	}

	inline static int32_t get_offset_of_extraGravity_17() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___extraGravity_17)); }
	inline float get_extraGravity_17() const { return ___extraGravity_17; }
	inline float* get_address_of_extraGravity_17() { return &___extraGravity_17; }
	inline void set_extraGravity_17(float value)
	{
		___extraGravity_17 = value;
	}

	inline static int32_t get_offset_of_limitFallVelocity_18() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___limitFallVelocity_18)); }
	inline float get_limitFallVelocity_18() const { return ___limitFallVelocity_18; }
	inline float* get_address_of_limitFallVelocity_18() { return &___limitFallVelocity_18; }
	inline void set_limitFallVelocity_18(float value)
	{
		___limitFallVelocity_18 = value;
	}

	inline static int32_t get_offset_of_groundLayer_19() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___groundLayer_19)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get_groundLayer_19() const { return ___groundLayer_19; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of_groundLayer_19() { return &___groundLayer_19; }
	inline void set_groundLayer_19(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		___groundLayer_19 = value;
	}

	inline static int32_t get_offset_of_groundMinDistance_20() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___groundMinDistance_20)); }
	inline float get_groundMinDistance_20() const { return ___groundMinDistance_20; }
	inline float* get_address_of_groundMinDistance_20() { return &___groundMinDistance_20; }
	inline void set_groundMinDistance_20(float value)
	{
		___groundMinDistance_20 = value;
	}

	inline static int32_t get_offset_of_groundMaxDistance_21() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___groundMaxDistance_21)); }
	inline float get_groundMaxDistance_21() const { return ___groundMaxDistance_21; }
	inline float* get_address_of_groundMaxDistance_21() { return &___groundMaxDistance_21; }
	inline void set_groundMaxDistance_21(float value)
	{
		___groundMaxDistance_21 = value;
	}

	inline static int32_t get_offset_of_slopeLimit_22() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___slopeLimit_22)); }
	inline float get_slopeLimit_22() const { return ___slopeLimit_22; }
	inline float* get_address_of_slopeLimit_22() { return &___slopeLimit_22; }
	inline void set_slopeLimit_22(float value)
	{
		___slopeLimit_22 = value;
	}

	inline static int32_t get_offset_of_animator_23() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___animator_23)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get_animator_23() const { return ___animator_23; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of_animator_23() { return &___animator_23; }
	inline void set_animator_23(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		___animator_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___animator_23), (void*)value);
	}

	inline static int32_t get_offset_of__rigidbody_24() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ____rigidbody_24)); }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * get__rigidbody_24() const { return ____rigidbody_24; }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A ** get_address_of__rigidbody_24() { return &____rigidbody_24; }
	inline void set__rigidbody_24(Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * value)
	{
		____rigidbody_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rigidbody_24), (void*)value);
	}

	inline static int32_t get_offset_of_frictionPhysics_25() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___frictionPhysics_25)); }
	inline PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 * get_frictionPhysics_25() const { return ___frictionPhysics_25; }
	inline PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 ** get_address_of_frictionPhysics_25() { return &___frictionPhysics_25; }
	inline void set_frictionPhysics_25(PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 * value)
	{
		___frictionPhysics_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___frictionPhysics_25), (void*)value);
	}

	inline static int32_t get_offset_of_maxFrictionPhysics_26() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___maxFrictionPhysics_26)); }
	inline PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 * get_maxFrictionPhysics_26() const { return ___maxFrictionPhysics_26; }
	inline PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 ** get_address_of_maxFrictionPhysics_26() { return &___maxFrictionPhysics_26; }
	inline void set_maxFrictionPhysics_26(PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 * value)
	{
		___maxFrictionPhysics_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___maxFrictionPhysics_26), (void*)value);
	}

	inline static int32_t get_offset_of_slippyPhysics_27() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___slippyPhysics_27)); }
	inline PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 * get_slippyPhysics_27() const { return ___slippyPhysics_27; }
	inline PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 ** get_address_of_slippyPhysics_27() { return &___slippyPhysics_27; }
	inline void set_slippyPhysics_27(PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 * value)
	{
		___slippyPhysics_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___slippyPhysics_27), (void*)value);
	}

	inline static int32_t get_offset_of__capsuleCollider_28() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ____capsuleCollider_28)); }
	inline CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * get__capsuleCollider_28() const { return ____capsuleCollider_28; }
	inline CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 ** get_address_of__capsuleCollider_28() { return &____capsuleCollider_28; }
	inline void set__capsuleCollider_28(CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * value)
	{
		____capsuleCollider_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____capsuleCollider_28), (void*)value);
	}

	inline static int32_t get_offset_of_isJumping_29() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___isJumping_29)); }
	inline bool get_isJumping_29() const { return ___isJumping_29; }
	inline bool* get_address_of_isJumping_29() { return &___isJumping_29; }
	inline void set_isJumping_29(bool value)
	{
		___isJumping_29 = value;
	}

	inline static int32_t get_offset_of_U3CisGroundedU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___U3CisGroundedU3Ek__BackingField_30)); }
	inline bool get_U3CisGroundedU3Ek__BackingField_30() const { return ___U3CisGroundedU3Ek__BackingField_30; }
	inline bool* get_address_of_U3CisGroundedU3Ek__BackingField_30() { return &___U3CisGroundedU3Ek__BackingField_30; }
	inline void set_U3CisGroundedU3Ek__BackingField_30(bool value)
	{
		___U3CisGroundedU3Ek__BackingField_30 = value;
	}

	inline static int32_t get_offset_of_U3CisSprintingU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___U3CisSprintingU3Ek__BackingField_31)); }
	inline bool get_U3CisSprintingU3Ek__BackingField_31() const { return ___U3CisSprintingU3Ek__BackingField_31; }
	inline bool* get_address_of_U3CisSprintingU3Ek__BackingField_31() { return &___U3CisSprintingU3Ek__BackingField_31; }
	inline void set_U3CisSprintingU3Ek__BackingField_31(bool value)
	{
		___U3CisSprintingU3Ek__BackingField_31 = value;
	}

	inline static int32_t get_offset_of_U3CstopMoveU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___U3CstopMoveU3Ek__BackingField_32)); }
	inline bool get_U3CstopMoveU3Ek__BackingField_32() const { return ___U3CstopMoveU3Ek__BackingField_32; }
	inline bool* get_address_of_U3CstopMoveU3Ek__BackingField_32() { return &___U3CstopMoveU3Ek__BackingField_32; }
	inline void set_U3CstopMoveU3Ek__BackingField_32(bool value)
	{
		___U3CstopMoveU3Ek__BackingField_32 = value;
	}

	inline static int32_t get_offset_of_inputMagnitude_33() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___inputMagnitude_33)); }
	inline float get_inputMagnitude_33() const { return ___inputMagnitude_33; }
	inline float* get_address_of_inputMagnitude_33() { return &___inputMagnitude_33; }
	inline void set_inputMagnitude_33(float value)
	{
		___inputMagnitude_33 = value;
	}

	inline static int32_t get_offset_of_verticalSpeed_34() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___verticalSpeed_34)); }
	inline float get_verticalSpeed_34() const { return ___verticalSpeed_34; }
	inline float* get_address_of_verticalSpeed_34() { return &___verticalSpeed_34; }
	inline void set_verticalSpeed_34(float value)
	{
		___verticalSpeed_34 = value;
	}

	inline static int32_t get_offset_of_horizontalSpeed_35() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___horizontalSpeed_35)); }
	inline float get_horizontalSpeed_35() const { return ___horizontalSpeed_35; }
	inline float* get_address_of_horizontalSpeed_35() { return &___horizontalSpeed_35; }
	inline void set_horizontalSpeed_35(float value)
	{
		___horizontalSpeed_35 = value;
	}

	inline static int32_t get_offset_of_moveSpeed_36() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___moveSpeed_36)); }
	inline float get_moveSpeed_36() const { return ___moveSpeed_36; }
	inline float* get_address_of_moveSpeed_36() { return &___moveSpeed_36; }
	inline void set_moveSpeed_36(float value)
	{
		___moveSpeed_36 = value;
	}

	inline static int32_t get_offset_of_verticalVelocity_37() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___verticalVelocity_37)); }
	inline float get_verticalVelocity_37() const { return ___verticalVelocity_37; }
	inline float* get_address_of_verticalVelocity_37() { return &___verticalVelocity_37; }
	inline void set_verticalVelocity_37(float value)
	{
		___verticalVelocity_37 = value;
	}

	inline static int32_t get_offset_of_colliderRadius_38() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___colliderRadius_38)); }
	inline float get_colliderRadius_38() const { return ___colliderRadius_38; }
	inline float* get_address_of_colliderRadius_38() { return &___colliderRadius_38; }
	inline void set_colliderRadius_38(float value)
	{
		___colliderRadius_38 = value;
	}

	inline static int32_t get_offset_of_colliderHeight_39() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___colliderHeight_39)); }
	inline float get_colliderHeight_39() const { return ___colliderHeight_39; }
	inline float* get_address_of_colliderHeight_39() { return &___colliderHeight_39; }
	inline void set_colliderHeight_39(float value)
	{
		___colliderHeight_39 = value;
	}

	inline static int32_t get_offset_of_heightReached_40() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___heightReached_40)); }
	inline float get_heightReached_40() const { return ___heightReached_40; }
	inline float* get_address_of_heightReached_40() { return &___heightReached_40; }
	inline void set_heightReached_40(float value)
	{
		___heightReached_40 = value;
	}

	inline static int32_t get_offset_of_jumpCounter_41() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___jumpCounter_41)); }
	inline float get_jumpCounter_41() const { return ___jumpCounter_41; }
	inline float* get_address_of_jumpCounter_41() { return &___jumpCounter_41; }
	inline void set_jumpCounter_41(float value)
	{
		___jumpCounter_41 = value;
	}

	inline static int32_t get_offset_of_groundDistance_42() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___groundDistance_42)); }
	inline float get_groundDistance_42() const { return ___groundDistance_42; }
	inline float* get_address_of_groundDistance_42() { return &___groundDistance_42; }
	inline void set_groundDistance_42(float value)
	{
		___groundDistance_42 = value;
	}

	inline static int32_t get_offset_of_groundHit_43() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___groundHit_43)); }
	inline RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89  get_groundHit_43() const { return ___groundHit_43; }
	inline RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * get_address_of_groundHit_43() { return &___groundHit_43; }
	inline void set_groundHit_43(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89  value)
	{
		___groundHit_43 = value;
	}

	inline static int32_t get_offset_of_lockMovement_44() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___lockMovement_44)); }
	inline bool get_lockMovement_44() const { return ___lockMovement_44; }
	inline bool* get_address_of_lockMovement_44() { return &___lockMovement_44; }
	inline void set_lockMovement_44(bool value)
	{
		___lockMovement_44 = value;
	}

	inline static int32_t get_offset_of_lockRotation_45() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___lockRotation_45)); }
	inline bool get_lockRotation_45() const { return ___lockRotation_45; }
	inline bool* get_address_of_lockRotation_45() { return &___lockRotation_45; }
	inline void set_lockRotation_45(bool value)
	{
		___lockRotation_45 = value;
	}

	inline static int32_t get_offset_of__isStrafing_46() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ____isStrafing_46)); }
	inline bool get__isStrafing_46() const { return ____isStrafing_46; }
	inline bool* get_address_of__isStrafing_46() { return &____isStrafing_46; }
	inline void set__isStrafing_46(bool value)
	{
		____isStrafing_46 = value;
	}

	inline static int32_t get_offset_of_rotateTarget_47() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___rotateTarget_47)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_rotateTarget_47() const { return ___rotateTarget_47; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_rotateTarget_47() { return &___rotateTarget_47; }
	inline void set_rotateTarget_47(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___rotateTarget_47 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rotateTarget_47), (void*)value);
	}

	inline static int32_t get_offset_of_input_48() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___input_48)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_input_48() const { return ___input_48; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_input_48() { return &___input_48; }
	inline void set_input_48(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___input_48 = value;
	}

	inline static int32_t get_offset_of_colliderCenter_49() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___colliderCenter_49)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_colliderCenter_49() const { return ___colliderCenter_49; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_colliderCenter_49() { return &___colliderCenter_49; }
	inline void set_colliderCenter_49(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___colliderCenter_49 = value;
	}

	inline static int32_t get_offset_of_inputSmooth_50() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___inputSmooth_50)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_inputSmooth_50() const { return ___inputSmooth_50; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_inputSmooth_50() { return &___inputSmooth_50; }
	inline void set_inputSmooth_50(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___inputSmooth_50 = value;
	}

	inline static int32_t get_offset_of_moveDirection_51() { return static_cast<int32_t>(offsetof(vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3, ___moveDirection_51)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_moveDirection_51() const { return ___moveDirection_51; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_moveDirection_51() { return &___moveDirection_51; }
	inline void set_moveDirection_51(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___moveDirection_51 = value;
	}
};


// UnityEngine.UI.Graphic
struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.Vector4 UnityEngine.UI.Graphic::m_RaycastPadding
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___m_RaycastPadding_11;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_RectTransform_12;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * ___m_CanvasRenderer_13;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___m_Canvas_14;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_15;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyLayoutCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyVertsCallback_18;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyMaterialCallback_19;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___m_CachedMesh_22;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___m_CachedUvs_23;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * ___m_ColorTweenRunner_24;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Material_6)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Material_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Color_7)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_SkipLayoutUpdate_8() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipLayoutUpdate_8)); }
	inline bool get_m_SkipLayoutUpdate_8() const { return ___m_SkipLayoutUpdate_8; }
	inline bool* get_address_of_m_SkipLayoutUpdate_8() { return &___m_SkipLayoutUpdate_8; }
	inline void set_m_SkipLayoutUpdate_8(bool value)
	{
		___m_SkipLayoutUpdate_8 = value;
	}

	inline static int32_t get_offset_of_m_SkipMaterialUpdate_9() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipMaterialUpdate_9)); }
	inline bool get_m_SkipMaterialUpdate_9() const { return ___m_SkipMaterialUpdate_9; }
	inline bool* get_address_of_m_SkipMaterialUpdate_9() { return &___m_SkipMaterialUpdate_9; }
	inline void set_m_SkipMaterialUpdate_9(bool value)
	{
		___m_SkipMaterialUpdate_9 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_10() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastTarget_10)); }
	inline bool get_m_RaycastTarget_10() const { return ___m_RaycastTarget_10; }
	inline bool* get_address_of_m_RaycastTarget_10() { return &___m_RaycastTarget_10; }
	inline void set_m_RaycastTarget_10(bool value)
	{
		___m_RaycastTarget_10 = value;
	}

	inline static int32_t get_offset_of_m_RaycastPadding_11() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastPadding_11)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_m_RaycastPadding_11() const { return ___m_RaycastPadding_11; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_m_RaycastPadding_11() { return &___m_RaycastPadding_11; }
	inline void set_m_RaycastPadding_11(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___m_RaycastPadding_11 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_12() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RectTransform_12)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_RectTransform_12() const { return ___m_RectTransform_12; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_RectTransform_12() { return &___m_RectTransform_12; }
	inline void set_m_RectTransform_12(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_RectTransform_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RectTransform_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_13() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CanvasRenderer_13)); }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * get_m_CanvasRenderer_13() const { return ___m_CanvasRenderer_13; }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E ** get_address_of_m_CanvasRenderer_13() { return &___m_CanvasRenderer_13; }
	inline void set_m_CanvasRenderer_13(CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * value)
	{
		___m_CanvasRenderer_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasRenderer_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_Canvas_14() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Canvas_14)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_m_Canvas_14() const { return ___m_Canvas_14; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_m_Canvas_14() { return &___m_Canvas_14; }
	inline void set_m_Canvas_14(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___m_Canvas_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_15() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_VertsDirty_15)); }
	inline bool get_m_VertsDirty_15() const { return ___m_VertsDirty_15; }
	inline bool* get_address_of_m_VertsDirty_15() { return &___m_VertsDirty_15; }
	inline void set_m_VertsDirty_15(bool value)
	{
		___m_VertsDirty_15 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_16() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_MaterialDirty_16)); }
	inline bool get_m_MaterialDirty_16() const { return ___m_MaterialDirty_16; }
	inline bool* get_address_of_m_MaterialDirty_16() { return &___m_MaterialDirty_16; }
	inline void set_m_MaterialDirty_16(bool value)
	{
		___m_MaterialDirty_16 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_17() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyLayoutCallback_17)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyLayoutCallback_17() const { return ___m_OnDirtyLayoutCallback_17; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyLayoutCallback_17() { return &___m_OnDirtyLayoutCallback_17; }
	inline void set_m_OnDirtyLayoutCallback_17(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyLayoutCallback_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyLayoutCallback_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_18() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyVertsCallback_18)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyVertsCallback_18() const { return ___m_OnDirtyVertsCallback_18; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyVertsCallback_18() { return &___m_OnDirtyVertsCallback_18; }
	inline void set_m_OnDirtyVertsCallback_18(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyVertsCallback_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyVertsCallback_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_19() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyMaterialCallback_19)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyMaterialCallback_19() const { return ___m_OnDirtyMaterialCallback_19; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyMaterialCallback_19() { return &___m_OnDirtyMaterialCallback_19; }
	inline void set_m_OnDirtyMaterialCallback_19(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyMaterialCallback_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyMaterialCallback_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedMesh_22() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedMesh_22)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_m_CachedMesh_22() const { return ___m_CachedMesh_22; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_m_CachedMesh_22() { return &___m_CachedMesh_22; }
	inline void set_m_CachedMesh_22(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___m_CachedMesh_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedMesh_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedUvs_23() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedUvs_23)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_m_CachedUvs_23() const { return ___m_CachedUvs_23; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_m_CachedUvs_23() { return &___m_CachedUvs_23; }
	inline void set_m_CachedUvs_23(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___m_CachedUvs_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedUvs_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_24() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_ColorTweenRunner_24)); }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * get_m_ColorTweenRunner_24() const { return ___m_ColorTweenRunner_24; }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 ** get_address_of_m_ColorTweenRunner_24() { return &___m_ColorTweenRunner_24; }
	inline void set_m_ColorTweenRunner_24(TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * value)
	{
		___m_ColorTweenRunner_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ColorTweenRunner_24), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_25(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_25 = value;
	}
};

struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___s_Mesh_20;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___s_VertexHelper_21;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultUI_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_WhiteTexture_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_Mesh_20() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_Mesh_20)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_s_Mesh_20() const { return ___s_Mesh_20; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_s_Mesh_20() { return &___s_Mesh_20; }
	inline void set_s_Mesh_20(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___s_Mesh_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Mesh_20), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_21() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_VertexHelper_21)); }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * get_s_VertexHelper_21() const { return ___s_VertexHelper_21; }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 ** get_address_of_s_VertexHelper_21() { return &___s_VertexHelper_21; }
	inline void set_s_VertexHelper_21(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * value)
	{
		___s_VertexHelper_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertexHelper_21), (void*)value);
	}
};


// UnityEngine.UI.Selectable
struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// System.Boolean UnityEngine.UI.Selectable::m_EnableCalled
	bool ___m_EnableCalled_6;
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  ___m_Navigation_7;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_8;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  ___m_Colors_9;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  ___m_SpriteState_10;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * ___m_AnimationTriggers_11;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_12;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___m_TargetGraphic_13;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_14;
	// System.Int32 UnityEngine.UI.Selectable::m_CurrentIndex
	int32_t ___m_CurrentIndex_15;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_16;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_17;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_18;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * ___m_CanvasGroupCache_19;

public:
	inline static int32_t get_offset_of_m_EnableCalled_6() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_EnableCalled_6)); }
	inline bool get_m_EnableCalled_6() const { return ___m_EnableCalled_6; }
	inline bool* get_address_of_m_EnableCalled_6() { return &___m_EnableCalled_6; }
	inline void set_m_EnableCalled_6(bool value)
	{
		___m_EnableCalled_6 = value;
	}

	inline static int32_t get_offset_of_m_Navigation_7() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Navigation_7)); }
	inline Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  get_m_Navigation_7() const { return ___m_Navigation_7; }
	inline Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A * get_address_of_m_Navigation_7() { return &___m_Navigation_7; }
	inline void set_m_Navigation_7(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  value)
	{
		___m_Navigation_7 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnUp_2), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnDown_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnLeft_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnRight_5), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Transition_8() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Transition_8)); }
	inline int32_t get_m_Transition_8() const { return ___m_Transition_8; }
	inline int32_t* get_address_of_m_Transition_8() { return &___m_Transition_8; }
	inline void set_m_Transition_8(int32_t value)
	{
		___m_Transition_8 = value;
	}

	inline static int32_t get_offset_of_m_Colors_9() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Colors_9)); }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  get_m_Colors_9() const { return ___m_Colors_9; }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 * get_address_of_m_Colors_9() { return &___m_Colors_9; }
	inline void set_m_Colors_9(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  value)
	{
		___m_Colors_9 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_10() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_SpriteState_10)); }
	inline SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  get_m_SpriteState_10() const { return ___m_SpriteState_10; }
	inline SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E * get_address_of_m_SpriteState_10() { return &___m_SpriteState_10; }
	inline void set_m_SpriteState_10(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  value)
	{
		___m_SpriteState_10 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_HighlightedSprite_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_PressedSprite_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_SelectedSprite_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_DisabledSprite_3), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_11() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_AnimationTriggers_11)); }
	inline AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * get_m_AnimationTriggers_11() const { return ___m_AnimationTriggers_11; }
	inline AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 ** get_address_of_m_AnimationTriggers_11() { return &___m_AnimationTriggers_11; }
	inline void set_m_AnimationTriggers_11(AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * value)
	{
		___m_AnimationTriggers_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AnimationTriggers_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_Interactable_12() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Interactable_12)); }
	inline bool get_m_Interactable_12() const { return ___m_Interactable_12; }
	inline bool* get_address_of_m_Interactable_12() { return &___m_Interactable_12; }
	inline void set_m_Interactable_12(bool value)
	{
		___m_Interactable_12 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_13() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_TargetGraphic_13)); }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * get_m_TargetGraphic_13() const { return ___m_TargetGraphic_13; }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 ** get_address_of_m_TargetGraphic_13() { return &___m_TargetGraphic_13; }
	inline void set_m_TargetGraphic_13(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * value)
	{
		___m_TargetGraphic_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TargetGraphic_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_14() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_GroupsAllowInteraction_14)); }
	inline bool get_m_GroupsAllowInteraction_14() const { return ___m_GroupsAllowInteraction_14; }
	inline bool* get_address_of_m_GroupsAllowInteraction_14() { return &___m_GroupsAllowInteraction_14; }
	inline void set_m_GroupsAllowInteraction_14(bool value)
	{
		___m_GroupsAllowInteraction_14 = value;
	}

	inline static int32_t get_offset_of_m_CurrentIndex_15() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_CurrentIndex_15)); }
	inline int32_t get_m_CurrentIndex_15() const { return ___m_CurrentIndex_15; }
	inline int32_t* get_address_of_m_CurrentIndex_15() { return &___m_CurrentIndex_15; }
	inline void set_m_CurrentIndex_15(int32_t value)
	{
		___m_CurrentIndex_15 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3CisPointerInsideU3Ek__BackingField_16)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_16() const { return ___U3CisPointerInsideU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_16() { return &___U3CisPointerInsideU3Ek__BackingField_16; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_16(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3CisPointerDownU3Ek__BackingField_17)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_17() const { return ___U3CisPointerDownU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_17() { return &___U3CisPointerDownU3Ek__BackingField_17; }
	inline void set_U3CisPointerDownU3Ek__BackingField_17(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3ChasSelectionU3Ek__BackingField_18)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_18() const { return ___U3ChasSelectionU3Ek__BackingField_18; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_18() { return &___U3ChasSelectionU3Ek__BackingField_18; }
	inline void set_U3ChasSelectionU3Ek__BackingField_18(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_19() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_CanvasGroupCache_19)); }
	inline List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * get_m_CanvasGroupCache_19() const { return ___m_CanvasGroupCache_19; }
	inline List_1_t34AA4AF4E7352129CA58045901530E41445AC16D ** get_address_of_m_CanvasGroupCache_19() { return &___m_CanvasGroupCache_19; }
	inline void set_m_CanvasGroupCache_19(List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * value)
	{
		___m_CanvasGroupCache_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasGroupCache_19), (void*)value);
	}
};

struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields
{
public:
	// UnityEngine.UI.Selectable[] UnityEngine.UI.Selectable::s_Selectables
	SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* ___s_Selectables_4;
	// System.Int32 UnityEngine.UI.Selectable::s_SelectableCount
	int32_t ___s_SelectableCount_5;

public:
	inline static int32_t get_offset_of_s_Selectables_4() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields, ___s_Selectables_4)); }
	inline SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* get_s_Selectables_4() const { return ___s_Selectables_4; }
	inline SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535** get_address_of_s_Selectables_4() { return &___s_Selectables_4; }
	inline void set_s_Selectables_4(SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* value)
	{
		___s_Selectables_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Selectables_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_SelectableCount_5() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields, ___s_SelectableCount_5)); }
	inline int32_t get_s_SelectableCount_5() const { return ___s_SelectableCount_5; }
	inline int32_t* get_address_of_s_SelectableCount_5() { return &___s_SelectableCount_5; }
	inline void set_s_SelectableCount_5(int32_t value)
	{
		___s_SelectableCount_5 = value;
	}
};


// Invector.vCharacterController.vThirdPersonAnimator
struct vThirdPersonAnimator_tDFBC4BC178521ACC390B82F0B5418961D2754CB0  : public vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3
{
public:

public:
};


// UnityEngine.UI.Button
struct Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D  : public Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD
{
public:
	// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::m_OnClick
	ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * ___m_OnClick_20;

public:
	inline static int32_t get_offset_of_m_OnClick_20() { return static_cast<int32_t>(offsetof(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D, ___m_OnClick_20)); }
	inline ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * get_m_OnClick_20() const { return ___m_OnClick_20; }
	inline ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F ** get_address_of_m_OnClick_20() { return &___m_OnClick_20; }
	inline void set_m_OnClick_20(ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * value)
	{
		___m_OnClick_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnClick_20), (void*)value);
	}
};


// UnityEngine.UI.MaskableGraphic
struct MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE  : public Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_26;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_MaskMaterial_27;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * ___m_ParentMask_28;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_29;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IsMaskingGraphic
	bool ___m_IsMaskingGraphic_30;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_31;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * ___m_OnCullStateChanged_32;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_33;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_34;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___m_Corners_35;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculateStencil_26)); }
	inline bool get_m_ShouldRecalculateStencil_26() const { return ___m_ShouldRecalculateStencil_26; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_26() { return &___m_ShouldRecalculateStencil_26; }
	inline void set_m_ShouldRecalculateStencil_26(bool value)
	{
		___m_ShouldRecalculateStencil_26 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_MaskMaterial_27)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_MaskMaterial_27() const { return ___m_MaskMaterial_27; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_MaskMaterial_27() { return &___m_MaskMaterial_27; }
	inline void set_m_MaskMaterial_27(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_MaskMaterial_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MaskMaterial_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_ParentMask_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ParentMask_28)); }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * get_m_ParentMask_28() const { return ___m_ParentMask_28; }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 ** get_address_of_m_ParentMask_28() { return &___m_ParentMask_28; }
	inline void set_m_ParentMask_28(RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * value)
	{
		___m_ParentMask_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParentMask_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_Maskable_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Maskable_29)); }
	inline bool get_m_Maskable_29() const { return ___m_Maskable_29; }
	inline bool* get_address_of_m_Maskable_29() { return &___m_Maskable_29; }
	inline void set_m_Maskable_29(bool value)
	{
		___m_Maskable_29 = value;
	}

	inline static int32_t get_offset_of_m_IsMaskingGraphic_30() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IsMaskingGraphic_30)); }
	inline bool get_m_IsMaskingGraphic_30() const { return ___m_IsMaskingGraphic_30; }
	inline bool* get_address_of_m_IsMaskingGraphic_30() { return &___m_IsMaskingGraphic_30; }
	inline void set_m_IsMaskingGraphic_30(bool value)
	{
		___m_IsMaskingGraphic_30 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_31() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IncludeForMasking_31)); }
	inline bool get_m_IncludeForMasking_31() const { return ___m_IncludeForMasking_31; }
	inline bool* get_address_of_m_IncludeForMasking_31() { return &___m_IncludeForMasking_31; }
	inline void set_m_IncludeForMasking_31(bool value)
	{
		___m_IncludeForMasking_31 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_32() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_OnCullStateChanged_32)); }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * get_m_OnCullStateChanged_32() const { return ___m_OnCullStateChanged_32; }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 ** get_address_of_m_OnCullStateChanged_32() { return &___m_OnCullStateChanged_32; }
	inline void set_m_OnCullStateChanged_32(CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * value)
	{
		___m_OnCullStateChanged_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnCullStateChanged_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_33() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculate_33)); }
	inline bool get_m_ShouldRecalculate_33() const { return ___m_ShouldRecalculate_33; }
	inline bool* get_address_of_m_ShouldRecalculate_33() { return &___m_ShouldRecalculate_33; }
	inline void set_m_ShouldRecalculate_33(bool value)
	{
		___m_ShouldRecalculate_33 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_34() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_StencilValue_34)); }
	inline int32_t get_m_StencilValue_34() const { return ___m_StencilValue_34; }
	inline int32_t* get_address_of_m_StencilValue_34() { return &___m_StencilValue_34; }
	inline void set_m_StencilValue_34(int32_t value)
	{
		___m_StencilValue_34 = value;
	}

	inline static int32_t get_offset_of_m_Corners_35() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Corners_35)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_m_Corners_35() const { return ___m_Corners_35; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_m_Corners_35() { return &___m_Corners_35; }
	inline void set_m_Corners_35(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___m_Corners_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Corners_35), (void*)value);
	}
};


// Invector.vCharacterController.vThirdPersonController
struct vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A  : public vThirdPersonAnimator_tDFBC4BC178521ACC390B82F0B5418961D2754CB0
{
public:

public:
};


// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * ___m_FontData_36;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_37;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * ___m_TextCache_38;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * ___m_TextCacheForLayout_39;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_41;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* ___m_TempVerts_42;

public:
	inline static int32_t get_offset_of_m_FontData_36() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_FontData_36)); }
	inline FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * get_m_FontData_36() const { return ___m_FontData_36; }
	inline FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 ** get_address_of_m_FontData_36() { return &___m_FontData_36; }
	inline void set_m_FontData_36(FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * value)
	{
		___m_FontData_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FontData_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_Text_37() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_Text_37)); }
	inline String_t* get_m_Text_37() const { return ___m_Text_37; }
	inline String_t** get_address_of_m_Text_37() { return &___m_Text_37; }
	inline void set_m_Text_37(String_t* value)
	{
		___m_Text_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Text_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCache_38() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TextCache_38)); }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * get_m_TextCache_38() const { return ___m_TextCache_38; }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 ** get_address_of_m_TextCache_38() { return &___m_TextCache_38; }
	inline void set_m_TextCache_38(TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * value)
	{
		___m_TextCache_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCache_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_39() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TextCacheForLayout_39)); }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * get_m_TextCacheForLayout_39() const { return ___m_TextCacheForLayout_39; }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 ** get_address_of_m_TextCacheForLayout_39() { return &___m_TextCacheForLayout_39; }
	inline void set_m_TextCacheForLayout_39(TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * value)
	{
		___m_TextCacheForLayout_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCacheForLayout_39), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_41() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_DisableFontTextureRebuiltCallback_41)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_41() const { return ___m_DisableFontTextureRebuiltCallback_41; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_41() { return &___m_DisableFontTextureRebuiltCallback_41; }
	inline void set_m_DisableFontTextureRebuiltCallback_41(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_41 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_42() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TempVerts_42)); }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* get_m_TempVerts_42() const { return ___m_TempVerts_42; }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A** get_address_of_m_TempVerts_42() { return &___m_TempVerts_42; }
	inline void set_m_TempVerts_42(UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* value)
	{
		___m_TempVerts_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TempVerts_42), (void*)value);
	}
};

struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultText_40;

public:
	inline static int32_t get_offset_of_s_DefaultText_40() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_StaticFields, ___s_DefaultText_40)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultText_40() const { return ___s_DefaultText_40; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultText_40() { return &___s_DefaultText_40; }
	inline void set_s_DefaultText_40(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultText_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultText_40), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.Transform[]
struct TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * m_Items[1];

public:
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * m_Items[1];

public:
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Renderer[]
struct RendererU5BU5D_tE2D3C4350893C593CA40DE876B9F2F0EBBEC49B7  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * m_Items[1];

public:
	inline Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t6778DB95346906446AAD3A1A36904F1846435A09  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89  m_Items[1];

public:
	inline RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89  value)
	{
		m_Items[index] = value;
	}
};


// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Object_Instantiate_TisRuntimeObject_mB05DEC51C29EF5BB8BD17D055E80217F11E571AA_gshared (RuntimeObject * ___original0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position1, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rotation2, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* Component_GetComponentsInChildren_TisRuntimeObject_mCA5B356D4B0824C6DE60A8E90E6A6D4188C56C2F_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Object_FindObjectOfType_TisRuntimeObject_m25AA6DB6AABFD5D66AFA1A8C0E91A7AF61429C37_gshared (const RuntimeMethod* method);

// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___obj0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290 (const RuntimeMethod* method);
// System.Void UnityEngine.Transform::Translate(UnityEngine.Vector3,UnityEngine.Space)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_Translate_mFB58CBF3FA00BD0EE09EC67457608F62564D0DDE (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___translation0, int32_t ___relativeTo1, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::Distance(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector3_Distance_mB648A79E4A1BAAFBF7B029644638C0D715480677 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// System.Void EnemigoIA::SiguientePuntoRuta()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemigoIA_SiguientePuntoRuta_m9273E16CE010336A1C31BB6C149A867D26F9D9BE (EnemigoIA_t7F4D590CF8C4897FCB1C723187317E99CD7B4B81 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_LookAt_m49185D782014D16DA747C1296BEBAC3FB3CEDC1F (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___target0, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Void UnityEngine.Time::set_timeScale(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Time_set_timeScale_m1987DE9E74FC6C0126CE4F59A6293E3B85BD01EA (float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Cursor::set_visible(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Cursor_set_visible_m4747F0DC20D06D1932EC740C5CCC738C1664903D (bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Application::Quit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Application_Quit_m8D720E5092786C2EE32310D85FE61C253D3B1F2A (const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, bool ___value0, const RuntimeMethod* method);
// System.String System.Single::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Single_ToString_m80E7ABED4F4D73F2BE19DDB80D3D92FCD8DFA010 (float* __this, const RuntimeMethod* method);
// System.Collections.IEnumerator InvocadorOleadas::InvocarOleada()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* InvocadorOleadas_InvocarOleada_mCBC6876F3763219B43365D35C5D977E7BEB5F6E2 (InvocadorOleadas_t7ADC79FAB86E1B7AE663CA487985C11C00B4C530 * __this, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719 (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, RuntimeObject* ___routine0, const RuntimeMethod* method);
// System.Void InvocadorOleadas/<InvocarOleada>d__9::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CInvocarOleadaU3Ed__9__ctor_mEA8339C3505658A60279289CD8D0CFD2195FAA53 (U3CInvocarOleadaU3Ed__9_t095DFE910F3D2E69D3A9F620E99A7FD143751007 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<UnityEngine.Transform>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Object_Instantiate_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_mC13E8DA0CA1DFD88310E2E066C5479E3994BAA30 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___original0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position1, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rotation2, const RuntimeMethod* method)
{
	return ((  Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * (*) (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E , Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 , const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_mB05DEC51C29EF5BB8BD17D055E80217F11E571AA_gshared)(___original0, ___position1, ___rotation2, method);
}
// System.Void UnityEngine.MonoBehaviour::InvokeRepeating(System.String,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour_InvokeRepeating_mB77F4276826FBA696A150831D190875CB5802C70 (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, String_t* ___methodName0, float ___time1, float ___repeatRate2, const RuntimeMethod* method);
// UnityEngine.GameObject[] UnityEngine.GameObject::FindGameObjectsWithTag(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* GameObject_FindGameObjectsWithTag_m0948320611DC82590D59A36D1C57155B1B6CE186 (String_t* ___tag0, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Void Pinchos::GolpeaObjetivo()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pinchos_GolpeaObjetivo_mB9D47819667C1804DE9D6313522B698D384C4372 (Pinchos_tBABE699D1BAED348FC2D8DAD13582F801A0644DC * __this, const RuntimeMethod* method);
// System.Void Pinchos::Da?ar(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pinchos_DaUF1ar_m700E5BC27A012D2DF00FCA5D951F084087B6A30C (Pinchos_tBABE699D1BAED348FC2D8DAD13582F801A0644DC * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___enemigo0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<EnemigoIA>()
inline EnemigoIA_t7F4D590CF8C4897FCB1C723187317E99CD7B4B81 * Component_GetComponent_TisEnemigoIA_t7F4D590CF8C4897FCB1C723187317E99CD7B4B81_m66AD0F9A8B1176782FDA81579318B9341B8A7621 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  EnemigoIA_t7F4D590CF8C4897FCB1C723187317E99CD7B4B81 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void EnemigoIA::RecibirDa?o(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemigoIA_RecibirDaUF1o_mF0EF8AD9F9B942597FF7CDC0B87F0B5C3E1D2F90 (EnemigoIA_t7F4D590CF8C4897FCB1C723187317E99CD7B4B81 * __this, int32_t ___cantidad0, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_red()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  Color_get_red_m9BD55EBF7A74A515330FA5F7AC7A67C8A8913DD8 (const RuntimeMethod* method);
// System.Void UnityEngine.Gizmos::set_color(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gizmos_set_color_m937ACC6288C81BAFFC3449FAA03BB4F680F4E74F (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Gizmos::DrawWireSphere(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gizmos_DrawWireSphere_m96C425145BBD85CF0192F9DDB3D1A8C69429B78B (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___center0, float ___radius1, const RuntimeMethod* method);
// System.Int32 UnityEngine.Transform::get_childCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Transform_get_childCount_mCBED4F6D3F6A7386C4D97C2C3FD25C383A0BCD05 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Transform_GetChild_mA7D94BEFF0144F76561D9B8FED61C5C939EC1F1C (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, int32_t ___index0, const RuntimeMethod* method);
// UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetActiveScene()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE  SceneManager_GetActiveScene_mB9A5037FFB576B2432D0BFEF6A161B7C4C1921A4 (const RuntimeMethod* method);
// System.Int32 UnityEngine.SceneManagement.Scene::get_buildIndex()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Scene_get_buildIndex_mE32CE766EA0790E4636A351BA353A7FD71A11DA4 (Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.Int32,UnityEngine.SceneManagement.LoadSceneMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SceneManager_LoadScene_m62031BCEF9B3C7179B005B8EA825989F696E6B76 (int32_t ___sceneBuildIndex0, int32_t ___mode1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_Rotate_m027A155054DDC4206F679EFB86BE0960D45C33A7 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___eulers0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Animator::StringToHash(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Animator_StringToHash_mA351F39D53E2AEFCF0BBD50E4FA92B7E1C99A668 (String_t* ___name0, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87 (float ___value0, float ___min1, float ___max2, const RuntimeMethod* method);
// System.Single UnityEngine.Camera::get_fieldOfView()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Camera_get_fieldOfView_mA9BA910800B2E33B572929CDA9A12CE596353920 (Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Camera::get_aspect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Camera_get_aspect_mD0A1FC8F998473DA08866FF9CD61C02E6D5F4987 (Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Camera::get_nearClipPlane()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Camera_get_nearClipPlane_m75A7270074A35D95B05F25EBF8CE392ECA6517DC (Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_right()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_right_mA8EB1882CCE8759766544448308C88D9200F06CE (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_up()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_up_mAB753D250A30C78924D5D22B0821F1D254525C31 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.AudioSource>()
inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * Component_GetComponent_TisAudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_mCC3B7A679ECE504BFAF8C70C4EF527511F46902F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Boolean UnityEngine.Component::CompareTag(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Component_CompareTag_m17D74EDCC81A10B18A0A588519F522E8DF1D7879 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, String_t* ___tag0, const RuntimeMethod* method);
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Renderer>()
inline RendererU5BU5D_tE2D3C4350893C593CA40DE876B9F2F0EBBEC49B7* Component_GetComponentsInChildren_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_m107B791DBC1E809192456359DFF8B8F45A84EAA1 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  RendererU5BU5D_tE2D3C4350893C593CA40DE876B9F2F0EBBEC49B7* (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponentsInChildren_TisRuntimeObject_mCA5B356D4B0824C6DE60A8E90E6A6D4188C56C2F_gshared)(__this, method);
}
// System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Renderer_set_enabled_mFFBA418C428C1B2B151C77B879DD10C393D9D95B (Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AudioSource::PlayOneShot(UnityEngine.AudioClip)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AudioSource_PlayOneShot_mA90B136041A61C30909301D45D0315088CA7D796 (AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * __this, AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ___clip0, const RuntimeMethod* method);
// System.Single UnityEngine.AudioClip::get_length()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float AudioClip_get_length_m2223F2281D853F847BE0048620BA6F61F26440E4 (AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_mAAAA103F4911E9FA18634BF9605C28559F5E2AC7 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___obj0, float ___t1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Behaviour::get_enabled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Behaviour_get_enabled_m08077AB79934634E1EAE909C2B482BEF4C15A800 (Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9 * __this, const RuntimeMethod* method);
// System.Boolean Invector.vCharacterController.vThirdPersonMotor::get_isStrafing()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool vThirdPersonMotor_get_isStrafing_m3FB9147D8588055A220D47101B80C6FFD2B2E791_inline (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetBool(System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Animator_SetBool_m0F0363B189AAB848FA3B428986C6A01470B3E38C (Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * __this, int32_t ___id0, bool ___value1, const RuntimeMethod* method);
// System.Boolean Invector.vCharacterController.vThirdPersonMotor::get_isSprinting()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool vThirdPersonMotor_get_isSprinting_m98A252FE6362DE6468A47BBC3B0F7FCACA2B3EDD_inline (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, const RuntimeMethod* method);
// System.Boolean Invector.vCharacterController.vThirdPersonMotor::get_isGrounded()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool vThirdPersonMotor_get_isGrounded_m3587831F44EEE43F49591F13918B05CFD28B65AB_inline (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetFloat(System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Animator_SetFloat_m22777620F85E25691F57A7CAD4190D7F5702E02C (Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * __this, int32_t ___id0, float ___value1, const RuntimeMethod* method);
// System.Boolean Invector.vCharacterController.vThirdPersonMotor::get_stopMove()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool vThirdPersonMotor_get_stopMove_m09C1FEC6B37CBF3071597D789E0322C93CD78C58_inline (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetFloat(System.Int32,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Animator_SetFloat_mB6D249371976254CDD71F4205E541945C66F887B (Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * __this, int32_t ___id0, float ___value1, float ___dampTime2, float ___deltaTime3, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformDirection(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_InverseTransformDirection_m9EB6F7A2598FD8D6B52F0A6EBA96A3BAAF68D696 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___direction0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method);
// System.Single UnityEngine.Vector2::get_magnitude()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector2_get_magnitude_mD30DB8EB73C4A5CD395745AE1CA1C38DC61D2E85 (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, const RuntimeMethod* method);
// System.Void Invector.vCharacterController.vThirdPersonMotor::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonMotor__ctor_m4AEDE34F41DF0A598D8C8DDB4BA8F5088AC7D06D (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, const RuntimeMethod* method);
// System.Void vThirdPersonCamera::Init()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonCamera_Init_mAFBED3541CD351DED52E79B067716668A5D56ED6 (vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * Component_GetComponent_TisCamera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_mE17146EF5B0D8E9F9D2D2D94567BF211AD00D320 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject__ctor_mDF8BF31EAE3E03F24421531B25FB4BEDB7C87144 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, String_t* ___name0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_set_hideFlags_m7DE229AF60B92F0C68819F77FEB27D775E66F3AC (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_eulerAngles()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_eulerAngles_mCF1E10C36ED1F03804A1D10A9BAB272E0EA8766F (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Void vThirdPersonCamera::CameraMovement()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonCamera_CameraMovement_m0F154B3318E4D78CFA3D8C4C4B38446CD1B38BC2 (vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___exists0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Quaternion::get_eulerAngles()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Quaternion_get_eulerAngles_m3DA616CAD670235A407E8A7A75925AA8E22338C3 (Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * __this, const RuntimeMethod* method);
// UnityEngine.Ray UnityEngine.Camera::ScreenPointToRay(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Ray_t2E9E67CC8B03EE6ED2BBF3D2C9C96DDF70E1D5E6  Camera_ScreenPointToRay_mD385213935A81030EDC604A39FD64761077CFBAB (Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___pos0, const RuntimeMethod* method);
// System.Single Invector.vExtensions::ClampAngle(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float vExtensions_ClampAngle_m748C49B4118E791FEB84C37FAE0006DE695EAC4B (float ___angle0, float ___min1, float ___max2, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::get_root()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Transform_get_root_mDEB1F3B4DB26B32CEED6DFFF734F85C79C4DDA91 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_localEulerAngles()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_localEulerAngles_m4C442107F523737ADAB54855FDC1777A9B71D545 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Lerp(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Lerp_m8A2A50B945F42D579EDF44D5EE79E85A4DA59616 (float ___a0, float ___b1, float ___t2, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(System.Single,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m079B29E4F58127F03BD52558C1FE1A528547328F_inline (float ___d0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a1, const RuntimeMethod* method);
// Invector.ClipPlanePoints Invector.vExtensions::NearClipPlanePoints(UnityEngine.Camera,UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ClipPlanePoints_tFA6B5EF4C87C83626BEAEA585214A9C4679433DA  vExtensions_NearClipPlanePoints_m95EBECC9448EE09DE106D39A4089ABFF27725772 (Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___camera0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___pos1, float ___clipPlaneMargin2, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50 (const RuntimeMethod* method);
// System.Int32 UnityEngine.LayerMask::op_Implicit(UnityEngine.LayerMask)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t LayerMask_op_Implicit_mD89E9970822613D8D19B2EBCA36C79391C287BE0 (LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___mask0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_SphereCast_m9943744EA02A42417719F4A0CBE5822DA12F15BC (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___origin0, float ___radius1, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___direction2, RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * ___hitInfo3, float ___maxDistance4, int32_t ___layerMask5, const RuntimeMethod* method);
// System.Single UnityEngine.RaycastHit::get_distance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float RaycastHit_get_distance_m85FCA98D7957C3BF1D449CA1B48C116CCD6226FA (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * __this, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_blue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  Color_get_blue_m6D62D515CA10A6E760848E1BFB997E27B90BD07B (const RuntimeMethod* method);
// System.Boolean vThirdPersonCamera::CullingRayCast(UnityEngine.Vector3,Invector.ClipPlanePoints,UnityEngine.RaycastHit&,System.Single,UnityEngine.LayerMask,UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool vThirdPersonCamera_CullingRayCast_mF01736BAE8BA542FF4F8F62F93A398909E1C37A2 (vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___from0, ClipPlanePoints_tFA6B5EF4C87C83626BEAEA585214A9C4679433DA  ____to1, RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * ___hitInfo2, float ___distance3, LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___cullingLayer4, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color5, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_cyan()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  Color_get_cyan_m0C608BC083FD98C45C1F4F15AE803D288C647686 (const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::Dot(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector3_Dot_mD19905B093915BA12852732EA27AA2DBE030D11F_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___lhs0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rhs1, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_Euler_m37BF99FFFA09F4B3F83DC066641B82C59B19A9C3 (float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::Slerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_Slerp_m6D2BD18286254E28D2288B51962EC71F85C7B5C8 (Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___a0, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___b1, float ___t2, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_LookRotation_m1B0BEBEBCC384324A6771B9EAC89761F73E1D6BF (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forward0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_get_zero_m621041B9DF5FAE86C1EF4CB28C224FEA089CB828 (const RuntimeMethod* method);
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_Raycast_mCBD5F7D498C246713EDDBB446E97205DA206C49C (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___origin0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___direction1, RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * ___hitInfo2, float ___maxDistance3, int32_t ___layerMask4, const RuntimeMethod* method);
// UnityEngine.Collider UnityEngine.RaycastHit::get_collider()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * RaycastHit_get_collider_m13A3DE16FBC631E0A1F987E0B22CE70AF8AB539E (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * __this, const RuntimeMethod* method);
// UnityEngine.LayerMask UnityEngine.LayerMask::op_Implicit(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  LayerMask_op_Implicit_mC7EE32122D2A4786D3C00B93E41604B71BF1397C (int32_t ___intVal0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6 (const RuntimeMethod* method);
// System.Boolean UnityEngine.Vector3::op_Equality(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Vector3_op_Equality_m8A98C7F38641110A2F90445EF8E98ECE14B08296 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___lhs0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rhs1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Animator::get_rootPosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Animator_get_rootPosition_mAF11A46CB8D953A1947FFF512F143899B3B9BFE9 (Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Animator::get_rootRotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Animator_get_rootRotation_mF887DA02BAFB6FE896257F3FC4DFCA0F670C4933 (Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * __this, const RuntimeMethod* method);
// System.Void Invector.vCharacterController.vThirdPersonMotor::set_isStrafing(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void vThirdPersonMotor_set_isStrafing_m90A9F59590C2DABE76449CE57C8FBCCCC9F39EFE_inline (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, bool ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Vector3::op_Inequality(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Vector3_op_Inequality_m15190A795B416EB699E69E6190DE6F1C1F208710 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___lhs0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rhs1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, float ___t2, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::get_magnitude()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector3_get_magnitude_mDDD40612220D8104E77E993E18A101A69A944991 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::AngleAxis(System.Single,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_AngleAxis_m4644D20F58ADF03E9EA297CB4A845E5BCDA1E398 (float ___angle0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___axis1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Quaternion_op_Multiply_mDC5F913E6B21FEC72AB2CF737D34CC6C7A69803D (Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rotation0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___point1, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::get_sqrMagnitude()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector3_get_sqrMagnitude_mC567EE6DF411501A8FE1F23A0038862630B88249 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, const RuntimeMethod* method);
// System.Void Invector.vCharacterController.vThirdPersonMotor::set_isSprinting(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void vThirdPersonMotor_set_isSprinting_m031D2D28AF417BB0EAD7679972CAFE117C6A7749_inline (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::CrossFadeInFixedTime(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Animator_CrossFadeInFixedTime_m424EE9ABD82B06A3D950A538E6A61FA55ECC654B (Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * __this, String_t* ___stateName0, float ___fixedTransitionDuration1, const RuntimeMethod* method);
// System.Void Invector.vCharacterController.vThirdPersonAnimator::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonAnimator__ctor_mBB69BF7A51196C407D664AD2A836167FF190352A (vThirdPersonAnimator_tDFBC4BC178521ACC390B82F0B5418961D2754CB0 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<Invector.vCharacterController.vThirdPersonController>()
inline vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * Component_GetComponent_TisvThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A_m89E57D0F784DA689BADD06ECF4BFEA8A8822A350 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void Invector.vCharacterController.vThirdPersonMotor::Init()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonMotor_Init_m8470D932C4BE26328DB0320B9E92E0AE4E3E8B37 (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::FindObjectOfType<vThirdPersonCamera>()
inline vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11 * Object_FindObjectOfType_TisvThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11_m291B5F056AEC7E08B699A8363A313162CAB2B4BD (const RuntimeMethod* method)
{
	return ((  vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11 * (*) (const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_m25AA6DB6AABFD5D66AFA1A8C0E91A7AF61429C37_gshared)(method);
}
// System.Void vThirdPersonCamera::SetMainTarget(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonCamera_SetMainTarget_m35387F3B3FC8B83915DC3441E9019F0EE7D878C7 (vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___newTarget0, const RuntimeMethod* method);
// System.Single UnityEngine.Input::GetAxis(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Input_GetAxis_m939297DEB2ECF8D8D09AD66EB69979AAD2B62326 (String_t* ___axisName0, const RuntimeMethod* method);
// UnityEngine.Camera UnityEngine.Camera::get_main()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C (const RuntimeMethod* method);
// System.Void vThirdPersonCamera::RotateCamera(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonCamera_RotateCamera_m38FD2DBF37023FD4885681827DFF017CF64FFC57 (vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11 * __this, float ___x0, float ___y1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetKeyDown(UnityEngine.KeyCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetKeyDown_m476116696E71771641BBECBAB1A4C55E69018220 (int32_t ___key0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetKeyUp(UnityEngine.KeyCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetKeyUp_mDE9D56FE11715566D4D54FD96F8E1EF9734D225F (int32_t ___key0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void UnityEngine.Animator::set_updateMode(UnityEngine.AnimatorUpdateMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Animator_set_updateMode_mD10A679F349D4BD7722E1D700215592BDC4B05CE (Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.PhysicMaterial::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhysicMaterial__ctor_m8E7DE1AB54FEC51DE0DA4207E5BB55BB45EFC873 (PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::set_name(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_set_name_m87C4006618ADB325ABE5439DF159E10DD8DD0781 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.PhysicMaterial::set_staticFriction(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhysicMaterial_set_staticFriction_mF2914B1027C9EB422643AD4A9056D04727637EC1 (PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.PhysicMaterial::set_dynamicFriction(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhysicMaterial_set_dynamicFriction_mEE2B55B29EECABA0C7A39B102F2BD259CE0D4566 (PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.PhysicMaterial::set_frictionCombine(UnityEngine.PhysicMaterialCombine)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhysicMaterial_set_frictionCombine_mDDFA5B298AC911112AF8B7C2849BCD3C7E6632CD (PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 * __this, int32_t ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_m9DC24AA806B0B65E917751F7A3AFDB58861157CE (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// !!0 UnityEngine.Component::GetComponent<UnityEngine.CapsuleCollider>()
inline CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * Component_GetComponent_TisCapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635_m2B9A09217489CB456933DEA112CA70C10AD3B9F9 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// UnityEngine.Vector3 UnityEngine.CapsuleCollider::get_center()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  CapsuleCollider_get_center_m6374F7457A9450CAFFAD2DF0C9D1419BF9E304CB (CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.CapsuleCollider::get_radius()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float CapsuleCollider_get_radius_m5746DDE5E6A099269FC9DAD253887C58E8A064D0 (CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.CapsuleCollider::get_height()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float CapsuleCollider_get_height_mD6CF93CB2C222F8E5B77D65B0356F8FD8005B526 (CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * __this, const RuntimeMethod* method);
// System.Void Invector.vCharacterController.vThirdPersonMotor::set_isGrounded(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void vThirdPersonMotor_set_isGrounded_m4F6F1C10637D6E9382C07E507FCD1773F846FA9B_inline (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::Normalize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector3_Normalize_m2258C159121FC81954C301DEE631BC24FCEDE780 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Rigidbody_get_position_m5F429382F610E324F39F33E8498A29D0828AD8E8 (Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Division(UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Division_mE5ACBFB168FED529587457A83BA98B7DB32E2A05_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_velocity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Rigidbody_get_velocity_mCFB033F3BD14C2BA68E797DFA4950F9307EC8E2C (Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::set_velocity(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_velocity_m8DC0988916EB38DFD7D4584830B41D79140BF18D (Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Physics::Linecast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_Linecast_m320471A14633A94B6B3AEE1F4D3C959C58890015 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___start0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___end1, RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * ___hitInfo2, int32_t ___layerMask3, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_normal()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  RaycastHit_get_normal_m2C813B25BAECD87FD9E9CB294278B291F4CC6674 (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::Angle(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector3_Angle_m3715AB03A36C59D8CF08F8D71E2F46454EB884C1 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___from0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___to1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  RaycastHit_get_point_m32F7282CBB2E13393A33BAD046BDA218E48DD21E (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * __this, const RuntimeMethod* method);
// System.Void Invector.vCharacterController.vThirdPersonMotor::set_stopMove(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void vThirdPersonMotor_set_stopMove_m700BD3D0627D69453D1BB1D453062FAD5F568BC8_inline (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, bool ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::RotateTowards(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_RotateTowards_mCE2B2820B2483A44056A74E9C2C22359ED7D1AD5 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___current0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___target1, float ___maxRadiansDelta2, float ___maxMagnitudeDelta3, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3,UnityEngine.ForceMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_AddForce_m78B9D94F505E19F3C63461362AD6DE7EA0836700 (Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___force0, int32_t ___mode1, const RuntimeMethod* method);
// UnityEngine.RaycastHit[] UnityEngine.Physics::CapsuleCastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,System.Single,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RaycastHitU5BU5D_t6778DB95346906446AAD3A1A36904F1846435A09* Physics_CapsuleCastAll_m17AD93A3F9EFAA514C83D8BDD951C7B4D850E454 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___point10, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___point21, float ___radius2, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___direction3, float ___maxDistance4, int32_t ___layerMask5, const RuntimeMethod* method);
// System.Void UnityEngine.Collider::set_material(UnityEngine.PhysicMaterial)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Collider_set_material_m3B07EBDE2756F6F250C6202EA1F67C95072B9D72 (Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * __this, PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 * ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_down()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_down_mFA85B870E184121D30F66395BB183ECAB9DD8629 (const RuntimeMethod* method);
// System.Void UnityEngine.Ray::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Ray__ctor_m75B1F651FF47EE6B887105101B7DA61CBF41F83C (Ray_t2E9E67CC8B03EE6ED2BBF3D2C9C96DDF70E1D5E6 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___origin0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___direction1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_Raycast_m46E12070D6996F4C8C91D49ADC27C74AC1D6A3D8 (Ray_t2E9E67CC8B03EE6ED2BBF3D2C9C96DDF70E1D5E6  ___ray0, RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * ___hitInfo1, float ___maxDistance2, int32_t ___layerMask3, const RuntimeMethod* method);
// System.Boolean UnityEngine.Collider::get_isTrigger()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Collider_get_isTrigger_m3A9C990365C94B7125DB5993D782D3D0FE876A60 (Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_UnaryNegation(UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_UnaryNegation_m362EA356F4CADEDB39F965A0DBDED6EA890925F7_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Ray,System.Single,UnityEngine.RaycastHit&,System.Single,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_SphereCast_mE188E0B54CBCA90E9676B9E17931F1649E568AD9 (Ray_t2E9E67CC8B03EE6ED2BBF3D2C9C96DDF70E1D5E6  ___ray0, float ___radius1, RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * ___hitInfo2, float ___maxDistance3, int32_t ___layerMask4, const RuntimeMethod* method);
// System.Double System.Math::Round(System.Double,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR double Math_Round_m394EEE2C796B3A1578E65037E0D57B3D6F9B1C70 (double ___value0, int32_t ___digits1, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void InvocadorOleadas::InvocarEnemigo()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InvocadorOleadas_InvocarEnemigo_m620453681026EA6BAA065D19200F4ECB33573A7A (InvocadorOleadas_t7ADC79FAB86E1B7AE663CA487985C11C00B4C530 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4 (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * __this, float ___seconds0, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C (float ___value0, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void EnemigoIA::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemigoIA_Start_m232470DA66A00D2EFF2C38B92324C049CD41504B (EnemigoIA_t7F4D590CF8C4897FCB1C723187317E99CD7B4B81 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PuntosRuta_t1BFAE08531BC3AEEE4B521035B0D948B4BA0F8AE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// objetivo = PuntosRuta.puntos[0];
		TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* L_0 = ((PuntosRuta_t1BFAE08531BC3AEEE4B521035B0D948B4BA0F8AE_StaticFields*)il2cpp_codegen_static_fields_for(PuntosRuta_t1BFAE08531BC3AEEE4B521035B0D948B4BA0F8AE_il2cpp_TypeInfo_var))->get_puntos_4();
		NullCheck(L_0);
		int32_t L_1 = 0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_1));
		__this->set_objetivo_8(L_2);
		// }
		return;
	}
}
// System.Void EnemigoIA::RecibirDa?o(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemigoIA_RecibirDaUF1o_mF0EF8AD9F9B942597FF7CDC0B87F0B5C3E1D2F90 (EnemigoIA_t7F4D590CF8C4897FCB1C723187317E99CD7B4B81 * __this, int32_t ___cantidad0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// vida -= cantidad;
		int32_t L_0 = __this->get_vida_5();
		int32_t L_1 = ___cantidad0;
		__this->set_vida_5(((int32_t)il2cpp_codegen_subtract((int32_t)L_0, (int32_t)L_1)));
		// if (vida <= 0)
		int32_t L_2 = __this->get_vida_5();
		if ((((int32_t)L_2) > ((int32_t)0)))
		{
			goto IL_0033;
		}
	}
	{
		// Estadisticas.dinero += valor;
		int32_t L_3 = ((Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D_StaticFields*)il2cpp_codegen_static_fields_for(Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D_il2cpp_TypeInfo_var))->get_dinero_4();
		int32_t L_4 = __this->get_valor_6();
		((Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D_StaticFields*)il2cpp_codegen_static_fields_for(Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D_il2cpp_TypeInfo_var))->set_dinero_4(((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)L_4)));
		// Destroy(gameObject);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5;
		L_5 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_5, /*hidden argument*/NULL);
	}

IL_0033:
	{
		// }
		return;
	}
}
// System.Void EnemigoIA::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemigoIA_Update_mE9B3CA8D712FC0EE3303A5ACE474C195B36E0586 (EnemigoIA_t7F4D590CF8C4897FCB1C723187317E99CD7B4B81 * __this, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// Vector3 direccion = objetivo.position - transform.position;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = __this->get_objetivo_8();
		NullCheck(L_0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_0, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2;
		L_2 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_2, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		// transform.Translate(direccion.normalized * velocidad * Time.deltaTime, Space.World);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_5;
		L_5 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		L_6 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), /*hidden argument*/NULL);
		float L_7 = __this->get_velocidad_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		L_8 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_6, L_7, /*hidden argument*/NULL);
		float L_9;
		L_9 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10;
		L_10 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_8, L_9, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_Translate_mFB58CBF3FA00BD0EE09EC67457608F62564D0DDE(L_5, L_10, 0, /*hidden argument*/NULL);
		// if (Vector3.Distance(transform.position, objetivo.position) <= 0.4f)
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_11;
		L_11 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		L_12 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_11, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_13 = __this->get_objetivo_8();
		NullCheck(L_13);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14;
		L_14 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_13, /*hidden argument*/NULL);
		float L_15;
		L_15 = Vector3_Distance_mB648A79E4A1BAAFBF7B029644638C0D715480677(L_12, L_14, /*hidden argument*/NULL);
		if ((!(((float)L_15) <= ((float)(0.400000006f)))))
		{
			goto IL_007d;
		}
	}
	{
		// SiguientePuntoRuta();
		EnemigoIA_SiguientePuntoRuta_m9273E16CE010336A1C31BB6C149A867D26F9D9BE(__this, /*hidden argument*/NULL);
		// transform.LookAt(objetivo);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_16;
		L_16 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_17 = __this->get_objetivo_8();
		NullCheck(L_16);
		Transform_LookAt_m49185D782014D16DA747C1296BEBAC3FB3CEDC1F(L_16, L_17, /*hidden argument*/NULL);
	}

IL_007d:
	{
		// }
		return;
	}
}
// System.Void EnemigoIA::SiguientePuntoRuta()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemigoIA_SiguientePuntoRuta_m9273E16CE010336A1C31BB6C149A867D26F9D9BE (EnemigoIA_t7F4D590CF8C4897FCB1C723187317E99CD7B4B81 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PuntosRuta_t1BFAE08531BC3AEEE4B521035B0D948B4BA0F8AE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF984C9674EDCA2C39EFAD66081B054F073C3815B);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (indicador >= PuntosRuta.puntos.Length -1)
		int32_t L_0 = __this->get_indicador_9();
		TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* L_1 = ((PuntosRuta_t1BFAE08531BC3AEEE4B521035B0D948B4BA0F8AE_StaticFields*)il2cpp_codegen_static_fields_for(PuntosRuta_t1BFAE08531BC3AEEE4B521035B0D948B4BA0F8AE_il2cpp_TypeInfo_var))->get_puntos_4();
		NullCheck(L_1);
		if ((((int32_t)L_0) < ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_1)->max_length))), (int32_t)1)))))
		{
			goto IL_0056;
		}
	}
	{
		// Destroy(gameObject);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2;
		L_2 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_2, /*hidden argument*/NULL);
		// Estadisticas.vida -= daño;
		int32_t L_3 = ((Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D_StaticFields*)il2cpp_codegen_static_fields_for(Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D_il2cpp_TypeInfo_var))->get_vida_7();
		int32_t L_4 = __this->get_daUF1o_7();
		((Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D_StaticFields*)il2cpp_codegen_static_fields_for(Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D_il2cpp_TypeInfo_var))->set_vida_7(((int32_t)il2cpp_codegen_subtract((int32_t)L_3, (int32_t)L_4)));
		// if (Estadisticas.vida == -1)
		int32_t L_5 = ((Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D_StaticFields*)il2cpp_codegen_static_fields_for(Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D_il2cpp_TypeInfo_var))->get_vida_7();
		if ((!(((uint32_t)L_5) == ((uint32_t)(-1)))))
		{
			goto IL_0055;
		}
	}
	{
		// Debug.Log("Game Over");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(_stringLiteralF984C9674EDCA2C39EFAD66081B054F073C3815B, /*hidden argument*/NULL);
		// Estadisticas.vivo = false;
		((Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D_StaticFields*)il2cpp_codegen_static_fields_for(Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D_il2cpp_TypeInfo_var))->set_vivo_10((bool)0);
		// Time.timeScale = 0;
		Time_set_timeScale_m1987DE9E74FC6C0126CE4F59A6293E3B85BD01EA((0.0f), /*hidden argument*/NULL);
		// Cursor.visible = true;
		Cursor_set_visible_m4747F0DC20D06D1932EC740C5CCC738C1664903D((bool)1, /*hidden argument*/NULL);
	}

IL_0055:
	{
		// return;
		return;
	}

IL_0056:
	{
		// indicador++;
		int32_t L_6 = __this->get_indicador_9();
		__this->set_indicador_9(((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1)));
		// objetivo = PuntosRuta.puntos[indicador];
		TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* L_7 = ((PuntosRuta_t1BFAE08531BC3AEEE4B521035B0D948B4BA0F8AE_StaticFields*)il2cpp_codegen_static_fields_for(PuntosRuta_t1BFAE08531BC3AEEE4B521035B0D948B4BA0F8AE_il2cpp_TypeInfo_var))->get_puntos_4();
		int32_t L_8 = __this->get_indicador_9();
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_objetivo_8(L_10);
		// }
		return;
	}
}
// System.Void EnemigoIA::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemigoIA__ctor_m549C82FB2C9BF69AED41E80110DE015DE6A54819 (EnemigoIA_t7F4D590CF8C4897FCB1C723187317E99CD7B4B81 * __this, const RuntimeMethod* method)
{
	{
		// public float velocidad = 10f;
		__this->set_velocidad_4((10.0f));
		// public int vida = 100;
		__this->set_vida_5(((int32_t)100));
		// public int valor = 5;
		__this->set_valor_6(5);
		// public int daño = 1;
		__this->set_daUF1o_7(1);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Estadisticas::salir()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Estadisticas_salir_m296998D70EB025E963948A81250EDBC94515A547 (Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D * __this, const RuntimeMethod* method)
{
	{
		// Application.Quit();
		Application_Quit_m8D720E5092786C2EE32310D85FE61C253D3B1F2A(/*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Estadisticas::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Estadisticas_Start_mF2ECC98C97A06252F37C3B0D35EC47CA772F52A9 (Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// vivo = true;
		((Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D_StaticFields*)il2cpp_codegen_static_fields_for(Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D_il2cpp_TypeInfo_var))->set_vivo_10((bool)1);
		// finTexto.gameObject.SetActive(false);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_0 = __this->get_finTexto_13();
		NullCheck(L_0);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1;
		L_1 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_1, (bool)0, /*hidden argument*/NULL);
		// dinero = dineroInicial;
		int32_t L_2 = __this->get_dineroInicial_5();
		((Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D_StaticFields*)il2cpp_codegen_static_fields_for(Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D_il2cpp_TypeInfo_var))->set_dinero_4(L_2);
		// vida = vidaInicial;
		int32_t L_3 = __this->get_vidaInicial_8();
		((Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D_StaticFields*)il2cpp_codegen_static_fields_for(Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D_il2cpp_TypeInfo_var))->set_vida_7(L_3);
		// }
		return;
	}
}
// System.Void Estadisticas::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Estadisticas_Update_m0CD8576D2C16A59FCDC2EB9996BC41B4C68EEE94 (Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		// dineroActual = dinero;
		int32_t L_0 = ((Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D_StaticFields*)il2cpp_codegen_static_fields_for(Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D_il2cpp_TypeInfo_var))->get_dinero_4();
		__this->set_dineroActual_6(L_0);
		// vidaActual = vida;
		int32_t L_1 = ((Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D_StaticFields*)il2cpp_codegen_static_fields_for(Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D_il2cpp_TypeInfo_var))->get_vida_7();
		__this->set_vidaActual_9(L_1);
		// dineroTexto.text = Mathf.Round(dineroActual).ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_2 = __this->get_dineroTexto_11();
		int32_t L_3 = __this->get_dineroActual_6();
		float L_4;
		L_4 = bankers_roundf(((float)((float)L_3)));
		V_0 = L_4;
		String_t* L_5;
		L_5 = Single_ToString_m80E7ABED4F4D73F2BE19DDB80D3D92FCD8DFA010((float*)(&V_0), /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_5);
		// vidaTexto.text = Mathf.Round(vidaActual).ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_6 = __this->get_vidaTexto_12();
		int32_t L_7 = __this->get_vidaActual_9();
		float L_8;
		L_8 = bankers_roundf(((float)((float)L_7)));
		V_0 = L_8;
		String_t* L_9;
		L_9 = Single_ToString_m80E7ABED4F4D73F2BE19DDB80D3D92FCD8DFA010((float*)(&V_0), /*hidden argument*/NULL);
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, L_9);
		// if (vivo == false)
		bool L_10 = ((Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D_StaticFields*)il2cpp_codegen_static_fields_for(Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D_il2cpp_TypeInfo_var))->get_vivo_10();
		if (L_10)
		{
			goto IL_007d;
		}
	}
	{
		// finTexto.gameObject.SetActive(true);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_11 = __this->get_finTexto_13();
		NullCheck(L_11);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_12;
		L_12 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_12, (bool)1, /*hidden argument*/NULL);
		// Reiniciar.gameObject.SetActive(true);
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_13 = __this->get_Reiniciar_14();
		NullCheck(L_13);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_14;
		L_14 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_14, (bool)1, /*hidden argument*/NULL);
	}

IL_007d:
	{
		// }
		return;
	}
}
// System.Void Estadisticas::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Estadisticas__ctor_m8721653D11F4FDBBBF74967754F66558418A7D53 (Estadisticas_tE2E3539C7D6EE867EB9443549F561984C52C7D8D * __this, const RuntimeMethod* method)
{
	{
		// public int dineroInicial = 200;
		__this->set_dineroInicial_5(((int32_t)200));
		// public int vidaInicial = 5;
		__this->set_vidaInicial_8(5);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void InvocadorOleadas::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InvocadorOleadas_Start_m5D969EE1C45F789968149A0BEA69F82C539E2A11 (InvocadorOleadas_t7ADC79FAB86E1B7AE663CA487985C11C00B4C530 * __this, const RuntimeMethod* method)
{
	{
		// aviso.gameObject.SetActive(false);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_0 = __this->get_aviso_9();
		NullCheck(L_0);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1;
		L_1 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_1, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void InvocadorOleadas::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InvocadorOleadas_Update_m3F0C89126C47C42157FE8BF52C328C01A4A894D4 (InvocadorOleadas_t7ADC79FAB86E1B7AE663CA487985C11C00B4C530 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		// if (contador <= 0f)
		float L_0 = __this->get_contador_7();
		if ((!(((float)L_0) <= ((float)(0.0f)))))
		{
			goto IL_0037;
		}
	}
	{
		// StartCoroutine(InvocarOleada());
		RuntimeObject* L_1;
		L_1 = InvocadorOleadas_InvocarOleada_mCBC6876F3763219B43365D35C5D977E7BEB5F6E2(__this, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_2;
		L_2 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_1, /*hidden argument*/NULL);
		// contador = tiempoEntreOleadas;
		float L_3 = __this->get_tiempoEntreOleadas_6();
		__this->set_contador_7(L_3);
		// aviso.gameObject.SetActive(true);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_4 = __this->get_aviso_9();
		NullCheck(L_4);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5;
		L_5 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_5, (bool)1, /*hidden argument*/NULL);
	}

IL_0037:
	{
		// contador -= Time.deltaTime;
		float L_6 = __this->get_contador_7();
		float L_7;
		L_7 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		__this->set_contador_7(((float)il2cpp_codegen_subtract((float)L_6, (float)L_7)));
		// oleadaNumeroTexto.text = Mathf.Round(contador).ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_8 = __this->get_oleadaNumeroTexto_8();
		float L_9 = __this->get_contador_7();
		float L_10;
		L_10 = bankers_roundf(L_9);
		V_0 = L_10;
		String_t* L_11;
		L_11 = Single_ToString_m80E7ABED4F4D73F2BE19DDB80D3D92FCD8DFA010((float*)(&V_0), /*hidden argument*/NULL);
		NullCheck(L_8);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_8, L_11);
		// }
		return;
	}
}
// System.Collections.IEnumerator InvocadorOleadas::InvocarOleada()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* InvocadorOleadas_InvocarOleada_mCBC6876F3763219B43365D35C5D977E7BEB5F6E2 (InvocadorOleadas_t7ADC79FAB86E1B7AE663CA487985C11C00B4C530 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInvocarOleadaU3Ed__9_t095DFE910F3D2E69D3A9F620E99A7FD143751007_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CInvocarOleadaU3Ed__9_t095DFE910F3D2E69D3A9F620E99A7FD143751007 * L_0 = (U3CInvocarOleadaU3Ed__9_t095DFE910F3D2E69D3A9F620E99A7FD143751007 *)il2cpp_codegen_object_new(U3CInvocarOleadaU3Ed__9_t095DFE910F3D2E69D3A9F620E99A7FD143751007_il2cpp_TypeInfo_var);
		U3CInvocarOleadaU3Ed__9__ctor_mEA8339C3505658A60279289CD8D0CFD2195FAA53(L_0, 0, /*hidden argument*/NULL);
		U3CInvocarOleadaU3Ed__9_t095DFE910F3D2E69D3A9F620E99A7FD143751007 * L_1 = L_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_2(__this);
		return L_1;
	}
}
// System.Void InvocadorOleadas::InvocarEnemigo()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InvocadorOleadas_InvocarEnemigo_m620453681026EA6BAA065D19200F4ECB33573A7A (InvocadorOleadas_t7ADC79FAB86E1B7AE663CA487985C11C00B4C530 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_mC13E8DA0CA1DFD88310E2E066C5479E3994BAA30_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Instantiate(EnemigoPrefabricado, puntoAparicion.position, puntoAparicion.rotation);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = __this->get_EnemigoPrefabricado_4();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1 = __this->get_puntoAparicion_5();
		NullCheck(L_1);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_1, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3 = __this->get_puntoAparicion_5();
		NullCheck(L_3);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_4;
		L_4 = Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_5;
		L_5 = Object_Instantiate_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_mC13E8DA0CA1DFD88310E2E066C5479E3994BAA30(L_0, L_2, L_4, /*hidden argument*/Object_Instantiate_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_mC13E8DA0CA1DFD88310E2E066C5479E3994BAA30_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void InvocadorOleadas::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InvocadorOleadas__ctor_mED52F47F1034090157E6CC5C1D4ECDF681AEA2DE (InvocadorOleadas_t7ADC79FAB86E1B7AE663CA487985C11C00B4C530 * __this, const RuntimeMethod* method)
{
	{
		// public float tiempoEntreOleadas = 5f;
		__this->set_tiempoEntreOleadas_6((5.0f));
		// private float contador = 2f;
		__this->set_contador_7((2.0f));
		// private int numeroOleada = 1;
		__this->set_numeroOleada_10(1);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Pinchos::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pinchos_Start_m66D4D83518EF1B6C298F7552A42566C3C9F08798 (Pinchos_tBABE699D1BAED348FC2D8DAD13582F801A0644DC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral279C1285646246614D8E6D24E992B80E25ECD01A);
		s_Il2CppMethodInitialized = true;
	}
	{
		// InvokeRepeating("EncuentraObjetivo", 0f, 0.5f);
		MonoBehaviour_InvokeRepeating_mB77F4276826FBA696A150831D190875CB5802C70(__this, _stringLiteral279C1285646246614D8E6D24E992B80E25ECD01A, (0.0f), (0.5f), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Pinchos::EncuentraObjetivo()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pinchos_EncuentraObjetivo_m6C3102F6BF2489E4A6ED3E7EE6ED0EA8F79958F0 (Pinchos_tBABE699D1BAED348FC2D8DAD13582F801A0644DC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * V_1 = NULL;
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* V_2 = NULL;
	int32_t V_3 = 0;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * V_4 = NULL;
	float V_5 = 0.0f;
	{
		// GameObject[] enemigos = GameObject.FindGameObjectsWithTag(TagEnemigos);
		String_t* L_0 = __this->get_TagEnemigos_7();
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_1;
		L_1 = GameObject_FindGameObjectsWithTag_m0948320611DC82590D59A36D1C57155B1B6CE186(L_0, /*hidden argument*/NULL);
		// float distanciaCorta = Mathf.Infinity;
		V_0 = (std::numeric_limits<float>::infinity());
		// GameObject enemigoCercano = null;
		V_1 = (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)NULL;
		// foreach(GameObject enemigo in enemigos)
		V_2 = L_1;
		V_3 = 0;
		goto IL_0053;
	}

IL_0018:
	{
		// foreach(GameObject enemigo in enemigos)
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_2 = V_2;
		int32_t L_3 = V_3;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_4 = L_5;
		// float distancia = Vector3.Distance(transform.position, enemigo.transform.position);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6;
		L_6 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_6, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_8 = V_4;
		NullCheck(L_8);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_9;
		L_9 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10;
		L_10 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_9, /*hidden argument*/NULL);
		float L_11;
		L_11 = Vector3_Distance_mB648A79E4A1BAAFBF7B029644638C0D715480677(L_7, L_10, /*hidden argument*/NULL);
		V_5 = L_11;
		// if (distancia < distanciaCorta)
		float L_12 = V_5;
		float L_13 = V_0;
		if ((!(((float)L_12) < ((float)L_13))))
		{
			goto IL_0048;
		}
	}
	{
		// distanciaCorta = distancia;
		float L_14 = V_5;
		V_0 = L_14;
		// enemigoCercano = enemigo;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_15 = V_4;
		V_1 = L_15;
		// }
		goto IL_004f;
	}

IL_0048:
	{
		// objetivo = null;
		__this->set_objetivo_6((Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)NULL);
	}

IL_004f:
	{
		int32_t L_16 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_16, (int32_t)1));
	}

IL_0053:
	{
		// foreach(GameObject enemigo in enemigos)
		int32_t L_17 = V_3;
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_18 = V_2;
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_18)->max_length))))))
		{
			goto IL_0018;
		}
	}
	{
		// if (enemigoCercano != null && distanciaCorta <= alcance)
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_20;
		L_20 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_19, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_0077;
		}
	}
	{
		float L_21 = V_0;
		float L_22 = __this->get_alcance_4();
		if ((!(((float)L_21) <= ((float)L_22))))
		{
			goto IL_0077;
		}
	}
	{
		// objetivo = enemigoCercano.transform;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_23 = V_1;
		NullCheck(L_23);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_24;
		L_24 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_23, /*hidden argument*/NULL);
		__this->set_objetivo_6(L_24);
	}

IL_0077:
	{
		// }
		return;
	}
}
// System.Void Pinchos::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pinchos_Update_m3AEE4A6AAD7D5A77852A1682E39F264CC813853A (Pinchos_tBABE699D1BAED348FC2D8DAD13582F801A0644DC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (objetivo == null)
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = __this->get_objetivo_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000f;
		}
	}
	{
		// return;
		return;
	}

IL_000f:
	{
		// if (temporizadorParaDañar <= 0f)
		float L_2 = __this->get_temporizadorParaDaUF1ar_8();
		if ((!(((float)L_2) <= ((float)(0.0f)))))
		{
			goto IL_002d;
		}
	}
	{
		// GolpeaObjetivo();
		Pinchos_GolpeaObjetivo_mB9D47819667C1804DE9D6313522B698D384C4372(__this, /*hidden argument*/NULL);
		// temporizadorParaDañar = 0.5f;
		__this->set_temporizadorParaDaUF1ar_8((0.5f));
	}

IL_002d:
	{
		// temporizadorParaDañar -= Time.deltaTime;
		float L_3 = __this->get_temporizadorParaDaUF1ar_8();
		float L_4;
		L_4 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		__this->set_temporizadorParaDaUF1ar_8(((float)il2cpp_codegen_subtract((float)L_3, (float)L_4)));
		// }
		return;
	}
}
// System.Void Pinchos::GolpeaObjetivo()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pinchos_GolpeaObjetivo_mB9D47819667C1804DE9D6313522B698D384C4372 (Pinchos_tBABE699D1BAED348FC2D8DAD13582F801A0644DC * __this, const RuntimeMethod* method)
{
	{
		// Dañar(objetivo);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = __this->get_objetivo_6();
		Pinchos_DaUF1ar_m700E5BC27A012D2DF00FCA5D951F084087B6A30C(__this, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Pinchos::Da?ar(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pinchos_DaUF1ar_m700E5BC27A012D2DF00FCA5D951F084087B6A30C (Pinchos_tBABE699D1BAED348FC2D8DAD13582F801A0644DC * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___enemigo0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisEnemigoIA_t7F4D590CF8C4897FCB1C723187317E99CD7B4B81_m66AD0F9A8B1176782FDA81579318B9341B8A7621_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EnemigoIA_t7F4D590CF8C4897FCB1C723187317E99CD7B4B81 * V_0 = NULL;
	{
		// EnemigoIA dañoEnemigo = enemigo.GetComponent<EnemigoIA>();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = ___enemigo0;
		NullCheck(L_0);
		EnemigoIA_t7F4D590CF8C4897FCB1C723187317E99CD7B4B81 * L_1;
		L_1 = Component_GetComponent_TisEnemigoIA_t7F4D590CF8C4897FCB1C723187317E99CD7B4B81_m66AD0F9A8B1176782FDA81579318B9341B8A7621(L_0, /*hidden argument*/Component_GetComponent_TisEnemigoIA_t7F4D590CF8C4897FCB1C723187317E99CD7B4B81_m66AD0F9A8B1176782FDA81579318B9341B8A7621_RuntimeMethod_var);
		V_0 = L_1;
		// if (dañoEnemigo != null)
		EnemigoIA_t7F4D590CF8C4897FCB1C723187317E99CD7B4B81 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_2, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		// dañoEnemigo.RecibirDaño(daño);
		EnemigoIA_t7F4D590CF8C4897FCB1C723187317E99CD7B4B81 * L_4 = V_0;
		int32_t L_5 = __this->get_daUF1o_5();
		NullCheck(L_4);
		EnemigoIA_RecibirDaUF1o_mF0EF8AD9F9B942597FF7CDC0B87F0B5C3E1D2F90(L_4, L_5, /*hidden argument*/NULL);
	}

IL_001c:
	{
		// }
		return;
	}
}
// System.Void Pinchos::OnDrawGizmosSelected()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pinchos_OnDrawGizmosSelected_m861CAD31A8B056EDD6430262493807D56DB98480 (Pinchos_tBABE699D1BAED348FC2D8DAD13582F801A0644DC * __this, const RuntimeMethod* method)
{
	{
		// Gizmos.color = Color.red;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0;
		L_0 = Color_get_red_m9BD55EBF7A74A515330FA5F7AC7A67C8A8913DD8(/*hidden argument*/NULL);
		Gizmos_set_color_m937ACC6288C81BAFFC3449FAA03BB4F680F4E74F(L_0, /*hidden argument*/NULL);
		// Gizmos.DrawWireSphere(transform.position, alcance);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_1, /*hidden argument*/NULL);
		float L_3 = __this->get_alcance_4();
		Gizmos_DrawWireSphere_m96C425145BBD85CF0192F9DDB3D1A8C69429B78B(L_2, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Pinchos::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pinchos__ctor_m83CE7F21A7386FD80262F522B29D1E20270DA216 (Pinchos_tBABE699D1BAED348FC2D8DAD13582F801A0644DC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral20265357F96C863C747BDDCB8CB50C9DB7DC428B);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public float alcance = 0.5f;
		__this->set_alcance_4((0.5f));
		// public int daño = 20;
		__this->set_daUF1o_5(((int32_t)20));
		// public string TagEnemigos = "Enemigo";
		__this->set_TagEnemigos_7(_stringLiteral20265357F96C863C747BDDCB8CB50C9DB7DC428B);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PuntosRuta::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PuntosRuta_Awake_m8B71EDC8E00151D4FEFC5A7D42510E10703D5CDA (PuntosRuta_t1BFAE08531BC3AEEE4B521035B0D948B4BA0F8AE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PuntosRuta_t1BFAE08531BC3AEEE4B521035B0D948B4BA0F8AE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// puntos = new Transform[transform.childCount];
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1;
		L_1 = Transform_get_childCount_mCBED4F6D3F6A7386C4D97C2C3FD25C383A0BCD05(L_0, /*hidden argument*/NULL);
		TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* L_2 = (TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D*)(TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D*)SZArrayNew(TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D_il2cpp_TypeInfo_var, (uint32_t)L_1);
		((PuntosRuta_t1BFAE08531BC3AEEE4B521035B0D948B4BA0F8AE_StaticFields*)il2cpp_codegen_static_fields_for(PuntosRuta_t1BFAE08531BC3AEEE4B521035B0D948B4BA0F8AE_il2cpp_TypeInfo_var))->set_puntos_4(L_2);
		// for (int i = 0; i < puntos.Length; i++)
		V_0 = 0;
		goto IL_0030;
	}

IL_0019:
	{
		// puntos[i] = transform.GetChild(i);
		TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* L_3 = ((PuntosRuta_t1BFAE08531BC3AEEE4B521035B0D948B4BA0F8AE_StaticFields*)il2cpp_codegen_static_fields_for(PuntosRuta_t1BFAE08531BC3AEEE4B521035B0D948B4BA0F8AE_il2cpp_TypeInfo_var))->get_puntos_4();
		int32_t L_4 = V_0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_5;
		L_5 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		NullCheck(L_5);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_7;
		L_7 = Transform_GetChild_mA7D94BEFF0144F76561D9B8FED61C5C939EC1F1C(L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_7);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)L_7);
		// for (int i = 0; i < puntos.Length; i++)
		int32_t L_8 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_0030:
	{
		// for (int i = 0; i < puntos.Length; i++)
		int32_t L_9 = V_0;
		TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* L_10 = ((PuntosRuta_t1BFAE08531BC3AEEE4B521035B0D948B4BA0F8AE_StaticFields*)il2cpp_codegen_static_fields_for(PuntosRuta_t1BFAE08531BC3AEEE4B521035B0D948B4BA0F8AE_il2cpp_TypeInfo_var))->get_puntos_4();
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_10)->max_length))))))
		{
			goto IL_0019;
		}
	}
	{
		// }
		return;
	}
}
// System.Void PuntosRuta::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PuntosRuta__ctor_m8EBB144ADD82D37459EA4877C914C5E82162B93C (PuntosRuta_t1BFAE08531BC3AEEE4B521035B0D948B4BA0F8AE * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Reiniciar::ReloadScreen()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Reiniciar_ReloadScreen_m574E0224817E3230F687E8470BFEF433E9BB8F54 (Reiniciar_tCA54EF25F87683B96AF8E690C069B2B3BC1B8510 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE  L_0;
		L_0 = SceneManager_GetActiveScene_mB9A5037FFB576B2432D0BFEF6A161B7C4C1921A4(/*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1;
		L_1 = Scene_get_buildIndex_mE32CE766EA0790E4636A351BA353A7FD71A11DA4((Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE *)(&V_0), /*hidden argument*/NULL);
		SceneManager_LoadScene_m62031BCEF9B3C7179B005B8EA825989F696E6B76(L_1, 0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Reiniciar::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Reiniciar__ctor_mFC5DC20F8A07C244CACE75121911A45ED844624E (Reiniciar_tCA54EF25F87683B96AF8E690C069B2B3BC1B8510 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Salir::ExitGame()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Salir_ExitGame_m1BA252D98558E43521346E3FEE4C5366BC81D010 (Salir_t1BBF788DCACDEDDAA6F34E80CD336C6A7BEC63C0 * __this, const RuntimeMethod* method)
{
	{
		// Application.Quit();
		Application_Quit_m8D720E5092786C2EE32310D85FE61C253D3B1F2A(/*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Salir::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Salir__ctor_m647DEF251A704ED14B036C69DCB92F31AF4522AC (Salir_t1BBF788DCACDEDDAA6F34E80CD336C6A7BEC63C0 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Test::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Test_Start_m4D6FA0B24EBFD6471F4596A93EC95EC1EB5355D8 (Test_tB521413BD14DFDD715F0C78DB4B6392F71A6EED5 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void Test::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Test_Update_m85FFE4CC559D42AAFA74D4DEAE19415052BB70D0 (Test_tB521413BD14DFDD715F0C78DB4B6392F71A6EED5 * __this, const RuntimeMethod* method)
{
	{
		// transform.localScale += scaleChange;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1 = L_0;
		NullCheck(L_1);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046(L_1, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = __this->get_scaleChange_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_2, L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A(L_1, L_4, /*hidden argument*/NULL);
		// transform.position += positionChange;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_5;
		L_5 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6 = L_5;
		NullCheck(L_6);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_6, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = __this->get_positionChange_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		L_9 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_7, L_8, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_6, L_9, /*hidden argument*/NULL);
		// transform.Rotate (rotateChange);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_10;
		L_10 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11 = __this->get_rotateChange_6();
		NullCheck(L_10);
		Transform_Rotate_m027A155054DDC4206F679EFB86BE0960D45C33A7(L_10, L_11, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Test::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Test__ctor_m1C2B5E11A339FD79C0C458FB86723E5F2B74545C (Test_tB521413BD14DFDD715F0C78DB4B6392F71A6EED5 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Invector.vCharacterController.vAnimatorParameters::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vAnimatorParameters__cctor_m5230FDD4B0EBF34E7AE3795A3829779FC8E0F66B (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0C3C4C204DF0A620548A74D496032120462EE463);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2226C21FDA0CD02B2E2682354C83A6ED9AC5D4EA);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral578746FC8C216F37F9C51638EE5054B739C4E1F6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5E16939F0A0663C8C38E3379DBD3B64A87C5032F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral960AF68E8119488D559BE4332DB7AED9EFB6B053);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD07564F70E250E1B2184D654ACE4DECC7C3AE068);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEA96601D79B410E78ED885E29D7A25794A833FD2);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static int InputHorizontal = Animator.StringToHash("InputHorizontal");
		int32_t L_0;
		L_0 = Animator_StringToHash_mA351F39D53E2AEFCF0BBD50E4FA92B7E1C99A668(_stringLiteral960AF68E8119488D559BE4332DB7AED9EFB6B053, /*hidden argument*/NULL);
		((vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_StaticFields*)il2cpp_codegen_static_fields_for(vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_il2cpp_TypeInfo_var))->set_InputHorizontal_0(L_0);
		// public static int InputVertical = Animator.StringToHash("InputVertical");
		int32_t L_1;
		L_1 = Animator_StringToHash_mA351F39D53E2AEFCF0BBD50E4FA92B7E1C99A668(_stringLiteral2226C21FDA0CD02B2E2682354C83A6ED9AC5D4EA, /*hidden argument*/NULL);
		((vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_StaticFields*)il2cpp_codegen_static_fields_for(vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_il2cpp_TypeInfo_var))->set_InputVertical_1(L_1);
		// public static int InputMagnitude = Animator.StringToHash("InputMagnitude");
		int32_t L_2;
		L_2 = Animator_StringToHash_mA351F39D53E2AEFCF0BBD50E4FA92B7E1C99A668(_stringLiteral0C3C4C204DF0A620548A74D496032120462EE463, /*hidden argument*/NULL);
		((vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_StaticFields*)il2cpp_codegen_static_fields_for(vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_il2cpp_TypeInfo_var))->set_InputMagnitude_2(L_2);
		// public static int IsGrounded = Animator.StringToHash("IsGrounded");
		int32_t L_3;
		L_3 = Animator_StringToHash_mA351F39D53E2AEFCF0BBD50E4FA92B7E1C99A668(_stringLiteralEA96601D79B410E78ED885E29D7A25794A833FD2, /*hidden argument*/NULL);
		((vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_StaticFields*)il2cpp_codegen_static_fields_for(vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_il2cpp_TypeInfo_var))->set_IsGrounded_3(L_3);
		// public static int IsStrafing = Animator.StringToHash("IsStrafing");
		int32_t L_4;
		L_4 = Animator_StringToHash_mA351F39D53E2AEFCF0BBD50E4FA92B7E1C99A668(_stringLiteralD07564F70E250E1B2184D654ACE4DECC7C3AE068, /*hidden argument*/NULL);
		((vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_StaticFields*)il2cpp_codegen_static_fields_for(vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_il2cpp_TypeInfo_var))->set_IsStrafing_4(L_4);
		// public static int IsSprinting = Animator.StringToHash("IsSprinting");
		int32_t L_5;
		L_5 = Animator_StringToHash_mA351F39D53E2AEFCF0BBD50E4FA92B7E1C99A668(_stringLiteral5E16939F0A0663C8C38E3379DBD3B64A87C5032F, /*hidden argument*/NULL);
		((vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_StaticFields*)il2cpp_codegen_static_fields_for(vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_il2cpp_TypeInfo_var))->set_IsSprinting_5(L_5);
		// public static int GroundDistance = Animator.StringToHash("GroundDistance");
		int32_t L_6;
		L_6 = Animator_StringToHash_mA351F39D53E2AEFCF0BBD50E4FA92B7E1C99A668(_stringLiteral578746FC8C216F37F9C51638EE5054B739C4E1F6, /*hidden argument*/NULL);
		((vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_StaticFields*)il2cpp_codegen_static_fields_for(vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_il2cpp_TypeInfo_var))->set_GroundDistance_6(L_6);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Invector.Utils.vComment::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vComment__ctor_mE26E2D55EC5441707328990E638BBB0B08E1F2EF (vComment_tEBF402879B60173DE75AD1509B72C5EB560FDA80 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single Invector.vExtensions::ClampAngle(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float vExtensions_ClampAngle_m748C49B4118E791FEB84C37FAE0006DE695EAC4B (float ___angle0, float ___min1, float ___max2, const RuntimeMethod* method)
{

IL_0000:
	{
		// if (angle < -360)
		float L_0 = ___angle0;
		if ((!(((float)L_0) < ((float)(-360.0f)))))
		{
			goto IL_0011;
		}
	}
	{
		// angle += 360;
		float L_1 = ___angle0;
		___angle0 = ((float)il2cpp_codegen_add((float)L_1, (float)(360.0f)));
	}

IL_0011:
	{
		// if (angle > 360)
		float L_2 = ___angle0;
		if ((!(((float)L_2) > ((float)(360.0f)))))
		{
			goto IL_0022;
		}
	}
	{
		// angle -= 360;
		float L_3 = ___angle0;
		___angle0 = ((float)il2cpp_codegen_subtract((float)L_3, (float)(360.0f)));
	}

IL_0022:
	{
		// } while (angle < -360 || angle > 360);
		float L_4 = ___angle0;
		if ((((float)L_4) < ((float)(-360.0f))))
		{
			goto IL_0000;
		}
	}
	{
		float L_5 = ___angle0;
		if ((((float)L_5) > ((float)(360.0f))))
		{
			goto IL_0000;
		}
	}
	{
		// return Mathf.Clamp(angle, min, max);
		float L_6 = ___angle0;
		float L_7 = ___min1;
		float L_8 = ___max2;
		float L_9;
		L_9 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_6, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// Invector.ClipPlanePoints Invector.vExtensions::NearClipPlanePoints(UnityEngine.Camera,UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ClipPlanePoints_tFA6B5EF4C87C83626BEAEA585214A9C4679433DA  vExtensions_NearClipPlanePoints_m95EBECC9448EE09DE106D39A4089ABFF27725772 (Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___camera0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___pos1, float ___clipPlaneMargin2, const RuntimeMethod* method)
{
	ClipPlanePoints_tFA6B5EF4C87C83626BEAEA585214A9C4679433DA  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * V_1 = NULL;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	{
		// var clipPlanePoints = new ClipPlanePoints();
		il2cpp_codegen_initobj((&V_0), sizeof(ClipPlanePoints_tFA6B5EF4C87C83626BEAEA585214A9C4679433DA ));
		// var transform = camera.transform;
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_0 = ___camera0;
		NullCheck(L_0);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		// var halfFOV = (camera.fieldOfView / 2) * Mathf.Deg2Rad;
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_2 = ___camera0;
		NullCheck(L_2);
		float L_3;
		L_3 = Camera_get_fieldOfView_mA9BA910800B2E33B572929CDA9A12CE596353920(L_2, /*hidden argument*/NULL);
		V_2 = ((float)il2cpp_codegen_multiply((float)((float)((float)L_3/(float)(2.0f))), (float)(0.0174532924f)));
		// var aspect = camera.aspect;
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_4 = ___camera0;
		NullCheck(L_4);
		float L_5;
		L_5 = Camera_get_aspect_mD0A1FC8F998473DA08866FF9CD61C02E6D5F4987(L_4, /*hidden argument*/NULL);
		V_3 = L_5;
		// var distance = camera.nearClipPlane;
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_6 = ___camera0;
		NullCheck(L_6);
		float L_7;
		L_7 = Camera_get_nearClipPlane_m75A7270074A35D95B05F25EBF8CE392ECA6517DC(L_6, /*hidden argument*/NULL);
		V_4 = L_7;
		// var height = distance * Mathf.Tan(halfFOV);
		float L_8 = V_4;
		float L_9 = V_2;
		float L_10;
		L_10 = tanf(L_9);
		V_5 = ((float)il2cpp_codegen_multiply((float)L_8, (float)L_10));
		// var width = height * aspect;
		float L_11 = V_5;
		float L_12 = V_3;
		V_6 = ((float)il2cpp_codegen_multiply((float)L_11, (float)L_12));
		// height *= 1 + clipPlaneMargin;
		float L_13 = V_5;
		float L_14 = ___clipPlaneMargin2;
		V_5 = ((float)il2cpp_codegen_multiply((float)L_13, (float)((float)il2cpp_codegen_add((float)(1.0f), (float)L_14))));
		// width *= 1 + clipPlaneMargin;
		float L_15 = V_6;
		float L_16 = ___clipPlaneMargin2;
		V_6 = ((float)il2cpp_codegen_multiply((float)L_15, (float)((float)il2cpp_codegen_add((float)(1.0f), (float)L_16))));
		// clipPlanePoints.LowerRight = pos + transform.right * width;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_17 = ___pos1;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_18 = V_1;
		NullCheck(L_18);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_19;
		L_19 = Transform_get_right_mA8EB1882CCE8759766544448308C88D9200F06CE(L_18, /*hidden argument*/NULL);
		float L_20 = V_6;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_21;
		L_21 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_19, L_20, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_22;
		L_22 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_17, L_21, /*hidden argument*/NULL);
		(&V_0)->set_LowerRight_3(L_22);
		// clipPlanePoints.LowerRight -= transform.up * height;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_23 = (&V_0)->get_address_of_LowerRight_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_24 = L_23;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_25 = (*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_24);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_26 = V_1;
		NullCheck(L_26);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_27;
		L_27 = Transform_get_up_mAB753D250A30C78924D5D22B0821F1D254525C31(L_26, /*hidden argument*/NULL);
		float L_28 = V_5;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_29;
		L_29 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_27, L_28, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_30;
		L_30 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_25, L_29, /*hidden argument*/NULL);
		*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_24 = L_30;
		// clipPlanePoints.LowerRight += transform.forward * distance;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_31 = (&V_0)->get_address_of_LowerRight_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_32 = L_31;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_33 = (*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_32);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_34 = V_1;
		NullCheck(L_34);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_35;
		L_35 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_34, /*hidden argument*/NULL);
		float L_36 = V_4;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_37;
		L_37 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_35, L_36, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_38;
		L_38 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_33, L_37, /*hidden argument*/NULL);
		*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_32 = L_38;
		// clipPlanePoints.LowerLeft = pos - transform.right * width;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_39 = ___pos1;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_40 = V_1;
		NullCheck(L_40);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_41;
		L_41 = Transform_get_right_mA8EB1882CCE8759766544448308C88D9200F06CE(L_40, /*hidden argument*/NULL);
		float L_42 = V_6;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_43;
		L_43 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_41, L_42, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_44;
		L_44 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_39, L_43, /*hidden argument*/NULL);
		(&V_0)->set_LowerLeft_2(L_44);
		// clipPlanePoints.LowerLeft -= transform.up * height;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_45 = (&V_0)->get_address_of_LowerLeft_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_46 = L_45;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_47 = (*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_46);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_48 = V_1;
		NullCheck(L_48);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_49;
		L_49 = Transform_get_up_mAB753D250A30C78924D5D22B0821F1D254525C31(L_48, /*hidden argument*/NULL);
		float L_50 = V_5;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_51;
		L_51 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_49, L_50, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_52;
		L_52 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_47, L_51, /*hidden argument*/NULL);
		*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_46 = L_52;
		// clipPlanePoints.LowerLeft += transform.forward * distance;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_53 = (&V_0)->get_address_of_LowerLeft_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_54 = L_53;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_55 = (*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_54);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_56 = V_1;
		NullCheck(L_56);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_57;
		L_57 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_56, /*hidden argument*/NULL);
		float L_58 = V_4;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_59;
		L_59 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_57, L_58, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_60;
		L_60 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_55, L_59, /*hidden argument*/NULL);
		*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_54 = L_60;
		// clipPlanePoints.UpperRight = pos + transform.right * width;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_61 = ___pos1;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_62 = V_1;
		NullCheck(L_62);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_63;
		L_63 = Transform_get_right_mA8EB1882CCE8759766544448308C88D9200F06CE(L_62, /*hidden argument*/NULL);
		float L_64 = V_6;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_65;
		L_65 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_63, L_64, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_66;
		L_66 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_61, L_65, /*hidden argument*/NULL);
		(&V_0)->set_UpperRight_1(L_66);
		// clipPlanePoints.UpperRight += transform.up * height;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_67 = (&V_0)->get_address_of_UpperRight_1();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_68 = L_67;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_69 = (*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_68);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_70 = V_1;
		NullCheck(L_70);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_71;
		L_71 = Transform_get_up_mAB753D250A30C78924D5D22B0821F1D254525C31(L_70, /*hidden argument*/NULL);
		float L_72 = V_5;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_73;
		L_73 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_71, L_72, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_74;
		L_74 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_69, L_73, /*hidden argument*/NULL);
		*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_68 = L_74;
		// clipPlanePoints.UpperRight += transform.forward * distance;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_75 = (&V_0)->get_address_of_UpperRight_1();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_76 = L_75;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_77 = (*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_76);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_78 = V_1;
		NullCheck(L_78);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_79;
		L_79 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_78, /*hidden argument*/NULL);
		float L_80 = V_4;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_81;
		L_81 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_79, L_80, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_82;
		L_82 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_77, L_81, /*hidden argument*/NULL);
		*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_76 = L_82;
		// clipPlanePoints.UpperLeft = pos - transform.right * width;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_83 = ___pos1;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_84 = V_1;
		NullCheck(L_84);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_85;
		L_85 = Transform_get_right_mA8EB1882CCE8759766544448308C88D9200F06CE(L_84, /*hidden argument*/NULL);
		float L_86 = V_6;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_87;
		L_87 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_85, L_86, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_88;
		L_88 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_83, L_87, /*hidden argument*/NULL);
		(&V_0)->set_UpperLeft_0(L_88);
		// clipPlanePoints.UpperLeft += transform.up * height;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_89 = (&V_0)->get_address_of_UpperLeft_0();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_90 = L_89;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_91 = (*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_90);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_92 = V_1;
		NullCheck(L_92);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_93;
		L_93 = Transform_get_up_mAB753D250A30C78924D5D22B0821F1D254525C31(L_92, /*hidden argument*/NULL);
		float L_94 = V_5;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_95;
		L_95 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_93, L_94, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_96;
		L_96 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_91, L_95, /*hidden argument*/NULL);
		*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_90 = L_96;
		// clipPlanePoints.UpperLeft += transform.forward * distance;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_97 = (&V_0)->get_address_of_UpperLeft_0();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_98 = L_97;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_99 = (*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_98);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_100 = V_1;
		NullCheck(L_100);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_101;
		L_101 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_100, /*hidden argument*/NULL);
		float L_102 = V_4;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_103;
		L_103 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_101, L_102, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_104;
		L_104 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_99, L_103, /*hidden argument*/NULL);
		*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_98 = L_104;
		// return clipPlanePoints;
		ClipPlanePoints_tFA6B5EF4C87C83626BEAEA585214A9C4679433DA  L_105 = V_0;
		return L_105;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vPickupItem::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vPickupItem_Start_mFA223AD3F583A5878AD577E8A4A329744C988070 (vPickupItem_t9764F53667CE8FA36C0E25286B6039029BD6B4E0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisAudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_mCC3B7A679ECE504BFAF8C70C4EF527511F46902F_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _audioSource = GetComponent<AudioSource>();
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_0;
		L_0 = Component_GetComponent_TisAudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_mCC3B7A679ECE504BFAF8C70C4EF527511F46902F(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_mCC3B7A679ECE504BFAF8C70C4EF527511F46902F_RuntimeMethod_var);
		__this->set__audioSource_4(L_0);
		// }
		return;
	}
}
// System.Void vPickupItem::OnTriggerEnter(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vPickupItem_OnTriggerEnter_m3BB4049DF1DB56C4ABE07E9F1574C91F8A2A9DA4 (vPickupItem_t9764F53667CE8FA36C0E25286B6039029BD6B4E0 * __this, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponentsInChildren_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_m107B791DBC1E809192456359DFF8B8F45A84EAA1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCAF8804297181FF007CA835529DD4477CFD94A70);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_tE2D3C4350893C593CA40DE876B9F2F0EBBEC49B7* V_0 = NULL;
	int32_t V_1 = 0;
	{
		// if(other.CompareTag("Player"))
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_0 = ___other0;
		NullCheck(L_0);
		bool L_1;
		L_1 = Component_CompareTag_m17D74EDCC81A10B18A0A588519F522E8DF1D7879(L_0, _stringLiteralCAF8804297181FF007CA835529DD4477CFD94A70, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0052;
		}
	}
	{
		// Renderer[] renderers = GetComponentsInChildren<Renderer>();
		RendererU5BU5D_tE2D3C4350893C593CA40DE876B9F2F0EBBEC49B7* L_2;
		L_2 = Component_GetComponentsInChildren_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_m107B791DBC1E809192456359DFF8B8F45A84EAA1(__this, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_m107B791DBC1E809192456359DFF8B8F45A84EAA1_RuntimeMethod_var);
		// foreach (Renderer r in renderers)
		V_0 = L_2;
		V_1 = 0;
		goto IL_0025;
	}

IL_0018:
	{
		// foreach (Renderer r in renderers)
		RendererU5BU5D_tE2D3C4350893C593CA40DE876B9F2F0EBBEC49B7* L_3 = V_0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		// r.enabled = false;
		NullCheck(L_6);
		Renderer_set_enabled_mFFBA418C428C1B2B151C77B879DD10C393D9D95B(L_6, (bool)0, /*hidden argument*/NULL);
		int32_t L_7 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1));
	}

IL_0025:
	{
		// foreach (Renderer r in renderers)
		int32_t L_8 = V_1;
		RendererU5BU5D_tE2D3C4350893C593CA40DE876B9F2F0EBBEC49B7* L_9 = V_0;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_9)->max_length))))))
		{
			goto IL_0018;
		}
	}
	{
		// _audioSource.PlayOneShot(_audioClip);
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_10 = __this->get__audioSource_4();
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_11 = __this->get__audioClip_5();
		NullCheck(L_10);
		AudioSource_PlayOneShot_mA90B136041A61C30909301D45D0315088CA7D796(L_10, L_11, /*hidden argument*/NULL);
		// Destroy(gameObject, _audioClip.length);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_12;
		L_12 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_13 = __this->get__audioClip_5();
		NullCheck(L_13);
		float L_14;
		L_14 = AudioClip_get_length_m2223F2281D853F847BE0048620BA6F61F26440E4(L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_mAAAA103F4911E9FA18634BF9605C28559F5E2AC7(L_12, L_14, /*hidden argument*/NULL);
	}

IL_0052:
	{
		// }
		return;
	}
}
// System.Void vPickupItem::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vPickupItem__ctor_m5679F317543E4A3F2B09A6DA9B8DDC6FBE9E5F5F (vPickupItem_t9764F53667CE8FA36C0E25286B6039029BD6B4E0 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Invector.vCharacterController.vThirdPersonAnimator::UpdateAnimator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonAnimator_UpdateAnimator_m3D924B91333BDF1C3C31CC77BF003930D966CC5E (vThirdPersonAnimator_tDFBC4BC178521ACC390B82F0B5418961D2754CB0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B6_0 = 0;
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * G_B6_1 = NULL;
	int32_t G_B5_0 = 0;
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * G_B5_1 = NULL;
	float G_B7_0 = 0.0f;
	int32_t G_B7_1 = 0;
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * G_B7_2 = NULL;
	int32_t G_B9_0 = 0;
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * G_B9_1 = NULL;
	int32_t G_B8_0 = 0;
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * G_B8_1 = NULL;
	float G_B10_0 = 0.0f;
	int32_t G_B10_1 = 0;
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * G_B10_2 = NULL;
	int32_t G_B13_0 = 0;
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * G_B13_1 = NULL;
	int32_t G_B12_0 = 0;
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * G_B12_1 = NULL;
	float G_B14_0 = 0.0f;
	int32_t G_B14_1 = 0;
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * G_B14_2 = NULL;
	int32_t G_B17_0 = 0;
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * G_B17_1 = NULL;
	int32_t G_B16_0 = 0;
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * G_B16_1 = NULL;
	float G_B18_0 = 0.0f;
	int32_t G_B18_1 = 0;
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * G_B18_2 = NULL;
	float G_B20_0 = 0.0f;
	int32_t G_B20_1 = 0;
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * G_B20_2 = NULL;
	float G_B19_0 = 0.0f;
	int32_t G_B19_1 = 0;
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * G_B19_2 = NULL;
	float G_B21_0 = 0.0f;
	float G_B21_1 = 0.0f;
	int32_t G_B21_2 = 0;
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * G_B21_3 = NULL;
	{
		// if (animator == null || !animator.enabled) return;
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_0 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_animator_23();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001b;
		}
	}
	{
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_2 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_animator_23();
		NullCheck(L_2);
		bool L_3;
		L_3 = Behaviour_get_enabled_m08077AB79934634E1EAE909C2B482BEF4C15A800(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_001c;
		}
	}

IL_001b:
	{
		// if (animator == null || !animator.enabled) return;
		return;
	}

IL_001c:
	{
		// animator.SetBool(vAnimatorParameters.IsStrafing, isStrafing); ;
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_4 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_animator_23();
		IL2CPP_RUNTIME_CLASS_INIT(vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_il2cpp_TypeInfo_var);
		int32_t L_5 = ((vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_StaticFields*)il2cpp_codegen_static_fields_for(vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_il2cpp_TypeInfo_var))->get_IsStrafing_4();
		bool L_6;
		L_6 = vThirdPersonMotor_get_isStrafing_m3FB9147D8588055A220D47101B80C6FFD2B2E791_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Animator_SetBool_m0F0363B189AAB848FA3B428986C6A01470B3E38C(L_4, L_5, L_6, /*hidden argument*/NULL);
		// animator.SetBool(vAnimatorParameters.IsSprinting, isSprinting);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_7 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_animator_23();
		int32_t L_8 = ((vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_StaticFields*)il2cpp_codegen_static_fields_for(vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_il2cpp_TypeInfo_var))->get_IsSprinting_5();
		bool L_9;
		L_9 = vThirdPersonMotor_get_isSprinting_m98A252FE6362DE6468A47BBC3B0F7FCACA2B3EDD_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Animator_SetBool_m0F0363B189AAB848FA3B428986C6A01470B3E38C(L_7, L_8, L_9, /*hidden argument*/NULL);
		// animator.SetBool(vAnimatorParameters.IsGrounded, isGrounded);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_10 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_animator_23();
		int32_t L_11 = ((vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_StaticFields*)il2cpp_codegen_static_fields_for(vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_il2cpp_TypeInfo_var))->get_IsGrounded_3();
		bool L_12;
		L_12 = vThirdPersonMotor_get_isGrounded_m3587831F44EEE43F49591F13918B05CFD28B65AB_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Animator_SetBool_m0F0363B189AAB848FA3B428986C6A01470B3E38C(L_10, L_11, L_12, /*hidden argument*/NULL);
		// animator.SetFloat(vAnimatorParameters.GroundDistance, groundDistance);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_13 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_animator_23();
		int32_t L_14 = ((vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_StaticFields*)il2cpp_codegen_static_fields_for(vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_il2cpp_TypeInfo_var))->get_GroundDistance_6();
		float L_15 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_groundDistance_42();
		NullCheck(L_13);
		Animator_SetFloat_m22777620F85E25691F57A7CAD4190D7F5702E02C(L_13, L_14, L_15, /*hidden argument*/NULL);
		// if (isStrafing)
		bool L_16;
		L_16 = vThirdPersonMotor_get_isStrafing_m3FB9147D8588055A220D47101B80C6FFD2B2E791_inline(__this, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00e8;
		}
	}
	{
		// animator.SetFloat(vAnimatorParameters.InputHorizontal, stopMove ? 0 : horizontalSpeed, strafeSpeed.animationSmooth, Time.deltaTime);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_17 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_animator_23();
		IL2CPP_RUNTIME_CLASS_INIT(vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_il2cpp_TypeInfo_var);
		int32_t L_18 = ((vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_StaticFields*)il2cpp_codegen_static_fields_for(vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_il2cpp_TypeInfo_var))->get_InputHorizontal_0();
		bool L_19;
		L_19 = vThirdPersonMotor_get_stopMove_m09C1FEC6B37CBF3071597D789E0322C93CD78C58_inline(__this, /*hidden argument*/NULL);
		G_B5_0 = L_18;
		G_B5_1 = L_17;
		if (L_19)
		{
			G_B6_0 = L_18;
			G_B6_1 = L_17;
			goto IL_0097;
		}
	}
	{
		float L_20 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_horizontalSpeed_35();
		G_B7_0 = L_20;
		G_B7_1 = G_B5_0;
		G_B7_2 = G_B5_1;
		goto IL_009c;
	}

IL_0097:
	{
		G_B7_0 = (0.0f);
		G_B7_1 = G_B6_0;
		G_B7_2 = G_B6_1;
	}

IL_009c:
	{
		vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * L_21 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_strafeSpeed_10();
		NullCheck(L_21);
		float L_22 = L_21->get_animationSmooth_1();
		float L_23;
		L_23 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		NullCheck(G_B7_2);
		Animator_SetFloat_mB6D249371976254CDD71F4205E541945C66F887B(G_B7_2, G_B7_1, G_B7_0, L_22, L_23, /*hidden argument*/NULL);
		// animator.SetFloat(vAnimatorParameters.InputVertical, stopMove ? 0 : verticalSpeed, strafeSpeed.animationSmooth, Time.deltaTime);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_24 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_animator_23();
		IL2CPP_RUNTIME_CLASS_INIT(vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_il2cpp_TypeInfo_var);
		int32_t L_25 = ((vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_StaticFields*)il2cpp_codegen_static_fields_for(vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_il2cpp_TypeInfo_var))->get_InputVertical_1();
		bool L_26;
		L_26 = vThirdPersonMotor_get_stopMove_m09C1FEC6B37CBF3071597D789E0322C93CD78C58_inline(__this, /*hidden argument*/NULL);
		G_B8_0 = L_25;
		G_B8_1 = L_24;
		if (L_26)
		{
			G_B9_0 = L_25;
			G_B9_1 = L_24;
			goto IL_00cc;
		}
	}
	{
		float L_27 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_verticalSpeed_34();
		G_B10_0 = L_27;
		G_B10_1 = G_B8_0;
		G_B10_2 = G_B8_1;
		goto IL_00d1;
	}

IL_00cc:
	{
		G_B10_0 = (0.0f);
		G_B10_1 = G_B9_0;
		G_B10_2 = G_B9_1;
	}

IL_00d1:
	{
		vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * L_28 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_strafeSpeed_10();
		NullCheck(L_28);
		float L_29 = L_28->get_animationSmooth_1();
		float L_30;
		L_30 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		NullCheck(G_B10_2);
		Animator_SetFloat_mB6D249371976254CDD71F4205E541945C66F887B(G_B10_2, G_B10_1, G_B10_0, L_29, L_30, /*hidden argument*/NULL);
		// }
		goto IL_011d;
	}

IL_00e8:
	{
		// animator.SetFloat(vAnimatorParameters.InputVertical, stopMove ? 0 : verticalSpeed, freeSpeed.animationSmooth, Time.deltaTime);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_31 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_animator_23();
		IL2CPP_RUNTIME_CLASS_INIT(vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_il2cpp_TypeInfo_var);
		int32_t L_32 = ((vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_StaticFields*)il2cpp_codegen_static_fields_for(vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_il2cpp_TypeInfo_var))->get_InputVertical_1();
		bool L_33;
		L_33 = vThirdPersonMotor_get_stopMove_m09C1FEC6B37CBF3071597D789E0322C93CD78C58_inline(__this, /*hidden argument*/NULL);
		G_B12_0 = L_32;
		G_B12_1 = L_31;
		if (L_33)
		{
			G_B13_0 = L_32;
			G_B13_1 = L_31;
			goto IL_0103;
		}
	}
	{
		float L_34 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_verticalSpeed_34();
		G_B14_0 = L_34;
		G_B14_1 = G_B12_0;
		G_B14_2 = G_B12_1;
		goto IL_0108;
	}

IL_0103:
	{
		G_B14_0 = (0.0f);
		G_B14_1 = G_B13_0;
		G_B14_2 = G_B13_1;
	}

IL_0108:
	{
		vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * L_35 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_freeSpeed_9();
		NullCheck(L_35);
		float L_36 = L_35->get_animationSmooth_1();
		float L_37;
		L_37 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		NullCheck(G_B14_2);
		Animator_SetFloat_mB6D249371976254CDD71F4205E541945C66F887B(G_B14_2, G_B14_1, G_B14_0, L_36, L_37, /*hidden argument*/NULL);
	}

IL_011d:
	{
		// animator.SetFloat(vAnimatorParameters.InputMagnitude, stopMove ? 0f : inputMagnitude, isStrafing ? strafeSpeed.animationSmooth : freeSpeed.animationSmooth, Time.deltaTime);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_38 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_animator_23();
		IL2CPP_RUNTIME_CLASS_INIT(vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_il2cpp_TypeInfo_var);
		int32_t L_39 = ((vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_StaticFields*)il2cpp_codegen_static_fields_for(vAnimatorParameters_t1A3C3673ACEF946E579FA99841A9E12B16B14DBF_il2cpp_TypeInfo_var))->get_InputMagnitude_2();
		bool L_40;
		L_40 = vThirdPersonMotor_get_stopMove_m09C1FEC6B37CBF3071597D789E0322C93CD78C58_inline(__this, /*hidden argument*/NULL);
		G_B16_0 = L_39;
		G_B16_1 = L_38;
		if (L_40)
		{
			G_B17_0 = L_39;
			G_B17_1 = L_38;
			goto IL_0138;
		}
	}
	{
		float L_41 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_inputMagnitude_33();
		G_B18_0 = L_41;
		G_B18_1 = G_B16_0;
		G_B18_2 = G_B16_1;
		goto IL_013d;
	}

IL_0138:
	{
		G_B18_0 = (0.0f);
		G_B18_1 = G_B17_0;
		G_B18_2 = G_B17_1;
	}

IL_013d:
	{
		bool L_42;
		L_42 = vThirdPersonMotor_get_isStrafing_m3FB9147D8588055A220D47101B80C6FFD2B2E791_inline(__this, /*hidden argument*/NULL);
		G_B19_0 = G_B18_0;
		G_B19_1 = G_B18_1;
		G_B19_2 = G_B18_2;
		if (L_42)
		{
			G_B20_0 = G_B18_0;
			G_B20_1 = G_B18_1;
			G_B20_2 = G_B18_2;
			goto IL_0152;
		}
	}
	{
		vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * L_43 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_freeSpeed_9();
		NullCheck(L_43);
		float L_44 = L_43->get_animationSmooth_1();
		G_B21_0 = L_44;
		G_B21_1 = G_B19_0;
		G_B21_2 = G_B19_1;
		G_B21_3 = G_B19_2;
		goto IL_015d;
	}

IL_0152:
	{
		vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * L_45 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_strafeSpeed_10();
		NullCheck(L_45);
		float L_46 = L_45->get_animationSmooth_1();
		G_B21_0 = L_46;
		G_B21_1 = G_B20_0;
		G_B21_2 = G_B20_1;
		G_B21_3 = G_B20_2;
	}

IL_015d:
	{
		float L_47;
		L_47 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		NullCheck(G_B21_3);
		Animator_SetFloat_mB6D249371976254CDD71F4205E541945C66F887B(G_B21_3, G_B21_2, G_B21_1, G_B21_0, L_47, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Invector.vCharacterController.vThirdPersonAnimator::SetAnimatorMoveSpeed(Invector.vCharacterController.vThirdPersonMotor/vMovementSpeed)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonAnimator_SetAnimatorMoveSpeed_mD798AE22158330F5A7DBC64EE61611C45EE4DF02 (vThirdPersonAnimator_tDFBC4BC178521ACC390B82F0B5418961D2754CB0 * __this, vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * ___speed0, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_1;
	memset((&V_1), 0, sizeof(V_1));
	float G_B3_0 = 0.0f;
	float G_B3_1 = 0.0f;
	vThirdPersonAnimator_tDFBC4BC178521ACC390B82F0B5418961D2754CB0 * G_B3_2 = NULL;
	float G_B2_0 = 0.0f;
	float G_B2_1 = 0.0f;
	vThirdPersonAnimator_tDFBC4BC178521ACC390B82F0B5418961D2754CB0 * G_B2_2 = NULL;
	float G_B4_0 = 0.0f;
	float G_B4_1 = 0.0f;
	float G_B4_2 = 0.0f;
	vThirdPersonAnimator_tDFBC4BC178521ACC390B82F0B5418961D2754CB0 * G_B4_3 = NULL;
	vThirdPersonAnimator_tDFBC4BC178521ACC390B82F0B5418961D2754CB0 * G_B7_0 = NULL;
	vThirdPersonAnimator_tDFBC4BC178521ACC390B82F0B5418961D2754CB0 * G_B6_0 = NULL;
	float G_B8_0 = 0.0f;
	vThirdPersonAnimator_tDFBC4BC178521ACC390B82F0B5418961D2754CB0 * G_B8_1 = NULL;
	float G_B10_0 = 0.0f;
	float G_B10_1 = 0.0f;
	vThirdPersonAnimator_tDFBC4BC178521ACC390B82F0B5418961D2754CB0 * G_B10_2 = NULL;
	float G_B9_0 = 0.0f;
	float G_B9_1 = 0.0f;
	vThirdPersonAnimator_tDFBC4BC178521ACC390B82F0B5418961D2754CB0 * G_B9_2 = NULL;
	float G_B11_0 = 0.0f;
	float G_B11_1 = 0.0f;
	float G_B11_2 = 0.0f;
	vThirdPersonAnimator_tDFBC4BC178521ACC390B82F0B5418961D2754CB0 * G_B11_3 = NULL;
	{
		// Vector3 relativeInput = transform.InverseTransformDirection(moveDirection);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_moveDirection_51();
		NullCheck(L_0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Transform_InverseTransformDirection_m9EB6F7A2598FD8D6B52F0A6EBA96A3BAAF68D696(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		// verticalSpeed = relativeInput.z;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = V_0;
		float L_4 = L_3.get_z_4();
		((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->set_verticalSpeed_34(L_4);
		// horizontalSpeed = relativeInput.x;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5 = V_0;
		float L_6 = L_5.get_x_2();
		((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->set_horizontalSpeed_35(L_6);
		// var newInput = new Vector2(verticalSpeed, horizontalSpeed);
		float L_7 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_verticalSpeed_34();
		float L_8 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_horizontalSpeed_35();
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 *)(&V_1), L_7, L_8, /*hidden argument*/NULL);
		// if (speed.walkByDefault)
		vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * L_9 = ___speed0;
		NullCheck(L_9);
		bool L_10 = L_9->get_walkByDefault_3();
		if (!L_10)
		{
			goto IL_0071;
		}
	}
	{
		// inputMagnitude = Mathf.Clamp(newInput.magnitude, 0, isSprinting ? runningSpeed : walkSpeed);
		float L_11;
		L_11 = Vector2_get_magnitude_mD30DB8EB73C4A5CD395745AE1CA1C38DC61D2E85((Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 *)(&V_1), /*hidden argument*/NULL);
		bool L_12;
		L_12 = vThirdPersonMotor_get_isSprinting_m98A252FE6362DE6468A47BBC3B0F7FCACA2B3EDD_inline(__this, /*hidden argument*/NULL);
		G_B2_0 = (0.0f);
		G_B2_1 = L_11;
		G_B2_2 = __this;
		if (L_12)
		{
			G_B3_0 = (0.0f);
			G_B3_1 = L_11;
			G_B3_2 = __this;
			goto IL_0061;
		}
	}
	{
		G_B4_0 = (0.5f);
		G_B4_1 = G_B2_0;
		G_B4_2 = G_B2_1;
		G_B4_3 = G_B2_2;
		goto IL_0066;
	}

IL_0061:
	{
		G_B4_0 = (1.0f);
		G_B4_1 = G_B3_0;
		G_B4_2 = G_B3_1;
		G_B4_3 = G_B3_2;
	}

IL_0066:
	{
		float L_13;
		L_13 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(G_B4_2, G_B4_1, G_B4_0, /*hidden argument*/NULL);
		NullCheck(G_B4_3);
		((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)G_B4_3)->set_inputMagnitude_33(L_13);
		return;
	}

IL_0071:
	{
		// inputMagnitude = Mathf.Clamp(isSprinting ? newInput.magnitude + 0.5f : newInput.magnitude, 0, isSprinting ? sprintSpeed : runningSpeed);
		bool L_14;
		L_14 = vThirdPersonMotor_get_isSprinting_m98A252FE6362DE6468A47BBC3B0F7FCACA2B3EDD_inline(__this, /*hidden argument*/NULL);
		G_B6_0 = __this;
		if (L_14)
		{
			G_B7_0 = __this;
			goto IL_0083;
		}
	}
	{
		float L_15;
		L_15 = Vector2_get_magnitude_mD30DB8EB73C4A5CD395745AE1CA1C38DC61D2E85((Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 *)(&V_1), /*hidden argument*/NULL);
		G_B8_0 = L_15;
		G_B8_1 = G_B6_0;
		goto IL_0090;
	}

IL_0083:
	{
		float L_16;
		L_16 = Vector2_get_magnitude_mD30DB8EB73C4A5CD395745AE1CA1C38DC61D2E85((Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 *)(&V_1), /*hidden argument*/NULL);
		G_B8_0 = ((float)il2cpp_codegen_add((float)L_16, (float)(0.5f)));
		G_B8_1 = G_B7_0;
	}

IL_0090:
	{
		bool L_17;
		L_17 = vThirdPersonMotor_get_isSprinting_m98A252FE6362DE6468A47BBC3B0F7FCACA2B3EDD_inline(__this, /*hidden argument*/NULL);
		G_B9_0 = (0.0f);
		G_B9_1 = G_B8_0;
		G_B9_2 = G_B8_1;
		if (L_17)
		{
			G_B10_0 = (0.0f);
			G_B10_1 = G_B8_0;
			G_B10_2 = G_B8_1;
			goto IL_00a4;
		}
	}
	{
		G_B11_0 = (1.0f);
		G_B11_1 = G_B9_0;
		G_B11_2 = G_B9_1;
		G_B11_3 = G_B9_2;
		goto IL_00a9;
	}

IL_00a4:
	{
		G_B11_0 = (1.5f);
		G_B11_1 = G_B10_0;
		G_B11_2 = G_B10_1;
		G_B11_3 = G_B10_2;
	}

IL_00a9:
	{
		float L_18;
		L_18 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(G_B11_2, G_B11_1, G_B11_0, /*hidden argument*/NULL);
		NullCheck(G_B11_3);
		((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)G_B11_3)->set_inputMagnitude_33(L_18);
		// }
		return;
	}
}
// System.Void Invector.vCharacterController.vThirdPersonAnimator::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonAnimator__ctor_mBB69BF7A51196C407D664AD2A836167FF190352A (vThirdPersonAnimator_tDFBC4BC178521ACC390B82F0B5418961D2754CB0 * __this, const RuntimeMethod* method)
{
	{
		vThirdPersonMotor__ctor_m4AEDE34F41DF0A598D8C8DDB4BA8F5088AC7D06D(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vThirdPersonCamera::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonCamera_Start_m1BC82E1F083B92594F31E827D33F8B8F97D76454 (vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11 * __this, const RuntimeMethod* method)
{
	{
		// Init();
		vThirdPersonCamera_Init_mAFBED3541CD351DED52E79B067716668A5D56ED6(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void vThirdPersonCamera::Init()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonCamera_Init_mAFBED3541CD351DED52E79B067716668A5D56ED6 (vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisCamera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_mE17146EF5B0D8E9F9D2D2D94567BF211AD00D320_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9A2C600D86C06D98E5159D56D3B075E446BB8500);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (target == null)
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = __this->get_target_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000f;
		}
	}
	{
		// return;
		return;
	}

IL_000f:
	{
		// _camera = GetComponent<Camera>();
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_2;
		L_2 = Component_GetComponent_TisCamera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_mE17146EF5B0D8E9F9D2D2D94567BF211AD00D320(__this, /*hidden argument*/Component_GetComponent_TisCamera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_mE17146EF5B0D8E9F9D2D2D94567BF211AD00D320_RuntimeMethod_var);
		__this->set__camera_27(L_2);
		// currentTarget = target;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3 = __this->get_target_4();
		__this->set_currentTarget_20(L_3);
		// currentTargetPos = new Vector3(currentTarget.position.x, currentTarget.position.y + offSetPlayerPivot, currentTarget.position.z);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4 = __this->get_currentTarget_20();
		NullCheck(L_4);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_4, /*hidden argument*/NULL);
		float L_6 = L_5.get_x_2();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_7 = __this->get_currentTarget_20();
		NullCheck(L_7);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		L_8 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_7, /*hidden argument*/NULL);
		float L_9 = L_8.get_y_3();
		float L_10 = __this->get_offSetPlayerPivot_18();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_11 = __this->get_currentTarget_20();
		NullCheck(L_11);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		L_12 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_11, /*hidden argument*/NULL);
		float L_13 = L_12.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14;
		memset((&L_14), 0, sizeof(L_14));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_14), L_6, ((float)il2cpp_codegen_add((float)L_9, (float)L_10)), L_13, /*hidden argument*/NULL);
		__this->set_currentTargetPos_23(L_14);
		// targetLookAt = new GameObject("targetLookAt").transform;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_15 = (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)il2cpp_codegen_object_new(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_il2cpp_TypeInfo_var);
		GameObject__ctor_mDF8BF31EAE3E03F24421531B25FB4BEDB7C87144(L_15, _stringLiteral9A2C600D86C06D98E5159D56D3B075E446BB8500, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_16;
		L_16 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_15, /*hidden argument*/NULL);
		__this->set_targetLookAt_22(L_16);
		// targetLookAt.position = currentTarget.position;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_17 = __this->get_targetLookAt_22();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_18 = __this->get_currentTarget_20();
		NullCheck(L_18);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_19;
		L_19 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_17, L_19, /*hidden argument*/NULL);
		// targetLookAt.hideFlags = HideFlags.HideInHierarchy;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_20 = __this->get_targetLookAt_22();
		NullCheck(L_20);
		Object_set_hideFlags_m7DE229AF60B92F0C68819F77FEB27D775E66F3AC(L_20, 1, /*hidden argument*/NULL);
		// targetLookAt.rotation = currentTarget.rotation;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_21 = __this->get_targetLookAt_22();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_22 = __this->get_currentTarget_20();
		NullCheck(L_22);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_23;
		L_23 = Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200(L_22, /*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4(L_21, L_23, /*hidden argument*/NULL);
		// mouseY = currentTarget.eulerAngles.x;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_24 = __this->get_currentTarget_20();
		NullCheck(L_24);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_25;
		L_25 = Transform_get_eulerAngles_mCF1E10C36ED1F03804A1D10A9BAB272E0EA8766F(L_24, /*hidden argument*/NULL);
		float L_26 = L_25.get_x_2();
		__this->set_mouseY_29(L_26);
		// mouseX = currentTarget.eulerAngles.y;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_27 = __this->get_currentTarget_20();
		NullCheck(L_27);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_28;
		L_28 = Transform_get_eulerAngles_mCF1E10C36ED1F03804A1D10A9BAB272E0EA8766F(L_27, /*hidden argument*/NULL);
		float L_29 = L_28.get_y_3();
		__this->set_mouseX_30(L_29);
		// distance = defaultDistance;
		float L_30 = __this->get_defaultDistance_9();
		__this->set_distance_28(L_30);
		// currentHeight = height;
		float L_31 = __this->get_height_10();
		__this->set_currentHeight_31(L_31);
		// }
		return;
	}
}
// System.Void vThirdPersonCamera::FixedUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonCamera_FixedUpdate_m88B441002F7583FDB36C614EF1757AEB4D744AF6 (vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (target == null || targetLookAt == null) return;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = __this->get_target_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2 = __this->get_targetLookAt_22();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_2, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001d;
		}
	}

IL_001c:
	{
		// if (target == null || targetLookAt == null) return;
		return;
	}

IL_001d:
	{
		// CameraMovement();
		vThirdPersonCamera_CameraMovement_m0F154B3318E4D78CFA3D8C4C4B38446CD1B38BC2(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void vThirdPersonCamera::SetTarget(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonCamera_SetTarget_m895630C8744043672053E8D0C1EBBF84227E41E7 (vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___newTarget0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11 * G_B2_0 = NULL;
	vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11 * G_B1_0 = NULL;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * G_B3_0 = NULL;
	vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11 * G_B3_1 = NULL;
	{
		// currentTarget = newTarget ? newTarget : target;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = ___newTarget0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if (L_1)
		{
			G_B2_0 = __this;
			goto IL_0011;
		}
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2 = __this->get_target_4();
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_0012;
	}

IL_0011:
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3 = ___newTarget0;
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
	}

IL_0012:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_currentTarget_20(G_B3_0);
		// }
		return;
	}
}
// System.Void vThirdPersonCamera::SetMainTarget(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonCamera_SetMainTarget_m35387F3B3FC8B83915DC3441E9019F0EE7D878C7 (vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___newTarget0, const RuntimeMethod* method)
{
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// target = newTarget;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = ___newTarget0;
		__this->set_target_4(L_0);
		// currentTarget = newTarget;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1 = ___newTarget0;
		__this->set_currentTarget_20(L_1);
		// mouseY = currentTarget.rotation.eulerAngles.x;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2 = __this->get_currentTarget_20();
		NullCheck(L_2);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_3;
		L_3 = Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Quaternion_get_eulerAngles_m3DA616CAD670235A407E8A7A75925AA8E22338C3((Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 *)(&V_0), /*hidden argument*/NULL);
		float L_5 = L_4.get_x_2();
		__this->set_mouseY_29(L_5);
		// mouseX = currentTarget.rotation.eulerAngles.y;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6 = __this->get_currentTarget_20();
		NullCheck(L_6);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_7;
		L_7 = Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200(L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		L_8 = Quaternion_get_eulerAngles_m3DA616CAD670235A407E8A7A75925AA8E22338C3((Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 *)(&V_0), /*hidden argument*/NULL);
		float L_9 = L_8.get_y_3();
		__this->set_mouseX_30(L_9);
		// Init();
		vThirdPersonCamera_Init_mAFBED3541CD351DED52E79B067716668A5D56ED6(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// UnityEngine.Ray vThirdPersonCamera::ScreenPointToRay(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Ray_t2E9E67CC8B03EE6ED2BBF3D2C9C96DDF70E1D5E6  vThirdPersonCamera_ScreenPointToRay_m0FC7E21A5C73322AB0422F1257131C08D96E0144 (vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___Point0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisCamera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_mE17146EF5B0D8E9F9D2D2D94567BF211AD00D320_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return this.GetComponent<Camera>().ScreenPointToRay(Point);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_0;
		L_0 = Component_GetComponent_TisCamera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_mE17146EF5B0D8E9F9D2D2D94567BF211AD00D320(__this, /*hidden argument*/Component_GetComponent_TisCamera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_mE17146EF5B0D8E9F9D2D2D94567BF211AD00D320_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1 = ___Point0;
		NullCheck(L_0);
		Ray_t2E9E67CC8B03EE6ED2BBF3D2C9C96DDF70E1D5E6  L_2;
		L_2 = Camera_ScreenPointToRay_mD385213935A81030EDC604A39FD64761077CFBAB(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void vThirdPersonCamera::RotateCamera(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonCamera_RotateCamera_m38FD2DBF37023FD4885681827DFF017CF64FFC57 (vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11 * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		// mouseX += x * xMouseSensitivity;
		float L_0 = __this->get_mouseX_30();
		float L_1 = ___x0;
		float L_2 = __this->get_xMouseSensitivity_12();
		__this->set_mouseX_30(((float)il2cpp_codegen_add((float)L_0, (float)((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)))));
		// mouseY -= y * yMouseSensitivity;
		float L_3 = __this->get_mouseY_29();
		float L_4 = ___y1;
		float L_5 = __this->get_yMouseSensitivity_13();
		__this->set_mouseY_29(((float)il2cpp_codegen_subtract((float)L_3, (float)((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)))));
		// movementSpeed.x = x;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_6 = __this->get_address_of_movementSpeed_21();
		float L_7 = ___x0;
		L_6->set_x_0(L_7);
		// movementSpeed.y = -y;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_8 = __this->get_address_of_movementSpeed_21();
		float L_9 = ___y1;
		L_8->set_y_1(((-L_9)));
		// if (!lockCamera)
		bool L_10 = __this->get_lockCamera_7();
		if (L_10)
		{
			goto IL_0086;
		}
	}
	{
		// mouseY = vExtensions.ClampAngle(mouseY, yMinLimit, yMaxLimit);
		float L_11 = __this->get_mouseY_29();
		float L_12 = __this->get_yMinLimit_14();
		float L_13 = __this->get_yMaxLimit_15();
		float L_14;
		L_14 = vExtensions_ClampAngle_m748C49B4118E791FEB84C37FAE0006DE695EAC4B(L_11, L_12, L_13, /*hidden argument*/NULL);
		__this->set_mouseY_29(L_14);
		// mouseX = vExtensions.ClampAngle(mouseX, xMinLimit, xMaxLimit);
		float L_15 = __this->get_mouseX_30();
		float L_16 = __this->get_xMinLimit_36();
		float L_17 = __this->get_xMaxLimit_37();
		float L_18;
		L_18 = vExtensions_ClampAngle_m748C49B4118E791FEB84C37FAE0006DE695EAC4B(L_15, L_16, L_17, /*hidden argument*/NULL);
		__this->set_mouseX_30(L_18);
		// }
		return;
	}

IL_0086:
	{
		// mouseY = currentTarget.root.localEulerAngles.x;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_19 = __this->get_currentTarget_20();
		NullCheck(L_19);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_20;
		L_20 = Transform_get_root_mDEB1F3B4DB26B32CEED6DFFF734F85C79C4DDA91(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_21;
		L_21 = Transform_get_localEulerAngles_m4C442107F523737ADAB54855FDC1777A9B71D545(L_20, /*hidden argument*/NULL);
		float L_22 = L_21.get_x_2();
		__this->set_mouseY_29(L_22);
		// mouseX = currentTarget.root.localEulerAngles.y;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_23 = __this->get_currentTarget_20();
		NullCheck(L_23);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_24;
		L_24 = Transform_get_root_mDEB1F3B4DB26B32CEED6DFFF734F85C79C4DDA91(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_25;
		L_25 = Transform_get_localEulerAngles_m4C442107F523737ADAB54855FDC1777A9B71D545(L_24, /*hidden argument*/NULL);
		float L_26 = L_25.get_y_3();
		__this->set_mouseX_30(L_26);
		// }
		return;
	}
}
// System.Void vThirdPersonCamera::CameraMovement()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonCamera_CameraMovement_m0F154B3318E4D78CFA3D8C4C4B38446CD1B38BC2 (vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_1;
	memset((&V_1), 0, sizeof(V_1));
	RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89  V_2;
	memset((&V_2), 0, sizeof(V_2));
	ClipPlanePoints_tFA6B5EF4C87C83626BEAEA585214A9C4679433DA  V_3;
	memset((&V_3), 0, sizeof(V_3));
	ClipPlanePoints_tFA6B5EF4C87C83626BEAEA585214A9C4679433DA  V_4;
	memset((&V_4), 0, sizeof(V_4));
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  V_5;
	memset((&V_5), 0, sizeof(V_5));
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  V_6;
	memset((&V_6), 0, sizeof(V_6));
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	{
		// if (currentTarget == null)
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = __this->get_currentTarget_20();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000f;
		}
	}
	{
		// return;
		return;
	}

IL_000f:
	{
		// distance = Mathf.Lerp(distance, defaultDistance, smoothFollow * Time.deltaTime);
		float L_2 = __this->get_distance_28();
		float L_3 = __this->get_defaultDistance_9();
		float L_4 = __this->get_smoothFollow_11();
		float L_5;
		L_5 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		float L_6;
		L_6 = Mathf_Lerp_m8A2A50B945F42D579EDF44D5EE79E85A4DA59616(L_2, L_3, ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), /*hidden argument*/NULL);
		__this->set_distance_28(L_6);
		// cullingDistance = Mathf.Lerp(cullingDistance, distance, Time.deltaTime);
		float L_7 = __this->get_cullingDistance_32();
		float L_8 = __this->get_distance_28();
		float L_9;
		L_9 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		float L_10;
		L_10 = Mathf_Lerp_m8A2A50B945F42D579EDF44D5EE79E85A4DA59616(L_7, L_8, L_9, /*hidden argument*/NULL);
		__this->set_cullingDistance_32(L_10);
		// var camDir = (forward * targetLookAt.forward) + (rightOffset * targetLookAt.right);
		float L_11 = __this->get_forward_35();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_12 = __this->get_targetLookAt_22();
		NullCheck(L_12);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13;
		L_13 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_12, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14;
		L_14 = Vector3_op_Multiply_m079B29E4F58127F03BD52558C1FE1A528547328F_inline(L_11, L_13, /*hidden argument*/NULL);
		float L_15 = __this->get_rightOffset_8();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_16 = __this->get_targetLookAt_22();
		NullCheck(L_16);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_17;
		L_17 = Transform_get_right_mA8EB1882CCE8759766544448308C88D9200F06CE(L_16, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_18;
		L_18 = Vector3_op_Multiply_m079B29E4F58127F03BD52558C1FE1A528547328F_inline(L_15, L_17, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_19;
		L_19 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_14, L_18, /*hidden argument*/NULL);
		V_0 = L_19;
		// camDir = camDir.normalized;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20;
		L_20 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), /*hidden argument*/NULL);
		V_0 = L_20;
		// var targetPos = new Vector3(currentTarget.position.x, currentTarget.position.y + offSetPlayerPivot, currentTarget.position.z);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_21 = __this->get_currentTarget_20();
		NullCheck(L_21);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_22;
		L_22 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_21, /*hidden argument*/NULL);
		float L_23 = L_22.get_x_2();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_24 = __this->get_currentTarget_20();
		NullCheck(L_24);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_25;
		L_25 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_24, /*hidden argument*/NULL);
		float L_26 = L_25.get_y_3();
		float L_27 = __this->get_offSetPlayerPivot_18();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_28 = __this->get_currentTarget_20();
		NullCheck(L_28);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_29;
		L_29 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_28, /*hidden argument*/NULL);
		float L_30 = L_29.get_z_4();
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_1), L_23, ((float)il2cpp_codegen_add((float)L_26, (float)L_27)), L_30, /*hidden argument*/NULL);
		// currentTargetPos = targetPos;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_31 = V_1;
		__this->set_currentTargetPos_23(L_31);
		// desired_cPos = targetPos + new Vector3(0, height, 0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_32 = V_1;
		float L_33 = __this->get_height_10();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_34;
		memset((&L_34), 0, sizeof(L_34));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_34), (0.0f), L_33, (0.0f), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_35;
		L_35 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_32, L_34, /*hidden argument*/NULL);
		__this->set_desired_cPos_26(L_35);
		// current_cPos = currentTargetPos + new Vector3(0, currentHeight, 0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_36 = __this->get_currentTargetPos_23();
		float L_37 = __this->get_currentHeight_31();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_38;
		memset((&L_38), 0, sizeof(L_38));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_38), (0.0f), L_37, (0.0f), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_39;
		L_39 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_36, L_38, /*hidden argument*/NULL);
		__this->set_current_cPos_25(L_39);
		// ClipPlanePoints planePoints = _camera.NearClipPlanePoints(current_cPos + (camDir * (distance)), clipPlaneMargin);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_40 = __this->get__camera_27();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_41 = __this->get_current_cPos_25();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_42 = V_0;
		float L_43 = __this->get_distance_28();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_44;
		L_44 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_42, L_43, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_45;
		L_45 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_41, L_44, /*hidden argument*/NULL);
		float L_46 = __this->get_clipPlaneMargin_34();
		ClipPlanePoints_tFA6B5EF4C87C83626BEAEA585214A9C4679433DA  L_47;
		L_47 = vExtensions_NearClipPlanePoints_m95EBECC9448EE09DE106D39A4089ABFF27725772(L_40, L_45, L_46, /*hidden argument*/NULL);
		V_3 = L_47;
		// ClipPlanePoints oldPoints = _camera.NearClipPlanePoints(desired_cPos + (camDir * distance), clipPlaneMargin);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_48 = __this->get__camera_27();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_49 = __this->get_desired_cPos_26();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_50 = V_0;
		float L_51 = __this->get_distance_28();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_52;
		L_52 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_50, L_51, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_53;
		L_53 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_49, L_52, /*hidden argument*/NULL);
		float L_54 = __this->get_clipPlaneMargin_34();
		ClipPlanePoints_tFA6B5EF4C87C83626BEAEA585214A9C4679433DA  L_55;
		L_55 = vExtensions_NearClipPlanePoints_m95EBECC9448EE09DE106D39A4089ABFF27725772(L_48, L_53, L_54, /*hidden argument*/NULL);
		V_4 = L_55;
		// if (Physics.SphereCast(targetPos, checkHeightRadius, Vector3.up, out hitInfo, cullingHeight + 0.2f, cullingLayer))
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_56 = V_1;
		float L_57 = __this->get_checkHeightRadius_33();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_58;
		L_58 = Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50(/*hidden argument*/NULL);
		float L_59 = __this->get_cullingHeight_38();
		LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  L_60 = __this->get_cullingLayer_6();
		int32_t L_61;
		L_61 = LayerMask_op_Implicit_mD89E9970822613D8D19B2EBCA36C79391C287BE0(L_60, /*hidden argument*/NULL);
		bool L_62;
		L_62 = Physics_SphereCast_m9943744EA02A42417719F4A0CBE5822DA12F15BC(L_56, L_57, L_58, (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)(&V_2), ((float)il2cpp_codegen_add((float)L_59, (float)(0.200000003f))), L_61, /*hidden argument*/NULL);
		if (!L_62)
		{
			goto IL_01e7;
		}
	}
	{
		// var t = hitInfo.distance - 0.2f;
		float L_63;
		L_63 = RaycastHit_get_distance_m85FCA98D7957C3BF1D449CA1B48C116CCD6226FA((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)(&V_2), /*hidden argument*/NULL);
		V_7 = ((float)il2cpp_codegen_subtract((float)L_63, (float)(0.200000003f)));
		// t -= height;
		float L_64 = V_7;
		float L_65 = __this->get_height_10();
		V_7 = ((float)il2cpp_codegen_subtract((float)L_64, (float)L_65));
		// t /= (cullingHeight - height);
		float L_66 = V_7;
		float L_67 = __this->get_cullingHeight_38();
		float L_68 = __this->get_height_10();
		V_7 = ((float)((float)L_66/(float)((float)il2cpp_codegen_subtract((float)L_67, (float)L_68))));
		// cullingHeight = Mathf.Lerp(height, cullingHeight, Mathf.Clamp(t, 0.0f, 1.0f));
		float L_69 = __this->get_height_10();
		float L_70 = __this->get_cullingHeight_38();
		float L_71 = V_7;
		float L_72;
		L_72 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_71, (0.0f), (1.0f), /*hidden argument*/NULL);
		float L_73;
		L_73 = Mathf_Lerp_m8A2A50B945F42D579EDF44D5EE79E85A4DA59616(L_69, L_70, L_72, /*hidden argument*/NULL);
		__this->set_cullingHeight_38(L_73);
	}

IL_01e7:
	{
		// if (CullingRayCast(desired_cPos, oldPoints, out hitInfo, distance + 0.2f, cullingLayer, Color.blue))
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_74 = __this->get_desired_cPos_26();
		ClipPlanePoints_tFA6B5EF4C87C83626BEAEA585214A9C4679433DA  L_75 = V_4;
		float L_76 = __this->get_distance_28();
		LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  L_77 = __this->get_cullingLayer_6();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_78;
		L_78 = Color_get_blue_m6D62D515CA10A6E760848E1BFB997E27B90BD07B(/*hidden argument*/NULL);
		bool L_79;
		L_79 = vThirdPersonCamera_CullingRayCast_mF01736BAE8BA542FF4F8F62F93A398909E1C37A2(__this, L_74, L_75, (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)(&V_2), ((float)il2cpp_codegen_add((float)L_76, (float)(0.200000003f))), L_77, L_78, /*hidden argument*/NULL);
		if (!L_79)
		{
			goto IL_02a3;
		}
	}
	{
		// distance = hitInfo.distance - 0.2f;
		float L_80;
		L_80 = RaycastHit_get_distance_m85FCA98D7957C3BF1D449CA1B48C116CCD6226FA((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)(&V_2), /*hidden argument*/NULL);
		__this->set_distance_28(((float)il2cpp_codegen_subtract((float)L_80, (float)(0.200000003f))));
		// if (distance < defaultDistance)
		float L_81 = __this->get_distance_28();
		float L_82 = __this->get_defaultDistance_9();
		if ((!(((float)L_81) < ((float)L_82))))
		{
			goto IL_02af;
		}
	}
	{
		// var t = hitInfo.distance;
		float L_83;
		L_83 = RaycastHit_get_distance_m85FCA98D7957C3BF1D449CA1B48C116CCD6226FA((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)(&V_2), /*hidden argument*/NULL);
		V_8 = L_83;
		// t -= cullingMinDist;
		float L_84 = V_8;
		float L_85 = __this->get_cullingMinDist_39();
		V_8 = ((float)il2cpp_codegen_subtract((float)L_84, (float)L_85));
		// t /= cullingMinDist;
		float L_86 = V_8;
		float L_87 = __this->get_cullingMinDist_39();
		V_8 = ((float)((float)L_86/(float)L_87));
		// currentHeight = Mathf.Lerp(cullingHeight, height, Mathf.Clamp(t, 0.0f, 1.0f));
		float L_88 = __this->get_cullingHeight_38();
		float L_89 = __this->get_height_10();
		float L_90 = V_8;
		float L_91;
		L_91 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_90, (0.0f), (1.0f), /*hidden argument*/NULL);
		float L_92;
		L_92 = Mathf_Lerp_m8A2A50B945F42D579EDF44D5EE79E85A4DA59616(L_88, L_89, L_91, /*hidden argument*/NULL);
		__this->set_currentHeight_31(L_92);
		// current_cPos = currentTargetPos + new Vector3(0, currentHeight, 0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_93 = __this->get_currentTargetPos_23();
		float L_94 = __this->get_currentHeight_31();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_95;
		memset((&L_95), 0, sizeof(L_95));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_95), (0.0f), L_94, (0.0f), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_96;
		L_96 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_93, L_95, /*hidden argument*/NULL);
		__this->set_current_cPos_25(L_96);
		// }
		goto IL_02af;
	}

IL_02a3:
	{
		// currentHeight = height;
		float L_97 = __this->get_height_10();
		__this->set_currentHeight_31(L_97);
	}

IL_02af:
	{
		// if (CullingRayCast(current_cPos, planePoints, out hitInfo, distance, cullingLayer, Color.cyan)) distance = Mathf.Clamp(cullingDistance, 0.0f, defaultDistance);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_98 = __this->get_current_cPos_25();
		ClipPlanePoints_tFA6B5EF4C87C83626BEAEA585214A9C4679433DA  L_99 = V_3;
		float L_100 = __this->get_distance_28();
		LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  L_101 = __this->get_cullingLayer_6();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_102;
		L_102 = Color_get_cyan_m0C608BC083FD98C45C1F4F15AE803D288C647686(/*hidden argument*/NULL);
		bool L_103;
		L_103 = vThirdPersonCamera_CullingRayCast_mF01736BAE8BA542FF4F8F62F93A398909E1C37A2(__this, L_98, L_99, (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)(&V_2), L_100, L_101, L_102, /*hidden argument*/NULL);
		if (!L_103)
		{
			goto IL_02ed;
		}
	}
	{
		// if (CullingRayCast(current_cPos, planePoints, out hitInfo, distance, cullingLayer, Color.cyan)) distance = Mathf.Clamp(cullingDistance, 0.0f, defaultDistance);
		float L_104 = __this->get_cullingDistance_32();
		float L_105 = __this->get_defaultDistance_9();
		float L_106;
		L_106 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_104, (0.0f), L_105, /*hidden argument*/NULL);
		__this->set_distance_28(L_106);
	}

IL_02ed:
	{
		// var lookPoint = current_cPos + targetLookAt.forward * 2f;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_107 = __this->get_current_cPos_25();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_108 = __this->get_targetLookAt_22();
		NullCheck(L_108);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_109;
		L_109 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_108, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_110;
		L_110 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_109, (2.0f), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_111;
		L_111 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_107, L_110, /*hidden argument*/NULL);
		// lookPoint += (targetLookAt.right * Vector3.Dot(camDir * (distance), targetLookAt.right));
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_112 = __this->get_targetLookAt_22();
		NullCheck(L_112);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_113;
		L_113 = Transform_get_right_mA8EB1882CCE8759766544448308C88D9200F06CE(L_112, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_114 = V_0;
		float L_115 = __this->get_distance_28();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_116;
		L_116 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_114, L_115, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_117 = __this->get_targetLookAt_22();
		NullCheck(L_117);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_118;
		L_118 = Transform_get_right_mA8EB1882CCE8759766544448308C88D9200F06CE(L_117, /*hidden argument*/NULL);
		float L_119;
		L_119 = Vector3_Dot_mD19905B093915BA12852732EA27AA2DBE030D11F_inline(L_116, L_118, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_120;
		L_120 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_113, L_119, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_121;
		L_121 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_111, L_120, /*hidden argument*/NULL);
		// targetLookAt.position = current_cPos;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_122 = __this->get_targetLookAt_22();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_123 = __this->get_current_cPos_25();
		NullCheck(L_122);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_122, L_123, /*hidden argument*/NULL);
		// Quaternion newRot = Quaternion.Euler(mouseY, mouseX, 0);
		float L_124 = __this->get_mouseY_29();
		float L_125 = __this->get_mouseX_30();
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_126;
		L_126 = Quaternion_Euler_m37BF99FFFA09F4B3F83DC066641B82C59B19A9C3(L_124, L_125, (0.0f), /*hidden argument*/NULL);
		V_5 = L_126;
		// targetLookAt.rotation = Quaternion.Slerp(targetLookAt.rotation, newRot, smoothCameraRotation * Time.deltaTime);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_127 = __this->get_targetLookAt_22();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_128 = __this->get_targetLookAt_22();
		NullCheck(L_128);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_129;
		L_129 = Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200(L_128, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_130 = V_5;
		float L_131 = __this->get_smoothCameraRotation_5();
		float L_132;
		L_132 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_133;
		L_133 = Quaternion_Slerp_m6D2BD18286254E28D2288B51962EC71F85C7B5C8(L_129, L_130, ((float)il2cpp_codegen_multiply((float)L_131, (float)L_132)), /*hidden argument*/NULL);
		NullCheck(L_127);
		Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4(L_127, L_133, /*hidden argument*/NULL);
		// transform.position = current_cPos + (camDir * (distance));
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_134;
		L_134 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_135 = __this->get_current_cPos_25();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_136 = V_0;
		float L_137 = __this->get_distance_28();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_138;
		L_138 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_136, L_137, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_139;
		L_139 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_135, L_138, /*hidden argument*/NULL);
		NullCheck(L_134);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_134, L_139, /*hidden argument*/NULL);
		// var rotation = Quaternion.LookRotation((lookPoint) - transform.position);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_140;
		L_140 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_140);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_141;
		L_141 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_140, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_142;
		L_142 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_121, L_141, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_143;
		L_143 = Quaternion_LookRotation_m1B0BEBEBCC384324A6771B9EAC89761F73E1D6BF(L_142, /*hidden argument*/NULL);
		V_6 = L_143;
		// transform.rotation = rotation;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_144;
		L_144 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_145 = V_6;
		NullCheck(L_144);
		Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4(L_144, L_145, /*hidden argument*/NULL);
		// movementSpeed = Vector2.zero;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_146;
		L_146 = Vector2_get_zero_m621041B9DF5FAE86C1EF4CB28C224FEA089CB828(/*hidden argument*/NULL);
		__this->set_movementSpeed_21(L_146);
		// }
		return;
	}
}
// System.Boolean vThirdPersonCamera::CullingRayCast(UnityEngine.Vector3,Invector.ClipPlanePoints,UnityEngine.RaycastHit&,System.Single,UnityEngine.LayerMask,UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool vThirdPersonCamera_CullingRayCast_mF01736BAE8BA542FF4F8F62F93A398909E1C37A2 (vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___from0, ClipPlanePoints_tFA6B5EF4C87C83626BEAEA585214A9C4679433DA  ____to1, RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * ___hitInfo2, float ___distance3, LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___cullingLayer4, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color5, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		// bool value = false;
		V_0 = (bool)0;
		// if (Physics.Raycast(from, _to.LowerLeft - from, out hitInfo, distance, cullingLayer))
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___from0;
		ClipPlanePoints_tFA6B5EF4C87C83626BEAEA585214A9C4679433DA  L_1 = ____to1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = L_1.get_LowerLeft_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = ___from0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_2, L_3, /*hidden argument*/NULL);
		RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * L_5 = ___hitInfo2;
		float L_6 = ___distance3;
		LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  L_7 = ___cullingLayer4;
		int32_t L_8;
		L_8 = LayerMask_op_Implicit_mD89E9970822613D8D19B2EBCA36C79391C287BE0(L_7, /*hidden argument*/NULL);
		bool L_9;
		L_9 = Physics_Raycast_mCBD5F7D498C246713EDDBB446E97205DA206C49C(L_0, L_4, (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)L_5, L_6, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_002e;
		}
	}
	{
		// value = true;
		V_0 = (bool)1;
		// cullingDistance = hitInfo.distance;
		RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * L_10 = ___hitInfo2;
		float L_11;
		L_11 = RaycastHit_get_distance_m85FCA98D7957C3BF1D449CA1B48C116CCD6226FA((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)L_10, /*hidden argument*/NULL);
		__this->set_cullingDistance_32(L_11);
	}

IL_002e:
	{
		// if (Physics.Raycast(from, _to.LowerRight - from, out hitInfo, distance, cullingLayer))
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12 = ___from0;
		ClipPlanePoints_tFA6B5EF4C87C83626BEAEA585214A9C4679433DA  L_13 = ____to1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14 = L_13.get_LowerRight_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15 = ___from0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16;
		L_16 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_14, L_15, /*hidden argument*/NULL);
		RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * L_17 = ___hitInfo2;
		float L_18 = ___distance3;
		LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  L_19 = ___cullingLayer4;
		int32_t L_20;
		L_20 = LayerMask_op_Implicit_mD89E9970822613D8D19B2EBCA36C79391C287BE0(L_19, /*hidden argument*/NULL);
		bool L_21;
		L_21 = Physics_Raycast_mCBD5F7D498C246713EDDBB446E97205DA206C49C(L_12, L_16, (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)L_17, L_18, L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_0068;
		}
	}
	{
		// value = true;
		V_0 = (bool)1;
		// if (cullingDistance > hitInfo.distance) cullingDistance = hitInfo.distance;
		float L_22 = __this->get_cullingDistance_32();
		RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * L_23 = ___hitInfo2;
		float L_24;
		L_24 = RaycastHit_get_distance_m85FCA98D7957C3BF1D449CA1B48C116CCD6226FA((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)L_23, /*hidden argument*/NULL);
		if ((!(((float)L_22) > ((float)L_24))))
		{
			goto IL_0068;
		}
	}
	{
		// if (cullingDistance > hitInfo.distance) cullingDistance = hitInfo.distance;
		RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * L_25 = ___hitInfo2;
		float L_26;
		L_26 = RaycastHit_get_distance_m85FCA98D7957C3BF1D449CA1B48C116CCD6226FA((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)L_25, /*hidden argument*/NULL);
		__this->set_cullingDistance_32(L_26);
	}

IL_0068:
	{
		// if (Physics.Raycast(from, _to.UpperLeft - from, out hitInfo, distance, cullingLayer))
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_27 = ___from0;
		ClipPlanePoints_tFA6B5EF4C87C83626BEAEA585214A9C4679433DA  L_28 = ____to1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_29 = L_28.get_UpperLeft_0();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_30 = ___from0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_31;
		L_31 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_29, L_30, /*hidden argument*/NULL);
		RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * L_32 = ___hitInfo2;
		float L_33 = ___distance3;
		LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  L_34 = ___cullingLayer4;
		int32_t L_35;
		L_35 = LayerMask_op_Implicit_mD89E9970822613D8D19B2EBCA36C79391C287BE0(L_34, /*hidden argument*/NULL);
		bool L_36;
		L_36 = Physics_Raycast_mCBD5F7D498C246713EDDBB446E97205DA206C49C(L_27, L_31, (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)L_32, L_33, L_35, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_00a2;
		}
	}
	{
		// value = true;
		V_0 = (bool)1;
		// if (cullingDistance > hitInfo.distance) cullingDistance = hitInfo.distance;
		float L_37 = __this->get_cullingDistance_32();
		RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * L_38 = ___hitInfo2;
		float L_39;
		L_39 = RaycastHit_get_distance_m85FCA98D7957C3BF1D449CA1B48C116CCD6226FA((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)L_38, /*hidden argument*/NULL);
		if ((!(((float)L_37) > ((float)L_39))))
		{
			goto IL_00a2;
		}
	}
	{
		// if (cullingDistance > hitInfo.distance) cullingDistance = hitInfo.distance;
		RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * L_40 = ___hitInfo2;
		float L_41;
		L_41 = RaycastHit_get_distance_m85FCA98D7957C3BF1D449CA1B48C116CCD6226FA((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)L_40, /*hidden argument*/NULL);
		__this->set_cullingDistance_32(L_41);
	}

IL_00a2:
	{
		// if (Physics.Raycast(from, _to.UpperRight - from, out hitInfo, distance, cullingLayer))
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_42 = ___from0;
		ClipPlanePoints_tFA6B5EF4C87C83626BEAEA585214A9C4679433DA  L_43 = ____to1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_44 = L_43.get_UpperRight_1();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_45 = ___from0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_46;
		L_46 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_44, L_45, /*hidden argument*/NULL);
		RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * L_47 = ___hitInfo2;
		float L_48 = ___distance3;
		LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  L_49 = ___cullingLayer4;
		int32_t L_50;
		L_50 = LayerMask_op_Implicit_mD89E9970822613D8D19B2EBCA36C79391C287BE0(L_49, /*hidden argument*/NULL);
		bool L_51;
		L_51 = Physics_Raycast_mCBD5F7D498C246713EDDBB446E97205DA206C49C(L_42, L_46, (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)L_47, L_48, L_50, /*hidden argument*/NULL);
		if (!L_51)
		{
			goto IL_00dc;
		}
	}
	{
		// value = true;
		V_0 = (bool)1;
		// if (cullingDistance > hitInfo.distance) cullingDistance = hitInfo.distance;
		float L_52 = __this->get_cullingDistance_32();
		RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * L_53 = ___hitInfo2;
		float L_54;
		L_54 = RaycastHit_get_distance_m85FCA98D7957C3BF1D449CA1B48C116CCD6226FA((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)L_53, /*hidden argument*/NULL);
		if ((!(((float)L_52) > ((float)L_54))))
		{
			goto IL_00dc;
		}
	}
	{
		// if (cullingDistance > hitInfo.distance) cullingDistance = hitInfo.distance;
		RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * L_55 = ___hitInfo2;
		float L_56;
		L_56 = RaycastHit_get_distance_m85FCA98D7957C3BF1D449CA1B48C116CCD6226FA((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)L_55, /*hidden argument*/NULL);
		__this->set_cullingDistance_32(L_56);
	}

IL_00dc:
	{
		// return hitInfo.collider && value;
		RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * L_57 = ___hitInfo2;
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_58;
		L_58 = RaycastHit_get_collider_m13A3DE16FBC631E0A1F987E0B22CE70AF8AB539E((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)L_57, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_59;
		L_59 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_58, /*hidden argument*/NULL);
		bool L_60 = V_0;
		return (bool)((int32_t)((int32_t)L_59&(int32_t)L_60));
	}
}
// System.Void vThirdPersonCamera::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonCamera__ctor_m7B933687272033ECAFFA0CD60EA7699EE32616C6 (vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11 * __this, const RuntimeMethod* method)
{
	{
		// public float smoothCameraRotation = 12f;
		__this->set_smoothCameraRotation_5((12.0f));
		// public LayerMask cullingLayer = 1 << 0;
		LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  L_0;
		L_0 = LayerMask_op_Implicit_mC7EE32122D2A4786D3C00B93E41604B71BF1397C(1, /*hidden argument*/NULL);
		__this->set_cullingLayer_6(L_0);
		// public float defaultDistance = 2.5f;
		__this->set_defaultDistance_9((2.5f));
		// public float height = 1.4f;
		__this->set_height_10((1.39999998f));
		// public float smoothFollow = 10f;
		__this->set_smoothFollow_11((10.0f));
		// public float xMouseSensitivity = 3f;
		__this->set_xMouseSensitivity_12((3.0f));
		// public float yMouseSensitivity = 3f;
		__this->set_yMouseSensitivity_13((3.0f));
		// public float yMinLimit = -40f;
		__this->set_yMinLimit_14((-40.0f));
		// public float yMaxLimit = 80f;
		__this->set_yMaxLimit_15((80.0f));
		// private float distance = 5f;
		__this->set_distance_28((5.0f));
		// private float checkHeightRadius = 0.4f;
		__this->set_checkHeightRadius_33((0.400000006f));
		// private float forward = -1f;
		__this->set_forward_35((-1.0f));
		// private float xMinLimit = -360f;
		__this->set_xMinLimit_36((-360.0f));
		// private float xMaxLimit = 360f;
		__this->set_xMaxLimit_37((360.0f));
		// private float cullingHeight = 0.2f;
		__this->set_cullingHeight_38((0.200000003f));
		// private float cullingMinDist = 0.1f;
		__this->set_cullingMinDist_39((0.100000001f));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Invector.vCharacterController.vThirdPersonController::ControlAnimatorRootMotion()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonController_ControlAnimatorRootMotion_m5A3282D8BF3432ACE3DA138BF92382CFB7CE0A9D (vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * __this, const RuntimeMethod* method)
{
	{
		// if (!this.enabled) return;
		bool L_0;
		L_0 = Behaviour_get_enabled_m08077AB79934634E1EAE909C2B482BEF4C15A800(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		// if (!this.enabled) return;
		return;
	}

IL_0009:
	{
		// if (inputSmooth == Vector3.zero)
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_inputSmooth_50();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		bool L_3;
		L_3 = Vector3_op_Equality_m8A98C7F38641110A2F90445EF8E98ECE14B08296(L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0047;
		}
	}
	{
		// transform.position = animator.rootPosition;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4;
		L_4 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_5 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_animator_23();
		NullCheck(L_5);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		L_6 = Animator_get_rootPosition_mAF11A46CB8D953A1947FFF512F143899B3B9BFE9(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_4, L_6, /*hidden argument*/NULL);
		// transform.rotation = animator.rootRotation;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_7;
		L_7 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_8 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_animator_23();
		NullCheck(L_8);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_9;
		L_9 = Animator_get_rootRotation_mF887DA02BAFB6FE896257F3FC4DFCA0F670C4933(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4(L_7, L_9, /*hidden argument*/NULL);
	}

IL_0047:
	{
		// if (useRootMotion)
		bool L_10 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_useRootMotion_4();
		if (!L_10)
		{
			goto IL_005b;
		}
	}
	{
		// MoveCharacter(moveDirection);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_moveDirection_51();
		VirtActionInvoker1< Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  >::Invoke(6 /* System.Void Invector.vCharacterController.vThirdPersonMotor::MoveCharacter(UnityEngine.Vector3) */, __this, L_11);
	}

IL_005b:
	{
		// }
		return;
	}
}
// System.Void Invector.vCharacterController.vThirdPersonController::ControlLocomotionType()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonController_ControlLocomotionType_m672B81295FB81C5F3765030F59B1ECEADB2B7D36 (vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LocomotionType_t273049CC7616CFB97FFD00D80B9900878D3B4C6A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (lockMovement) return;
		bool L_0 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_lockMovement_44();
		if (!L_0)
		{
			goto IL_0009;
		}
	}
	{
		// if (lockMovement) return;
		return;
	}

IL_0009:
	{
		// if (locomotionType.Equals(LocomotionType.FreeWithStrafe) && !isStrafing || locomotionType.Equals(LocomotionType.OnlyFree))
		int32_t* L_1 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_address_of_locomotionType_8();
		int32_t L_2 = 0;
		RuntimeObject * L_3 = Box(LocomotionType_t273049CC7616CFB97FFD00D80B9900878D3B4C6A_il2cpp_TypeInfo_var, &L_2);
		RuntimeObject * L_4 = Box(LocomotionType_t273049CC7616CFB97FFD00D80B9900878D3B4C6A_il2cpp_TypeInfo_var, L_1);
		NullCheck(L_4);
		bool L_5;
		L_5 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_4, L_3);
		*L_1 = *(int32_t*)UnBox(L_4);
		if (!L_5)
		{
			goto IL_002a;
		}
	}
	{
		bool L_6;
		L_6 = vThirdPersonMotor_get_isStrafing_m3FB9147D8588055A220D47101B80C6FFD2B2E791_inline(__this, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0043;
		}
	}

IL_002a:
	{
		int32_t* L_7 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_address_of_locomotionType_8();
		int32_t L_8 = 2;
		RuntimeObject * L_9 = Box(LocomotionType_t273049CC7616CFB97FFD00D80B9900878D3B4C6A_il2cpp_TypeInfo_var, &L_8);
		RuntimeObject * L_10 = Box(LocomotionType_t273049CC7616CFB97FFD00D80B9900878D3B4C6A_il2cpp_TypeInfo_var, L_7);
		NullCheck(L_10);
		bool L_11;
		L_11 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_10, L_9);
		*L_7 = *(int32_t*)UnBox(L_10);
		if (!L_11)
		{
			goto IL_005d;
		}
	}

IL_0043:
	{
		// SetControllerMoveSpeed(freeSpeed);
		vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * L_12 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_freeSpeed_9();
		VirtActionInvoker1< vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * >::Invoke(5 /* System.Void Invector.vCharacterController.vThirdPersonMotor::SetControllerMoveSpeed(Invector.vCharacterController.vThirdPersonMotor/vMovementSpeed) */, __this, L_12);
		// SetAnimatorMoveSpeed(freeSpeed);
		vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * L_13 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_freeSpeed_9();
		VirtActionInvoker1< vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * >::Invoke(20 /* System.Void Invector.vCharacterController.vThirdPersonAnimator::SetAnimatorMoveSpeed(Invector.vCharacterController.vThirdPersonMotor/vMovementSpeed) */, __this, L_13);
		// }
		goto IL_00b6;
	}

IL_005d:
	{
		// else if (locomotionType.Equals(LocomotionType.OnlyStrafe) || locomotionType.Equals(LocomotionType.FreeWithStrafe) && isStrafing)
		int32_t* L_14 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_address_of_locomotionType_8();
		int32_t L_15 = 1;
		RuntimeObject * L_16 = Box(LocomotionType_t273049CC7616CFB97FFD00D80B9900878D3B4C6A_il2cpp_TypeInfo_var, &L_15);
		RuntimeObject * L_17 = Box(LocomotionType_t273049CC7616CFB97FFD00D80B9900878D3B4C6A_il2cpp_TypeInfo_var, L_14);
		NullCheck(L_17);
		bool L_18;
		L_18 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_17, L_16);
		*L_14 = *(int32_t*)UnBox(L_17);
		if (L_18)
		{
			goto IL_0097;
		}
	}
	{
		int32_t* L_19 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_address_of_locomotionType_8();
		int32_t L_20 = 0;
		RuntimeObject * L_21 = Box(LocomotionType_t273049CC7616CFB97FFD00D80B9900878D3B4C6A_il2cpp_TypeInfo_var, &L_20);
		RuntimeObject * L_22 = Box(LocomotionType_t273049CC7616CFB97FFD00D80B9900878D3B4C6A_il2cpp_TypeInfo_var, L_19);
		NullCheck(L_22);
		bool L_23;
		L_23 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_22, L_21);
		*L_19 = *(int32_t*)UnBox(L_22);
		if (!L_23)
		{
			goto IL_00b6;
		}
	}
	{
		bool L_24;
		L_24 = vThirdPersonMotor_get_isStrafing_m3FB9147D8588055A220D47101B80C6FFD2B2E791_inline(__this, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00b6;
		}
	}

IL_0097:
	{
		// isStrafing = true;
		vThirdPersonMotor_set_isStrafing_m90A9F59590C2DABE76449CE57C8FBCCCC9F39EFE_inline(__this, (bool)1, /*hidden argument*/NULL);
		// SetControllerMoveSpeed(strafeSpeed);
		vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * L_25 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_strafeSpeed_10();
		VirtActionInvoker1< vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * >::Invoke(5 /* System.Void Invector.vCharacterController.vThirdPersonMotor::SetControllerMoveSpeed(Invector.vCharacterController.vThirdPersonMotor/vMovementSpeed) */, __this, L_25);
		// SetAnimatorMoveSpeed(strafeSpeed);
		vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * L_26 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_strafeSpeed_10();
		VirtActionInvoker1< vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * >::Invoke(20 /* System.Void Invector.vCharacterController.vThirdPersonAnimator::SetAnimatorMoveSpeed(Invector.vCharacterController.vThirdPersonMotor/vMovementSpeed) */, __this, L_26);
	}

IL_00b6:
	{
		// if (!useRootMotion)
		bool L_27 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_useRootMotion_4();
		if (L_27)
		{
			goto IL_00ca;
		}
	}
	{
		// MoveCharacter(moveDirection);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_28 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_moveDirection_51();
		VirtActionInvoker1< Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  >::Invoke(6 /* System.Void Invector.vCharacterController.vThirdPersonMotor::MoveCharacter(UnityEngine.Vector3) */, __this, L_28);
	}

IL_00ca:
	{
		// }
		return;
	}
}
// System.Void Invector.vCharacterController.vThirdPersonController::ControlRotationType()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonController_ControlRotationType_m8708EBDBAB4C5DBE1590075F6E3A1967A560273A (vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	int32_t G_B7_0 = 0;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  G_B10_0;
	memset((&G_B10_0), 0, sizeof(G_B10_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  G_B10_1;
	memset((&G_B10_1), 0, sizeof(G_B10_1));
	vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * G_B10_2 = NULL;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  G_B9_0;
	memset((&G_B9_0), 0, sizeof(G_B9_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  G_B9_1;
	memset((&G_B9_1), 0, sizeof(G_B9_1));
	vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * G_B9_2 = NULL;
	float G_B11_0 = 0.0f;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  G_B11_1;
	memset((&G_B11_1), 0, sizeof(G_B11_1));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  G_B11_2;
	memset((&G_B11_2), 0, sizeof(G_B11_2));
	vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * G_B11_3 = NULL;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  G_B19_0;
	memset((&G_B19_0), 0, sizeof(G_B19_0));
	{
		// if (lockRotation) return;
		bool L_0 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_lockRotation_45();
		if (!L_0)
		{
			goto IL_0009;
		}
	}
	{
		// if (lockRotation) return;
		return;
	}

IL_0009:
	{
		// bool validInput = input != Vector3.zero || (isStrafing ? strafeSpeed.rotateWithCamera : freeSpeed.rotateWithCamera);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_input_48();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		bool L_3;
		L_3 = Vector3_op_Inequality_m15190A795B416EB699E69E6190DE6F1C1F208710(L_1, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_003d;
		}
	}
	{
		bool L_4;
		L_4 = vThirdPersonMotor_get_isStrafing_m3FB9147D8588055A220D47101B80C6FFD2B2E791_inline(__this, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0030;
		}
	}
	{
		vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * L_5 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_freeSpeed_9();
		NullCheck(L_5);
		bool L_6 = L_5->get_rotateWithCamera_4();
		G_B7_0 = ((int32_t)(L_6));
		goto IL_003e;
	}

IL_0030:
	{
		vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * L_7 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_strafeSpeed_10();
		NullCheck(L_7);
		bool L_8 = L_7->get_rotateWithCamera_4();
		G_B7_0 = ((int32_t)(L_8));
		goto IL_003e;
	}

IL_003d:
	{
		G_B7_0 = 1;
	}

IL_003e:
	{
		// if (validInput)
		if (!G_B7_0)
		{
			goto IL_00df;
		}
	}
	{
		// inputSmooth = Vector3.Lerp(inputSmooth, input, (isStrafing ? strafeSpeed.movementSmooth : freeSpeed.movementSmooth) * Time.deltaTime);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_inputSmooth_50();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_input_48();
		bool L_11;
		L_11 = vThirdPersonMotor_get_isStrafing_m3FB9147D8588055A220D47101B80C6FFD2B2E791_inline(__this, /*hidden argument*/NULL);
		G_B9_0 = L_10;
		G_B9_1 = L_9;
		G_B9_2 = __this;
		if (L_11)
		{
			G_B10_0 = L_10;
			G_B10_1 = L_9;
			G_B10_2 = __this;
			goto IL_0065;
		}
	}
	{
		vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * L_12 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_freeSpeed_9();
		NullCheck(L_12);
		float L_13 = L_12->get_movementSmooth_0();
		G_B11_0 = L_13;
		G_B11_1 = G_B9_0;
		G_B11_2 = G_B9_1;
		G_B11_3 = G_B9_2;
		goto IL_0070;
	}

IL_0065:
	{
		vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * L_14 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_strafeSpeed_10();
		NullCheck(L_14);
		float L_15 = L_14->get_movementSmooth_0();
		G_B11_0 = L_15;
		G_B11_1 = G_B10_0;
		G_B11_2 = G_B10_1;
		G_B11_3 = G_B10_2;
	}

IL_0070:
	{
		float L_16;
		L_16 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_17;
		L_17 = Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524_inline(G_B11_2, G_B11_1, ((float)il2cpp_codegen_multiply((float)G_B11_0, (float)L_16)), /*hidden argument*/NULL);
		NullCheck(G_B11_3);
		((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)G_B11_3)->set_inputSmooth_50(L_17);
		// Vector3 dir = (isStrafing && (!isSprinting || sprintOnlyFree == false) || (freeSpeed.rotateWithCamera && input == Vector3.zero)) && rotateTarget ? rotateTarget.forward : moveDirection;
		bool L_18;
		L_18 = vThirdPersonMotor_get_isStrafing_m3FB9147D8588055A220D47101B80C6FFD2B2E791_inline(__this, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0098;
		}
	}
	{
		bool L_19;
		L_19 = vThirdPersonMotor_get_isSprinting_m98A252FE6362DE6468A47BBC3B0F7FCACA2B3EDD_inline(__this, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00b7;
		}
	}
	{
		bool L_20 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_sprintOnlyFree_7();
		if (!L_20)
		{
			goto IL_00b7;
		}
	}

IL_0098:
	{
		vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * L_21 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_freeSpeed_9();
		NullCheck(L_21);
		bool L_22 = L_21->get_rotateWithCamera_4();
		if (!L_22)
		{
			goto IL_00c4;
		}
	}
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_23 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_input_48();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_24;
		L_24 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		bool L_25;
		L_25 = Vector3_op_Equality_m8A98C7F38641110A2F90445EF8E98ECE14B08296(L_23, L_24, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00c4;
		}
	}

IL_00b7:
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_26 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_rotateTarget_47();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_27;
		L_27 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_26, /*hidden argument*/NULL);
		if (L_27)
		{
			goto IL_00cc;
		}
	}

IL_00c4:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_28 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_moveDirection_51();
		G_B19_0 = L_28;
		goto IL_00d7;
	}

IL_00cc:
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_29 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_rotateTarget_47();
		NullCheck(L_29);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_30;
		L_30 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_29, /*hidden argument*/NULL);
		G_B19_0 = L_30;
	}

IL_00d7:
	{
		V_0 = G_B19_0;
		// RotateToDirection(dir);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_31 = V_0;
		VirtActionInvoker1< Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  >::Invoke(9 /* System.Void Invector.vCharacterController.vThirdPersonMotor::RotateToDirection(UnityEngine.Vector3) */, __this, L_31);
	}

IL_00df:
	{
		// }
		return;
	}
}
// System.Void Invector.vCharacterController.vThirdPersonController::UpdateMoveDirection(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonController_UpdateMoveDirection_m84010F9B7CF5694FEE7246E7A3BCFC2D6000BFB7 (vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___referenceTransform0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  G_B3_0;
	memset((&G_B3_0), 0, sizeof(G_B3_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  G_B3_1;
	memset((&G_B3_1), 0, sizeof(G_B3_1));
	vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * G_B3_2 = NULL;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  G_B2_0;
	memset((&G_B2_0), 0, sizeof(G_B2_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  G_B2_1;
	memset((&G_B2_1), 0, sizeof(G_B2_1));
	vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * G_B2_2 = NULL;
	float G_B4_0 = 0.0f;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  G_B4_1;
	memset((&G_B4_1), 0, sizeof(G_B4_1));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  G_B4_2;
	memset((&G_B4_2), 0, sizeof(G_B4_2));
	vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * G_B4_3 = NULL;
	{
		// if (input.magnitude <= 0.01)
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_0 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_address_of_input_48();
		float L_1;
		L_1 = Vector3_get_magnitude_mDDD40612220D8104E77E993E18A101A69A944991((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_0, /*hidden argument*/NULL);
		if ((!(((double)((double)((double)L_1))) <= ((double)(0.01)))))
		{
			goto IL_0054;
		}
	}
	{
		// moveDirection = Vector3.Lerp(moveDirection, Vector3.zero, (isStrafing ? strafeSpeed.movementSmooth : freeSpeed.movementSmooth) * Time.deltaTime);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_moveDirection_51();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		bool L_4;
		L_4 = vThirdPersonMotor_get_isStrafing_m3FB9147D8588055A220D47101B80C6FFD2B2E791_inline(__this, /*hidden argument*/NULL);
		G_B2_0 = L_3;
		G_B2_1 = L_2;
		G_B2_2 = __this;
		if (L_4)
		{
			G_B3_0 = L_3;
			G_B3_1 = L_2;
			G_B3_2 = __this;
			goto IL_0038;
		}
	}
	{
		vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * L_5 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_freeSpeed_9();
		NullCheck(L_5);
		float L_6 = L_5->get_movementSmooth_0();
		G_B4_0 = L_6;
		G_B4_1 = G_B2_0;
		G_B4_2 = G_B2_1;
		G_B4_3 = G_B2_2;
		goto IL_0043;
	}

IL_0038:
	{
		vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * L_7 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_strafeSpeed_10();
		NullCheck(L_7);
		float L_8 = L_7->get_movementSmooth_0();
		G_B4_0 = L_8;
		G_B4_1 = G_B3_0;
		G_B4_2 = G_B3_1;
		G_B4_3 = G_B3_2;
	}

IL_0043:
	{
		float L_9;
		L_9 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10;
		L_10 = Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524_inline(G_B4_2, G_B4_1, ((float)il2cpp_codegen_multiply((float)G_B4_0, (float)L_9)), /*hidden argument*/NULL);
		NullCheck(G_B4_3);
		((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)G_B4_3)->set_moveDirection_51(L_10);
		// return;
		return;
	}

IL_0054:
	{
		// if (referenceTransform && !rotateByWorld)
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_11 = ___referenceTransform0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_12;
		L_12 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00bb;
		}
	}
	{
		bool L_13 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_rotateByWorld_5();
		if (L_13)
		{
			goto IL_00bb;
		}
	}
	{
		// var right = referenceTransform.right;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_14 = ___referenceTransform0;
		NullCheck(L_14);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15;
		L_15 = Transform_get_right_mA8EB1882CCE8759766544448308C88D9200F06CE(L_14, /*hidden argument*/NULL);
		V_0 = L_15;
		// right.y = 0;
		(&V_0)->set_y_3((0.0f));
		// var forward = Quaternion.AngleAxis(-90, Vector3.up) * right;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16;
		L_16 = Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50(/*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_17;
		L_17 = Quaternion_AngleAxis_m4644D20F58ADF03E9EA297CB4A845E5BCDA1E398((-90.0f), L_16, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_18 = V_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_19;
		L_19 = Quaternion_op_Multiply_mDC5F913E6B21FEC72AB2CF737D34CC6C7A69803D(L_17, L_18, /*hidden argument*/NULL);
		V_1 = L_19;
		// moveDirection = (inputSmooth.x * right) + (inputSmooth.z * forward);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_20 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_address_of_inputSmooth_50();
		float L_21 = L_20->get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_22 = V_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_23;
		L_23 = Vector3_op_Multiply_m079B29E4F58127F03BD52558C1FE1A528547328F_inline(L_21, L_22, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_24 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_address_of_inputSmooth_50();
		float L_25 = L_24->get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_26 = V_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_27;
		L_27 = Vector3_op_Multiply_m079B29E4F58127F03BD52558C1FE1A528547328F_inline(L_25, L_26, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_28;
		L_28 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_23, L_27, /*hidden argument*/NULL);
		((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->set_moveDirection_51(L_28);
		// }
		return;
	}

IL_00bb:
	{
		// moveDirection = new Vector3(inputSmooth.x, 0, inputSmooth.z);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_29 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_address_of_inputSmooth_50();
		float L_30 = L_29->get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_31 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_address_of_inputSmooth_50();
		float L_32 = L_31->get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_33;
		memset((&L_33), 0, sizeof(L_33));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_33), L_30, (0.0f), L_32, /*hidden argument*/NULL);
		((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->set_moveDirection_51(L_33);
		// }
		return;
	}
}
// System.Void Invector.vCharacterController.vThirdPersonController::Sprint(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonController_Sprint_mC67D3C939B8F2CADC786E5E62B36E32A2D6C59DC (vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * __this, bool ___value0, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B10_0 = 0;
	{
		// var sprintConditions = (input.sqrMagnitude > 0.1f && isGrounded &&
		//     !(isStrafing && !strafeSpeed.walkByDefault && (horizontalSpeed >= 0.5 || horizontalSpeed <= -0.5 || verticalSpeed <= 0.1f)));
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_0 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_address_of_input_48();
		float L_1;
		L_1 = Vector3_get_sqrMagnitude_mC567EE6DF411501A8FE1F23A0038862630B88249((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_0, /*hidden argument*/NULL);
		if ((!(((float)L_1) > ((float)(0.100000001f)))))
		{
			goto IL_0068;
		}
	}
	{
		bool L_2;
		L_2 = vThirdPersonMotor_get_isGrounded_m3587831F44EEE43F49591F13918B05CFD28B65AB_inline(__this, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0068;
		}
	}
	{
		bool L_3;
		L_3 = vThirdPersonMotor_get_isStrafing_m3FB9147D8588055A220D47101B80C6FFD2B2E791_inline(__this, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0065;
		}
	}
	{
		vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * L_4 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_strafeSpeed_10();
		NullCheck(L_4);
		bool L_5 = L_4->get_walkByDefault_3();
		if (L_5)
		{
			goto IL_0065;
		}
	}
	{
		float L_6 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_horizontalSpeed_35();
		if ((((double)((double)((double)L_6))) >= ((double)(0.5))))
		{
			goto IL_0062;
		}
	}
	{
		float L_7 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_horizontalSpeed_35();
		if ((((double)((double)((double)L_7))) <= ((double)(-0.5))))
		{
			goto IL_0062;
		}
	}
	{
		float L_8 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_verticalSpeed_34();
		G_B10_0 = ((!(((float)L_8) <= ((float)(0.100000001f))))? 1 : 0);
		goto IL_0069;
	}

IL_0062:
	{
		G_B10_0 = 0;
		goto IL_0069;
	}

IL_0065:
	{
		G_B10_0 = 1;
		goto IL_0069;
	}

IL_0068:
	{
		G_B10_0 = 0;
	}

IL_0069:
	{
		V_0 = (bool)G_B10_0;
		// if (value && sprintConditions)
		bool L_9 = ___value0;
		bool L_10 = V_0;
		if (!((int32_t)((int32_t)L_9&(int32_t)L_10)))
		{
			goto IL_00c9;
		}
	}
	{
		// if (input.sqrMagnitude > 0.1f)
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_11 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_address_of_input_48();
		float L_12;
		L_12 = Vector3_get_sqrMagnitude_mC567EE6DF411501A8FE1F23A0038862630B88249((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_11, /*hidden argument*/NULL);
		if ((!(((float)L_12) > ((float)(0.100000001f)))))
		{
			goto IL_00b1;
		}
	}
	{
		// if (isGrounded && useContinuousSprint)
		bool L_13;
		L_13 = vThirdPersonMotor_get_isGrounded_m3587831F44EEE43F49591F13918B05CFD28B65AB_inline(__this, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00a1;
		}
	}
	{
		bool L_14 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_useContinuousSprint_6();
		if (!L_14)
		{
			goto IL_00a1;
		}
	}
	{
		// isSprinting = !isSprinting;
		bool L_15;
		L_15 = vThirdPersonMotor_get_isSprinting_m98A252FE6362DE6468A47BBC3B0F7FCACA2B3EDD_inline(__this, /*hidden argument*/NULL);
		vThirdPersonMotor_set_isSprinting_m031D2D28AF417BB0EAD7679972CAFE117C6A7749_inline(__this, (bool)((((int32_t)L_15) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		// }
		return;
	}

IL_00a1:
	{
		// else if (!isSprinting)
		bool L_16;
		L_16 = vThirdPersonMotor_get_isSprinting_m98A252FE6362DE6468A47BBC3B0F7FCACA2B3EDD_inline(__this, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_00d8;
		}
	}
	{
		// isSprinting = true;
		vThirdPersonMotor_set_isSprinting_m031D2D28AF417BB0EAD7679972CAFE117C6A7749_inline(__this, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}

IL_00b1:
	{
		// else if (!useContinuousSprint && isSprinting)
		bool L_17 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_useContinuousSprint_6();
		if (L_17)
		{
			goto IL_00d8;
		}
	}
	{
		bool L_18;
		L_18 = vThirdPersonMotor_get_isSprinting_m98A252FE6362DE6468A47BBC3B0F7FCACA2B3EDD_inline(__this, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00d8;
		}
	}
	{
		// isSprinting = false;
		vThirdPersonMotor_set_isSprinting_m031D2D28AF417BB0EAD7679972CAFE117C6A7749_inline(__this, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_00c9:
	{
		// else if (isSprinting)
		bool L_19;
		L_19 = vThirdPersonMotor_get_isSprinting_m98A252FE6362DE6468A47BBC3B0F7FCACA2B3EDD_inline(__this, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00d8;
		}
	}
	{
		// isSprinting = false;
		vThirdPersonMotor_set_isSprinting_m031D2D28AF417BB0EAD7679972CAFE117C6A7749_inline(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_00d8:
	{
		// }
		return;
	}
}
// System.Void Invector.vCharacterController.vThirdPersonController::Strafe()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonController_Strafe_m8B54F15DB5B12194AB03CC110B08B4981414A189 (vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * __this, const RuntimeMethod* method)
{
	{
		// isStrafing = !isStrafing;
		bool L_0;
		L_0 = vThirdPersonMotor_get_isStrafing_m3FB9147D8588055A220D47101B80C6FFD2B2E791_inline(__this, /*hidden argument*/NULL);
		vThirdPersonMotor_set_isStrafing_m90A9F59590C2DABE76449CE57C8FBCCCC9F39EFE_inline(__this, (bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Invector.vCharacterController.vThirdPersonController::Jump()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonController_Jump_m6979A24997531E5AA1C70EE943CACC3416FA90A8 (vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral32BEEE1B185DCAAA32545866BF4C372603A16426);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral70253F929BCE7F81DF1A5A1C0900BED744E86C9C);
		s_Il2CppMethodInitialized = true;
	}
	{
		// jumpCounter = jumpTimer;
		float L_0 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_jumpTimer_13();
		((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->set_jumpCounter_41(L_0);
		// isJumping = true;
		((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->set_isJumping_29((bool)1);
		// if (input.sqrMagnitude < 0.1f)
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_1 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_address_of_input_48();
		float L_2;
		L_2 = Vector3_get_sqrMagnitude_mC567EE6DF411501A8FE1F23A0038862630B88249((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_1, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)(0.100000001f)))))
		{
			goto IL_003b;
		}
	}
	{
		// animator.CrossFadeInFixedTime("Jump", 0.1f);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_3 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_animator_23();
		NullCheck(L_3);
		Animator_CrossFadeInFixedTime_m424EE9ABD82B06A3D950A538E6A61FA55ECC654B(L_3, _stringLiteral70253F929BCE7F81DF1A5A1C0900BED744E86C9C, (0.100000001f), /*hidden argument*/NULL);
		return;
	}

IL_003b:
	{
		// animator.CrossFadeInFixedTime("JumpMove", .2f);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_4 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)__this)->get_animator_23();
		NullCheck(L_4);
		Animator_CrossFadeInFixedTime_m424EE9ABD82B06A3D950A538E6A61FA55ECC654B(L_4, _stringLiteral32BEEE1B185DCAAA32545866BF4C372603A16426, (0.200000003f), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Invector.vCharacterController.vThirdPersonController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonController__ctor_m4DB358FB0991D3EFD4CF8BCBD7D76A9F22C3D164 (vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * __this, const RuntimeMethod* method)
{
	{
		vThirdPersonAnimator__ctor_mBB69BF7A51196C407D664AD2A836167FF190352A(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Invector.vCharacterController.vThirdPersonInput::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonInput_Start_m1AFD99CDA83D86DC215EAC7230C8A01FB71DC080 (vThirdPersonInput_tB15CFA93BCDA688B2E2D8F976E90C4262CB82C50 * __this, const RuntimeMethod* method)
{
	{
		// InitilizeController();
		VirtActionInvoker0::Invoke(8 /* System.Void Invector.vCharacterController.vThirdPersonInput::InitilizeController() */, __this);
		// InitializeTpCamera();
		VirtActionInvoker0::Invoke(9 /* System.Void Invector.vCharacterController.vThirdPersonInput::InitializeTpCamera() */, __this);
		// }
		return;
	}
}
// System.Void Invector.vCharacterController.vThirdPersonInput::FixedUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonInput_FixedUpdate_m9BCF162748D6061DE5E819DF37785CDD21D54A33 (vThirdPersonInput_tB15CFA93BCDA688B2E2D8F976E90C4262CB82C50 * __this, const RuntimeMethod* method)
{
	{
		// cc.UpdateMotor();               // updates the ThirdPersonMotor methods
		vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * L_0 = __this->get_cc_11();
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(4 /* System.Void Invector.vCharacterController.vThirdPersonMotor::UpdateMotor() */, L_0);
		// cc.ControlLocomotionType();     // handle the controller locomotion type and movespeed
		vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * L_1 = __this->get_cc_11();
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(22 /* System.Void Invector.vCharacterController.vThirdPersonController::ControlLocomotionType() */, L_1);
		// cc.ControlRotationType();       // handle the controller rotation type
		vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * L_2 = __this->get_cc_11();
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(23 /* System.Void Invector.vCharacterController.vThirdPersonController::ControlRotationType() */, L_2);
		// }
		return;
	}
}
// System.Void Invector.vCharacterController.vThirdPersonInput::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonInput_Update_m013378AF0EB94763805A20FD8825867AE3356633 (vThirdPersonInput_tB15CFA93BCDA688B2E2D8F976E90C4262CB82C50 * __this, const RuntimeMethod* method)
{
	{
		// InputHandle();                  // update the input methods
		VirtActionInvoker0::Invoke(10 /* System.Void Invector.vCharacterController.vThirdPersonInput::InputHandle() */, __this);
		// cc.UpdateAnimator();            // updates the Animator Parameters
		vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * L_0 = __this->get_cc_11();
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(19 /* System.Void Invector.vCharacterController.vThirdPersonAnimator::UpdateAnimator() */, L_0);
		// }
		return;
	}
}
// System.Void Invector.vCharacterController.vThirdPersonInput::OnAnimatorMove()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonInput_OnAnimatorMove_m5BB743D83E25A815A22EA378D2E43D98F4A2B117 (vThirdPersonInput_tB15CFA93BCDA688B2E2D8F976E90C4262CB82C50 * __this, const RuntimeMethod* method)
{
	{
		// cc.ControlAnimatorRootMotion(); // handle root motion animations
		vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * L_0 = __this->get_cc_11();
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(21 /* System.Void Invector.vCharacterController.vThirdPersonController::ControlAnimatorRootMotion() */, L_0);
		// }
		return;
	}
}
// System.Void Invector.vCharacterController.vThirdPersonInput::InitilizeController()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonInput_InitilizeController_m0FEC4A7F8577A1A347512576F0D569EB3BDC4FD1 (vThirdPersonInput_tB15CFA93BCDA688B2E2D8F976E90C4262CB82C50 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisvThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A_m89E57D0F784DA689BADD06ECF4BFEA8A8822A350_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// cc = GetComponent<vThirdPersonController>();
		vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * L_0;
		L_0 = Component_GetComponent_TisvThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A_m89E57D0F784DA689BADD06ECF4BFEA8A8822A350(__this, /*hidden argument*/Component_GetComponent_TisvThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A_m89E57D0F784DA689BADD06ECF4BFEA8A8822A350_RuntimeMethod_var);
		__this->set_cc_11(L_0);
		// if (cc != null)
		vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * L_1 = __this->get_cc_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_1, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		// cc.Init();
		vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * L_3 = __this->get_cc_11();
		NullCheck(L_3);
		vThirdPersonMotor_Init_m8470D932C4BE26328DB0320B9E92E0AE4E3E8B37(L_3, /*hidden argument*/NULL);
	}

IL_0025:
	{
		// }
		return;
	}
}
// System.Void Invector.vCharacterController.vThirdPersonInput::InitializeTpCamera()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonInput_InitializeTpCamera_m872208B4E8F98A9E4EA9F54F73DE26BA2A63F9A8 (vThirdPersonInput_tB15CFA93BCDA688B2E2D8F976E90C4262CB82C50 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_FindObjectOfType_TisvThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11_m291B5F056AEC7E08B699A8363A313162CAB2B4BD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (tpCamera == null)
		vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11 * L_0 = __this->get_tpCamera_12();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0051;
		}
	}
	{
		// tpCamera = FindObjectOfType<vThirdPersonCamera>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11 * L_2;
		L_2 = Object_FindObjectOfType_TisvThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11_m291B5F056AEC7E08B699A8363A313162CAB2B4BD(/*hidden argument*/Object_FindObjectOfType_TisvThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11_m291B5F056AEC7E08B699A8363A313162CAB2B4BD_RuntimeMethod_var);
		__this->set_tpCamera_12(L_2);
		// if (tpCamera == null)
		vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11 * L_3 = __this->get_tpCamera_12();
		bool L_4;
		L_4 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_3, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0028;
		}
	}
	{
		// return;
		return;
	}

IL_0028:
	{
		// if (tpCamera)
		vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11 * L_5 = __this->get_tpCamera_12();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_6;
		L_6 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0051;
		}
	}
	{
		// tpCamera.SetMainTarget(this.transform);
		vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11 * L_7 = __this->get_tpCamera_12();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_8;
		L_8 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		vThirdPersonCamera_SetMainTarget_m35387F3B3FC8B83915DC3441E9019F0EE7D878C7(L_7, L_8, /*hidden argument*/NULL);
		// tpCamera.Init();
		vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11 * L_9 = __this->get_tpCamera_12();
		NullCheck(L_9);
		vThirdPersonCamera_Init_mAFBED3541CD351DED52E79B067716668A5D56ED6(L_9, /*hidden argument*/NULL);
	}

IL_0051:
	{
		// }
		return;
	}
}
// System.Void Invector.vCharacterController.vThirdPersonInput::InputHandle()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonInput_InputHandle_m88B1CAB599AAC1273638B72E277CCB31DB377875 (vThirdPersonInput_tB15CFA93BCDA688B2E2D8F976E90C4262CB82C50 * __this, const RuntimeMethod* method)
{
	{
		// MoveInput();
		VirtActionInvoker0::Invoke(11 /* System.Void Invector.vCharacterController.vThirdPersonInput::MoveInput() */, __this);
		// CameraInput();
		VirtActionInvoker0::Invoke(12 /* System.Void Invector.vCharacterController.vThirdPersonInput::CameraInput() */, __this);
		// SprintInput();
		VirtActionInvoker0::Invoke(14 /* System.Void Invector.vCharacterController.vThirdPersonInput::SprintInput() */, __this);
		// StrafeInput();
		VirtActionInvoker0::Invoke(13 /* System.Void Invector.vCharacterController.vThirdPersonInput::StrafeInput() */, __this);
		// JumpInput();
		VirtActionInvoker0::Invoke(16 /* System.Void Invector.vCharacterController.vThirdPersonInput::JumpInput() */, __this);
		// }
		return;
	}
}
// System.Void Invector.vCharacterController.vThirdPersonInput::MoveInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonInput_MoveInput_m86A8818A0FAB74282A427C773019C776E647A830 (vThirdPersonInput_tB15CFA93BCDA688B2E2D8F976E90C4262CB82C50 * __this, const RuntimeMethod* method)
{
	{
		// cc.input.x = Input.GetAxis(horizontalInput);
		vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * L_0 = __this->get_cc_11();
		NullCheck(L_0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_1 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)L_0)->get_address_of_input_48();
		String_t* L_2 = __this->get_horizontalInput_4();
		float L_3;
		L_3 = Input_GetAxis_m939297DEB2ECF8D8D09AD66EB69979AAD2B62326(L_2, /*hidden argument*/NULL);
		L_1->set_x_2(L_3);
		// cc.input.z = Input.GetAxis(verticallInput);
		vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * L_4 = __this->get_cc_11();
		NullCheck(L_4);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_5 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)L_4)->get_address_of_input_48();
		String_t* L_6 = __this->get_verticallInput_5();
		float L_7;
		L_7 = Input_GetAxis_m939297DEB2ECF8D8D09AD66EB69979AAD2B62326(L_6, /*hidden argument*/NULL);
		L_5->set_z_4(L_7);
		// }
		return;
	}
}
// System.Void Invector.vCharacterController.vThirdPersonInput::CameraInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonInput_CameraInput_m55F640928EC595A1B7C6A784144C529058DFB576 (vThirdPersonInput_tB15CFA93BCDA688B2E2D8F976E90C4262CB82C50 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral835843700B5BAEFFB185610011BAA3028FB07587);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		// if (!cameraMain)
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_0 = __this->get_cameraMain_13();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0046;
		}
	}
	{
		// if (!Camera.main) Debug.Log("Missing a Camera with the tag MainCamera, please add one.");
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_2;
		L_2 = Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0025;
		}
	}
	{
		// if (!Camera.main) Debug.Log("Missing a Camera with the tag MainCamera, please add one.");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(_stringLiteral835843700B5BAEFFB185610011BAA3028FB07587, /*hidden argument*/NULL);
		goto IL_0046;
	}

IL_0025:
	{
		// cameraMain = Camera.main;
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_4;
		L_4 = Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C(/*hidden argument*/NULL);
		__this->set_cameraMain_13(L_4);
		// cc.rotateTarget = cameraMain.transform;
		vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * L_5 = __this->get_cc_11();
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_6 = __this->get_cameraMain_13();
		NullCheck(L_6);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_7;
		L_7 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)L_5)->set_rotateTarget_47(L_7);
	}

IL_0046:
	{
		// if (cameraMain)
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_8 = __this->get_cameraMain_13();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_9;
		L_9 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0069;
		}
	}
	{
		// cc.UpdateMoveDirection(cameraMain.transform);
		vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * L_10 = __this->get_cc_11();
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_11 = __this->get_cameraMain_13();
		NullCheck(L_11);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_12;
		L_12 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		VirtActionInvoker1< Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * >::Invoke(24 /* System.Void Invector.vCharacterController.vThirdPersonController::UpdateMoveDirection(UnityEngine.Transform) */, L_10, L_12);
	}

IL_0069:
	{
		// if (tpCamera == null)
		vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11 * L_13 = __this->get_tpCamera_12();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_14;
		L_14 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_13, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0078;
		}
	}
	{
		// return;
		return;
	}

IL_0078:
	{
		// var Y = Input.GetAxis(rotateCameraYInput);
		String_t* L_15 = __this->get_rotateCameraYInput_10();
		float L_16;
		L_16 = Input_GetAxis_m939297DEB2ECF8D8D09AD66EB69979AAD2B62326(L_15, /*hidden argument*/NULL);
		V_0 = L_16;
		// var X = Input.GetAxis(rotateCameraXInput);
		String_t* L_17 = __this->get_rotateCameraXInput_9();
		float L_18;
		L_18 = Input_GetAxis_m939297DEB2ECF8D8D09AD66EB69979AAD2B62326(L_17, /*hidden argument*/NULL);
		V_1 = L_18;
		// tpCamera.RotateCamera(X, Y);
		vThirdPersonCamera_t9A4D0623E70D89A5B6E7D6B0B7411509D9EC6C11 * L_19 = __this->get_tpCamera_12();
		float L_20 = V_1;
		float L_21 = V_0;
		NullCheck(L_19);
		vThirdPersonCamera_RotateCamera_m38FD2DBF37023FD4885681827DFF017CF64FFC57(L_19, L_20, L_21, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Invector.vCharacterController.vThirdPersonInput::StrafeInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonInput_StrafeInput_m0B27009F3EF0B17BAD69C2D377A5A57F82C96ACD (vThirdPersonInput_tB15CFA93BCDA688B2E2D8F976E90C4262CB82C50 * __this, const RuntimeMethod* method)
{
	{
		// if (Input.GetKeyDown(strafeInput))
		int32_t L_0 = __this->get_strafeInput_7();
		bool L_1;
		L_1 = Input_GetKeyDown_m476116696E71771641BBECBAB1A4C55E69018220(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		// cc.Strafe();
		vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * L_2 = __this->get_cc_11();
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(26 /* System.Void Invector.vCharacterController.vThirdPersonController::Strafe() */, L_2);
	}

IL_0018:
	{
		// }
		return;
	}
}
// System.Void Invector.vCharacterController.vThirdPersonInput::SprintInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonInput_SprintInput_m9E1F56CDCBD4630652143B643A146AAFDE077B57 (vThirdPersonInput_tB15CFA93BCDA688B2E2D8F976E90C4262CB82C50 * __this, const RuntimeMethod* method)
{
	{
		// if (Input.GetKeyDown(sprintInput))
		int32_t L_0 = __this->get_sprintInput_8();
		bool L_1;
		L_1 = Input_GetKeyDown_m476116696E71771641BBECBAB1A4C55E69018220(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		// cc.Sprint(true);
		vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * L_2 = __this->get_cc_11();
		NullCheck(L_2);
		VirtActionInvoker1< bool >::Invoke(25 /* System.Void Invector.vCharacterController.vThirdPersonController::Sprint(System.Boolean) */, L_2, (bool)1);
		return;
	}

IL_001a:
	{
		// else if (Input.GetKeyUp(sprintInput))
		int32_t L_3 = __this->get_sprintInput_8();
		bool L_4;
		L_4 = Input_GetKeyUp_mDE9D56FE11715566D4D54FD96F8E1EF9734D225F(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0033;
		}
	}
	{
		// cc.Sprint(false);
		vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * L_5 = __this->get_cc_11();
		NullCheck(L_5);
		VirtActionInvoker1< bool >::Invoke(25 /* System.Void Invector.vCharacterController.vThirdPersonController::Sprint(System.Boolean) */, L_5, (bool)0);
	}

IL_0033:
	{
		// }
		return;
	}
}
// System.Boolean Invector.vCharacterController.vThirdPersonInput::JumpConditions()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool vThirdPersonInput_JumpConditions_m9CB2DAB3C189B7E5750F7EE0EDADDD7CD1409EF4 (vThirdPersonInput_tB15CFA93BCDA688B2E2D8F976E90C4262CB82C50 * __this, const RuntimeMethod* method)
{
	{
		// return cc.isGrounded && cc.GroundAngle() < cc.slopeLimit && !cc.isJumping && !cc.stopMove;
		vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * L_0 = __this->get_cc_11();
		NullCheck(L_0);
		bool L_1;
		L_1 = vThirdPersonMotor_get_isGrounded_m3587831F44EEE43F49591F13918B05CFD28B65AB_inline(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0041;
		}
	}
	{
		vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * L_2 = __this->get_cc_11();
		NullCheck(L_2);
		float L_3;
		L_3 = VirtFuncInvoker0< float >::Invoke(17 /* System.Single Invector.vCharacterController.vThirdPersonMotor::GroundAngle() */, L_2);
		vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * L_4 = __this->get_cc_11();
		NullCheck(L_4);
		float L_5 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)L_4)->get_slopeLimit_22();
		if ((!(((float)L_3) < ((float)L_5))))
		{
			goto IL_0041;
		}
	}
	{
		vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * L_6 = __this->get_cc_11();
		NullCheck(L_6);
		bool L_7 = ((vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 *)L_6)->get_isJumping_29();
		if (L_7)
		{
			goto IL_0041;
		}
	}
	{
		vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * L_8 = __this->get_cc_11();
		NullCheck(L_8);
		bool L_9;
		L_9 = vThirdPersonMotor_get_stopMove_m09C1FEC6B37CBF3071597D789E0322C93CD78C58_inline(L_8, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_9) == ((int32_t)0))? 1 : 0);
	}

IL_0041:
	{
		return (bool)0;
	}
}
// System.Void Invector.vCharacterController.vThirdPersonInput::JumpInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonInput_JumpInput_mCF691AA0405D14B9E86ED5011F3D924479A825C5 (vThirdPersonInput_tB15CFA93BCDA688B2E2D8F976E90C4262CB82C50 * __this, const RuntimeMethod* method)
{
	{
		// if (Input.GetKeyDown(jumpInput) && JumpConditions())
		int32_t L_0 = __this->get_jumpInput_6();
		bool L_1;
		L_1 = Input_GetKeyDown_m476116696E71771641BBECBAB1A4C55E69018220(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		bool L_2;
		L_2 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean Invector.vCharacterController.vThirdPersonInput::JumpConditions() */, __this);
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		// cc.Jump();
		vThirdPersonController_t8EC3154FCA649889A4AD28FC1EFEBB35527F583A * L_3 = __this->get_cc_11();
		NullCheck(L_3);
		VirtActionInvoker0::Invoke(27 /* System.Void Invector.vCharacterController.vThirdPersonController::Jump() */, L_3);
	}

IL_0020:
	{
		// }
		return;
	}
}
// System.Void Invector.vCharacterController.vThirdPersonInput::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonInput__ctor_m6377D0158E4C85892B3AD6F29D1475E4DD5BC3BB (vThirdPersonInput_tB15CFA93BCDA688B2E2D8F976E90C4262CB82C50 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral16DD21BE77B115D392226EB71A2D3A9FDC29E3F0);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral265E15F1F86F1C766555899D5771CF29055DE75A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral88BEE283254D7094E258B3A88730F4CC4F1E4AC7);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public string horizontalInput = "Horizontal";
		__this->set_horizontalInput_4(_stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E);
		// public string verticallInput = "Vertical";
		__this->set_verticallInput_5(_stringLiteral265E15F1F86F1C766555899D5771CF29055DE75A);
		// public KeyCode jumpInput = KeyCode.Space;
		__this->set_jumpInput_6(((int32_t)32));
		// public KeyCode strafeInput = KeyCode.Tab;
		__this->set_strafeInput_7(((int32_t)9));
		// public KeyCode sprintInput = KeyCode.LeftShift;
		__this->set_sprintInput_8(((int32_t)304));
		// public string rotateCameraXInput = "Mouse X";
		__this->set_rotateCameraXInput_9(_stringLiteral88BEE283254D7094E258B3A88730F4CC4F1E4AC7);
		// public string rotateCameraYInput = "Mouse Y";
		__this->set_rotateCameraYInput_10(_stringLiteral16DD21BE77B115D392226EB71A2D3A9FDC29E3F0);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean Invector.vCharacterController.vThirdPersonMotor::get_isStrafing()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool vThirdPersonMotor_get_isStrafing_m3FB9147D8588055A220D47101B80C6FFD2B2E791 (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, const RuntimeMethod* method)
{
	{
		// return _isStrafing;
		bool L_0 = __this->get__isStrafing_46();
		return L_0;
	}
}
// System.Void Invector.vCharacterController.vThirdPersonMotor::set_isStrafing(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonMotor_set_isStrafing_m90A9F59590C2DABE76449CE57C8FBCCCC9F39EFE (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// _isStrafing = value;
		bool L_0 = ___value0;
		__this->set__isStrafing_46(L_0);
		// }
		return;
	}
}
// System.Boolean Invector.vCharacterController.vThirdPersonMotor::get_isGrounded()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool vThirdPersonMotor_get_isGrounded_m3587831F44EEE43F49591F13918B05CFD28B65AB (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, const RuntimeMethod* method)
{
	{
		// internal bool isGrounded { get; set; }
		bool L_0 = __this->get_U3CisGroundedU3Ek__BackingField_30();
		return L_0;
	}
}
// System.Void Invector.vCharacterController.vThirdPersonMotor::set_isGrounded(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonMotor_set_isGrounded_m4F6F1C10637D6E9382C07E507FCD1773F846FA9B (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// internal bool isGrounded { get; set; }
		bool L_0 = ___value0;
		__this->set_U3CisGroundedU3Ek__BackingField_30(L_0);
		return;
	}
}
// System.Boolean Invector.vCharacterController.vThirdPersonMotor::get_isSprinting()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool vThirdPersonMotor_get_isSprinting_m98A252FE6362DE6468A47BBC3B0F7FCACA2B3EDD (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, const RuntimeMethod* method)
{
	{
		// internal bool isSprinting { get; set; }
		bool L_0 = __this->get_U3CisSprintingU3Ek__BackingField_31();
		return L_0;
	}
}
// System.Void Invector.vCharacterController.vThirdPersonMotor::set_isSprinting(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonMotor_set_isSprinting_m031D2D28AF417BB0EAD7679972CAFE117C6A7749 (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// internal bool isSprinting { get; set; }
		bool L_0 = ___value0;
		__this->set_U3CisSprintingU3Ek__BackingField_31(L_0);
		return;
	}
}
// System.Boolean Invector.vCharacterController.vThirdPersonMotor::get_stopMove()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool vThirdPersonMotor_get_stopMove_m09C1FEC6B37CBF3071597D789E0322C93CD78C58 (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, const RuntimeMethod* method)
{
	{
		// public bool stopMove { get; protected set; }
		bool L_0 = __this->get_U3CstopMoveU3Ek__BackingField_32();
		return L_0;
	}
}
// System.Void Invector.vCharacterController.vThirdPersonMotor::set_stopMove(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonMotor_set_stopMove_m700BD3D0627D69453D1BB1D453062FAD5F568BC8 (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool stopMove { get; protected set; }
		bool L_0 = ___value0;
		__this->set_U3CstopMoveU3Ek__BackingField_32(L_0);
		return;
	}
}
// System.Void Invector.vCharacterController.vThirdPersonMotor::Init()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonMotor_Init_m8470D932C4BE26328DB0320B9E92E0AE4E3E8B37 (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisCapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635_m2B9A09217489CB456933DEA112CA70C10AD3B9F9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_m9DC24AA806B0B65E917751F7A3AFDB58861157CE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1E303E60E7B8FCF9AC7391E2758609BE04B7C842);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4DF60B3121C256419E90E065DA63E5CC56468056);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFFEAEBFEFE135DF7CF4812C255322760E0C94F82);
		s_Il2CppMethodInitialized = true;
	}
	{
		// animator = GetComponent<Animator>();
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_0;
		L_0 = Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var);
		__this->set_animator_23(L_0);
		// animator.updateMode = AnimatorUpdateMode.AnimatePhysics;
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_1 = __this->get_animator_23();
		NullCheck(L_1);
		Animator_set_updateMode_mD10A679F349D4BD7722E1D700215592BDC4B05CE(L_1, 1, /*hidden argument*/NULL);
		// frictionPhysics = new PhysicMaterial();
		PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 * L_2 = (PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 *)il2cpp_codegen_object_new(PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2_il2cpp_TypeInfo_var);
		PhysicMaterial__ctor_m8E7DE1AB54FEC51DE0DA4207E5BB55BB45EFC873(L_2, /*hidden argument*/NULL);
		__this->set_frictionPhysics_25(L_2);
		// frictionPhysics.name = "frictionPhysics";
		PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 * L_3 = __this->get_frictionPhysics_25();
		NullCheck(L_3);
		Object_set_name_m87C4006618ADB325ABE5439DF159E10DD8DD0781(L_3, _stringLiteral4DF60B3121C256419E90E065DA63E5CC56468056, /*hidden argument*/NULL);
		// frictionPhysics.staticFriction = .25f;
		PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 * L_4 = __this->get_frictionPhysics_25();
		NullCheck(L_4);
		PhysicMaterial_set_staticFriction_mF2914B1027C9EB422643AD4A9056D04727637EC1(L_4, (0.25f), /*hidden argument*/NULL);
		// frictionPhysics.dynamicFriction = .25f;
		PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 * L_5 = __this->get_frictionPhysics_25();
		NullCheck(L_5);
		PhysicMaterial_set_dynamicFriction_mEE2B55B29EECABA0C7A39B102F2BD259CE0D4566(L_5, (0.25f), /*hidden argument*/NULL);
		// frictionPhysics.frictionCombine = PhysicMaterialCombine.Multiply;
		PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 * L_6 = __this->get_frictionPhysics_25();
		NullCheck(L_6);
		PhysicMaterial_set_frictionCombine_mDDFA5B298AC911112AF8B7C2849BCD3C7E6632CD(L_6, 1, /*hidden argument*/NULL);
		// maxFrictionPhysics = new PhysicMaterial();
		PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 * L_7 = (PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 *)il2cpp_codegen_object_new(PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2_il2cpp_TypeInfo_var);
		PhysicMaterial__ctor_m8E7DE1AB54FEC51DE0DA4207E5BB55BB45EFC873(L_7, /*hidden argument*/NULL);
		__this->set_maxFrictionPhysics_26(L_7);
		// maxFrictionPhysics.name = "maxFrictionPhysics";
		PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 * L_8 = __this->get_maxFrictionPhysics_26();
		NullCheck(L_8);
		Object_set_name_m87C4006618ADB325ABE5439DF159E10DD8DD0781(L_8, _stringLiteral1E303E60E7B8FCF9AC7391E2758609BE04B7C842, /*hidden argument*/NULL);
		// maxFrictionPhysics.staticFriction = 1f;
		PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 * L_9 = __this->get_maxFrictionPhysics_26();
		NullCheck(L_9);
		PhysicMaterial_set_staticFriction_mF2914B1027C9EB422643AD4A9056D04727637EC1(L_9, (1.0f), /*hidden argument*/NULL);
		// maxFrictionPhysics.dynamicFriction = 1f;
		PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 * L_10 = __this->get_maxFrictionPhysics_26();
		NullCheck(L_10);
		PhysicMaterial_set_dynamicFriction_mEE2B55B29EECABA0C7A39B102F2BD259CE0D4566(L_10, (1.0f), /*hidden argument*/NULL);
		// maxFrictionPhysics.frictionCombine = PhysicMaterialCombine.Maximum;
		PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 * L_11 = __this->get_maxFrictionPhysics_26();
		NullCheck(L_11);
		PhysicMaterial_set_frictionCombine_mDDFA5B298AC911112AF8B7C2849BCD3C7E6632CD(L_11, 3, /*hidden argument*/NULL);
		// slippyPhysics = new PhysicMaterial();
		PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 * L_12 = (PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 *)il2cpp_codegen_object_new(PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2_il2cpp_TypeInfo_var);
		PhysicMaterial__ctor_m8E7DE1AB54FEC51DE0DA4207E5BB55BB45EFC873(L_12, /*hidden argument*/NULL);
		__this->set_slippyPhysics_27(L_12);
		// slippyPhysics.name = "slippyPhysics";
		PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 * L_13 = __this->get_slippyPhysics_27();
		NullCheck(L_13);
		Object_set_name_m87C4006618ADB325ABE5439DF159E10DD8DD0781(L_13, _stringLiteralFFEAEBFEFE135DF7CF4812C255322760E0C94F82, /*hidden argument*/NULL);
		// slippyPhysics.staticFriction = 0f;
		PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 * L_14 = __this->get_slippyPhysics_27();
		NullCheck(L_14);
		PhysicMaterial_set_staticFriction_mF2914B1027C9EB422643AD4A9056D04727637EC1(L_14, (0.0f), /*hidden argument*/NULL);
		// slippyPhysics.dynamicFriction = 0f;
		PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 * L_15 = __this->get_slippyPhysics_27();
		NullCheck(L_15);
		PhysicMaterial_set_dynamicFriction_mEE2B55B29EECABA0C7A39B102F2BD259CE0D4566(L_15, (0.0f), /*hidden argument*/NULL);
		// slippyPhysics.frictionCombine = PhysicMaterialCombine.Minimum;
		PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 * L_16 = __this->get_slippyPhysics_27();
		NullCheck(L_16);
		PhysicMaterial_set_frictionCombine_mDDFA5B298AC911112AF8B7C2849BCD3C7E6632CD(L_16, 2, /*hidden argument*/NULL);
		// _rigidbody = GetComponent<Rigidbody>();
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_17;
		L_17 = Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_m9DC24AA806B0B65E917751F7A3AFDB58861157CE(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_m9DC24AA806B0B65E917751F7A3AFDB58861157CE_RuntimeMethod_var);
		__this->set__rigidbody_24(L_17);
		// _capsuleCollider = GetComponent<CapsuleCollider>();
		CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * L_18;
		L_18 = Component_GetComponent_TisCapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635_m2B9A09217489CB456933DEA112CA70C10AD3B9F9(__this, /*hidden argument*/Component_GetComponent_TisCapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635_m2B9A09217489CB456933DEA112CA70C10AD3B9F9_RuntimeMethod_var);
		__this->set__capsuleCollider_28(L_18);
		// colliderCenter = GetComponent<CapsuleCollider>().center;
		CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * L_19;
		L_19 = Component_GetComponent_TisCapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635_m2B9A09217489CB456933DEA112CA70C10AD3B9F9(__this, /*hidden argument*/Component_GetComponent_TisCapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635_m2B9A09217489CB456933DEA112CA70C10AD3B9F9_RuntimeMethod_var);
		NullCheck(L_19);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20;
		L_20 = CapsuleCollider_get_center_m6374F7457A9450CAFFAD2DF0C9D1419BF9E304CB(L_19, /*hidden argument*/NULL);
		__this->set_colliderCenter_49(L_20);
		// colliderRadius = GetComponent<CapsuleCollider>().radius;
		CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * L_21;
		L_21 = Component_GetComponent_TisCapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635_m2B9A09217489CB456933DEA112CA70C10AD3B9F9(__this, /*hidden argument*/Component_GetComponent_TisCapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635_m2B9A09217489CB456933DEA112CA70C10AD3B9F9_RuntimeMethod_var);
		NullCheck(L_21);
		float L_22;
		L_22 = CapsuleCollider_get_radius_m5746DDE5E6A099269FC9DAD253887C58E8A064D0(L_21, /*hidden argument*/NULL);
		__this->set_colliderRadius_38(L_22);
		// colliderHeight = GetComponent<CapsuleCollider>().height;
		CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * L_23;
		L_23 = Component_GetComponent_TisCapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635_m2B9A09217489CB456933DEA112CA70C10AD3B9F9(__this, /*hidden argument*/Component_GetComponent_TisCapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635_m2B9A09217489CB456933DEA112CA70C10AD3B9F9_RuntimeMethod_var);
		NullCheck(L_23);
		float L_24;
		L_24 = CapsuleCollider_get_height_mD6CF93CB2C222F8E5B77D65B0356F8FD8005B526(L_23, /*hidden argument*/NULL);
		__this->set_colliderHeight_39(L_24);
		// isGrounded = true;
		vThirdPersonMotor_set_isGrounded_m4F6F1C10637D6E9382C07E507FCD1773F846FA9B_inline(__this, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Invector.vCharacterController.vThirdPersonMotor::UpdateMotor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonMotor_UpdateMotor_mFBFA2A2D18DE13F102C8D4CF7DAFA0AB1043A483 (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, const RuntimeMethod* method)
{
	{
		// CheckGround();
		VirtActionInvoker0::Invoke(14 /* System.Void Invector.vCharacterController.vThirdPersonMotor::CheckGround() */, __this);
		// CheckSlopeLimit();
		VirtActionInvoker0::Invoke(7 /* System.Void Invector.vCharacterController.vThirdPersonMotor::CheckSlopeLimit() */, __this);
		// ControlJumpBehaviour();
		VirtActionInvoker0::Invoke(11 /* System.Void Invector.vCharacterController.vThirdPersonMotor::ControlJumpBehaviour() */, __this);
		// AirControl();
		VirtActionInvoker0::Invoke(12 /* System.Void Invector.vCharacterController.vThirdPersonMotor::AirControl() */, __this);
		// }
		return;
	}
}
// System.Void Invector.vCharacterController.vThirdPersonMotor::SetControllerMoveSpeed(Invector.vCharacterController.vThirdPersonMotor/vMovementSpeed)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonMotor_SetControllerMoveSpeed_m7424F68F2F99576696729CACA574C86DBAE34FB4 (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * ___speed0, const RuntimeMethod* method)
{
	float G_B3_0 = 0.0f;
	vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * G_B3_1 = NULL;
	float G_B2_0 = 0.0f;
	vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * G_B2_1 = NULL;
	float G_B4_0 = 0.0f;
	float G_B4_1 = 0.0f;
	vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * G_B4_2 = NULL;
	float G_B7_0 = 0.0f;
	vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * G_B7_1 = NULL;
	float G_B6_0 = 0.0f;
	vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * G_B6_1 = NULL;
	float G_B8_0 = 0.0f;
	float G_B8_1 = 0.0f;
	vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * G_B8_2 = NULL;
	{
		// if (speed.walkByDefault)
		vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * L_0 = ___speed0;
		NullCheck(L_0);
		bool L_1 = L_0->get_walkByDefault_3();
		if (!L_1)
		{
			goto IL_003c;
		}
	}
	{
		// moveSpeed = Mathf.Lerp(moveSpeed, isSprinting ? speed.runningSpeed : speed.walkSpeed, speed.movementSmooth * Time.deltaTime);
		float L_2 = __this->get_moveSpeed_36();
		bool L_3;
		L_3 = vThirdPersonMotor_get_isSprinting_m98A252FE6362DE6468A47BBC3B0F7FCACA2B3EDD_inline(__this, /*hidden argument*/NULL);
		G_B2_0 = L_2;
		G_B2_1 = __this;
		if (L_3)
		{
			G_B3_0 = L_2;
			G_B3_1 = __this;
			goto IL_001f;
		}
	}
	{
		vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * L_4 = ___speed0;
		NullCheck(L_4);
		float L_5 = L_4->get_walkSpeed_5();
		G_B4_0 = L_5;
		G_B4_1 = G_B2_0;
		G_B4_2 = G_B2_1;
		goto IL_0025;
	}

IL_001f:
	{
		vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * L_6 = ___speed0;
		NullCheck(L_6);
		float L_7 = L_6->get_runningSpeed_6();
		G_B4_0 = L_7;
		G_B4_1 = G_B3_0;
		G_B4_2 = G_B3_1;
	}

IL_0025:
	{
		vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * L_8 = ___speed0;
		NullCheck(L_8);
		float L_9 = L_8->get_movementSmooth_0();
		float L_10;
		L_10 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		float L_11;
		L_11 = Mathf_Lerp_m8A2A50B945F42D579EDF44D5EE79E85A4DA59616(G_B4_1, G_B4_0, ((float)il2cpp_codegen_multiply((float)L_9, (float)L_10)), /*hidden argument*/NULL);
		NullCheck(G_B4_2);
		G_B4_2->set_moveSpeed_36(L_11);
		return;
	}

IL_003c:
	{
		// moveSpeed = Mathf.Lerp(moveSpeed, isSprinting ? speed.sprintSpeed : speed.runningSpeed, speed.movementSmooth * Time.deltaTime);
		float L_12 = __this->get_moveSpeed_36();
		bool L_13;
		L_13 = vThirdPersonMotor_get_isSprinting_m98A252FE6362DE6468A47BBC3B0F7FCACA2B3EDD_inline(__this, /*hidden argument*/NULL);
		G_B6_0 = L_12;
		G_B6_1 = __this;
		if (L_13)
		{
			G_B7_0 = L_12;
			G_B7_1 = __this;
			goto IL_0053;
		}
	}
	{
		vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * L_14 = ___speed0;
		NullCheck(L_14);
		float L_15 = L_14->get_runningSpeed_6();
		G_B8_0 = L_15;
		G_B8_1 = G_B6_0;
		G_B8_2 = G_B6_1;
		goto IL_0059;
	}

IL_0053:
	{
		vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * L_16 = ___speed0;
		NullCheck(L_16);
		float L_17 = L_16->get_sprintSpeed_7();
		G_B8_0 = L_17;
		G_B8_1 = G_B7_0;
		G_B8_2 = G_B7_1;
	}

IL_0059:
	{
		vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * L_18 = ___speed0;
		NullCheck(L_18);
		float L_19 = L_18->get_movementSmooth_0();
		float L_20;
		L_20 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		float L_21;
		L_21 = Mathf_Lerp_m8A2A50B945F42D579EDF44D5EE79E85A4DA59616(G_B8_1, G_B8_0, ((float)il2cpp_codegen_multiply((float)L_19, (float)L_20)), /*hidden argument*/NULL);
		NullCheck(G_B8_2);
		G_B8_2->set_moveSpeed_36(L_21);
		// }
		return;
	}
}
// System.Void Invector.vCharacterController.vThirdPersonMotor::MoveCharacter(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonMotor_MoveCharacter_m832DD2D8FCFEF78CD2AD7FD80E2B1178E7AEEA55 (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____direction0, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  G_B2_0;
	memset((&G_B2_0), 0, sizeof(G_B2_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  G_B2_1;
	memset((&G_B2_1), 0, sizeof(G_B2_1));
	vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * G_B2_2 = NULL;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  G_B1_0;
	memset((&G_B1_0), 0, sizeof(G_B1_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  G_B1_1;
	memset((&G_B1_1), 0, sizeof(G_B1_1));
	vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * G_B1_2 = NULL;
	float G_B3_0 = 0.0f;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  G_B3_1;
	memset((&G_B3_1), 0, sizeof(G_B3_1));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  G_B3_2;
	memset((&G_B3_2), 0, sizeof(G_B3_2));
	vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * G_B3_3 = NULL;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  G_B11_0;
	memset((&G_B11_0), 0, sizeof(G_B11_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  G_B13_0;
	memset((&G_B13_0), 0, sizeof(G_B13_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  G_B13_1;
	memset((&G_B13_1), 0, sizeof(G_B13_1));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  G_B12_0;
	memset((&G_B12_0), 0, sizeof(G_B12_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  G_B12_1;
	memset((&G_B12_1), 0, sizeof(G_B12_1));
	float G_B14_0 = 0.0f;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  G_B14_1;
	memset((&G_B14_1), 0, sizeof(G_B14_1));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  G_B14_2;
	memset((&G_B14_2), 0, sizeof(G_B14_2));
	{
		// inputSmooth = Vector3.Lerp(inputSmooth, input, (isStrafing ? strafeSpeed.movementSmooth : freeSpeed.movementSmooth) * Time.deltaTime);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = __this->get_inputSmooth_50();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1 = __this->get_input_48();
		bool L_2;
		L_2 = vThirdPersonMotor_get_isStrafing_m3FB9147D8588055A220D47101B80C6FFD2B2E791_inline(__this, /*hidden argument*/NULL);
		G_B1_0 = L_1;
		G_B1_1 = L_0;
		G_B1_2 = __this;
		if (L_2)
		{
			G_B2_0 = L_1;
			G_B2_1 = L_0;
			G_B2_2 = __this;
			goto IL_0022;
		}
	}
	{
		vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * L_3 = __this->get_freeSpeed_9();
		NullCheck(L_3);
		float L_4 = L_3->get_movementSmooth_0();
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_002d;
	}

IL_0022:
	{
		vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * L_5 = __this->get_strafeSpeed_10();
		NullCheck(L_5);
		float L_6 = L_5->get_movementSmooth_0();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_002d:
	{
		float L_7;
		L_7 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		L_8 = Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524_inline(G_B3_2, G_B3_1, ((float)il2cpp_codegen_multiply((float)G_B3_0, (float)L_7)), /*hidden argument*/NULL);
		NullCheck(G_B3_3);
		G_B3_3->set_inputSmooth_50(L_8);
		// if (!isGrounded || isJumping) return;
		bool L_9;
		L_9 = vThirdPersonMotor_get_isGrounded_m3587831F44EEE43F49591F13918B05CFD28B65AB_inline(__this, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_004d;
		}
	}
	{
		bool L_10 = __this->get_isJumping_29();
		if (!L_10)
		{
			goto IL_004e;
		}
	}

IL_004d:
	{
		// if (!isGrounded || isJumping) return;
		return;
	}

IL_004e:
	{
		// _direction.y = 0;
		(&____direction0)->set_y_3((0.0f));
		// _direction.x = Mathf.Clamp(_direction.x, -1f, 1f);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11 = ____direction0;
		float L_12 = L_11.get_x_2();
		float L_13;
		L_13 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_12, (-1.0f), (1.0f), /*hidden argument*/NULL);
		(&____direction0)->set_x_2(L_13);
		// _direction.z = Mathf.Clamp(_direction.z, -1f, 1f);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14 = ____direction0;
		float L_15 = L_14.get_z_4();
		float L_16;
		L_16 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_15, (-1.0f), (1.0f), /*hidden argument*/NULL);
		(&____direction0)->set_z_4(L_16);
		// if (_direction.magnitude > 1f)
		float L_17;
		L_17 = Vector3_get_magnitude_mDDD40612220D8104E77E993E18A101A69A944991((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&____direction0), /*hidden argument*/NULL);
		if ((!(((float)L_17) > ((float)(1.0f)))))
		{
			goto IL_00a7;
		}
	}
	{
		// _direction.Normalize();
		Vector3_Normalize_m2258C159121FC81954C301DEE631BC24FCEDE780((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&____direction0), /*hidden argument*/NULL);
	}

IL_00a7:
	{
		// Vector3 targetPosition = (useRootMotion ? animator.rootPosition : _rigidbody.position) + _direction * (stopMove ? 0 : moveSpeed) * Time.deltaTime;
		bool L_18 = __this->get_useRootMotion_4();
		if (L_18)
		{
			goto IL_00bc;
		}
	}
	{
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_19 = __this->get__rigidbody_24();
		NullCheck(L_19);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20;
		L_20 = Rigidbody_get_position_m5F429382F610E324F39F33E8498A29D0828AD8E8(L_19, /*hidden argument*/NULL);
		G_B11_0 = L_20;
		goto IL_00c7;
	}

IL_00bc:
	{
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_21 = __this->get_animator_23();
		NullCheck(L_21);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_22;
		L_22 = Animator_get_rootPosition_mAF11A46CB8D953A1947FFF512F143899B3B9BFE9(L_21, /*hidden argument*/NULL);
		G_B11_0 = L_22;
	}

IL_00c7:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_23 = ____direction0;
		bool L_24;
		L_24 = vThirdPersonMotor_get_stopMove_m09C1FEC6B37CBF3071597D789E0322C93CD78C58_inline(__this, /*hidden argument*/NULL);
		G_B12_0 = L_23;
		G_B12_1 = G_B11_0;
		if (L_24)
		{
			G_B13_0 = L_23;
			G_B13_1 = G_B11_0;
			goto IL_00d8;
		}
	}
	{
		float L_25 = __this->get_moveSpeed_36();
		G_B14_0 = L_25;
		G_B14_1 = G_B12_0;
		G_B14_2 = G_B12_1;
		goto IL_00dd;
	}

IL_00d8:
	{
		G_B14_0 = (0.0f);
		G_B14_1 = G_B13_0;
		G_B14_2 = G_B13_1;
	}

IL_00dd:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_26;
		L_26 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(G_B14_1, G_B14_0, /*hidden argument*/NULL);
		float L_27;
		L_27 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_28;
		L_28 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_26, L_27, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_29;
		L_29 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(G_B14_2, L_28, /*hidden argument*/NULL);
		// Vector3 targetVelocity = (targetPosition - transform.position) / Time.deltaTime;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_30;
		L_30 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_30);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_31;
		L_31 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_30, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_32;
		L_32 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_29, L_31, /*hidden argument*/NULL);
		float L_33;
		L_33 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_34;
		L_34 = Vector3_op_Division_mE5ACBFB168FED529587457A83BA98B7DB32E2A05_inline(L_32, L_33, /*hidden argument*/NULL);
		V_0 = L_34;
		// bool useVerticalVelocity = true;
		// if (useVerticalVelocity) targetVelocity.y = _rigidbody.velocity.y;
		if (!1)
		{
			goto IL_0126;
		}
	}
	{
		// if (useVerticalVelocity) targetVelocity.y = _rigidbody.velocity.y;
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_35 = __this->get__rigidbody_24();
		NullCheck(L_35);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_36;
		L_36 = Rigidbody_get_velocity_mCFB033F3BD14C2BA68E797DFA4950F9307EC8E2C(L_35, /*hidden argument*/NULL);
		float L_37 = L_36.get_y_3();
		(&V_0)->set_y_3(L_37);
	}

IL_0126:
	{
		// _rigidbody.velocity = targetVelocity;
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_38 = __this->get__rigidbody_24();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_39 = V_0;
		NullCheck(L_38);
		Rigidbody_set_velocity_m8DC0988916EB38DFD7D4584830B41D79140BF18D(L_38, L_39, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Invector.vCharacterController.vThirdPersonMotor::CheckSlopeLimit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonMotor_CheckSlopeLimit_m118A87D02B7F95849C6DEAF2229969892CCAC1F6 (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, const RuntimeMethod* method)
{
	RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89  V_0;
	memset((&V_0), 0, sizeof(V_0));
	float V_1 = 0.0f;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		// if (input.sqrMagnitude < 0.1) return;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_0 = __this->get_address_of_input_48();
		float L_1;
		L_1 = Vector3_get_sqrMagnitude_mC567EE6DF411501A8FE1F23A0038862630B88249((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_0, /*hidden argument*/NULL);
		if ((!(((double)((double)((double)L_1))) < ((double)(0.10000000000000001)))))
		{
			goto IL_0018;
		}
	}
	{
		// if (input.sqrMagnitude < 0.1) return;
		return;
	}

IL_0018:
	{
		// var hitAngle = 0f;
		V_1 = (0.0f);
		// if (Physics.Linecast(transform.position + Vector3.up * (_capsuleCollider.height * 0.5f), transform.position + moveDirection.normalized * (_capsuleCollider.radius + 0.2f), out hitinfo, groundLayer))
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2;
		L_2 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_2, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50(/*hidden argument*/NULL);
		CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * L_5 = __this->get__capsuleCollider_28();
		NullCheck(L_5);
		float L_6;
		L_6 = CapsuleCollider_get_height_mD6CF93CB2C222F8E5B77D65B0356F8FD8005B526(L_5, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_4, ((float)il2cpp_codegen_multiply((float)L_6, (float)(0.5f))), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		L_8 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_3, L_7, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_9;
		L_9 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10;
		L_10 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_9, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_11 = __this->get_address_of_moveDirection_51();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		L_12 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_11, /*hidden argument*/NULL);
		CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * L_13 = __this->get__capsuleCollider_28();
		NullCheck(L_13);
		float L_14;
		L_14 = CapsuleCollider_get_radius_m5746DDE5E6A099269FC9DAD253887C58E8A064D0(L_13, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15;
		L_15 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_12, ((float)il2cpp_codegen_add((float)L_14, (float)(0.200000003f))), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16;
		L_16 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_10, L_15, /*hidden argument*/NULL);
		LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  L_17 = __this->get_groundLayer_19();
		int32_t L_18;
		L_18 = LayerMask_op_Implicit_mD89E9970822613D8D19B2EBCA36C79391C287BE0(L_17, /*hidden argument*/NULL);
		bool L_19;
		L_19 = Physics_Linecast_m320471A14633A94B6B3AEE1F4D3C959C58890015(L_8, L_16, (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)(&V_0), L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_013f;
		}
	}
	{
		// hitAngle = Vector3.Angle(Vector3.up, hitinfo.normal);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20;
		L_20 = Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_21;
		L_21 = RaycastHit_get_normal_m2C813B25BAECD87FD9E9CB294278B291F4CC6674((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)(&V_0), /*hidden argument*/NULL);
		float L_22;
		L_22 = Vector3_Angle_m3715AB03A36C59D8CF08F8D71E2F46454EB884C1(L_20, L_21, /*hidden argument*/NULL);
		V_1 = L_22;
		// var targetPoint = hitinfo.point + moveDirection.normalized * _capsuleCollider.radius;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_23;
		L_23 = RaycastHit_get_point_m32F7282CBB2E13393A33BAD046BDA218E48DD21E((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)(&V_0), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_24 = __this->get_address_of_moveDirection_51();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_25;
		L_25 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_24, /*hidden argument*/NULL);
		CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * L_26 = __this->get__capsuleCollider_28();
		NullCheck(L_26);
		float L_27;
		L_27 = CapsuleCollider_get_radius_m5746DDE5E6A099269FC9DAD253887C58E8A064D0(L_26, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_28;
		L_28 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_25, L_27, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_29;
		L_29 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_23, L_28, /*hidden argument*/NULL);
		V_2 = L_29;
		// if ((hitAngle > slopeLimit) && Physics.Linecast(transform.position + Vector3.up * (_capsuleCollider.height * 0.5f), targetPoint, out hitinfo, groundLayer))
		float L_30 = V_1;
		float L_31 = __this->get_slopeLimit_22();
		if ((!(((float)L_30) > ((float)L_31))))
		{
			goto IL_013f;
		}
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_32;
		L_32 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_32);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_33;
		L_33 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_32, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_34;
		L_34 = Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50(/*hidden argument*/NULL);
		CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * L_35 = __this->get__capsuleCollider_28();
		NullCheck(L_35);
		float L_36;
		L_36 = CapsuleCollider_get_height_mD6CF93CB2C222F8E5B77D65B0356F8FD8005B526(L_35, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_37;
		L_37 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_34, ((float)il2cpp_codegen_multiply((float)L_36, (float)(0.5f))), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_38;
		L_38 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_33, L_37, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_39 = V_2;
		LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  L_40 = __this->get_groundLayer_19();
		int32_t L_41;
		L_41 = LayerMask_op_Implicit_mD89E9970822613D8D19B2EBCA36C79391C287BE0(L_40, /*hidden argument*/NULL);
		bool L_42;
		L_42 = Physics_Linecast_m320471A14633A94B6B3AEE1F4D3C959C58890015(L_38, L_39, (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)(&V_0), L_41, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_013f;
		}
	}
	{
		// hitAngle = Vector3.Angle(Vector3.up, hitinfo.normal);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_43;
		L_43 = Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_44;
		L_44 = RaycastHit_get_normal_m2C813B25BAECD87FD9E9CB294278B291F4CC6674((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)(&V_0), /*hidden argument*/NULL);
		float L_45;
		L_45 = Vector3_Angle_m3715AB03A36C59D8CF08F8D71E2F46454EB884C1(L_43, L_44, /*hidden argument*/NULL);
		V_1 = L_45;
		// if (hitAngle > slopeLimit && hitAngle < 85f)
		float L_46 = V_1;
		float L_47 = __this->get_slopeLimit_22();
		if ((!(((float)L_46) > ((float)L_47))))
		{
			goto IL_013f;
		}
	}
	{
		float L_48 = V_1;
		if ((!(((float)L_48) < ((float)(85.0f)))))
		{
			goto IL_013f;
		}
	}
	{
		// stopMove = true;
		vThirdPersonMotor_set_stopMove_m700BD3D0627D69453D1BB1D453062FAD5F568BC8_inline(__this, (bool)1, /*hidden argument*/NULL);
		// return;
		return;
	}

IL_013f:
	{
		// stopMove = false;
		vThirdPersonMotor_set_stopMove_m700BD3D0627D69453D1BB1D453062FAD5F568BC8_inline(__this, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Invector.vCharacterController.vThirdPersonMotor::RotateToPosition(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonMotor_RotateToPosition_m25177D466A510225531D8E6916E21094271A0631 (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// Vector3 desiredDirection = position - transform.position;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___position0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_1, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_0, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		// RotateToDirection(desiredDirection.normalized);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), /*hidden argument*/NULL);
		VirtActionInvoker1< Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  >::Invoke(9 /* System.Void Invector.vCharacterController.vThirdPersonMotor::RotateToDirection(UnityEngine.Vector3) */, __this, L_4);
		// }
		return;
	}
}
// System.Void Invector.vCharacterController.vThirdPersonMotor::RotateToDirection(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonMotor_RotateToDirection_m957699965A599CD6FE6BD48E190181FD4DC40C82 (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___direction0, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  G_B2_0;
	memset((&G_B2_0), 0, sizeof(G_B2_0));
	vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * G_B2_1 = NULL;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  G_B1_0;
	memset((&G_B1_0), 0, sizeof(G_B1_0));
	vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * G_B1_1 = NULL;
	float G_B3_0 = 0.0f;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  G_B3_1;
	memset((&G_B3_1), 0, sizeof(G_B3_1));
	vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * G_B3_2 = NULL;
	{
		// RotateToDirection(direction, isStrafing ? strafeSpeed.rotationSpeed : freeSpeed.rotationSpeed);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___direction0;
		bool L_1;
		L_1 = vThirdPersonMotor_get_isStrafing_m3FB9147D8588055A220D47101B80C6FFD2B2E791_inline(__this, /*hidden argument*/NULL);
		G_B1_0 = L_0;
		G_B1_1 = __this;
		if (L_1)
		{
			G_B2_0 = L_0;
			G_B2_1 = __this;
			goto IL_0017;
		}
	}
	{
		vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * L_2 = __this->get_freeSpeed_9();
		NullCheck(L_2);
		float L_3 = L_2->get_rotationSpeed_2();
		G_B3_0 = L_3;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0022;
	}

IL_0017:
	{
		vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * L_4 = __this->get_strafeSpeed_10();
		NullCheck(L_4);
		float L_5 = L_4->get_rotationSpeed_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0022:
	{
		NullCheck(G_B3_2);
		VirtActionInvoker2< Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E , float >::Invoke(10 /* System.Void Invector.vCharacterController.vThirdPersonMotor::RotateToDirection(UnityEngine.Vector3,System.Single) */, G_B3_2, G_B3_1, G_B3_0);
		// }
		return;
	}
}
// System.Void Invector.vCharacterController.vThirdPersonMotor::RotateToDirection(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonMotor_RotateToDirection_m3D8A7E7D74A1FDAF4242E7D6494214118B2FE62A (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___direction0, float ___rotationSpeed1, const RuntimeMethod* method)
{
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if (!jumpAndRotate && !isGrounded) return;
		bool L_0 = __this->get_jumpAndRotate_12();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		bool L_1;
		L_1 = vThirdPersonMotor_get_isGrounded_m3587831F44EEE43F49591F13918B05CFD28B65AB_inline(__this, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		// if (!jumpAndRotate && !isGrounded) return;
		return;
	}

IL_0011:
	{
		// direction.y = 0f;
		(&___direction0)->set_y_3((0.0f));
		// Vector3 desiredForward = Vector3.RotateTowards(transform.forward, direction.normalized, rotationSpeed * Time.deltaTime, .1f);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2;
		L_2 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_2, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&___direction0), /*hidden argument*/NULL);
		float L_5 = ___rotationSpeed1;
		float L_6;
		L_6 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Vector3_RotateTowards_mCE2B2820B2483A44056A74E9C2C22359ED7D1AD5(L_3, L_4, ((float)il2cpp_codegen_multiply((float)L_5, (float)L_6)), (0.100000001f), /*hidden argument*/NULL);
		// Quaternion _newRotation = Quaternion.LookRotation(desiredForward);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_8;
		L_8 = Quaternion_LookRotation_m1B0BEBEBCC384324A6771B9EAC89761F73E1D6BF(L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		// transform.rotation = _newRotation;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_9;
		L_9 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_10 = V_0;
		NullCheck(L_9);
		Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4(L_9, L_10, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Invector.vCharacterController.vThirdPersonMotor::ControlJumpBehaviour()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonMotor_ControlJumpBehaviour_mF60B9850DBA522019E086DBD1152B633D7FC1189 (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if (!isJumping) return;
		bool L_0 = __this->get_isJumping_29();
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		// if (!isJumping) return;
		return;
	}

IL_0009:
	{
		// jumpCounter -= Time.deltaTime;
		float L_1 = __this->get_jumpCounter_41();
		float L_2;
		L_2 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		__this->set_jumpCounter_41(((float)il2cpp_codegen_subtract((float)L_1, (float)L_2)));
		// if (jumpCounter <= 0)
		float L_3 = __this->get_jumpCounter_41();
		if ((!(((float)L_3) <= ((float)(0.0f)))))
		{
			goto IL_003a;
		}
	}
	{
		// jumpCounter = 0;
		__this->set_jumpCounter_41((0.0f));
		// isJumping = false;
		__this->set_isJumping_29((bool)0);
	}

IL_003a:
	{
		// var vel = _rigidbody.velocity;
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_4 = __this->get__rigidbody_24();
		NullCheck(L_4);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = Rigidbody_get_velocity_mCFB033F3BD14C2BA68E797DFA4950F9307EC8E2C(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		// vel.y = jumpHeight;
		float L_6 = __this->get_jumpHeight_14();
		(&V_0)->set_y_3(L_6);
		// _rigidbody.velocity = vel;
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_7 = __this->get__rigidbody_24();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = V_0;
		NullCheck(L_7);
		Rigidbody_set_velocity_m8DC0988916EB38DFD7D4584830B41D79140BF18D(L_7, L_8, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Invector.vCharacterController.vThirdPersonMotor::AirControl()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonMotor_AirControl_m168338C0D35FC62784D2453584D4C4AF6510D676 (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if ((isGrounded && !isJumping)) return;
		bool L_0;
		L_0 = vThirdPersonMotor_get_isGrounded_m3587831F44EEE43F49591F13918B05CFD28B65AB_inline(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		bool L_1 = __this->get_isJumping_29();
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		// if ((isGrounded && !isJumping)) return;
		return;
	}

IL_0011:
	{
		// if (transform.position.y > heightReached) heightReached = transform.position.y;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2;
		L_2 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_2, /*hidden argument*/NULL);
		float L_4 = L_3.get_y_3();
		float L_5 = __this->get_heightReached_40();
		if ((!(((float)L_4) > ((float)L_5))))
		{
			goto IL_003f;
		}
	}
	{
		// if (transform.position.y > heightReached) heightReached = transform.position.y;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6;
		L_6 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_6, /*hidden argument*/NULL);
		float L_8 = L_7.get_y_3();
		__this->set_heightReached_40(L_8);
	}

IL_003f:
	{
		// inputSmooth = Vector3.Lerp(inputSmooth, input, airSmooth * Time.deltaTime);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9 = __this->get_inputSmooth_50();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = __this->get_input_48();
		float L_11 = __this->get_airSmooth_16();
		float L_12;
		L_12 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13;
		L_13 = Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524_inline(L_9, L_10, ((float)il2cpp_codegen_multiply((float)L_11, (float)L_12)), /*hidden argument*/NULL);
		__this->set_inputSmooth_50(L_13);
		// if (jumpWithRigidbodyForce && !isGrounded)
		bool L_14 = __this->get_jumpWithRigidbodyForce_11();
		if (!L_14)
		{
			goto IL_009a;
		}
	}
	{
		bool L_15;
		L_15 = vThirdPersonMotor_get_isGrounded_m3587831F44EEE43F49591F13918B05CFD28B65AB_inline(__this, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_009a;
		}
	}
	{
		// _rigidbody.AddForce(moveDirection * airSpeed * Time.deltaTime, ForceMode.VelocityChange);
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_16 = __this->get__rigidbody_24();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_17 = __this->get_moveDirection_51();
		float L_18 = __this->get_airSpeed_15();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_19;
		L_19 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_17, L_18, /*hidden argument*/NULL);
		float L_20;
		L_20 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_21;
		L_21 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_19, L_20, /*hidden argument*/NULL);
		NullCheck(L_16);
		Rigidbody_AddForce_m78B9D94F505E19F3C63461362AD6DE7EA0836700(L_16, L_21, 2, /*hidden argument*/NULL);
		// return;
		return;
	}

IL_009a:
	{
		// moveDirection.y = 0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_22 = __this->get_address_of_moveDirection_51();
		L_22->set_y_3((0.0f));
		// moveDirection.x = Mathf.Clamp(moveDirection.x, -1f, 1f);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_23 = __this->get_address_of_moveDirection_51();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_24 = __this->get_address_of_moveDirection_51();
		float L_25 = L_24->get_x_2();
		float L_26;
		L_26 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_25, (-1.0f), (1.0f), /*hidden argument*/NULL);
		L_23->set_x_2(L_26);
		// moveDirection.z = Mathf.Clamp(moveDirection.z, -1f, 1f);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_27 = __this->get_address_of_moveDirection_51();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_28 = __this->get_address_of_moveDirection_51();
		float L_29 = L_28->get_z_4();
		float L_30;
		L_30 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_29, (-1.0f), (1.0f), /*hidden argument*/NULL);
		L_27->set_z_4(L_30);
		// Vector3 targetPosition = _rigidbody.position + (moveDirection * airSpeed) * Time.deltaTime;
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_31 = __this->get__rigidbody_24();
		NullCheck(L_31);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_32;
		L_32 = Rigidbody_get_position_m5F429382F610E324F39F33E8498A29D0828AD8E8(L_31, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_33 = __this->get_moveDirection_51();
		float L_34 = __this->get_airSpeed_15();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_35;
		L_35 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_33, L_34, /*hidden argument*/NULL);
		float L_36;
		L_36 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_37;
		L_37 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_35, L_36, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_38;
		L_38 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_32, L_37, /*hidden argument*/NULL);
		// Vector3 targetVelocity = (targetPosition - transform.position) / Time.deltaTime;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_39;
		L_39 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_39);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_40;
		L_40 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_39, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_41;
		L_41 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_38, L_40, /*hidden argument*/NULL);
		float L_42;
		L_42 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_43;
		L_43 = Vector3_op_Division_mE5ACBFB168FED529587457A83BA98B7DB32E2A05_inline(L_41, L_42, /*hidden argument*/NULL);
		V_0 = L_43;
		// targetVelocity.y = _rigidbody.velocity.y;
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_44 = __this->get__rigidbody_24();
		NullCheck(L_44);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_45;
		L_45 = Rigidbody_get_velocity_mCFB033F3BD14C2BA68E797DFA4950F9307EC8E2C(L_44, /*hidden argument*/NULL);
		float L_46 = L_45.get_y_3();
		(&V_0)->set_y_3(L_46);
		// _rigidbody.velocity = Vector3.Lerp(_rigidbody.velocity, targetVelocity, airSmooth * Time.deltaTime);
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_47 = __this->get__rigidbody_24();
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_48 = __this->get__rigidbody_24();
		NullCheck(L_48);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_49;
		L_49 = Rigidbody_get_velocity_mCFB033F3BD14C2BA68E797DFA4950F9307EC8E2C(L_48, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_50 = V_0;
		float L_51 = __this->get_airSmooth_16();
		float L_52;
		L_52 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_53;
		L_53 = Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524_inline(L_49, L_50, ((float)il2cpp_codegen_multiply((float)L_51, (float)L_52)), /*hidden argument*/NULL);
		NullCheck(L_47);
		Rigidbody_set_velocity_m8DC0988916EB38DFD7D4584830B41D79140BF18D(L_47, L_53, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Boolean Invector.vCharacterController.vThirdPersonMotor::get_jumpFwdCondition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool vThirdPersonMotor_get_jumpFwdCondition_mC547BB5DBD086F71A7A8DF12E1BF3959296FA6BD (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// Vector3 p1 = transform.position + _capsuleCollider.center + Vector3.up * -_capsuleCollider.height * 0.5F;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_0, /*hidden argument*/NULL);
		CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * L_2 = __this->get__capsuleCollider_28();
		NullCheck(L_2);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = CapsuleCollider_get_center_m6374F7457A9450CAFFAD2DF0C9D1419BF9E304CB(L_2, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_1, L_3, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50(/*hidden argument*/NULL);
		CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * L_6 = __this->get__capsuleCollider_28();
		NullCheck(L_6);
		float L_7;
		L_7 = CapsuleCollider_get_height_mD6CF93CB2C222F8E5B77D65B0356F8FD8005B526(L_6, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		L_8 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_5, ((-L_7)), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		L_9 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_8, (0.5f), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10;
		L_10 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_4, L_9, /*hidden argument*/NULL);
		// Vector3 p2 = p1 + Vector3.up * _capsuleCollider.height;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11 = L_10;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		L_12 = Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50(/*hidden argument*/NULL);
		CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * L_13 = __this->get__capsuleCollider_28();
		NullCheck(L_13);
		float L_14;
		L_14 = CapsuleCollider_get_height_mD6CF93CB2C222F8E5B77D65B0356F8FD8005B526(L_13, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15;
		L_15 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_12, L_14, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16;
		L_16 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_11, L_15, /*hidden argument*/NULL);
		V_0 = L_16;
		// return Physics.CapsuleCastAll(p1, p2, _capsuleCollider.radius * 0.5f, transform.forward, 0.6f, groundLayer).Length == 0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_17 = V_0;
		CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * L_18 = __this->get__capsuleCollider_28();
		NullCheck(L_18);
		float L_19;
		L_19 = CapsuleCollider_get_radius_m5746DDE5E6A099269FC9DAD253887C58E8A064D0(L_18, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_20;
		L_20 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_21;
		L_21 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_20, /*hidden argument*/NULL);
		LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  L_22 = __this->get_groundLayer_19();
		int32_t L_23;
		L_23 = LayerMask_op_Implicit_mD89E9970822613D8D19B2EBCA36C79391C287BE0(L_22, /*hidden argument*/NULL);
		RaycastHitU5BU5D_t6778DB95346906446AAD3A1A36904F1846435A09* L_24;
		L_24 = Physics_CapsuleCastAll_m17AD93A3F9EFAA514C83D8BDD951C7B4D850E454(L_11, L_17, ((float)il2cpp_codegen_multiply((float)L_19, (float)(0.5f))), L_21, (0.600000024f), L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		return (bool)((((int32_t)(((RuntimeArray*)L_24)->max_length)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void Invector.vCharacterController.vThirdPersonMotor::CheckGround()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonMotor_CheckGround_mC52E020122920DCBE0148C9F4A4A7E0618D3B2E8 (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, const RuntimeMethod* method)
{
	{
		// CheckGroundDistance();
		VirtActionInvoker0::Invoke(16 /* System.Void Invector.vCharacterController.vThirdPersonMotor::CheckGroundDistance() */, __this);
		// ControlMaterialPhysics();
		VirtActionInvoker0::Invoke(15 /* System.Void Invector.vCharacterController.vThirdPersonMotor::ControlMaterialPhysics() */, __this);
		// if (groundDistance <= groundMinDistance)
		float L_0 = __this->get_groundDistance_42();
		float L_1 = __this->get_groundMinDistance_20();
		if ((!(((float)L_0) <= ((float)L_1))))
		{
			goto IL_007b;
		}
	}
	{
		// isGrounded = true;
		vThirdPersonMotor_set_isGrounded_m4F6F1C10637D6E9382C07E507FCD1773F846FA9B_inline(__this, (bool)1, /*hidden argument*/NULL);
		// if (!isJumping && groundDistance > 0.05f)
		bool L_2 = __this->get_isJumping_29();
		if (L_2)
		{
			goto IL_0064;
		}
	}
	{
		float L_3 = __this->get_groundDistance_42();
		if ((!(((float)L_3) > ((float)(0.0500000007f)))))
		{
			goto IL_0064;
		}
	}
	{
		// _rigidbody.AddForce(transform.up * (extraGravity * 2 * Time.deltaTime), ForceMode.VelocityChange);
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_4 = __this->get__rigidbody_24();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_5;
		L_5 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		L_6 = Transform_get_up_mAB753D250A30C78924D5D22B0821F1D254525C31(L_5, /*hidden argument*/NULL);
		float L_7 = __this->get_extraGravity_17();
		float L_8;
		L_8 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		L_9 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_6, ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)L_7, (float)(2.0f))), (float)L_8)), /*hidden argument*/NULL);
		NullCheck(L_4);
		Rigidbody_AddForce_m78B9D94F505E19F3C63461362AD6DE7EA0836700(L_4, L_9, 2, /*hidden argument*/NULL);
	}

IL_0064:
	{
		// heightReached = transform.position.y;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_10;
		L_10 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11;
		L_11 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_10, /*hidden argument*/NULL);
		float L_12 = L_11.get_y_3();
		__this->set_heightReached_40(L_12);
		// }
		return;
	}

IL_007b:
	{
		// if (groundDistance >= groundMaxDistance)
		float L_13 = __this->get_groundDistance_42();
		float L_14 = __this->get_groundMaxDistance_21();
		if ((!(((float)L_13) >= ((float)L_14))))
		{
			goto IL_00db;
		}
	}
	{
		// isGrounded = false;
		vThirdPersonMotor_set_isGrounded_m4F6F1C10637D6E9382C07E507FCD1773F846FA9B_inline(__this, (bool)0, /*hidden argument*/NULL);
		// verticalVelocity = _rigidbody.velocity.y;
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_15 = __this->get__rigidbody_24();
		NullCheck(L_15);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16;
		L_16 = Rigidbody_get_velocity_mCFB033F3BD14C2BA68E797DFA4950F9307EC8E2C(L_15, /*hidden argument*/NULL);
		float L_17 = L_16.get_y_3();
		__this->set_verticalVelocity_37(L_17);
		// if (!isJumping)
		bool L_18 = __this->get_isJumping_29();
		if (L_18)
		{
			goto IL_0111;
		}
	}
	{
		// _rigidbody.AddForce(transform.up * extraGravity * Time.deltaTime, ForceMode.VelocityChange);
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_19 = __this->get__rigidbody_24();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_20;
		L_20 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_21;
		L_21 = Transform_get_up_mAB753D250A30C78924D5D22B0821F1D254525C31(L_20, /*hidden argument*/NULL);
		float L_22 = __this->get_extraGravity_17();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_23;
		L_23 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_21, L_22, /*hidden argument*/NULL);
		float L_24;
		L_24 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_25;
		L_25 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_23, L_24, /*hidden argument*/NULL);
		NullCheck(L_19);
		Rigidbody_AddForce_m78B9D94F505E19F3C63461362AD6DE7EA0836700(L_19, L_25, 2, /*hidden argument*/NULL);
		// }
		return;
	}

IL_00db:
	{
		// else if (!isJumping)
		bool L_26 = __this->get_isJumping_29();
		if (L_26)
		{
			goto IL_0111;
		}
	}
	{
		// _rigidbody.AddForce(transform.up * (extraGravity * 2 * Time.deltaTime), ForceMode.VelocityChange);
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_27 = __this->get__rigidbody_24();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_28;
		L_28 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_28);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_29;
		L_29 = Transform_get_up_mAB753D250A30C78924D5D22B0821F1D254525C31(L_28, /*hidden argument*/NULL);
		float L_30 = __this->get_extraGravity_17();
		float L_31;
		L_31 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_32;
		L_32 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_29, ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)L_30, (float)(2.0f))), (float)L_31)), /*hidden argument*/NULL);
		NullCheck(L_27);
		Rigidbody_AddForce_m78B9D94F505E19F3C63461362AD6DE7EA0836700(L_27, L_32, 2, /*hidden argument*/NULL);
	}

IL_0111:
	{
		// }
		return;
	}
}
// System.Void Invector.vCharacterController.vThirdPersonMotor::ControlMaterialPhysics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonMotor_ControlMaterialPhysics_mEC6FAD551AB23C371DF71115713081883DC1BF01 (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, const RuntimeMethod* method)
{
	CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * G_B2_0 = NULL;
	CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * G_B1_0 = NULL;
	CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * G_B3_0 = NULL;
	PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 * G_B4_0 = NULL;
	CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * G_B4_1 = NULL;
	{
		// _capsuleCollider.material = (isGrounded && GroundAngle() <= slopeLimit + 1) ? frictionPhysics : slippyPhysics;
		CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * L_0 = __this->get__capsuleCollider_28();
		bool L_1;
		L_1 = vThirdPersonMotor_get_isGrounded_m3587831F44EEE43F49591F13918B05CFD28B65AB_inline(__this, /*hidden argument*/NULL);
		G_B1_0 = L_0;
		if (!L_1)
		{
			G_B2_0 = L_0;
			goto IL_0022;
		}
	}
	{
		float L_2;
		L_2 = VirtFuncInvoker0< float >::Invoke(17 /* System.Single Invector.vCharacterController.vThirdPersonMotor::GroundAngle() */, __this);
		float L_3 = __this->get_slopeLimit_22();
		G_B2_0 = G_B1_0;
		if ((((float)L_2) <= ((float)((float)il2cpp_codegen_add((float)L_3, (float)(1.0f))))))
		{
			G_B3_0 = G_B1_0;
			goto IL_002a;
		}
	}

IL_0022:
	{
		PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 * L_4 = __this->get_slippyPhysics_27();
		G_B4_0 = L_4;
		G_B4_1 = G_B2_0;
		goto IL_0030;
	}

IL_002a:
	{
		PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 * L_5 = __this->get_frictionPhysics_25();
		G_B4_0 = L_5;
		G_B4_1 = G_B3_0;
	}

IL_0030:
	{
		NullCheck(G_B4_1);
		Collider_set_material_m3B07EBDE2756F6F250C6202EA1F67C95072B9D72(G_B4_1, G_B4_0, /*hidden argument*/NULL);
		// if (isGrounded && input == Vector3.zero)
		bool L_6;
		L_6 = vThirdPersonMotor_get_isGrounded_m3587831F44EEE43F49591F13918B05CFD28B65AB_inline(__this, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0061;
		}
	}
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7 = __this->get_input_48();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		L_8 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		bool L_9;
		L_9 = Vector3_op_Equality_m8A98C7F38641110A2F90445EF8E98ECE14B08296(L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0061;
		}
	}
	{
		// _capsuleCollider.material = maxFrictionPhysics;
		CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * L_10 = __this->get__capsuleCollider_28();
		PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 * L_11 = __this->get_maxFrictionPhysics_26();
		NullCheck(L_10);
		Collider_set_material_m3B07EBDE2756F6F250C6202EA1F67C95072B9D72(L_10, L_11, /*hidden argument*/NULL);
		return;
	}

IL_0061:
	{
		// else if (isGrounded && input != Vector3.zero)
		bool L_12;
		L_12 = vThirdPersonMotor_get_isGrounded_m3587831F44EEE43F49591F13918B05CFD28B65AB_inline(__this, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_008d;
		}
	}
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = __this->get_input_48();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14;
		L_14 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		bool L_15;
		L_15 = Vector3_op_Inequality_m15190A795B416EB699E69E6190DE6F1C1F208710(L_13, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_008d;
		}
	}
	{
		// _capsuleCollider.material = frictionPhysics;
		CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * L_16 = __this->get__capsuleCollider_28();
		PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 * L_17 = __this->get_frictionPhysics_25();
		NullCheck(L_16);
		Collider_set_material_m3B07EBDE2756F6F250C6202EA1F67C95072B9D72(L_16, L_17, /*hidden argument*/NULL);
		return;
	}

IL_008d:
	{
		// _capsuleCollider.material = slippyPhysics;
		CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * L_18 = __this->get__capsuleCollider_28();
		PhysicMaterial_tD3D9C84806E95BABF076A74331DF8D9A4B03E3C2 * L_19 = __this->get_slippyPhysics_27();
		NullCheck(L_18);
		Collider_set_material_m3B07EBDE2756F6F250C6202EA1F67C95072B9D72(L_18, L_19, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Invector.vCharacterController.vThirdPersonMotor::CheckGroundDistance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonMotor_CheckGroundDistance_mA4A2343321F0A6E19109C18716E69F945C9269E9 (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_2;
	memset((&V_2), 0, sizeof(V_2));
	float V_3 = 0.0f;
	{
		// if (_capsuleCollider != null)
		CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * L_0 = __this->get__capsuleCollider_28();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_01c6;
		}
	}
	{
		// float radius = _capsuleCollider.radius * 0.9f;
		CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * L_2 = __this->get__capsuleCollider_28();
		NullCheck(L_2);
		float L_3;
		L_3 = CapsuleCollider_get_radius_m5746DDE5E6A099269FC9DAD253887C58E8A064D0(L_2, /*hidden argument*/NULL);
		V_0 = ((float)il2cpp_codegen_multiply((float)L_3, (float)(0.899999976f)));
		// var dist = 10f;
		V_1 = (10.0f);
		// Ray ray2 = new Ray(transform.position + new Vector3(0, colliderHeight / 2, 0), Vector3.down);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4;
		L_4 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_4, /*hidden argument*/NULL);
		float L_6 = __this->get_colliderHeight_39();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		memset((&L_7), 0, sizeof(L_7));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_7), (0.0f), ((float)((float)L_6/(float)(2.0f))), (0.0f), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		L_8 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_5, L_7, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		L_9 = Vector3_get_down_mFA85B870E184121D30F66395BB183ECAB9DD8629(/*hidden argument*/NULL);
		Ray_t2E9E67CC8B03EE6ED2BBF3D2C9C96DDF70E1D5E6  L_10;
		memset((&L_10), 0, sizeof(L_10));
		Ray__ctor_m75B1F651FF47EE6B887105101B7DA61CBF41F83C((&L_10), L_8, L_9, /*hidden argument*/NULL);
		// if (Physics.Raycast(ray2, out groundHit, (colliderHeight / 2) + dist, groundLayer) && !groundHit.collider.isTrigger)
		RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * L_11 = __this->get_address_of_groundHit_43();
		float L_12 = __this->get_colliderHeight_39();
		float L_13 = V_1;
		LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  L_14 = __this->get_groundLayer_19();
		int32_t L_15;
		L_15 = LayerMask_op_Implicit_mD89E9970822613D8D19B2EBCA36C79391C287BE0(L_14, /*hidden argument*/NULL);
		bool L_16;
		L_16 = Physics_Raycast_m46E12070D6996F4C8C91D49ADC27C74AC1D6A3D8(L_10, (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)L_11, ((float)il2cpp_codegen_add((float)((float)((float)L_12/(float)(2.0f))), (float)L_13)), L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00b8;
		}
	}
	{
		RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * L_17 = __this->get_address_of_groundHit_43();
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_18;
		L_18 = RaycastHit_get_collider_m13A3DE16FBC631E0A1F987E0B22CE70AF8AB539E((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		bool L_19;
		L_19 = Collider_get_isTrigger_m3A9C990365C94B7125DB5993D782D3D0FE876A60(L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_00b8;
		}
	}
	{
		// dist = transform.position.y - groundHit.point.y;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_20;
		L_20 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_21;
		L_21 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_20, /*hidden argument*/NULL);
		float L_22 = L_21.get_y_3();
		RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * L_23 = __this->get_address_of_groundHit_43();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_24;
		L_24 = RaycastHit_get_point_m32F7282CBB2E13393A33BAD046BDA218E48DD21E((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)L_23, /*hidden argument*/NULL);
		float L_25 = L_24.get_y_3();
		V_1 = ((float)il2cpp_codegen_subtract((float)L_22, (float)L_25));
	}

IL_00b8:
	{
		// if (dist >= groundMinDistance)
		float L_26 = V_1;
		float L_27 = __this->get_groundMinDistance_20();
		if ((!(((float)L_26) >= ((float)L_27))))
		{
			goto IL_01b7;
		}
	}
	{
		// Vector3 pos = transform.position + Vector3.up * (_capsuleCollider.radius);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_28;
		L_28 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_28);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_29;
		L_29 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_28, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_30;
		L_30 = Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50(/*hidden argument*/NULL);
		CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * L_31 = __this->get__capsuleCollider_28();
		NullCheck(L_31);
		float L_32;
		L_32 = CapsuleCollider_get_radius_m5746DDE5E6A099269FC9DAD253887C58E8A064D0(L_31, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_33;
		L_33 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_30, L_32, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_34;
		L_34 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_29, L_33, /*hidden argument*/NULL);
		V_2 = L_34;
		// Ray ray = new Ray(pos, -Vector3.up);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_35 = V_2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_36;
		L_36 = Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_37;
		L_37 = Vector3_op_UnaryNegation_m362EA356F4CADEDB39F965A0DBDED6EA890925F7_inline(L_36, /*hidden argument*/NULL);
		Ray_t2E9E67CC8B03EE6ED2BBF3D2C9C96DDF70E1D5E6  L_38;
		memset((&L_38), 0, sizeof(L_38));
		Ray__ctor_m75B1F651FF47EE6B887105101B7DA61CBF41F83C((&L_38), L_35, L_37, /*hidden argument*/NULL);
		// if (Physics.SphereCast(ray, radius, out groundHit, _capsuleCollider.radius + groundMaxDistance, groundLayer) && !groundHit.collider.isTrigger)
		float L_39 = V_0;
		RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * L_40 = __this->get_address_of_groundHit_43();
		CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * L_41 = __this->get__capsuleCollider_28();
		NullCheck(L_41);
		float L_42;
		L_42 = CapsuleCollider_get_radius_m5746DDE5E6A099269FC9DAD253887C58E8A064D0(L_41, /*hidden argument*/NULL);
		float L_43 = __this->get_groundMaxDistance_21();
		LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  L_44 = __this->get_groundLayer_19();
		int32_t L_45;
		L_45 = LayerMask_op_Implicit_mD89E9970822613D8D19B2EBCA36C79391C287BE0(L_44, /*hidden argument*/NULL);
		bool L_46;
		L_46 = Physics_SphereCast_mE188E0B54CBCA90E9676B9E17931F1649E568AD9(L_38, L_39, (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)L_40, ((float)il2cpp_codegen_add((float)L_42, (float)L_43)), L_45, /*hidden argument*/NULL);
		if (!L_46)
		{
			goto IL_01b7;
		}
	}
	{
		RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * L_47 = __this->get_address_of_groundHit_43();
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_48;
		L_48 = RaycastHit_get_collider_m13A3DE16FBC631E0A1F987E0B22CE70AF8AB539E((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)L_47, /*hidden argument*/NULL);
		NullCheck(L_48);
		bool L_49;
		L_49 = Collider_get_isTrigger_m3A9C990365C94B7125DB5993D782D3D0FE876A60(L_48, /*hidden argument*/NULL);
		if (L_49)
		{
			goto IL_01b7;
		}
	}
	{
		// Physics.Linecast(groundHit.point + (Vector3.up * 0.1f), groundHit.point + Vector3.down * 0.15f, out groundHit, groundLayer);
		RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * L_50 = __this->get_address_of_groundHit_43();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_51;
		L_51 = RaycastHit_get_point_m32F7282CBB2E13393A33BAD046BDA218E48DD21E((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)L_50, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_52;
		L_52 = Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_53;
		L_53 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_52, (0.100000001f), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_54;
		L_54 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_51, L_53, /*hidden argument*/NULL);
		RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * L_55 = __this->get_address_of_groundHit_43();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_56;
		L_56 = RaycastHit_get_point_m32F7282CBB2E13393A33BAD046BDA218E48DD21E((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)L_55, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_57;
		L_57 = Vector3_get_down_mFA85B870E184121D30F66395BB183ECAB9DD8629(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_58;
		L_58 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_57, (0.150000006f), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_59;
		L_59 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_56, L_58, /*hidden argument*/NULL);
		RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * L_60 = __this->get_address_of_groundHit_43();
		LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  L_61 = __this->get_groundLayer_19();
		int32_t L_62;
		L_62 = LayerMask_op_Implicit_mD89E9970822613D8D19B2EBCA36C79391C287BE0(L_61, /*hidden argument*/NULL);
		bool L_63;
		L_63 = Physics_Linecast_m320471A14633A94B6B3AEE1F4D3C959C58890015(L_54, L_59, (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)L_60, L_62, /*hidden argument*/NULL);
		// float newDist = transform.position.y - groundHit.point.y;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_64;
		L_64 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_64);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_65;
		L_65 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_64, /*hidden argument*/NULL);
		float L_66 = L_65.get_y_3();
		RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * L_67 = __this->get_address_of_groundHit_43();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_68;
		L_68 = RaycastHit_get_point_m32F7282CBB2E13393A33BAD046BDA218E48DD21E((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)L_67, /*hidden argument*/NULL);
		float L_69 = L_68.get_y_3();
		V_3 = ((float)il2cpp_codegen_subtract((float)L_66, (float)L_69));
		// if (dist > newDist) dist = newDist;
		float L_70 = V_1;
		float L_71 = V_3;
		if ((!(((float)L_70) > ((float)L_71))))
		{
			goto IL_01b7;
		}
	}
	{
		// if (dist > newDist) dist = newDist;
		float L_72 = V_3;
		V_1 = L_72;
	}

IL_01b7:
	{
		// groundDistance = (float)System.Math.Round(dist, 2);
		float L_73 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		double L_74;
		L_74 = Math_Round_m394EEE2C796B3A1578E65037E0D57B3D6F9B1C70(((double)((double)L_73)), 2, /*hidden argument*/NULL);
		__this->set_groundDistance_42(((float)((float)L_74)));
	}

IL_01c6:
	{
		// }
		return;
	}
}
// System.Single Invector.vCharacterController.vThirdPersonMotor::GroundAngle()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float vThirdPersonMotor_GroundAngle_m2359CDDC90DAA5B812C8824B9D41FB5D07BC41B7 (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, const RuntimeMethod* method)
{
	{
		// var groundAngle = Vector3.Angle(groundHit.normal, Vector3.up);
		RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * L_0 = __this->get_address_of_groundHit_43();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = RaycastHit_get_normal_m2C813B25BAECD87FD9E9CB294278B291F4CC6674((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)L_0, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50(/*hidden argument*/NULL);
		float L_3;
		L_3 = Vector3_Angle_m3715AB03A36C59D8CF08F8D71E2F46454EB884C1(L_1, L_2, /*hidden argument*/NULL);
		// return groundAngle;
		return L_3;
	}
}
// System.Single Invector.vCharacterController.vThirdPersonMotor::GroundAngleFromDirection()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float vThirdPersonMotor_GroundAngleFromDirection_m1448DCC1649C7294DD053E6C363DF0E26267443F (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  G_B4_0;
	memset((&G_B4_0), 0, sizeof(G_B4_0));
	{
		// var dir = isStrafing && input.magnitude > 0 ? (transform.right * input.x + transform.forward * input.z).normalized : transform.forward;
		bool L_0;
		L_0 = vThirdPersonMotor_get_isStrafing_m3FB9147D8588055A220D47101B80C6FFD2B2E791_inline(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_1 = __this->get_address_of_input_48();
		float L_2;
		L_2 = Vector3_get_magnitude_mDDD40612220D8104E77E993E18A101A69A944991((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_1, /*hidden argument*/NULL);
		if ((((float)L_2) > ((float)(0.0f))))
		{
			goto IL_0027;
		}
	}

IL_001a:
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3;
		L_3 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_3, /*hidden argument*/NULL);
		G_B4_0 = L_4;
		goto IL_006a;
	}

IL_0027:
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_5;
		L_5 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		L_6 = Transform_get_right_mA8EB1882CCE8759766544448308C88D9200F06CE(L_5, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_7 = __this->get_address_of_input_48();
		float L_8 = L_7->get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		L_9 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_6, L_8, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_10;
		L_10 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11;
		L_11 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_10, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_12 = __this->get_address_of_input_48();
		float L_13 = L_12->get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14;
		L_14 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_11, L_13, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15;
		L_15 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_9, L_14, /*hidden argument*/NULL);
		V_0 = L_15;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16;
		L_16 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), /*hidden argument*/NULL);
		G_B4_0 = L_16;
	}

IL_006a:
	{
		// var movementAngle = Vector3.Angle(dir, groundHit.normal) - 90;
		RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * L_17 = __this->get_address_of_groundHit_43();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_18;
		L_18 = RaycastHit_get_normal_m2C813B25BAECD87FD9E9CB294278B291F4CC6674((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)L_17, /*hidden argument*/NULL);
		float L_19;
		L_19 = Vector3_Angle_m3715AB03A36C59D8CF08F8D71E2F46454EB884C1(G_B4_0, L_18, /*hidden argument*/NULL);
		// return movementAngle;
		return ((float)il2cpp_codegen_subtract((float)L_19, (float)(90.0f)));
	}
}
// System.Void Invector.vCharacterController.vThirdPersonMotor::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vThirdPersonMotor__ctor_m4AEDE34F41DF0A598D8C8DDB4BA8F5088AC7D06D (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, const RuntimeMethod* method)
{
	{
		// public bool useContinuousSprint = true;
		__this->set_useContinuousSprint_6((bool)1);
		// public bool sprintOnlyFree = true;
		__this->set_sprintOnlyFree_7((bool)1);
		// public bool jumpAndRotate = true;
		__this->set_jumpAndRotate_12((bool)1);
		// public float jumpTimer = 0.3f;
		__this->set_jumpTimer_13((0.300000012f));
		// public float jumpHeight = 4f;
		__this->set_jumpHeight_14((4.0f));
		// public float airSpeed = 5f;
		__this->set_airSpeed_15((5.0f));
		// public float airSmooth = 6f;
		__this->set_airSmooth_16((6.0f));
		// public float extraGravity = -10f;
		__this->set_extraGravity_17((-10.0f));
		// public float limitFallVelocity = -15f;
		__this->set_limitFallVelocity_18((-15.0f));
		// public LayerMask groundLayer = 1 << 0;
		LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  L_0;
		L_0 = LayerMask_op_Implicit_mC7EE32122D2A4786D3C00B93E41604B71BF1397C(1, /*hidden argument*/NULL);
		__this->set_groundLayer_19(L_0);
		// public float groundMinDistance = 0.25f;
		__this->set_groundMinDistance_20((0.25f));
		// public float groundMaxDistance = 0.5f;
		__this->set_groundMaxDistance_21((0.5f));
		// [Range(30, 80)] public float slopeLimit = 75f;
		__this->set_slopeLimit_22((75.0f));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void InvocadorOleadas/<InvocarOleada>d__9::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CInvocarOleadaU3Ed__9__ctor_mEA8339C3505658A60279289CD8D0CFD2195FAA53 (U3CInvocarOleadaU3Ed__9_t095DFE910F3D2E69D3A9F620E99A7FD143751007 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void InvocadorOleadas/<InvocarOleada>d__9::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CInvocarOleadaU3Ed__9_System_IDisposable_Dispose_m873ADCB92CA17BB6FF49F24B13E92EFF3AE467E6 (U3CInvocarOleadaU3Ed__9_t095DFE910F3D2E69D3A9F620E99A7FD143751007 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean InvocadorOleadas/<InvocarOleada>d__9::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CInvocarOleadaU3Ed__9_MoveNext_m00641A8575EF644444C8414060D10905741093FE (U3CInvocarOleadaU3Ed__9_t095DFE910F3D2E69D3A9F620E99A7FD143751007 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	InvocadorOleadas_t7ADC79FAB86E1B7AE663CA487985C11C00B4C530 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		InvocadorOleadas_t7ADC79FAB86E1B7AE663CA487985C11C00B4C530 * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0046;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->set_U3CU3E1__state_0((-1));
		// for (int i = 0; i < numeroOleada; i++)
		__this->set_U3CiU3E5__2_3(0);
		goto IL_005d;
	}

IL_0027:
	{
		// InvocarEnemigo();
		InvocadorOleadas_t7ADC79FAB86E1B7AE663CA487985C11C00B4C530 * L_4 = V_1;
		NullCheck(L_4);
		InvocadorOleadas_InvocarEnemigo_m620453681026EA6BAA065D19200F4ECB33573A7A(L_4, /*hidden argument*/NULL);
		// yield return new WaitForSeconds(0.5f);
		WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * L_5 = (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 *)il2cpp_codegen_object_new(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4(L_5, (0.5f), /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_5);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0046:
	{
		__this->set_U3CU3E1__state_0((-1));
		// for (int i = 0; i < numeroOleada; i++)
		int32_t L_6 = __this->get_U3CiU3E5__2_3();
		V_2 = L_6;
		int32_t L_7 = V_2;
		__this->set_U3CiU3E5__2_3(((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1)));
	}

IL_005d:
	{
		// for (int i = 0; i < numeroOleada; i++)
		int32_t L_8 = __this->get_U3CiU3E5__2_3();
		InvocadorOleadas_t7ADC79FAB86E1B7AE663CA487985C11C00B4C530 * L_9 = V_1;
		NullCheck(L_9);
		int32_t L_10 = L_9->get_numeroOleada_10();
		if ((((int32_t)L_8) < ((int32_t)L_10)))
		{
			goto IL_0027;
		}
	}
	{
		// numeroOleada++;
		InvocadorOleadas_t7ADC79FAB86E1B7AE663CA487985C11C00B4C530 * L_11 = V_1;
		InvocadorOleadas_t7ADC79FAB86E1B7AE663CA487985C11C00B4C530 * L_12 = V_1;
		NullCheck(L_12);
		int32_t L_13 = L_12->get_numeroOleada_10();
		NullCheck(L_11);
		L_11->set_numeroOleada_10(((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1)));
		// aviso.gameObject.SetActive(false);
		InvocadorOleadas_t7ADC79FAB86E1B7AE663CA487985C11C00B4C530 * L_14 = V_1;
		NullCheck(L_14);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_15 = L_14->get_aviso_9();
		NullCheck(L_15);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_16;
		L_16 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_16, (bool)0, /*hidden argument*/NULL);
		// }
		return (bool)0;
	}
}
// System.Object InvocadorOleadas/<InvocarOleada>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CInvocarOleadaU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFC6813F16024A7C4D990499877B4B059FA4A9523 (U3CInvocarOleadaU3Ed__9_t095DFE910F3D2E69D3A9F620E99A7FD143751007 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void InvocadorOleadas/<InvocarOleada>d__9::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CInvocarOleadaU3Ed__9_System_Collections_IEnumerator_Reset_m36DD77A221E7624753B5F896258F11EC87949022 (U3CInvocarOleadaU3Ed__9_t095DFE910F3D2E69D3A9F620E99A7FD143751007 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CInvocarOleadaU3Ed__9_System_Collections_IEnumerator_Reset_m36DD77A221E7624753B5F896258F11EC87949022_RuntimeMethod_var)));
	}
}
// System.Object InvocadorOleadas/<InvocarOleada>d__9::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CInvocarOleadaU3Ed__9_System_Collections_IEnumerator_get_Current_m376E2436B934AE768423E6088A6B7B4F62370012 (U3CInvocarOleadaU3Ed__9_t095DFE910F3D2E69D3A9F620E99A7FD143751007 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Invector.vCharacterController.vThirdPersonMotor/vMovementSpeed::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vMovementSpeed__ctor_m52DC8BDAF03A84775E1E8805B582EFE944686D90 (vMovementSpeed_t4E6723503341F445407BA3E046D5E20678B85E25 * __this, const RuntimeMethod* method)
{
	{
		// public float movementSmooth = 6f;
		__this->set_movementSmooth_0((6.0f));
		// public float animationSmooth = 0.2f;
		__this->set_animationSmooth_1((0.200000003f));
		// public float rotationSpeed = 16f;
		__this->set_rotationSpeed_2((16.0f));
		// public float walkSpeed = 2f;
		__this->set_walkSpeed_5((2.0f));
		// public float runningSpeed = 4f;
		__this->set_runningSpeed_6((4.0f));
		// public float sprintSpeed = 6f;
		__this->set_sprintSpeed_7((6.0f));
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_subtract((float)L_1, (float)L_3)), ((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), ((float)il2cpp_codegen_subtract((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		float L_2 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = ___a0;
		float L_4 = L_3.get_y_3();
		float L_5 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a0;
		float L_7 = L_6.get_z_4();
		float L_8 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_9), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = V_0;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_add((float)L_1, (float)L_3)), ((float)il2cpp_codegen_add((float)L_5, (float)L_7)), ((float)il2cpp_codegen_add((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool vThirdPersonMotor_get_isStrafing_m3FB9147D8588055A220D47101B80C6FFD2B2E791_inline (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, const RuntimeMethod* method)
{
	{
		// return _isStrafing;
		bool L_0 = __this->get__isStrafing_46();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool vThirdPersonMotor_get_isSprinting_m98A252FE6362DE6468A47BBC3B0F7FCACA2B3EDD_inline (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, const RuntimeMethod* method)
{
	{
		// internal bool isSprinting { get; set; }
		bool L_0 = __this->get_U3CisSprintingU3Ek__BackingField_31();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool vThirdPersonMotor_get_isGrounded_m3587831F44EEE43F49591F13918B05CFD28B65AB_inline (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, const RuntimeMethod* method)
{
	{
		// internal bool isGrounded { get; set; }
		bool L_0 = __this->get_U3CisGroundedU3Ek__BackingField_30();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool vThirdPersonMotor_get_stopMove_m09C1FEC6B37CBF3071597D789E0322C93CD78C58_inline (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, const RuntimeMethod* method)
{
	{
		// public bool stopMove { get; protected set; }
		bool L_0 = __this->get_U3CstopMoveU3Ek__BackingField_32();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		float L_2 = ___z2;
		__this->set_z_4(L_2);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m079B29E4F58127F03BD52558C1FE1A528547328F_inline (float ___d0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a1;
		float L_1 = L_0.get_x_2();
		float L_2 = ___d0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = ___a1;
		float L_4 = L_3.get_y_3();
		float L_5 = ___d0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a1;
		float L_7 = L_6.get_z_4();
		float L_8 = ___d0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_9), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = V_0;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector3_Dot_mD19905B093915BA12852732EA27AA2DBE030D11F_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___lhs0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rhs1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___lhs0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___rhs1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___lhs0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___rhs1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___lhs0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___rhs1;
		float L_11 = L_10.get_z_4();
		V_0 = ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_1, (float)L_3)), (float)((float)il2cpp_codegen_multiply((float)L_5, (float)L_7)))), (float)((float)il2cpp_codegen_multiply((float)L_9, (float)L_11))));
		goto IL_002d;
	}

IL_002d:
	{
		float L_12 = V_0;
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void vThirdPersonMotor_set_isStrafing_m90A9F59590C2DABE76449CE57C8FBCCCC9F39EFE_inline (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// _isStrafing = value;
		bool L_0 = ___value0;
		__this->set__isStrafing_46(L_0);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, float ___t2, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		float L_0 = ___t2;
		float L_1;
		L_1 = Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C(L_0, /*hidden argument*/NULL);
		___t2 = L_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___a0;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___b1;
		float L_5 = L_4.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a0;
		float L_7 = L_6.get_x_2();
		float L_8 = ___t2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9 = ___a0;
		float L_10 = L_9.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11 = ___b1;
		float L_12 = L_11.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = ___a0;
		float L_14 = L_13.get_y_3();
		float L_15 = ___t2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16 = ___a0;
		float L_17 = L_16.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_18 = ___b1;
		float L_19 = L_18.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20 = ___a0;
		float L_21 = L_20.get_z_4();
		float L_22 = ___t2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_23;
		memset((&L_23), 0, sizeof(L_23));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_23), ((float)il2cpp_codegen_add((float)L_3, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), (float)L_8)))), ((float)il2cpp_codegen_add((float)L_10, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_12, (float)L_14)), (float)L_15)))), ((float)il2cpp_codegen_add((float)L_17, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_19, (float)L_21)), (float)L_22)))), /*hidden argument*/NULL);
		V_0 = L_23;
		goto IL_0053;
	}

IL_0053:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_24 = V_0;
		return L_24;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void vThirdPersonMotor_set_isSprinting_m031D2D28AF417BB0EAD7679972CAFE117C6A7749_inline (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// internal bool isSprinting { get; set; }
		bool L_0 = ___value0;
		__this->set_U3CisSprintingU3Ek__BackingField_31(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void vThirdPersonMotor_set_isGrounded_m4F6F1C10637D6E9382C07E507FCD1773F846FA9B_inline (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// internal bool isGrounded { get; set; }
		bool L_0 = ___value0;
		__this->set_U3CisGroundedU3Ek__BackingField_30(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Division_mE5ACBFB168FED529587457A83BA98B7DB32E2A05_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		float L_2 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = ___a0;
		float L_4 = L_3.get_y_3();
		float L_5 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a0;
		float L_7 = L_6.get_z_4();
		float L_8 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_9), ((float)((float)L_1/(float)L_2)), ((float)((float)L_4/(float)L_5)), ((float)((float)L_7/(float)L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = V_0;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void vThirdPersonMotor_set_stopMove_m700BD3D0627D69453D1BB1D453062FAD5F568BC8_inline (vThirdPersonMotor_t1CFAD93B0180ECC79D05920194CC1293659139A3 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool stopMove { get; protected set; }
		bool L_0 = ___value0;
		__this->set_U3CstopMoveU3Ek__BackingField_32(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_UnaryNegation_m362EA356F4CADEDB39F965A0DBDED6EA890925F7_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___a0;
		float L_3 = L_2.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_6), ((-L_1)), ((-L_3)), ((-L_5)), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_001e;
	}

IL_001e:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7 = V_0;
		return L_7;
	}
}
