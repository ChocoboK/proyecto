﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void vThirdPersonCamera::Start()
extern void vThirdPersonCamera_Start_m1BC82E1F083B92594F31E827D33F8B8F97D76454 (void);
// 0x00000002 System.Void vThirdPersonCamera::Init()
extern void vThirdPersonCamera_Init_mAFBED3541CD351DED52E79B067716668A5D56ED6 (void);
// 0x00000003 System.Void vThirdPersonCamera::FixedUpdate()
extern void vThirdPersonCamera_FixedUpdate_m88B441002F7583FDB36C614EF1757AEB4D744AF6 (void);
// 0x00000004 System.Void vThirdPersonCamera::SetTarget(UnityEngine.Transform)
extern void vThirdPersonCamera_SetTarget_m895630C8744043672053E8D0C1EBBF84227E41E7 (void);
// 0x00000005 System.Void vThirdPersonCamera::SetMainTarget(UnityEngine.Transform)
extern void vThirdPersonCamera_SetMainTarget_m35387F3B3FC8B83915DC3441E9019F0EE7D878C7 (void);
// 0x00000006 UnityEngine.Ray vThirdPersonCamera::ScreenPointToRay(UnityEngine.Vector3)
extern void vThirdPersonCamera_ScreenPointToRay_m0FC7E21A5C73322AB0422F1257131C08D96E0144 (void);
// 0x00000007 System.Void vThirdPersonCamera::RotateCamera(System.Single,System.Single)
extern void vThirdPersonCamera_RotateCamera_m38FD2DBF37023FD4885681827DFF017CF64FFC57 (void);
// 0x00000008 System.Void vThirdPersonCamera::CameraMovement()
extern void vThirdPersonCamera_CameraMovement_m0F154B3318E4D78CFA3D8C4C4B38446CD1B38BC2 (void);
// 0x00000009 System.Boolean vThirdPersonCamera::CullingRayCast(UnityEngine.Vector3,Invector.ClipPlanePoints,UnityEngine.RaycastHit&,System.Single,UnityEngine.LayerMask,UnityEngine.Color)
extern void vThirdPersonCamera_CullingRayCast_mF01736BAE8BA542FF4F8F62F93A398909E1C37A2 (void);
// 0x0000000A System.Void vThirdPersonCamera::.ctor()
extern void vThirdPersonCamera__ctor_m7B933687272033ECAFFA0CD60EA7699EE32616C6 (void);
// 0x0000000B System.Void vPickupItem::Start()
extern void vPickupItem_Start_mFA223AD3F583A5878AD577E8A4A329744C988070 (void);
// 0x0000000C System.Void vPickupItem::OnTriggerEnter(UnityEngine.Collider)
extern void vPickupItem_OnTriggerEnter_m3BB4049DF1DB56C4ABE07E9F1574C91F8A2A9DA4 (void);
// 0x0000000D System.Void vPickupItem::.ctor()
extern void vPickupItem__ctor_m5679F317543E4A3F2B09A6DA9B8DDC6FBE9E5F5F (void);
// 0x0000000E System.Void EnemigoIA::Start()
extern void EnemigoIA_Start_m232470DA66A00D2EFF2C38B92324C049CD41504B (void);
// 0x0000000F System.Void EnemigoIA::RecibirDa?o(System.Int32)
extern void EnemigoIA_RecibirDaUF1o_mF0EF8AD9F9B942597FF7CDC0B87F0B5C3E1D2F90 (void);
// 0x00000010 System.Void EnemigoIA::Update()
extern void EnemigoIA_Update_mE9B3CA8D712FC0EE3303A5ACE474C195B36E0586 (void);
// 0x00000011 System.Void EnemigoIA::SiguientePuntoRuta()
extern void EnemigoIA_SiguientePuntoRuta_m9273E16CE010336A1C31BB6C149A867D26F9D9BE (void);
// 0x00000012 System.Void EnemigoIA::.ctor()
extern void EnemigoIA__ctor_m549C82FB2C9BF69AED41E80110DE015DE6A54819 (void);
// 0x00000013 System.Void Estadisticas::salir()
extern void Estadisticas_salir_m296998D70EB025E963948A81250EDBC94515A547 (void);
// 0x00000014 System.Void Estadisticas::Start()
extern void Estadisticas_Start_mF2ECC98C97A06252F37C3B0D35EC47CA772F52A9 (void);
// 0x00000015 System.Void Estadisticas::Update()
extern void Estadisticas_Update_m0CD8576D2C16A59FCDC2EB9996BC41B4C68EEE94 (void);
// 0x00000016 System.Void Estadisticas::.ctor()
extern void Estadisticas__ctor_m8721653D11F4FDBBBF74967754F66558418A7D53 (void);
// 0x00000017 System.Void InvocadorOleadas::Start()
extern void InvocadorOleadas_Start_m5D969EE1C45F789968149A0BEA69F82C539E2A11 (void);
// 0x00000018 System.Void InvocadorOleadas::Update()
extern void InvocadorOleadas_Update_m3F0C89126C47C42157FE8BF52C328C01A4A894D4 (void);
// 0x00000019 System.Collections.IEnumerator InvocadorOleadas::InvocarOleada()
extern void InvocadorOleadas_InvocarOleada_mCBC6876F3763219B43365D35C5D977E7BEB5F6E2 (void);
// 0x0000001A System.Void InvocadorOleadas::InvocarEnemigo()
extern void InvocadorOleadas_InvocarEnemigo_m620453681026EA6BAA065D19200F4ECB33573A7A (void);
// 0x0000001B System.Void InvocadorOleadas::.ctor()
extern void InvocadorOleadas__ctor_mED52F47F1034090157E6CC5C1D4ECDF681AEA2DE (void);
// 0x0000001C System.Void InvocadorOleadas/<InvocarOleada>d__9::.ctor(System.Int32)
extern void U3CInvocarOleadaU3Ed__9__ctor_mEA8339C3505658A60279289CD8D0CFD2195FAA53 (void);
// 0x0000001D System.Void InvocadorOleadas/<InvocarOleada>d__9::System.IDisposable.Dispose()
extern void U3CInvocarOleadaU3Ed__9_System_IDisposable_Dispose_m873ADCB92CA17BB6FF49F24B13E92EFF3AE467E6 (void);
// 0x0000001E System.Boolean InvocadorOleadas/<InvocarOleada>d__9::MoveNext()
extern void U3CInvocarOleadaU3Ed__9_MoveNext_m00641A8575EF644444C8414060D10905741093FE (void);
// 0x0000001F System.Object InvocadorOleadas/<InvocarOleada>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInvocarOleadaU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFC6813F16024A7C4D990499877B4B059FA4A9523 (void);
// 0x00000020 System.Void InvocadorOleadas/<InvocarOleada>d__9::System.Collections.IEnumerator.Reset()
extern void U3CInvocarOleadaU3Ed__9_System_Collections_IEnumerator_Reset_m36DD77A221E7624753B5F896258F11EC87949022 (void);
// 0x00000021 System.Object InvocadorOleadas/<InvocarOleada>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CInvocarOleadaU3Ed__9_System_Collections_IEnumerator_get_Current_m376E2436B934AE768423E6088A6B7B4F62370012 (void);
// 0x00000022 System.Void Pinchos::Start()
extern void Pinchos_Start_m66D4D83518EF1B6C298F7552A42566C3C9F08798 (void);
// 0x00000023 System.Void Pinchos::EncuentraObjetivo()
extern void Pinchos_EncuentraObjetivo_m6C3102F6BF2489E4A6ED3E7EE6ED0EA8F79958F0 (void);
// 0x00000024 System.Void Pinchos::Update()
extern void Pinchos_Update_m3AEE4A6AAD7D5A77852A1682E39F264CC813853A (void);
// 0x00000025 System.Void Pinchos::GolpeaObjetivo()
extern void Pinchos_GolpeaObjetivo_mB9D47819667C1804DE9D6313522B698D384C4372 (void);
// 0x00000026 System.Void Pinchos::Da?ar(UnityEngine.Transform)
extern void Pinchos_DaUF1ar_m700E5BC27A012D2DF00FCA5D951F084087B6A30C (void);
// 0x00000027 System.Void Pinchos::OnDrawGizmosSelected()
extern void Pinchos_OnDrawGizmosSelected_m861CAD31A8B056EDD6430262493807D56DB98480 (void);
// 0x00000028 System.Void Pinchos::.ctor()
extern void Pinchos__ctor_m83CE7F21A7386FD80262F522B29D1E20270DA216 (void);
// 0x00000029 System.Void PuntosRuta::Awake()
extern void PuntosRuta_Awake_m8B71EDC8E00151D4FEFC5A7D42510E10703D5CDA (void);
// 0x0000002A System.Void PuntosRuta::.ctor()
extern void PuntosRuta__ctor_m8EBB144ADD82D37459EA4877C914C5E82162B93C (void);
// 0x0000002B System.Void Reiniciar::ReloadScreen()
extern void Reiniciar_ReloadScreen_m574E0224817E3230F687E8470BFEF433E9BB8F54 (void);
// 0x0000002C System.Void Reiniciar::.ctor()
extern void Reiniciar__ctor_mFC5DC20F8A07C244CACE75121911A45ED844624E (void);
// 0x0000002D System.Void Salir::ExitGame()
extern void Salir_ExitGame_m1BA252D98558E43521346E3FEE4C5366BC81D010 (void);
// 0x0000002E System.Void Salir::.ctor()
extern void Salir__ctor_m647DEF251A704ED14B036C69DCB92F31AF4522AC (void);
// 0x0000002F System.Void Test::Start()
extern void Test_Start_m4D6FA0B24EBFD6471F4596A93EC95EC1EB5355D8 (void);
// 0x00000030 System.Void Test::Update()
extern void Test_Update_m85FFE4CC559D42AAFA74D4DEAE19415052BB70D0 (void);
// 0x00000031 System.Void Test::.ctor()
extern void Test__ctor_m1C2B5E11A339FD79C0C458FB86723E5F2B74545C (void);
// 0x00000032 T[] Invector.vExtensions::Append(T[],T[])
// 0x00000033 T[] Invector.vExtensions::vToArray(System.Collections.Generic.List`1<T>)
// 0x00000034 System.Single Invector.vExtensions::ClampAngle(System.Single,System.Single,System.Single)
extern void vExtensions_ClampAngle_m748C49B4118E791FEB84C37FAE0006DE695EAC4B (void);
// 0x00000035 Invector.ClipPlanePoints Invector.vExtensions::NearClipPlanePoints(UnityEngine.Camera,UnityEngine.Vector3,System.Single)
extern void vExtensions_NearClipPlanePoints_m95EBECC9448EE09DE106D39A4089ABFF27725772 (void);
// 0x00000036 System.Void Invector.Utils.vComment::.ctor()
extern void vComment__ctor_mE26E2D55EC5441707328990E638BBB0B08E1F2EF (void);
// 0x00000037 System.Void Invector.vCharacterController.vThirdPersonAnimator::UpdateAnimator()
extern void vThirdPersonAnimator_UpdateAnimator_m3D924B91333BDF1C3C31CC77BF003930D966CC5E (void);
// 0x00000038 System.Void Invector.vCharacterController.vThirdPersonAnimator::SetAnimatorMoveSpeed(Invector.vCharacterController.vThirdPersonMotor/vMovementSpeed)
extern void vThirdPersonAnimator_SetAnimatorMoveSpeed_mD798AE22158330F5A7DBC64EE61611C45EE4DF02 (void);
// 0x00000039 System.Void Invector.vCharacterController.vThirdPersonAnimator::.ctor()
extern void vThirdPersonAnimator__ctor_mBB69BF7A51196C407D664AD2A836167FF190352A (void);
// 0x0000003A System.Void Invector.vCharacterController.vAnimatorParameters::.cctor()
extern void vAnimatorParameters__cctor_m5230FDD4B0EBF34E7AE3795A3829779FC8E0F66B (void);
// 0x0000003B System.Void Invector.vCharacterController.vThirdPersonController::ControlAnimatorRootMotion()
extern void vThirdPersonController_ControlAnimatorRootMotion_m5A3282D8BF3432ACE3DA138BF92382CFB7CE0A9D (void);
// 0x0000003C System.Void Invector.vCharacterController.vThirdPersonController::ControlLocomotionType()
extern void vThirdPersonController_ControlLocomotionType_m672B81295FB81C5F3765030F59B1ECEADB2B7D36 (void);
// 0x0000003D System.Void Invector.vCharacterController.vThirdPersonController::ControlRotationType()
extern void vThirdPersonController_ControlRotationType_m8708EBDBAB4C5DBE1590075F6E3A1967A560273A (void);
// 0x0000003E System.Void Invector.vCharacterController.vThirdPersonController::UpdateMoveDirection(UnityEngine.Transform)
extern void vThirdPersonController_UpdateMoveDirection_m84010F9B7CF5694FEE7246E7A3BCFC2D6000BFB7 (void);
// 0x0000003F System.Void Invector.vCharacterController.vThirdPersonController::Sprint(System.Boolean)
extern void vThirdPersonController_Sprint_mC67D3C939B8F2CADC786E5E62B36E32A2D6C59DC (void);
// 0x00000040 System.Void Invector.vCharacterController.vThirdPersonController::Strafe()
extern void vThirdPersonController_Strafe_m8B54F15DB5B12194AB03CC110B08B4981414A189 (void);
// 0x00000041 System.Void Invector.vCharacterController.vThirdPersonController::Jump()
extern void vThirdPersonController_Jump_m6979A24997531E5AA1C70EE943CACC3416FA90A8 (void);
// 0x00000042 System.Void Invector.vCharacterController.vThirdPersonController::.ctor()
extern void vThirdPersonController__ctor_m4DB358FB0991D3EFD4CF8BCBD7D76A9F22C3D164 (void);
// 0x00000043 System.Void Invector.vCharacterController.vThirdPersonInput::Start()
extern void vThirdPersonInput_Start_m1AFD99CDA83D86DC215EAC7230C8A01FB71DC080 (void);
// 0x00000044 System.Void Invector.vCharacterController.vThirdPersonInput::FixedUpdate()
extern void vThirdPersonInput_FixedUpdate_m9BCF162748D6061DE5E819DF37785CDD21D54A33 (void);
// 0x00000045 System.Void Invector.vCharacterController.vThirdPersonInput::Update()
extern void vThirdPersonInput_Update_m013378AF0EB94763805A20FD8825867AE3356633 (void);
// 0x00000046 System.Void Invector.vCharacterController.vThirdPersonInput::OnAnimatorMove()
extern void vThirdPersonInput_OnAnimatorMove_m5BB743D83E25A815A22EA378D2E43D98F4A2B117 (void);
// 0x00000047 System.Void Invector.vCharacterController.vThirdPersonInput::InitilizeController()
extern void vThirdPersonInput_InitilizeController_m0FEC4A7F8577A1A347512576F0D569EB3BDC4FD1 (void);
// 0x00000048 System.Void Invector.vCharacterController.vThirdPersonInput::InitializeTpCamera()
extern void vThirdPersonInput_InitializeTpCamera_m872208B4E8F98A9E4EA9F54F73DE26BA2A63F9A8 (void);
// 0x00000049 System.Void Invector.vCharacterController.vThirdPersonInput::InputHandle()
extern void vThirdPersonInput_InputHandle_m88B1CAB599AAC1273638B72E277CCB31DB377875 (void);
// 0x0000004A System.Void Invector.vCharacterController.vThirdPersonInput::MoveInput()
extern void vThirdPersonInput_MoveInput_m86A8818A0FAB74282A427C773019C776E647A830 (void);
// 0x0000004B System.Void Invector.vCharacterController.vThirdPersonInput::CameraInput()
extern void vThirdPersonInput_CameraInput_m55F640928EC595A1B7C6A784144C529058DFB576 (void);
// 0x0000004C System.Void Invector.vCharacterController.vThirdPersonInput::StrafeInput()
extern void vThirdPersonInput_StrafeInput_m0B27009F3EF0B17BAD69C2D377A5A57F82C96ACD (void);
// 0x0000004D System.Void Invector.vCharacterController.vThirdPersonInput::SprintInput()
extern void vThirdPersonInput_SprintInput_m9E1F56CDCBD4630652143B643A146AAFDE077B57 (void);
// 0x0000004E System.Boolean Invector.vCharacterController.vThirdPersonInput::JumpConditions()
extern void vThirdPersonInput_JumpConditions_m9CB2DAB3C189B7E5750F7EE0EDADDD7CD1409EF4 (void);
// 0x0000004F System.Void Invector.vCharacterController.vThirdPersonInput::JumpInput()
extern void vThirdPersonInput_JumpInput_mCF691AA0405D14B9E86ED5011F3D924479A825C5 (void);
// 0x00000050 System.Void Invector.vCharacterController.vThirdPersonInput::.ctor()
extern void vThirdPersonInput__ctor_m6377D0158E4C85892B3AD6F29D1475E4DD5BC3BB (void);
// 0x00000051 System.Boolean Invector.vCharacterController.vThirdPersonMotor::get_isStrafing()
extern void vThirdPersonMotor_get_isStrafing_m3FB9147D8588055A220D47101B80C6FFD2B2E791 (void);
// 0x00000052 System.Void Invector.vCharacterController.vThirdPersonMotor::set_isStrafing(System.Boolean)
extern void vThirdPersonMotor_set_isStrafing_m90A9F59590C2DABE76449CE57C8FBCCCC9F39EFE (void);
// 0x00000053 System.Boolean Invector.vCharacterController.vThirdPersonMotor::get_isGrounded()
extern void vThirdPersonMotor_get_isGrounded_m3587831F44EEE43F49591F13918B05CFD28B65AB (void);
// 0x00000054 System.Void Invector.vCharacterController.vThirdPersonMotor::set_isGrounded(System.Boolean)
extern void vThirdPersonMotor_set_isGrounded_m4F6F1C10637D6E9382C07E507FCD1773F846FA9B (void);
// 0x00000055 System.Boolean Invector.vCharacterController.vThirdPersonMotor::get_isSprinting()
extern void vThirdPersonMotor_get_isSprinting_m98A252FE6362DE6468A47BBC3B0F7FCACA2B3EDD (void);
// 0x00000056 System.Void Invector.vCharacterController.vThirdPersonMotor::set_isSprinting(System.Boolean)
extern void vThirdPersonMotor_set_isSprinting_m031D2D28AF417BB0EAD7679972CAFE117C6A7749 (void);
// 0x00000057 System.Boolean Invector.vCharacterController.vThirdPersonMotor::get_stopMove()
extern void vThirdPersonMotor_get_stopMove_m09C1FEC6B37CBF3071597D789E0322C93CD78C58 (void);
// 0x00000058 System.Void Invector.vCharacterController.vThirdPersonMotor::set_stopMove(System.Boolean)
extern void vThirdPersonMotor_set_stopMove_m700BD3D0627D69453D1BB1D453062FAD5F568BC8 (void);
// 0x00000059 System.Void Invector.vCharacterController.vThirdPersonMotor::Init()
extern void vThirdPersonMotor_Init_m8470D932C4BE26328DB0320B9E92E0AE4E3E8B37 (void);
// 0x0000005A System.Void Invector.vCharacterController.vThirdPersonMotor::UpdateMotor()
extern void vThirdPersonMotor_UpdateMotor_mFBFA2A2D18DE13F102C8D4CF7DAFA0AB1043A483 (void);
// 0x0000005B System.Void Invector.vCharacterController.vThirdPersonMotor::SetControllerMoveSpeed(Invector.vCharacterController.vThirdPersonMotor/vMovementSpeed)
extern void vThirdPersonMotor_SetControllerMoveSpeed_m7424F68F2F99576696729CACA574C86DBAE34FB4 (void);
// 0x0000005C System.Void Invector.vCharacterController.vThirdPersonMotor::MoveCharacter(UnityEngine.Vector3)
extern void vThirdPersonMotor_MoveCharacter_m832DD2D8FCFEF78CD2AD7FD80E2B1178E7AEEA55 (void);
// 0x0000005D System.Void Invector.vCharacterController.vThirdPersonMotor::CheckSlopeLimit()
extern void vThirdPersonMotor_CheckSlopeLimit_m118A87D02B7F95849C6DEAF2229969892CCAC1F6 (void);
// 0x0000005E System.Void Invector.vCharacterController.vThirdPersonMotor::RotateToPosition(UnityEngine.Vector3)
extern void vThirdPersonMotor_RotateToPosition_m25177D466A510225531D8E6916E21094271A0631 (void);
// 0x0000005F System.Void Invector.vCharacterController.vThirdPersonMotor::RotateToDirection(UnityEngine.Vector3)
extern void vThirdPersonMotor_RotateToDirection_m957699965A599CD6FE6BD48E190181FD4DC40C82 (void);
// 0x00000060 System.Void Invector.vCharacterController.vThirdPersonMotor::RotateToDirection(UnityEngine.Vector3,System.Single)
extern void vThirdPersonMotor_RotateToDirection_m3D8A7E7D74A1FDAF4242E7D6494214118B2FE62A (void);
// 0x00000061 System.Void Invector.vCharacterController.vThirdPersonMotor::ControlJumpBehaviour()
extern void vThirdPersonMotor_ControlJumpBehaviour_mF60B9850DBA522019E086DBD1152B633D7FC1189 (void);
// 0x00000062 System.Void Invector.vCharacterController.vThirdPersonMotor::AirControl()
extern void vThirdPersonMotor_AirControl_m168338C0D35FC62784D2453584D4C4AF6510D676 (void);
// 0x00000063 System.Boolean Invector.vCharacterController.vThirdPersonMotor::get_jumpFwdCondition()
extern void vThirdPersonMotor_get_jumpFwdCondition_mC547BB5DBD086F71A7A8DF12E1BF3959296FA6BD (void);
// 0x00000064 System.Void Invector.vCharacterController.vThirdPersonMotor::CheckGround()
extern void vThirdPersonMotor_CheckGround_mC52E020122920DCBE0148C9F4A4A7E0618D3B2E8 (void);
// 0x00000065 System.Void Invector.vCharacterController.vThirdPersonMotor::ControlMaterialPhysics()
extern void vThirdPersonMotor_ControlMaterialPhysics_mEC6FAD551AB23C371DF71115713081883DC1BF01 (void);
// 0x00000066 System.Void Invector.vCharacterController.vThirdPersonMotor::CheckGroundDistance()
extern void vThirdPersonMotor_CheckGroundDistance_mA4A2343321F0A6E19109C18716E69F945C9269E9 (void);
// 0x00000067 System.Single Invector.vCharacterController.vThirdPersonMotor::GroundAngle()
extern void vThirdPersonMotor_GroundAngle_m2359CDDC90DAA5B812C8824B9D41FB5D07BC41B7 (void);
// 0x00000068 System.Single Invector.vCharacterController.vThirdPersonMotor::GroundAngleFromDirection()
extern void vThirdPersonMotor_GroundAngleFromDirection_m1448DCC1649C7294DD053E6C363DF0E26267443F (void);
// 0x00000069 System.Void Invector.vCharacterController.vThirdPersonMotor::.ctor()
extern void vThirdPersonMotor__ctor_m4AEDE34F41DF0A598D8C8DDB4BA8F5088AC7D06D (void);
// 0x0000006A System.Void Invector.vCharacterController.vThirdPersonMotor/vMovementSpeed::.ctor()
extern void vMovementSpeed__ctor_m52DC8BDAF03A84775E1E8805B582EFE944686D90 (void);
static Il2CppMethodPointer s_methodPointers[106] = 
{
	vThirdPersonCamera_Start_m1BC82E1F083B92594F31E827D33F8B8F97D76454,
	vThirdPersonCamera_Init_mAFBED3541CD351DED52E79B067716668A5D56ED6,
	vThirdPersonCamera_FixedUpdate_m88B441002F7583FDB36C614EF1757AEB4D744AF6,
	vThirdPersonCamera_SetTarget_m895630C8744043672053E8D0C1EBBF84227E41E7,
	vThirdPersonCamera_SetMainTarget_m35387F3B3FC8B83915DC3441E9019F0EE7D878C7,
	vThirdPersonCamera_ScreenPointToRay_m0FC7E21A5C73322AB0422F1257131C08D96E0144,
	vThirdPersonCamera_RotateCamera_m38FD2DBF37023FD4885681827DFF017CF64FFC57,
	vThirdPersonCamera_CameraMovement_m0F154B3318E4D78CFA3D8C4C4B38446CD1B38BC2,
	vThirdPersonCamera_CullingRayCast_mF01736BAE8BA542FF4F8F62F93A398909E1C37A2,
	vThirdPersonCamera__ctor_m7B933687272033ECAFFA0CD60EA7699EE32616C6,
	vPickupItem_Start_mFA223AD3F583A5878AD577E8A4A329744C988070,
	vPickupItem_OnTriggerEnter_m3BB4049DF1DB56C4ABE07E9F1574C91F8A2A9DA4,
	vPickupItem__ctor_m5679F317543E4A3F2B09A6DA9B8DDC6FBE9E5F5F,
	EnemigoIA_Start_m232470DA66A00D2EFF2C38B92324C049CD41504B,
	EnemigoIA_RecibirDaUF1o_mF0EF8AD9F9B942597FF7CDC0B87F0B5C3E1D2F90,
	EnemigoIA_Update_mE9B3CA8D712FC0EE3303A5ACE474C195B36E0586,
	EnemigoIA_SiguientePuntoRuta_m9273E16CE010336A1C31BB6C149A867D26F9D9BE,
	EnemigoIA__ctor_m549C82FB2C9BF69AED41E80110DE015DE6A54819,
	Estadisticas_salir_m296998D70EB025E963948A81250EDBC94515A547,
	Estadisticas_Start_mF2ECC98C97A06252F37C3B0D35EC47CA772F52A9,
	Estadisticas_Update_m0CD8576D2C16A59FCDC2EB9996BC41B4C68EEE94,
	Estadisticas__ctor_m8721653D11F4FDBBBF74967754F66558418A7D53,
	InvocadorOleadas_Start_m5D969EE1C45F789968149A0BEA69F82C539E2A11,
	InvocadorOleadas_Update_m3F0C89126C47C42157FE8BF52C328C01A4A894D4,
	InvocadorOleadas_InvocarOleada_mCBC6876F3763219B43365D35C5D977E7BEB5F6E2,
	InvocadorOleadas_InvocarEnemigo_m620453681026EA6BAA065D19200F4ECB33573A7A,
	InvocadorOleadas__ctor_mED52F47F1034090157E6CC5C1D4ECDF681AEA2DE,
	U3CInvocarOleadaU3Ed__9__ctor_mEA8339C3505658A60279289CD8D0CFD2195FAA53,
	U3CInvocarOleadaU3Ed__9_System_IDisposable_Dispose_m873ADCB92CA17BB6FF49F24B13E92EFF3AE467E6,
	U3CInvocarOleadaU3Ed__9_MoveNext_m00641A8575EF644444C8414060D10905741093FE,
	U3CInvocarOleadaU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFC6813F16024A7C4D990499877B4B059FA4A9523,
	U3CInvocarOleadaU3Ed__9_System_Collections_IEnumerator_Reset_m36DD77A221E7624753B5F896258F11EC87949022,
	U3CInvocarOleadaU3Ed__9_System_Collections_IEnumerator_get_Current_m376E2436B934AE768423E6088A6B7B4F62370012,
	Pinchos_Start_m66D4D83518EF1B6C298F7552A42566C3C9F08798,
	Pinchos_EncuentraObjetivo_m6C3102F6BF2489E4A6ED3E7EE6ED0EA8F79958F0,
	Pinchos_Update_m3AEE4A6AAD7D5A77852A1682E39F264CC813853A,
	Pinchos_GolpeaObjetivo_mB9D47819667C1804DE9D6313522B698D384C4372,
	Pinchos_DaUF1ar_m700E5BC27A012D2DF00FCA5D951F084087B6A30C,
	Pinchos_OnDrawGizmosSelected_m861CAD31A8B056EDD6430262493807D56DB98480,
	Pinchos__ctor_m83CE7F21A7386FD80262F522B29D1E20270DA216,
	PuntosRuta_Awake_m8B71EDC8E00151D4FEFC5A7D42510E10703D5CDA,
	PuntosRuta__ctor_m8EBB144ADD82D37459EA4877C914C5E82162B93C,
	Reiniciar_ReloadScreen_m574E0224817E3230F687E8470BFEF433E9BB8F54,
	Reiniciar__ctor_mFC5DC20F8A07C244CACE75121911A45ED844624E,
	Salir_ExitGame_m1BA252D98558E43521346E3FEE4C5366BC81D010,
	Salir__ctor_m647DEF251A704ED14B036C69DCB92F31AF4522AC,
	Test_Start_m4D6FA0B24EBFD6471F4596A93EC95EC1EB5355D8,
	Test_Update_m85FFE4CC559D42AAFA74D4DEAE19415052BB70D0,
	Test__ctor_m1C2B5E11A339FD79C0C458FB86723E5F2B74545C,
	NULL,
	NULL,
	vExtensions_ClampAngle_m748C49B4118E791FEB84C37FAE0006DE695EAC4B,
	vExtensions_NearClipPlanePoints_m95EBECC9448EE09DE106D39A4089ABFF27725772,
	vComment__ctor_mE26E2D55EC5441707328990E638BBB0B08E1F2EF,
	vThirdPersonAnimator_UpdateAnimator_m3D924B91333BDF1C3C31CC77BF003930D966CC5E,
	vThirdPersonAnimator_SetAnimatorMoveSpeed_mD798AE22158330F5A7DBC64EE61611C45EE4DF02,
	vThirdPersonAnimator__ctor_mBB69BF7A51196C407D664AD2A836167FF190352A,
	vAnimatorParameters__cctor_m5230FDD4B0EBF34E7AE3795A3829779FC8E0F66B,
	vThirdPersonController_ControlAnimatorRootMotion_m5A3282D8BF3432ACE3DA138BF92382CFB7CE0A9D,
	vThirdPersonController_ControlLocomotionType_m672B81295FB81C5F3765030F59B1ECEADB2B7D36,
	vThirdPersonController_ControlRotationType_m8708EBDBAB4C5DBE1590075F6E3A1967A560273A,
	vThirdPersonController_UpdateMoveDirection_m84010F9B7CF5694FEE7246E7A3BCFC2D6000BFB7,
	vThirdPersonController_Sprint_mC67D3C939B8F2CADC786E5E62B36E32A2D6C59DC,
	vThirdPersonController_Strafe_m8B54F15DB5B12194AB03CC110B08B4981414A189,
	vThirdPersonController_Jump_m6979A24997531E5AA1C70EE943CACC3416FA90A8,
	vThirdPersonController__ctor_m4DB358FB0991D3EFD4CF8BCBD7D76A9F22C3D164,
	vThirdPersonInput_Start_m1AFD99CDA83D86DC215EAC7230C8A01FB71DC080,
	vThirdPersonInput_FixedUpdate_m9BCF162748D6061DE5E819DF37785CDD21D54A33,
	vThirdPersonInput_Update_m013378AF0EB94763805A20FD8825867AE3356633,
	vThirdPersonInput_OnAnimatorMove_m5BB743D83E25A815A22EA378D2E43D98F4A2B117,
	vThirdPersonInput_InitilizeController_m0FEC4A7F8577A1A347512576F0D569EB3BDC4FD1,
	vThirdPersonInput_InitializeTpCamera_m872208B4E8F98A9E4EA9F54F73DE26BA2A63F9A8,
	vThirdPersonInput_InputHandle_m88B1CAB599AAC1273638B72E277CCB31DB377875,
	vThirdPersonInput_MoveInput_m86A8818A0FAB74282A427C773019C776E647A830,
	vThirdPersonInput_CameraInput_m55F640928EC595A1B7C6A784144C529058DFB576,
	vThirdPersonInput_StrafeInput_m0B27009F3EF0B17BAD69C2D377A5A57F82C96ACD,
	vThirdPersonInput_SprintInput_m9E1F56CDCBD4630652143B643A146AAFDE077B57,
	vThirdPersonInput_JumpConditions_m9CB2DAB3C189B7E5750F7EE0EDADDD7CD1409EF4,
	vThirdPersonInput_JumpInput_mCF691AA0405D14B9E86ED5011F3D924479A825C5,
	vThirdPersonInput__ctor_m6377D0158E4C85892B3AD6F29D1475E4DD5BC3BB,
	vThirdPersonMotor_get_isStrafing_m3FB9147D8588055A220D47101B80C6FFD2B2E791,
	vThirdPersonMotor_set_isStrafing_m90A9F59590C2DABE76449CE57C8FBCCCC9F39EFE,
	vThirdPersonMotor_get_isGrounded_m3587831F44EEE43F49591F13918B05CFD28B65AB,
	vThirdPersonMotor_set_isGrounded_m4F6F1C10637D6E9382C07E507FCD1773F846FA9B,
	vThirdPersonMotor_get_isSprinting_m98A252FE6362DE6468A47BBC3B0F7FCACA2B3EDD,
	vThirdPersonMotor_set_isSprinting_m031D2D28AF417BB0EAD7679972CAFE117C6A7749,
	vThirdPersonMotor_get_stopMove_m09C1FEC6B37CBF3071597D789E0322C93CD78C58,
	vThirdPersonMotor_set_stopMove_m700BD3D0627D69453D1BB1D453062FAD5F568BC8,
	vThirdPersonMotor_Init_m8470D932C4BE26328DB0320B9E92E0AE4E3E8B37,
	vThirdPersonMotor_UpdateMotor_mFBFA2A2D18DE13F102C8D4CF7DAFA0AB1043A483,
	vThirdPersonMotor_SetControllerMoveSpeed_m7424F68F2F99576696729CACA574C86DBAE34FB4,
	vThirdPersonMotor_MoveCharacter_m832DD2D8FCFEF78CD2AD7FD80E2B1178E7AEEA55,
	vThirdPersonMotor_CheckSlopeLimit_m118A87D02B7F95849C6DEAF2229969892CCAC1F6,
	vThirdPersonMotor_RotateToPosition_m25177D466A510225531D8E6916E21094271A0631,
	vThirdPersonMotor_RotateToDirection_m957699965A599CD6FE6BD48E190181FD4DC40C82,
	vThirdPersonMotor_RotateToDirection_m3D8A7E7D74A1FDAF4242E7D6494214118B2FE62A,
	vThirdPersonMotor_ControlJumpBehaviour_mF60B9850DBA522019E086DBD1152B633D7FC1189,
	vThirdPersonMotor_AirControl_m168338C0D35FC62784D2453584D4C4AF6510D676,
	vThirdPersonMotor_get_jumpFwdCondition_mC547BB5DBD086F71A7A8DF12E1BF3959296FA6BD,
	vThirdPersonMotor_CheckGround_mC52E020122920DCBE0148C9F4A4A7E0618D3B2E8,
	vThirdPersonMotor_ControlMaterialPhysics_mEC6FAD551AB23C371DF71115713081883DC1BF01,
	vThirdPersonMotor_CheckGroundDistance_mA4A2343321F0A6E19109C18716E69F945C9269E9,
	vThirdPersonMotor_GroundAngle_m2359CDDC90DAA5B812C8824B9D41FB5D07BC41B7,
	vThirdPersonMotor_GroundAngleFromDirection_m1448DCC1649C7294DD053E6C363DF0E26267443F,
	vThirdPersonMotor__ctor_m4AEDE34F41DF0A598D8C8DDB4BA8F5088AC7D06D,
	vMovementSpeed__ctor_m52DC8BDAF03A84775E1E8805B582EFE944686D90,
};
static const int32_t s_InvokerIndices[106] = 
{
	1983,
	1983,
	1983,
	1704,
	1704,
	1493,
	1015,
	1983,
	85,
	1983,
	1983,
	1704,
	1983,
	1983,
	1693,
	1983,
	1983,
	1983,
	1983,
	1983,
	1983,
	1983,
	1983,
	1983,
	1938,
	1983,
	1983,
	1693,
	1983,
	1904,
	1938,
	1983,
	1938,
	1983,
	1983,
	1983,
	1983,
	1704,
	1983,
	1983,
	1983,
	1983,
	1983,
	1983,
	1983,
	1983,
	1983,
	1983,
	1983,
	-1,
	-1,
	2551,
	2457,
	1983,
	1983,
	1704,
	1983,
	3251,
	1983,
	1983,
	1983,
	1704,
	1665,
	1983,
	1983,
	1983,
	1983,
	1983,
	1983,
	1983,
	1983,
	1983,
	1983,
	1983,
	1983,
	1983,
	1983,
	1904,
	1983,
	1983,
	1904,
	1665,
	1904,
	1665,
	1904,
	1665,
	1904,
	1665,
	1983,
	1983,
	1704,
	1748,
	1983,
	1748,
	1748,
	1031,
	1983,
	1983,
	1904,
	1983,
	1983,
	1983,
	1968,
	1968,
	1983,
	1983,
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x06000032, { 0, 1 } },
	{ 0x06000033, { 1, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[4] = 
{
	{ (Il2CppRGCTXDataType)2, 2785 },
	{ (Il2CppRGCTXDataType)3, 7241 },
	{ (Il2CppRGCTXDataType)2, 2786 },
	{ (Il2CppRGCTXDataType)3, 7242 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	106,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	2,
	s_rgctxIndices,
	4,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
